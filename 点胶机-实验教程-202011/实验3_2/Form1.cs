﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;
using System.Diagnostics;
///视觉库命名空间
using XTVisioncs;
///运动控制库命名空间
using zmcauxcs;
///相机库命名空间
using DVPCameraType;

namespace 实验3_2
{
    /// <summary>
    /// 实验3_2：图像标定和直线校正实验
    /// 简述：图像标定实验用于计算出每个图像像素对应电机走动实际距离mm/px
    /// 通过图像标定，可以将图像坐标系和运动平台坐标系关联起来
    /// 在算出每像素对应实际机械距离后，结合两次图像定位位置的差值计算出新的直线位置
    /// </summary>
    public partial class Form1 : Form
    {
        //控制器句柄
        IntPtr g_handle;

        //控制器轴数量
        int AxisNum = 4;

        //轴参数配置数组
        AxisConfig[] AxisConfigList;

        //轴速度配置数组
        BSpeed[] AxisSpeedList;

        //鼠标按下标志
        bool AddDown = false;

        //鼠标按下标志
        bool SubDown = false;

        //持续运动标志
        bool ContiWork = false;

        //构造函数
        public Form1()
        {
            InitializeComponent();
            InitializeForm();
        }

        //读取配置文件
        void ReadIni()
        {
            //运行目录下的card.ini文件
            string filePath = Application.StartupPath + "\\card.ini";
            //可变字符串
            StringBuilder temp = new StringBuilder(128);

            //读取轴参数
            for (int idx = 0; idx < AxisNum; ++idx)
            {
                Win32.GetPrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "PPR", "10000", temp, 128, filePath);
                AxisConfigList[idx].PPR = Convert.ToInt32(temp.ToString());

                Win32.GetPrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "Pitch", "10", temp, 128, filePath);
                AxisConfigList[idx].Pitch = Convert.ToDouble(temp.ToString());

                Win32.GetPrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "PulseMode", "0", temp, 128, filePath);
                AxisConfigList[idx].PulseMode = Convert.ToInt32(temp.ToString());

                Win32.GetPrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "AxisType", "0", temp, 128, filePath);
                AxisConfigList[idx].AxisType = Convert.ToInt32(temp.ToString());

                Win32.GetPrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "Merge", "1", temp, 128, filePath);
                AxisConfigList[idx].Merge = Convert.ToInt32(temp.ToString());

                Win32.GetPrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "LowLimit", "0", temp, 128, filePath);
                AxisConfigList[idx].LowLimit = Convert.ToInt32(temp.ToString());

                Win32.GetPrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "HighLimit", "300", temp, 128, filePath);
                AxisConfigList[idx].HighLimit = Convert.ToInt32(temp.ToString());

                Win32.GetPrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "HomeDir", "4", temp, 128, filePath);
                AxisConfigList[idx].HomeMode = Convert.ToInt32(temp.ToString());

                Win32.GetPrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "SensorMode", "0", temp, 128, filePath);
                AxisConfigList[idx].SensorMode = Convert.ToInt32(temp.ToString());
            }

            //读取轴速度
            for (int idx = 0; idx < AxisNum; ++idx)
            {
                Win32.GetPrivateProfileString(string.Format("AXIS_SPEED{0}", idx), "SPEED", "10", temp, 128, filePath);
                AxisSpeedList[idx].Speed = Convert.ToDouble(temp.ToString());

                Win32.GetPrivateProfileString(string.Format("AXIS_SPEED{0}", idx), "ATime", "10", temp, 128, filePath);
                AxisSpeedList[idx].ATime = Convert.ToDouble(temp.ToString());

                Win32.GetPrivateProfileString(string.Format("AXIS_SPEED{0}", idx), "DTime", "10", temp, 128, filePath);
                AxisSpeedList[idx].DTime = Convert.ToDouble(temp.ToString());
            }
        }

        //写入配置文件
        void SaveIni()
        {
            //运行目录下的card.ini文件
            string filePath = Application.StartupPath + "\\card.ini";

            //保存轴参数
            for (int idx = 0; idx < AxisNum; ++idx)
            {
                Win32.WritePrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "PPR", Convert.ToString(AxisConfigList[idx].PPR), filePath);
                Win32.WritePrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "Pitch", Convert.ToString(AxisConfigList[idx].Pitch), filePath);
                Win32.WritePrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "PulseMode", Convert.ToString(AxisConfigList[idx].PulseMode), filePath);
                Win32.WritePrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "AxisType", Convert.ToString(AxisConfigList[idx].AxisType), filePath);
                Win32.WritePrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "Merge", Convert.ToString(AxisConfigList[idx].Merge), filePath);
                Win32.WritePrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "LowLimit", Convert.ToString(AxisConfigList[idx].LowLimit), filePath);
                Win32.WritePrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "HighLimit", Convert.ToString(AxisConfigList[idx].HighLimit), filePath);
                Win32.WritePrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "HomeDir", Convert.ToString(AxisConfigList[idx].HomeMode), filePath);
                Win32.WritePrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "SensorMode", Convert.ToString(AxisConfigList[idx].SensorMode), filePath);
            }

            //保存轴速度
            for (int idx = 0; idx < AxisNum; ++idx)
            {
                Win32.WritePrivateProfileString(string.Format("AXIS_SPEED{0}", idx), "SPEED", Convert.ToString(AxisSpeedList[idx].Speed), filePath);
                Win32.WritePrivateProfileString(string.Format("AXIS_SPEED{0}", idx), "ATime", Convert.ToString(AxisSpeedList[idx].ATime), filePath);
                Win32.WritePrivateProfileString(string.Format("AXIS_SPEED{0}", idx), "DTime", Convert.ToString(AxisSpeedList[idx].DTime), filePath);
            }
        }

        //保存配置数据
        void SaveForm()
        {
            int axisIndex = 0;
            if (radioButtonAxisX.Checked == true) axisIndex = 0;
            if (radioButtonAxisY.Checked == true) axisIndex = 1;
            if (radioButtonAxisZ.Checked == true) axisIndex = 2;
            if (radioButtonAxisU.Checked == true) axisIndex = 3;

            AxisConfigList[axisIndex].PulseMode = comboBoxPulseMode.SelectedIndex;
            AxisConfigList[axisIndex].AxisType = comboBoxAxisType.SelectedIndex;
            AxisConfigList[axisIndex].SensorMode = comboBoxSensorMode.SelectedIndex;
            AxisConfigList[axisIndex].PPR = (int)numericUpDownPPR.Value;
            AxisConfigList[axisIndex].Pitch = (double)numericUpDownPitch.Value;
            AxisConfigList[axisIndex].LowLimit = (int)numericUpDownLowLimit.Value;
            AxisConfigList[axisIndex].HighLimit = (int)numericUpDownHighLimit.Value;

            AxisSpeedList[axisIndex].Speed = (int)numericUpDownSpeed.Value;
            AxisSpeedList[axisIndex].ATime = (int)numericUpDownATime.Value;
            AxisSpeedList[axisIndex].DTime = (int)numericUpDownDTime.Value;
        }

        //更新配置界面
        void UpdateForm()
        {
            int axisIndex = 0;
            if (radioButtonAxisX.Checked == true) axisIndex = 0;
            if (radioButtonAxisY.Checked == true) axisIndex = 1;
            if (radioButtonAxisZ.Checked == true) axisIndex = 2;
            if (radioButtonAxisU.Checked == true) axisIndex = 3;

            comboBoxPulseMode.SelectedIndex = AxisConfigList[axisIndex].PulseMode;
            comboBoxAxisType.SelectedIndex = AxisConfigList[axisIndex].AxisType;
            comboBoxSensorMode.SelectedIndex = AxisConfigList[axisIndex].SensorMode;
            numericUpDownPPR.Value = AxisConfigList[axisIndex].PPR;
            numericUpDownPitch.Value = (decimal)AxisConfigList[axisIndex].Pitch;
            numericUpDownLowLimit.Value = AxisConfigList[axisIndex].LowLimit;
            numericUpDownHighLimit.Value = AxisConfigList[axisIndex].HighLimit;

            numericUpDownSpeed.Value = (int)AxisSpeedList[axisIndex].Speed;
            numericUpDownATime.Value = (int)AxisSpeedList[axisIndex].ATime;
            numericUpDownDTime.Value = (int)AxisSpeedList[axisIndex].DTime;
        }

        //窗口辅助初始化
        void InitializeForm()
        {
            //控制器IP
            comboBoxCardip.Items.Add("192.168.0.11");
            comboBoxCardip.Items.Add("127.0.0.1");
            comboBoxCardip.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxCardip.SelectedIndex = 0;

            //脉冲方式
            comboBoxPulseMode.Items.Add("脉冲方向:下跳沿有效");
            comboBoxPulseMode.Items.Add("脉冲方向:上跳沿有效");
            comboBoxPulseMode.Items.Add("脉冲方向:下跳沿有效,方向线反转");
            comboBoxPulseMode.Items.Add("脉冲方向:上跳沿有效,方向线反转");
            comboBoxPulseMode.Items.Add("双脉冲:常态低电平,缺省电平");
            comboBoxPulseMode.Items.Add("双脉冲:常态高电平,缺省电平");
            comboBoxPulseMode.Items.Add("双脉冲:常态低电平");
            comboBoxPulseMode.Items.Add("双脉冲:常态高电平");
            comboBoxPulseMode.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxPulseMode.SelectedIndex = 0;

            //轴类型
            comboBoxAxisType.Items.Add("脉冲轴:无编码反馈");
            comboBoxAxisType.Items.Add("编码轴:带编码反馈");
            comboBoxAxisType.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxAxisType.SelectedIndex = 0;

            //原点电平
            comboBoxSensorMode.Items.Add("低电平");
            comboBoxSensorMode.Items.Add("高电平");
            comboBoxSensorMode.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxSensorMode.SelectedIndex = 0;

            //电机细分
            numericUpDownPPR.Minimum = 1;
            numericUpDownPPR.Maximum = 100000;
            numericUpDownPPR.Value = 1000;

            //丝杆导程
            numericUpDownPitch.Minimum = 1;
            numericUpDownPitch.Maximum = 1000;
            numericUpDownPitch.Value = 10;
            numericUpDownPitch.DecimalPlaces = 3;
            numericUpDownPitch.Increment = 0.001M;

            //轴软限位
            numericUpDownLowLimit.Minimum = -200000000;
            numericUpDownLowLimit.Maximum = 200000000;
            numericUpDownLowLimit.Value = 0;

            //轴软限位
            numericUpDownHighLimit.Minimum = -200000000;
            numericUpDownHighLimit.Maximum = 200000000;
            numericUpDownHighLimit.Value = 300;

            //初始化轴配置参数数组
            AxisConfigList = new AxisConfig[AxisNum];
            for (int idx = 0; idx < AxisNum; ++idx)
            {
                AxisConfigList[idx] = new AxisConfig();
            }

            //初始化轴速度参数数组
            AxisSpeedList = new BSpeed[AxisNum];
            for (int idx = 0; idx < AxisNum; ++idx)
            {
                AxisSpeedList[idx] = new BSpeed();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //读取配置
            ReadIni();

            //更新界面
            UpdateForm();

            //默认选中X轴
            radioButtonAxisX.Checked = true;

            //默认低速运动
            radioButtonLow.Checked = true;

            //启动定时器，监控按钮是否被按下
            timerMotor.Start();

            //启动定时器，显示当前轴参数信息
            timerStatus.Start();


            //图像相关初始化
            InitializeForm1();
            //打开图像采集定时器
            timerFrame.Start();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            //断开链接/关闭运动控制器
            zmcaux.ZAux_Close(g_handle);
            g_handle = (IntPtr)0;

            this.Text = "控制器已经断开";

            //关闭相机
            CloseCamera();
        }

        private void buttonCardLink_Click(object sender, EventArgs e)
        {
            //链接控制器 
            string ipaddr = comboBoxCardip.Text;
            zmcaux.ZAux_OpenEth(ipaddr, out g_handle);

            //判断句柄是否有效
            if ((int)g_handle != 0)
            {
                this.Text = "控制器链接成功";
            }
            else
            {
                this.Text = "控制器链接失败";
            }

            if ((int)g_handle != 0)
            {
                int err = 0;

                //设置脉冲输出类型：脉冲+方向/双脉冲
                for (int Cnt = 0; Cnt < AxisNum; Cnt++)
                {
                    AxisConfig aParam = AxisConfigList[Cnt];
                    int PulseMode = aParam.PulseMode;

                    //设置脉冲方式
                    //0 脉冲方向：下跳沿有效
                    //1 脉冲方向：上跳沿有效
                    //2 脉冲方向方式，下跳沿有效，方向线反转
                    //3 脉冲方向方式，上跳沿有效，方向线反转
                    //4 双脉冲常态低，缺省电平
                    //5 双脉冲常态高，缺省电平
                    //6 双脉冲常态低
                    //7 双脉冲常态高
                    err = zmcaux.ZAux_Direct_SetInvertStep(g_handle, Cnt, PulseMode);

                    //设置轴类型
                    int AxisType = 1;                               //轴类型：脉冲方向方式的步进或伺服
                    if (aParam.AxisType == 0) { AxisType = 1; }     //脉冲轴:无编码反馈
                    if (aParam.AxisType == 1) { AxisType = 4; }     //编码轴:带编码反馈
                    err = zmcaux.ZAux_Direct_SetAtype(g_handle, Cnt, AxisType);

                    if (err != 0) return;
                }

                //设定脉冲当量：每转脉冲数/丝杆导程
                for (int Cnt = 0; Cnt < AxisNum; Cnt++)
                {
                    AxisConfig aParam = AxisConfigList[Cnt];
                    float PPR = aParam.PPR;                 //电机细分
                    float Pitch = (float)aParam.Pitch;      //丝杆导程
                    err = zmcaux.ZAux_Direct_SetUnits(g_handle, Cnt, PPR / Pitch);

                    if (err != 0) return;
                }

                //设置正负软限位
                for (int Cnt = 0; Cnt < AxisNum; ++Cnt)
                {
                    AxisConfig aParam = AxisConfigList[Cnt];
                    float LowLimit = aParam.LowLimit;
                    float HighLimit = aParam.HighLimit;

                    err = zmcaux.ZAux_Direct_SetRsLimit(g_handle, Cnt, LowLimit);	//设置负向软限位位置/需要取消是设置成特别小的数即可，如-200000000
                    err = zmcaux.ZAux_Direct_SetFsLimit(g_handle, Cnt, HighLimit);	//设置正向软限位位置/需要取消是设置成特别大的数即可，如+200000000

                    if (err != 0) return;
                }

                //取消正负硬限位/当不接正负硬限位传感器时可以取消
                for (int Cnt = 0; Cnt < AxisNum; ++Cnt)
                {
                    err = zmcaux.ZAux_Direct_SetRevIn(g_handle, Cnt, -1);           //取消负向硬限位位置
                    err = zmcaux.ZAux_Direct_SetFwdIn(g_handle, Cnt, -1);           //取消正向硬限位位置

                    if (err != 0) return;
                }

                //取消轴报警信号/当不接轴报警信号到控制器时可以取消
                for (int Cnt = 0; Cnt < AxisNum; ++Cnt)
                {
                    err = zmcaux.ZAux_Direct_SetAlmIn(g_handle, Cnt, -1);           //取消轴报警信号

                    if (err != 0) return;
                }

                //设置运动速度以及加速度和减速度
                for (int Cnt = 0; Cnt < AxisNum; Cnt++)
                {
                    double speed = AxisSpeedList[Cnt].Speed;
                    double accel = speed / AxisSpeedList[Cnt].ATime * 1000;
                    double decel = speed / AxisSpeedList[Cnt].DTime * 1000;

                    err = zmcaux.ZAux_Direct_SetSpeed(g_handle, Cnt, (float)speed); //设置轴速度
                    err = zmcaux.ZAux_Direct_SetAccel(g_handle, Cnt, (float)accel); //设置轴加速度
                    err = zmcaux.ZAux_Direct_SetDecel(g_handle, Cnt, (float)decel); //设置轴减速度
                }
            }
            else
            {
                this.Text = "控制器尚未链接";
            }
        }

        private void buttonCardClose_Click(object sender, EventArgs e)
        {
            //断开链接/关闭卡
            zmcaux.ZAux_Close(g_handle);
            g_handle = (IntPtr)0;

            this.Text = "控制器已经断开";
        }

        private void buttonSaveInfo_Click(object sender, EventArgs e)
        {
            SaveForm();
            SaveIni();
        }

        private void buttonHome_Click(object sender, EventArgs e)
        {
            //判断句柄是否有效
            if ((int)g_handle == 0)
            {
                MessageBox.Show("未链接到控制器!", "提示");
            }
            else
            {
                int nAxis = 0;
                if (radioButtonAxisX.Checked == true) nAxis = 0;
                if (radioButtonAxisY.Checked == true) nAxis = 1;
                if (radioButtonAxisZ.Checked == true) nAxis = 2;
                if (radioButtonAxisU.Checked == true) nAxis = 3;

                int SensorMode = AxisConfigList[nAxis].SensorMode;
                double speed = AxisSpeedList[nAxis].Speed;
                double accel = speed / AxisSpeedList[nAxis].ATime * 1000;
                double decel = speed / AxisSpeedList[nAxis].DTime * 1000;

                //设置零点传感器输入端口/这里假定将20口作为原点端口
                zmcaux.ZAux_Direct_SetDatumIn(g_handle, nAxis, nAxis + 20);
                //设置零点输入口电平有效模式/默认低电平有效，当SensorMode=1时表示将输入口反转，变为高电平有效
                zmcaux.ZAux_Direct_SetInvertIn(g_handle, nAxis + 20, SensorMode);
                //设置运动速度
                zmcaux.ZAux_Direct_SetSpeed(g_handle, nAxis, (float)speed);
                //设置运动加速度
                zmcaux.ZAux_Direct_SetAccel(g_handle, nAxis, (float)accel);
                //设置运动减速度
                zmcaux.ZAux_Direct_SetDecel(g_handle, nAxis, (float)decel);
                //设置零点出入口信号有效后，反向爬行速度
                zmcaux.ZAux_Direct_SetCreep(g_handle, nAxis, (float)5);

                //开始调用回零函数，执行回零运动
                zmcaux.ZAux_Direct_Singl_Datum(g_handle, nAxis, AxisConfigList[nAxis].HomeMode);
            }
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            //查看控制器句柄是否有效
            if ((int)g_handle == 0)
            {
                MessageBox.Show("未链接到控制器!", "提示");
            }
            else
            {
                int nAxis = 0;
                if (radioButtonAxisX.Checked == true) nAxis = 0;
                if (radioButtonAxisY.Checked == true) nAxis = 1;
                if (radioButtonAxisZ.Checked == true) nAxis = 2;
                if (radioButtonAxisU.Checked == true) nAxis = 3;

                //取消轴所有运动
                zmcaux.ZAux_Direct_Singl_Cancel(g_handle, nAxis, 2);
            }
        }

        private void buttonZero_Click(object sender, EventArgs e)
        {
            //查看控制器句柄是否有效
            if ((int)g_handle == 0)
            {
                MessageBox.Show("未链接到控制器!", "提示");
            }
            else
            {
                for (int i = 0; i < AxisNum; i++)
                {
                    //将轴当前位置修改为0
                    zmcaux.ZAux_Direct_SetDpos(g_handle, i, 0);
                }
            }
        }

        private void buttonMoveAdd_MouseDown(object sender, MouseEventArgs e)
        {
            AddDown = true;
        }

        private void buttonMoveAdd_MouseUp(object sender, MouseEventArgs e)
        {
            AddDown = false;
        }

        private void buttonMoveSub_MouseDown(object sender, MouseEventArgs e)
        {
            SubDown = true;
        }

        private void buttonMoveSub_MouseUp(object sender, MouseEventArgs e)
        {
            SubDown = false;
        }

        private void radioButtonAxisX_CheckedChanged(object sender, EventArgs e)
        {
            UpdateForm();
        }

        private void radioButtonAxisY_CheckedChanged(object sender, EventArgs e)
        {
            UpdateForm();
        }

        private void radioButtonAxisZ_CheckedChanged(object sender, EventArgs e)
        {
            UpdateForm();
        }

        private void radioButtonAxisU_CheckedChanged(object sender, EventArgs e)
        {
            UpdateForm();
        }

        private void timerMotor_Tick(object sender, EventArgs e)
        {
            //判断运动控制器句柄是否有效
            if ((int)g_handle == 0) return;

            //判断是否有按钮被按下
            if (SubDown || AddDown)
            {
                //判断当前选择的是哪个轴
                int nAxis = 0;
                if (radioButtonAxisX.Checked == true) nAxis = 0;
                if (radioButtonAxisY.Checked == true) nAxis = 1;
                if (radioButtonAxisZ.Checked == true) nAxis = 2;
                if (radioButtonAxisU.Checked == true) nAxis = 3;

                //设置速度/高速
                if (radioButtonLow.Checked)
                {
                    zmcaux.ZAux_Direct_SetSpeed(g_handle, nAxis, 10);
                    ContiWork = true;   //设置持续运动标志
                }
                //设置速度/低速
                if (radioButtonHigh.Checked)
                {
                    zmcaux.ZAux_Direct_SetSpeed(g_handle, nAxis, 50);
                    ContiWork = true;   //设置持续运动标志
                }

                //持续运动标志
                if (ContiWork)
                {
                    if (AddDown)
                    {
                        //正向持续运动
                        zmcaux.ZAux_Direct_Singl_Vmove(g_handle, nAxis, 1);
                    }
                    else if (SubDown)
                    {
                        //反向持续运动
                        zmcaux.ZAux_Direct_Singl_Vmove(g_handle, nAxis, -1);
                    }
                }
            }
            else
            {
                //持续运动标志
                if (ContiWork)
                {
                    //判断当前选择的是哪个轴
                    int nAxis = 0;
                    if (radioButtonAxisX.Checked == true) nAxis = 0;
                    if (radioButtonAxisY.Checked == true) nAxis = 1;
                    if (radioButtonAxisZ.Checked == true) nAxis = 2;
                    if (radioButtonAxisU.Checked == true) nAxis = 3;

                    //反置持续运动标志
                    ContiWork = false;

                    //取消轴所有运动
                    zmcaux.ZAux_Direct_Singl_Cancel(g_handle, nAxis, 2);
                }
            }
        }

        private void timerStatus_Tick(object sender, EventArgs e)
        {
            //判断控制器句柄是否有效
            if ((int)g_handle != 0)
            {
                int[] runstate = new int[AxisNum];          //轴的运行状态数组
                float[] curdpos = new float[AxisNum];       //轴的当前位置数组/虚拟位置
                float[] curmpos = new float[AxisNum];       //轴的编码位置数组/开环接法时同curdpos

                for (int i = 0; i < AxisNum; i++)
                {

                    //获取当前轴是否空闲
                    zmcaux.ZAux_Direct_GetIfIdle(g_handle, i, ref runstate[i]);
                    //获取当前轴的虚拟位置
                    zmcaux.ZAux_Direct_GetDpos(g_handle, i, ref curdpos[i]);
                    //获取当前轴的编码位置/闭环接法有效
                    zmcaux.ZAux_Direct_GetMpos(g_handle, i, ref curmpos[i]);
                }

                label_X.Text = "X " + Convert.ToString(runstate[0] == 0 ? " 运行中 " : " 停止中 ") + curdpos[0];
                label_Y.Text = "Y " + Convert.ToString(runstate[1] == 0 ? " 运行中 " : " 停止中 ") + curdpos[1];
                label_Z.Text = "Z " + Convert.ToString(runstate[2] == 0 ? " 运行中 " : " 停止中 ") + curdpos[2];
                label_U.Text = "U " + Convert.ToString(runstate[3] == 0 ? " 运行中 " : " 停止中 ") + curdpos[3];
            }
        }


        private void buttonMoveAdd_Click(object sender, EventArgs e)
        {
            //判断当前选择的是哪个轴
            int nAxis = 0;
            if (radioButtonAxisX.Checked == true) nAxis = 0;
            if (radioButtonAxisY.Checked == true) nAxis = 1;
            if (radioButtonAxisZ.Checked == true) nAxis = 2;
            if (radioButtonAxisU.Checked == true) nAxis = 3;

            //单步1mm移动电机
            if (radioButton1.Checked == true)
            {
                //使用单步相对移动指令移动电机
                zmcaux.ZAux_Direct_Singl_Move(g_handle, nAxis, 1.0f);
            }

            //单步0.1mm移动电机
            if (radioButton2.Checked == true)
            {
                //使用单步相对移动指令移动电机
                zmcaux.ZAux_Direct_Singl_Move(g_handle, nAxis, 0.1f);
            }
        }

        private void buttonMoveSub_Click(object sender, EventArgs e)
        {
            //判断当前选择的是哪个轴
            int nAxis = 0;
            if (radioButtonAxisX.Checked == true) nAxis = 0;
            if (radioButtonAxisY.Checked == true) nAxis = 1;
            if (radioButtonAxisZ.Checked == true) nAxis = 2;
            if (radioButtonAxisU.Checked == true) nAxis = 3;

            //单步1mm移动电机
            if (radioButton1.Checked == true)
            {
                //使用单步相对移动指令移动电机
                zmcaux.ZAux_Direct_Singl_Move(g_handle, nAxis, -1.0f);
            }

            //单步0.1mm移动电机
            if (radioButton2.Checked == true)
            {
                //使用单步相对移动指令移动电机
                zmcaux.ZAux_Direct_Singl_Move(g_handle, nAxis, -0.1f);
            }
        }


        private void buttonCloseCom_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                serialPort1.Close();
            }
            else
            {
                MessageBox.Show("串口已经关闭", "提示");
            }
        }

        private void buttonOpenCom_Click(object sender, EventArgs e)
        {
            if (!serialPort1.IsOpen)
            {
                try
                {
                    serialPort1.Open();
                    //打开之后发送数据点亮光源
                    numericUpDownLightValue_ValueChanged(null, null);
                }
                catch (Exception ex) { MessageBox.Show(ex.Message, "提示"); }
            }
            else
            {
                MessageBox.Show("串口已经打开", "提示");
            }
        }

        private void numericUpDownLightValue_ValueChanged(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                int Channel = 0;                                    //使用通道1（控制器可以接多个光源，每个光源对应一个通道）
                int LightValue = (int)numericUpDownLightVaue.Value;
                byte[] sendData = new byte[7];
                sendData[0] = 0x02;                                 //开始码
                sendData[1] = (byte)(Channel + 0x30);               //通道号
                sendData[2] = (byte)(LightValue / 100 + 0x30);      //百
                sendData[3] = (byte)(LightValue % 100 / 10 + 0x30); //十
                sendData[4] = (byte)(LightValue % 10 + 0x30);       //个
                sendData[5] = (byte)(sendData[1] + sendData[2] + sendData[3] + sendData[4]);    //校验
                sendData[6] = 0x03;                                 //结束码

                //发送数据
                serialPort1.Write(sendData, 0, sendData.Length);
            }
        }

        private void buttonLight_Click(object sender, EventArgs e)
        {
            //获取光源亮度/该值可以和图像定位参数放一起，以后图像定位时前先用该亮度值点亮光源
            textBoxLightValue.Text = numericUpDownLightVaue.Value.ToString();
        }



        //每个窗口绑定到一个编号，每个图像绑定到一个图像号

        //给每一个窗口分配一个0--255之间的编号
        //窗口号
        int windowNum1 = 10;

        //给每一个图像分配一个0--255之间的编号
        //图像号
        int imageNum1 = 12;

        //给每一个模板分配一个0--255之间的编号
        //模板号
        int templateNum1 = 1;

        //给每一个搜索区域分配一个0--255之间的编号
        //搜索号
        int roiNum1 = 4;


        uint m_handle = 0;                                      //相机句柄
        bool m_b_open = false;                                  //相机打开
        bool m_b_start = false;                                 //视频流开始
        IntPtr m_ptr = new IntPtr();                            //相机上下文

        bool stopShowFrame = false;                             //停止显示图像

        class ModelPos
        {
            public float MPosX = 0;                            //模板坐标X
            public float MPosY = 0;                            //模板坐标Y
            public float MPosZ = 0;                            //模板坐标Z

            public float VPosX = 0;                            //模板坐标X
            public float VPosY = 0;                            //模板坐标Y
            public float VPosA = 0;                            //模板角度A
        }

        ModelPos ModelSet = new ModelPos();                     //模板设置位置
        ModelPos ModelRun = new ModelPos();                     //工作定位位置

        float calibrationX = 0;                                //相机标定值X/每像素对应的机械距离
        float calibrationY = 0;                                //相机标定值Y/每像素对应的机械距离
        int imageWidth = 1280, imageHeight = 960;               //相机分辨率/在初始化视觉窗口时会更新为相机实际分辨率

        float matchX = 0;                                      //定位结果
        float matchY = 0;                                      //定位结果
        float matchA = 0;                                      //定位结果
        float matchS = 0;                                      //定位结果


        #region 相机相关

        //打开相机
        void OpenCamera()
        {
            dvpStatus status;
            uint i = 0, n = 0;

            // 获得当前能连接的相机数量
            status = DVPCamera.dvpRefresh(ref n);

            //当相机数量大于1且还没打开时
            if (n > 0 && !IsValidHandle(m_handle))
            {
                //打开相机，返回相机链接标识
                status = DVPCamera.dvpOpen(i, dvpOpenMode.OPEN_NORMAL, ref m_handle);
                m_b_open = status == dvpStatus.DVP_STATUS_OK ? true : false;
                if (status != dvpStatus.DVP_STATUS_OK)
                {
                    m_handle = 0;
                    MessageBox.Show("打开失败");
                }
                else
                {
                    DVPCamera.dvpSetTriggerState(m_handle, false);                          //关闭软触发模式
                    DVPCamera.dvpSetTargetFormat(m_handle, dvpImageFormat.FORMAT_BGR24);    //设置图像数据格式

                    _proc = _dvpStreamCallback;
                    using (Process curProcess = Process.GetCurrentProcess())
                    using (ProcessModule curModule = curProcess.MainModule)
                    {
                        //注册图像返回事件
                        dvpStatus s = DVPCamera.dvpRegisterStreamCallback(m_handle, _proc, dvpStreamEvent.STREAM_EVENT_PROCESSED, m_ptr);
                    }
                }
            }
        }

        //开始视频流
        void StartVideo()
        {
            //查看相机是否可用
            if (IsValidHandle(m_handle))
            {
                dvpStreamState state = dvpStreamState.STATE_STOPED;
                dvpStatus status = dvpStatus.DVP_STATUS_UNKNOW;

                //查看流状态
                status = DVPCamera.dvpGetStreamState(m_handle, ref state);
                if (state != dvpStreamState.STATE_STARTED)
                {
                    status = DVPCamera.dvpStart(m_handle);
                    m_b_start = status == dvpStatus.DVP_STATUS_OK ? true : false;
                    // 更新抗频闪设置控件/直流模式
                    DVPCamera.dvpSetAntiFlick(m_handle, dvpAntiFlick.ANTIFLICK_DISABLE);
                }
            }
        }

        //停止视频流
        void StopVideo()
        {
            //查看相机是否可用
            if (IsValidHandle(m_handle))
            {
                dvpStreamState state = dvpStreamState.STATE_STOPED;
                dvpStatus status = dvpStatus.DVP_STATUS_UNKNOW;

                //查看流状态
                status = DVPCamera.dvpGetStreamState(m_handle, ref state);
                if (state == dvpStreamState.STATE_STARTED)
                {
                    status = DVPCamera.dvpStop(m_handle);
                    m_b_start = status == dvpStatus.DVP_STATUS_OK ? false : true;
                }
            }
        }

        //关闭相机
        void CloseCamera()
        {
            //查看相机是否可用
            if (IsValidHandle(m_handle))
            {
                DVPCamera.dvpStop(m_handle);
                m_b_open = false;
                m_b_start = false;

                //反注册图像返回事件
                DVPCamera.dvpStreamCallback pf = new DVPCamera.dvpStreamCallback(_dvpStreamCallback);
                dvpStatus s = DVPCamera.dvpUnregisterStreamCallback(m_handle, pf, dvpStreamEvent.STREAM_EVENT_PROCESSED, m_ptr);
                DVPCamera.dvpClose(m_handle);
                m_handle = 0;
            }
        }

        //查看相机句柄是否有效
        public bool IsValidHandle(uint handle)
        {
            bool bValidHandle = false;
            dvpStatus status = DVPCamera.dvpIsValid(handle, ref bValidHandle);
            return bValidHandle;
        }

        //初始化视觉窗口
        void InitializeVision()
        {
            if (IsValidHandle(m_handle))
            {
                dvpQuickRoi QuickRoiDetail = new dvpQuickRoi();
                dvpStatus status;
                uint QuickRoiSel = 0;
                //获取图像分辨率
                status = DVPCamera.dvpGetQuickRoiSelDetail(m_handle, QuickRoiSel, ref QuickRoiDetail);
                if (status == dvpStatus.DVP_STATUS_OK)
                {
                    imageWidth = QuickRoiDetail.roi.W;
                    imageHeight = QuickRoiDetail.roi.H;
                }
            }

            //初始化视觉窗口
            XTVision_V1.XTCloseWindow(windowNum1);
            XTVision_V1.XTInitWindow(windowNum1, pictureBox1.Handle, imageWidth, imageHeight);
        }

        //图像回调委托
        private DVPCamera.dvpStreamCallback _proc;

        // 数据接收回调函数
        private static int _dvpStreamCallback(/*dvpHandle*/uint handle, dvpStreamEvent _event, /*void **/IntPtr pContext, ref dvpFrame refFrame, /*void **/IntPtr pBuffer)
        {
            RECT rt = new RECT();
            rt.Bottom = 100;
            rt.Left = 0;
            rt.Top = 0;
            rt.Right = 100;

            //将图形数据拷贝到本地
            ImageWidth = refFrame.iWidth;
            ImageHeight = refFrame.iHeight;
            Marshal.Copy(pBuffer, FrameData, 0, (int)refFrame.uBytes);
            IsFrameReady = true;

            return 1;
        }

        //申请一个足够大的本地缓存，在回调函数中将图像数据拷贝到本地缓存/需要使用图像数据的时候，从本地缓存中提取图像数据
        static int ImageWidth = 1280, ImageHeight = 960;

        //图像数据本地缓存/大小需要能容纳采集回来的图像
        static byte[] FrameData = new byte[2592 * 1944 * 3];

        //图像准备好标志/
        static bool IsFrameReady = false;

        #endregion


        //窗体辅助初始化1
        void InitializeForm1()
        {
            //初始化视觉库
            XTVision_V1.XTInitialize(true);

            //打开相机
            OpenCamera();

            //打开视频
            StartVideo();

            //初始化视觉窗口
            InitializeVision();

            //定位模板参数
            textBoxMaxAngle.Text = "30";
            textBoxMinScore.Text = "700";
            textBoxLightValue.Text = "0";
            textBoxCalibStepSize.Text = "2";
            textBoxStableTime.Text = "1000";

            //定位方式
            comboBoxModelType.Items.Add("模糊定位");
            comboBoxModelType.Items.Add("形状定位");
            //comboBoxModelType.Items.Add("形变定位");
            comboBoxModelType.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxModelType.SelectedIndex = 0;

            //区域形状
            comboBoxAreaShape.Items.Add("矩形形状");
            comboBoxAreaShape.Items.Add("圆形形状");
            comboBoxAreaShape.Items.Add("任意形状");
            comboBoxAreaShape.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxAreaShape.SelectedIndex = 0;

            //控件颜色
            pictureBox1.BackColor = Color.LightGray;

            //自动加载模板参数
            buttonLoadTemplate_Click(null, null);
        }

        private void timerFrame_Tick(object sender, EventArgs e)
        {
            //如果标志有效则直接返回/当选择模板的时候需要暂时停止显示图像
            if (stopShowFrame == true) return;
            //相机没连接，直接返回
            if (IsValidHandle(m_handle) == false) return;
            //如果新的图像还没回来则直接返回
            if (IsFrameReady == false) return;
            IsFrameReady = false;

            int lineWidth = 1;  //显示线宽
            int colorType = 0;  //0 -24位黑白数据；1 -24位彩色数据；2 -8位黑白数据
            int channel = 3;    //图像通道数
            //将相机采集回来的数据传入图像库中的imageNum1图像
            XTVision_V1.XTGrabFrame(imageNum1, colorType, channel, ImageWidth, ImageHeight, FrameData);
            //将图像imageNum1显示到窗口windowNum1中/缓冲显示
            XTVision_V1.XTDisplayPicture(windowNum1, imageNum1, 1);
            //在窗口windowNum1中显示大十字线/缓冲显示
            XTVision_V1.XTDispBigCross(windowNum1, lineWidth, "green", 1);
            //在窗口windowNum1中显示搜索区域roiNum1/缓冲显示
            XTVision_V1.XTDisplayRegion(windowNum1, roiNum1, lineWidth, "blue", 1);
            //在窗口windowNum1中显示模板区域templateNum1/缓冲显示
            XTVision_V1.XTDisplayTemplateArea(windowNum1, templateNum1, lineWidth, "green", 1);

            //运动控制器连接检查
            if (0 != (int)g_handle)
            {
                //取得当前位置
                float pfValue = 0;
                float[] cPos = new float[AxisNum];
                zmcaux.ZAux_Direct_GetDpos(g_handle, 0, ref pfValue);
                cPos[0] = pfValue;
                zmcaux.ZAux_Direct_GetDpos(g_handle, 1, ref pfValue);
                cPos[1] = pfValue;
                //显示直线坐标
                float orderX1 = Convert.ToSingle(textBoxWorkPosX1.Text);
                float orderY1 = Convert.ToSingle(textBoxWorkPosY1.Text);
                float dispX1 = (orderX1 - cPos[0]) / calibrationX + imageWidth * 0.5f;
                float dispY1 = (orderY1 - cPos[1]) / calibrationY + imageHeight * 0.5f;
                float orderX2 = Convert.ToSingle(textBoxWorkPosX2.Text);
                float orderY2 = Convert.ToSingle(textBoxWorkPosY2.Text);
                float dispX2 = (orderX2 - cPos[0]) / calibrationX + imageWidth * 0.5f;
                float dispY2 = (orderY2 - cPos[1]) / calibrationY + imageHeight * 0.5f;
                //将直线显示到windowNum1窗口中，线宽为1颜色为yellow
                XTVision_V1.XTDispLine(windowNum1, dispY1, dispX1, dispY2, dispX2, 1, "yellow", 1);
            }

            //显示缓冲中的图像到窗口windowNum1中
            XTVision_V1.XTDisplayPicture(windowNum1, imageNum1, 2);
        }

        private void buttonSelectTemplateArea_Click(object sender, EventArgs e)
        {
            //暂停图像显示
            stopShowFrame = true;

            //将焦点转移到图像控件上
            pictureBox1.Focus();
            //鼠标画图形状0-矩形 1-圆形 2-任意形状
            int AreaShape = comboBoxAreaShape.SelectedIndex;

            //将图像imageNum1显示到窗口windowNum1中/缓存方式
            XTVision_V1.XTDisplayPicture(windowNum1, imageNum1, 1);
            //在windowNum1窗口中显示大十字线/缓存方式
            XTVision_V1.XTDispBigCross(windowNum1, 1, "green", 1);
            //将缓存图像imageNum1显示到窗口windowNum1中
            XTVision_V1.XTDisplayPicture(windowNum1, imageNum1, 2);
            //调用以上三个函数实现刷新窗口图像显示功能

            this.Text = @"提示：请用鼠标左键在图像上选择和修改区域，右键结束修改！";

            //调用“选择模板区域”函数/每调用一次函数，都需要鼠标在窗口上做画图操作并以右键结束
            int err = XTVision_V1.XTSelectTemplateArea(windowNum1, imageNum1, templateNum1, AreaShape, "red", "green");

            this.Text = @"信息提示";

            //恢复图像显示
            stopShowFrame = false;
        }

        private void buttonProcessTemplate_Click(object sender, EventArgs e)
        {
            //暂停图像显示
            stopShowFrame = true;

            this.Text = @"提示：正在设置模板，请稍后。。。";

            int modelType = comboBoxModelType.SelectedIndex;            //定位类型：0-模糊定位 1-形状定位 2-形变定位
            float maxAngle = Convert.ToSingle(textBoxMaxAngle.Text);    //定位角度

            maxAngle = maxAngle * (float)Math.PI / 180;
            float angleStart = -1 * maxAngle;                           //开始角度
            float angleRange = 2 * maxAngle;                            //旋转角度
            float scaleMin = 0.8f;                                      //最小缩放比例80%
            float scaleMax = 1.1f;                                      //最大缩放比例110%
            //调用“设定模板”函数，训练好一个模板用于后续定位使用
            int err = XTVision_V1.XTCreateTemplate(imageNum1, templateNum1, modelType, angleStart, angleRange, scaleMin, scaleMax);
            if (err == 0)
            {
                //创建模板成功则刷新窗口/0表示直接显示图像
                XTVision_V1.XTDisplayPicture(windowNum1, imageNum1, 0);
            }

            this.Text = @"信息提示";

            //恢复图像显示
            stopShowFrame = false;

            //在执行设置模板时，获取模板当前的机械位置
            float piValue = 0;
            zmcaux.ZAux_Direct_GetDpos(g_handle, 0, ref piValue);
            ModelSet.MPosX = piValue;
            zmcaux.ZAux_Direct_GetDpos(g_handle, 1, ref piValue);
            ModelSet.MPosY = piValue;
            zmcaux.ZAux_Direct_GetDpos(g_handle, 2, ref piValue);
            ModelSet.MPosZ = piValue;
        }

        private void buttonSelectSearchArea_Click(object sender, EventArgs e)
        {
            //暂停图像显示
            stopShowFrame = true;

            //将焦点转移到图像控件上
            pictureBox1.Focus();
            //鼠标画图形状0-矩形 1-圆形 2-任意形状
            int AreaShape = comboBoxAreaShape.SelectedIndex;

            this.Text = @"提示：请用鼠标左键在图像上选择和修改区域，右键结束修改！";

            //调用“选择搜索区域”函数/每调用一次函数，都需要鼠标在窗口上做画图操作并以右键结束
            int err = XTVision_V1.XTSelectRegion(windowNum1, imageNum1, roiNum1, AreaShape, "red", "green");

            this.Text = @"信息提示";

            //恢复图像显示
            stopShowFrame = false;
        }

        private void buttonProcessImage_Click(object sender, EventArgs e)
        {
            float maxAngle = Convert.ToSingle(textBoxMaxAngle.Text);    //旋转角度
            float minScore = Convert.ToSingle(textBoxMinScore.Text);    //最小分数
            int findNumber = 1;                                         //寻找模板数量
            int matchNumber = 0;                                        //定位结果数量

            maxAngle = maxAngle * (float)Math.PI / 180;
            float angleStart = -1 * maxAngle;                           //开始角度
            float angleRange = 2 * maxAngle;                            //旋转角度
            float scaleMin = 0.8f;                                      //最小缩放比例80%
            float scaleMax = 1.1f;                                      //最大缩放比例110%
            //调用“图像定位”函数，使用训练好的模板在图像中寻找匹配度最好的图像区域，并得到该区域的图像坐标以及实际匹配度(匹配分数)
            int err = XTVision_V1.XTFindTemplate(imageNum1, roiNum1, templateNum1, angleStart, angleRange, scaleMin, scaleMax, minScore, findNumber, ref matchNumber);

            //函数执行没有错误，并且寻找模板数量和定位结果数量
            if (err == 0 && matchNumber == findNumber)
            {
                if (matchNumber > 0)
                {
                    //读取定位结果
                    float[] MatchX = new float[matchNumber];
                    float[] MatchY = new float[matchNumber];
                    float[] MatchAngle = new float[matchNumber];
                    float[] MatchScore = new float[matchNumber];
                    float[] MatchScale = new float[matchNumber];
                    XTVision_V1.XTReadFindResult(templateNum1, matchNumber, MatchY, MatchX, MatchAngle, MatchScore, MatchScale);
                    for (int idx = 0; idx < matchNumber; ++idx)
                    {
                        //转换角度单位
                        MatchAngle[idx] = MatchAngle[idx] * 180 / (float)Math.PI;
                    }

                    int dispType = 0;   //显示方式
                    //在windowNum1窗口上显示roiNum1搜索区域
                    XTVision_V1.XTDisplayRegion(windowNum1, roiNum1, 1, "blue", dispType);
                    //在windowNum1窗口上显示templateNum1模板区域(在定位结果位置显示)
                    XTVision_V1.XTDisplayTemplateArea(windowNum1, templateNum1, 1, "green", dispType);

                    //显示定位结果/文字
                    string strMsg = null;
                    strMsg += string.Format("坐标X:{0:N2} ", MatchX[0]);
                    strMsg += string.Format("坐标Y:{0:N2} ", MatchY[0]);
                    strMsg += string.Format("角度A:{0:N2} ", MatchAngle[0]);
                    strMsg += string.Format("分数S:{0:N2} ", MatchScore[0]);
                    this.Text = strMsg;
                }
            }
            else
            {
                this.Text = @"提示：模板定位失败";
            }
        }

        private void buttonSaveTemplate_Click(object sender, EventArgs e)
        {
            try
            {
                //如果目录不存在则创建文件夹
                string fileDir = "实验3_2";
                if (Directory.Exists(fileDir) == false)
                {
                    Directory.CreateDirectory(fileDir);
                }

                string filePath = null;
                int saveTemplateImage = 1;
                filePath = fileDir + "/" + "roi" + roiNum1;
                //保存搜索区域roiNum1到文件"roi" + roiNum1中
                XTVision_V1.XTSaveRegion(roiNum1, filePath);
                filePath = fileDir + "/" + "tmp" + templateNum1;
                //保存模板到templateNum1到文件"tmp" + templateNum1中
                XTVision_V1.XTSaveTemplate(templateNum1, filePath, saveTemplateImage);
            }
            catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.Message); }


            //保存图像相关参数  
            //运行目录下的image.ini文件/用于管理图像相关的数据
            string imageFilePath = Application.StartupPath + "\\image3_2.ini";
            Win32.WritePrivateProfileString("MODEL_MATCH", "MODEL_TYPE", comboBoxModelType.SelectedIndex.ToString(), imageFilePath);
            Win32.WritePrivateProfileString("MODEL_MATCH", "MAX_ANGLE", textBoxMaxAngle.Text, imageFilePath);
            Win32.WritePrivateProfileString("MODEL_MATCH", "MIN_SCORE", textBoxMinScore.Text, imageFilePath);
            Win32.WritePrivateProfileString("MODEL_MATCH", "LIGHT_VALUE", textBoxLightValue.Text, imageFilePath);
            Win32.WritePrivateProfileString("MODEL_MATCH", "STEP_SIZE", textBoxCalibStepSize.Text, imageFilePath);
            Win32.WritePrivateProfileString("MODEL_MATCH", "STABLE_TIME", textBoxStableTime.Text, imageFilePath);


            //保存相机标定值
            Win32.WritePrivateProfileString("CAMERA_CALIB", "CALIBX", calibrationX.ToString(), imageFilePath);
            Win32.WritePrivateProfileString("CAMERA_CALIB", "CALIBY", calibrationY.ToString(), imageFilePath);


            //保存模板设置位置
            Win32.WritePrivateProfileString("MODEL_POS", "MODEL_M_POSX", ModelSet.MPosX.ToString(), imageFilePath);
            Win32.WritePrivateProfileString("MODEL_POS", "MODEL_M_POSY", ModelSet.MPosY.ToString(), imageFilePath);
            Win32.WritePrivateProfileString("MODEL_POS", "MODEL_M_POSZ", ModelSet.MPosZ.ToString(), imageFilePath);

            Win32.WritePrivateProfileString("MODEL_POS", "MODEL_V_POSX", ModelSet.VPosX.ToString(), imageFilePath);
            Win32.WritePrivateProfileString("MODEL_POS", "MODEL_V_POSY", ModelSet.VPosY.ToString(), imageFilePath);
            Win32.WritePrivateProfileString("MODEL_POS", "MODEL_V_POSA", ModelSet.VPosA.ToString(), imageFilePath);


            //保存设定工作位置
            Win32.WritePrivateProfileString("WORK_POS", "WORK_POSX1", textBoxWorkPosX1.Text, imageFilePath);
            Win32.WritePrivateProfileString("WORK_POS", "WORK_POSY1", textBoxWorkPosY1.Text, imageFilePath);
            Win32.WritePrivateProfileString("WORK_POS", "WORK_POSZ1", textBoxWorkPosZ1.Text, imageFilePath);
            Win32.WritePrivateProfileString("WORK_POS", "WORK_POSX2", textBoxWorkPosX2.Text, imageFilePath);
            Win32.WritePrivateProfileString("WORK_POS", "WORK_POSY2", textBoxWorkPosY2.Text, imageFilePath);
            Win32.WritePrivateProfileString("WORK_POS", "WORK_POSZ2", textBoxWorkPosZ2.Text, imageFilePath);

            //保存光源控制亮度
            Win32.WritePrivateProfileString("LIGHT_CONTROLLER", "LIGHT_VALUE", numericUpDownLightVaue.Value.ToString(), imageFilePath);
        }

        private void buttonLoadTemplate_Click(object sender, EventArgs e)
        {
            try
            {
                //如果目录不存在则创建文件夹
                string fileDir = "实验3_2";
                if (Directory.Exists(fileDir) == false)
                {
                    Directory.CreateDirectory(fileDir);
                }

                string filePath = null;
                int loadTemplateImage = 1;
                filePath = fileDir + "/" + "roi" + roiNum1;
                //从文件"roi" + roiNum1中加载搜索区到roiNum1中
                XTVision_V1.XTLoadRegion(roiNum1, filePath);
                filePath = fileDir + "/" + "tmp" + templateNum1;
                //从文件"tmp" + templateNum1中加载模板到templateNum1中
                XTVision_V1.XTLoadTemplate(templateNum1, filePath, loadTemplateImage);
            }
            catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.Message); }


            //读取图像相关参数  
            //运行目录下的image.ini文件/用于管理图像相关的数据
            string imageFilePath = Application.StartupPath + "\\image3_2.ini";
            //可变字符串
            StringBuilder temp = new StringBuilder(128);

            Win32.GetPrivateProfileString("MODEL_MATCH", "MODEL_TYPE", "0", temp, 128, imageFilePath);
            comboBoxModelType.SelectedIndex = Convert.ToInt32(temp.ToString());

            Win32.GetPrivateProfileString("MODEL_MATCH", "MAX_ANGLE", "30", temp, 128, imageFilePath);
            textBoxMaxAngle.Text = temp.ToString();

            Win32.GetPrivateProfileString("MODEL_MATCH", "MIN_SCORE", "700", temp, 128, imageFilePath);
            textBoxMinScore.Text = temp.ToString();

            Win32.GetPrivateProfileString("MODEL_MATCH", "LIGHT_VALUE", "0", temp, 128, imageFilePath);
            textBoxLightValue.Text = temp.ToString();

            Win32.GetPrivateProfileString("MODEL_MATCH", "STEP_SIZE", "1", temp, 128, imageFilePath);
            textBoxCalibStepSize.Text = temp.ToString();

            Win32.GetPrivateProfileString("MODEL_MATCH", "STABLE_TIME", "100", temp, 128, imageFilePath);
            textBoxStableTime.Text = temp.ToString();


            //读取相机标定值
            Win32.GetPrivateProfileString("CAMERA_CALIB", "CALIBX", "1", temp, 128, imageFilePath);
            calibrationX = Convert.ToSingle(temp.ToString());

            Win32.GetPrivateProfileString("CAMERA_CALIB", "CALIBY", "1", temp, 128, imageFilePath);
            calibrationY = Convert.ToSingle(temp.ToString());


            //读取模板设置位置
            Win32.GetPrivateProfileString("MODEL_POS", "MODEL_M_POSX", "0", temp, 128, imageFilePath);
            ModelSet.MPosX = Convert.ToSingle(temp.ToString());

            Win32.GetPrivateProfileString("MODEL_POS", "MODEL_M_POSY", "0", temp, 128, imageFilePath);
            ModelSet.MPosY = Convert.ToSingle(temp.ToString());

            Win32.GetPrivateProfileString("MODEL_POS", "MODEL_M_POSZ", "0", temp, 128, imageFilePath);
            ModelSet.MPosZ = Convert.ToSingle(temp.ToString());

            Win32.GetPrivateProfileString("MODEL_POS", "MODEL_V_POSX", "0", temp, 128, imageFilePath);
            ModelSet.VPosX = Convert.ToSingle(temp.ToString());

            Win32.GetPrivateProfileString("MODEL_POS", "MODEL_V_POSY", "0", temp, 128, imageFilePath);
            ModelSet.VPosY = Convert.ToSingle(temp.ToString());

            Win32.GetPrivateProfileString("MODEL_POS", "MODEL_V_POSA", "0", temp, 128, imageFilePath);
            ModelSet.VPosA = Convert.ToSingle(temp.ToString());


            //读取设定工作位置
            Win32.GetPrivateProfileString("WORK_POS", "WORK_POSX1", "0", temp, 128, imageFilePath);
            textBoxWorkPosX1.Text = temp.ToString();

            Win32.GetPrivateProfileString("WORK_POS", "WORK_POSY1", "0", temp, 128, imageFilePath);
            textBoxWorkPosY1.Text = temp.ToString();

            Win32.GetPrivateProfileString("WORK_POS", "WORK_POSZ1", "0", temp, 128, imageFilePath);
            textBoxWorkPosZ1.Text = temp.ToString();

            Win32.GetPrivateProfileString("WORK_POS", "WORK_POSX2", "0", temp, 128, imageFilePath);
            textBoxWorkPosX2.Text = temp.ToString();

            Win32.GetPrivateProfileString("WORK_POS", "WORK_POSY2", "0", temp, 128, imageFilePath);
            textBoxWorkPosY2.Text = temp.ToString();

            Win32.GetPrivateProfileString("WORK_POS", "WORK_POSZ2", "0", temp, 128, imageFilePath);
            textBoxWorkPosZ2.Text = temp.ToString();

            //加载光源控制器亮度
            Win32.GetPrivateProfileString("LIGHT_CONTROLLER", "LIGHT_VALUE", "0", temp, 128, imageFilePath);
            numericUpDownLightVaue.Value = Convert.ToDecimal(temp.ToString());
        }

        private void buttonToTemplatePos_Click(object sender, EventArgs e)
        {
            int[] piIfIdle = new int[8];

            //设置移动速度
            zmcaux.ZAux_Direct_SetSpeed(g_handle, 0, (float)50);
            zmcaux.ZAux_Direct_SetAccel(g_handle, 0, (float)50);
            zmcaux.ZAux_Direct_SetDecel(g_handle, 0, (float)50);
            zmcaux.ZAux_Direct_SetSpeed(g_handle, 1, (float)50);
            zmcaux.ZAux_Direct_SetAccel(g_handle, 1, (float)50);
            zmcaux.ZAux_Direct_SetDecel(g_handle, 1, (float)50);
            zmcaux.ZAux_Direct_SetSpeed(g_handle, 2, (float)50);
            zmcaux.ZAux_Direct_SetAccel(g_handle, 2, (float)50);
            zmcaux.ZAux_Direct_SetDecel(g_handle, 2, (float)50);

            //移动到模板设置位置
            zmcaux.ZAux_Direct_Singl_MoveAbs(g_handle, 0, (float)ModelSet.MPosX);
            zmcaux.ZAux_Direct_Singl_MoveAbs(g_handle, 1, (float)ModelSet.MPosY);
            zmcaux.ZAux_Direct_Singl_MoveAbs(g_handle, 2, (float)ModelSet.MPosZ);

            //等待运行结束
            piIfIdle[0] = 0; piIfIdle[1] = 0; piIfIdle[2] = 0;
            while (-1 != piIfIdle[0] || -1 != piIfIdle[1] || -1 != piIfIdle[2])
            {
                zmcaux.ZAux_Direct_GetIfIdle(g_handle, 0, ref piIfIdle[0]);
                zmcaux.ZAux_Direct_GetIfIdle(g_handle, 1, ref piIfIdle[1]);
                zmcaux.ZAux_Direct_GetIfIdle(g_handle, 2, ref piIfIdle[2]);
            }
        }


        private void buttonCameraParam_Click(object sender, EventArgs e)
        {
            //查看相机是否可用
            if (IsValidHandle(m_handle))
            {
                //显示相机参数对话框
                DVPCamera.dvpShowPropertyModalDialog(m_handle, this.Handle);
            }
        }

        private void buttonCalcPixelDist_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("即将校正图像，是否继续？", "提示", MessageBoxButtons.YesNo))
            {
                calibrationX = 1;
                calibrationY = 1;
                //停止定时器图像显示
                stopShowFrame = true;
                int err = PosePerPixelSize();
                //恢复定时器图像显示
                stopShowFrame = false;
                string text = string.Format("像素距离：X={0:N6},Y={1:N6}", calibrationX, calibrationY);
                if (err != 0 || calibrationX == 0 || calibrationY == 0 || Math.Abs(calibrationX) > 1 || Math.Abs(calibrationY) > 1)
                {
                    text = "标定失败,请检查图像是否清晰";
                }
                if (Math.Abs(calibrationX - calibrationY) > 0.02)
                {
                    text = "XY步距相差过大,请检查相机是否已经固定";
                }
                MessageBox.Show(text, "提示", MessageBoxButtons.YesNo);
            }
        }

        private void buttonGetCurrentPos1_Click(object sender, EventArgs e)
        {
            //获取当前位置
            float pfValue = 0;
            zmcaux.ZAux_Direct_GetDpos(g_handle, 0, ref pfValue);
            textBoxWorkPosX1.Text = string.Format("{0:N2}", pfValue);

            zmcaux.ZAux_Direct_GetDpos(g_handle, 1, ref pfValue);
            textBoxWorkPosY1.Text = string.Format("{0:N2}", pfValue);

            zmcaux.ZAux_Direct_GetDpos(g_handle, 2, ref pfValue);
            textBoxWorkPosZ1.Text = string.Format("{0:N2}", pfValue);
        }

        private void buttonGetCurrentPos2_Click(object sender, EventArgs e)
        {
            //获取当前位置
            float pfValue = 0;
            zmcaux.ZAux_Direct_GetDpos(g_handle, 0, ref pfValue);
            textBoxWorkPosX2.Text = string.Format("{0:N2}", pfValue);

            zmcaux.ZAux_Direct_GetDpos(g_handle, 1, ref pfValue);
            textBoxWorkPosY2.Text = string.Format("{0:N2}", pfValue);

            zmcaux.ZAux_Direct_GetDpos(g_handle, 2, ref pfValue);
            textBoxWorkPosZ2.Text = string.Format("{0:N2}", pfValue);
        }

        private void buttonLineRevise_Click(object sender, EventArgs e)
        {
            //停止定时器图像显示
            stopShowFrame = true;
            int err = ModelMatchWork();
            //恢复定时器图像显示
            stopShowFrame = false;
            if (err == 0)
            {
                //平台移动到目标修正点
                var taskA = new System.Threading.Tasks.Task(() =>
                {
                    LineRevise();
                });
                taskA.Start();
            }
            else
            {
                this.Text = "定位失败";
            }
        }

        //执行模板定位操作
        int MatchModel()
        {
            int err = 0;
            int stabTime = Convert.ToInt32(textBoxStableTime.Text);
            float stepSize = Convert.ToSingle(textBoxCalibStepSize.Text);

            float maxAngle = Convert.ToSingle(textBoxMaxAngle.Text);    //旋转角度
            float minScore = Convert.ToSingle(textBoxMinScore.Text);    //最小分数
            int findNumber = 1;                                         //寻找模板数量
            int matchNumber = 0;                                        //定位结果数量

            maxAngle = maxAngle * (float)Math.PI / 180;
            float angleStart = -1 * maxAngle;                           //开始角度
            float angleRange = 2 * maxAngle;                            //旋转角度
            float scaleMin = 0.8f;                                      //最小缩放比例80%
            float scaleMax = 1.1f;                                      //最大缩放比例110%

            //图像定位操作
            int dispType = 0;   //显示模式0 -直接显示，1 -显示到缓存 2 -显示缓存中图像
            int colorType = 0;  //0 -24位黑白数据；1 -24位彩色数据；2 -8位黑白数据
            int channel = 3;    //图像通道数
            //将相机采集回来的数据传入图像库中的imageNum1图像
            XTVision_V1.XTGrabFrame(imageNum1, colorType, channel, ImageWidth, ImageHeight, FrameData);
            //调用定位函数在图像imageNum1上进行图像定位
            err = XTVision_V1.XTFindTemplate(imageNum1, roiNum1, templateNum1, angleStart, angleRange, scaleMin, scaleMax, minScore, findNumber, ref matchNumber);
            //将图像imageNum1显示到窗口windowNum1中/缓冲显示
            XTVision_V1.XTDisplayPicture(windowNum1, imageNum1, dispType);
            //在窗口windowNum1中显示大十字线/缓冲显示
            XTVision_V1.XTDispBigCross(windowNum1, 1, "green", dispType);
            //在窗口windowNum1中显示搜索区域roiNum1/缓冲显示
            XTVision_V1.XTDisplayRegion(windowNum1, roiNum1, 1, "blue", dispType);
            //在窗口windowNum1中显示模板区域templateNum1/缓冲显示
            XTVision_V1.XTDisplayTemplateArea(windowNum1, templateNum1, 1, "green", dispType);
            //定位成功且定位数量达到设定要求
            if (err == 0 && matchNumber == findNumber)
            {
                if (matchNumber > 0)
                {
                    //读取定位结果
                    float[] MatchX = new float[matchNumber];
                    float[] MatchY = new float[matchNumber];
                    float[] MatchAngle = new float[matchNumber];
                    float[] MatchScore = new float[matchNumber];
                    float[] MatchScale = new float[matchNumber];
                    XTVision_V1.XTReadFindResult(templateNum1, matchNumber, MatchY, MatchX, MatchAngle, MatchScore, MatchScale);
                    for (int idx = 0; idx < matchNumber; ++idx)
                    {
                        //转换角度单位
                        MatchAngle[idx] = MatchAngle[idx] * 180 / (float)Math.PI;
                    }

                    //显示定位结果
                    string strMsg = null;
                    strMsg += string.Format("坐标X:{0:N2} ", MatchX[0]);
                    strMsg += string.Format("坐标Y:{0:N2} ", MatchY[0]);
                    strMsg += string.Format("角度A:{0:N2} ", MatchAngle[0]);
                    strMsg += string.Format("分数S:{0:N2} ", MatchScore[0]);
                    this.Text = strMsg;

                    //提取定位结果
                    matchX = (float)MatchX[0];
                    matchY = (float)MatchY[0];
                    matchA = (float)MatchAngle[0];
                    matchS = (float)MatchScale[0];
                }
                else
                {
                    this.Text = @"提示：模板定位失败";
                }
            }
            return err;
        }

        //运动计算像素距离
        int PosePerPixelSize()
        {
            //执行图像校正操作，计算每像素对应的机械距离
            int err = 0;
            int timeTick = 0;
            int stabTime = Convert.ToInt32(textBoxStableTime.Text);
            float stepSize = Convert.ToSingle(textBoxCalibStepSize.Text);
            float pfValue = 0;

            //检查相机连接
            if (m_b_open == false)
            {
                MessageBox.Show("相机还没打开", "提示");
                err = -1; return err;
            }

            int[] piIfIdle = new int[AxisNum];
            float[] curMPosX = new float[8];
            float[] curMPosY = new float[8];
            float[] curVPosX = new float[8];
            float[] curVPosY = new float[8];

            //设置移动速度
            zmcaux.ZAux_Direct_SetSpeed(g_handle, 0, (float)20);
            zmcaux.ZAux_Direct_SetAccel(g_handle, 0, (float)20);
            zmcaux.ZAux_Direct_SetDecel(g_handle, 0, (float)20);
            zmcaux.ZAux_Direct_SetSpeed(g_handle, 1, (float)20);
            zmcaux.ZAux_Direct_SetAccel(g_handle, 1, (float)20);
            zmcaux.ZAux_Direct_SetDecel(g_handle, 1, (float)20);
            zmcaux.ZAux_Direct_SetSpeed(g_handle, 2, (float)20);
            zmcaux.ZAux_Direct_SetAccel(g_handle, 2, (float)20);
            zmcaux.ZAux_Direct_SetDecel(g_handle, 2, (float)20);

            //移动到模板设置位置
            zmcaux.ZAux_Direct_Singl_MoveAbs(g_handle, 0, (float)ModelSet.MPosX);
            zmcaux.ZAux_Direct_Singl_MoveAbs(g_handle, 1, (float)ModelSet.MPosY);
            zmcaux.ZAux_Direct_Singl_MoveAbs(g_handle, 2, (float)ModelSet.MPosZ);

            //等待运行结束
            piIfIdle[0] = 0; piIfIdle[1] = 0; piIfIdle[2] = 0;
            while (-1 != piIfIdle[0] || -1 != piIfIdle[1] || -1 != piIfIdle[2])
            {
                zmcaux.ZAux_Direct_GetIfIdle(g_handle, 0, ref piIfIdle[0]);
                zmcaux.ZAux_Direct_GetIfIdle(g_handle, 1, ref piIfIdle[1]);
                zmcaux.ZAux_Direct_GetIfIdle(g_handle, 2, ref piIfIdle[2]);
            }
            //等待平台稳定
            System.Threading.Thread.Sleep(stabTime);

            //相机采集图像
            timeTick = 0;
            //采集前先把标识清除
            IsFrameReady = false;
            while (IsFrameReady == false && ++timeTick < 200)
            {
                System.Threading.Thread.Sleep(10);
            }
            if (IsFrameReady == false)
            {
                this.Text = "图像校验采图超时";
                return -1;
            }

            //执行定位操作
            err = MatchModel();
            if (err != 0) return err;

            //获取图像位置
            curVPosX[0] = matchX;
            curVPosY[0] = matchY;

            //获取机械位置
            zmcaux.ZAux_Direct_GetDpos(g_handle, 0, ref pfValue);
            curMPosX[0] = pfValue;
            zmcaux.ZAux_Direct_GetDpos(g_handle, 1, ref pfValue);
            curMPosY[0] = pfValue;

            //运行到取样点1
            zmcaux.ZAux_Direct_Singl_Move(g_handle, 0, stepSize);
            zmcaux.ZAux_Direct_Singl_Move(g_handle, 1, stepSize);
            piIfIdle[0] = 0; piIfIdle[1] = 0;
            while (-1 != piIfIdle[0] || -1 != piIfIdle[1])
            {
                zmcaux.ZAux_Direct_GetIfIdle(g_handle, 0, ref piIfIdle[0]);
                zmcaux.ZAux_Direct_GetIfIdle(g_handle, 1, ref piIfIdle[1]);
            }
            //等待平台稳定
            System.Threading.Thread.Sleep(stabTime);

            //相机采集图像
            timeTick = 0;
            //采集前先把标识清除
            IsFrameReady = false;
            while (IsFrameReady == false && ++timeTick < 200)
            {
                System.Threading.Thread.Sleep(10);
            }
            if (IsFrameReady == false)
            {
                this.Text = "图像校验采图超时";
                return -1;
            }

            //执行定位操作
            err = MatchModel();
            if (err != 0) return err;

            //获取图像位置
            curVPosX[1] = matchX;
            curVPosY[1] = matchY;

            //获取机械位置
            zmcaux.ZAux_Direct_GetDpos(g_handle, 0, ref pfValue);
            curMPosX[1] = pfValue;
            zmcaux.ZAux_Direct_GetDpos(g_handle, 1, ref pfValue);
            curMPosY[1] = pfValue;

            //运行到取样点2
            zmcaux.ZAux_Direct_Singl_Move(g_handle, 0, -2 * stepSize);
            zmcaux.ZAux_Direct_Singl_Move(g_handle, 1, 0);
            piIfIdle[0] = 0; piIfIdle[1] = 0;
            while (-1 != piIfIdle[0] || -1 != piIfIdle[1])
            {
                zmcaux.ZAux_Direct_GetIfIdle(g_handle, 0, ref piIfIdle[0]);
                zmcaux.ZAux_Direct_GetIfIdle(g_handle, 1, ref piIfIdle[1]);
            }
            //等待平台稳定
            System.Threading.Thread.Sleep(stabTime);

            //相机采集图像
            timeTick = 0;
            //采集前先把标识清除
            IsFrameReady = false;
            while (IsFrameReady == false && ++timeTick < 200)
            {
                System.Threading.Thread.Sleep(10);
            }
            if (IsFrameReady == false)
            {
                this.Text = "图像校验采图超时";
                return -1;
            }

            //执行定位操作
            err = MatchModel();
            if (err != 0) return err;

            //获取图像位置
            curVPosX[2] = matchX;
            curVPosY[2] = matchY;

            //获取机械位置
            zmcaux.ZAux_Direct_GetDpos(g_handle, 0, ref pfValue);
            curMPosX[2] = pfValue;
            zmcaux.ZAux_Direct_GetDpos(g_handle, 1, ref pfValue);
            curMPosY[2] = pfValue;

            //运行到取样点3
            zmcaux.ZAux_Direct_Singl_Move(g_handle, 0, 0);
            zmcaux.ZAux_Direct_Singl_Move(g_handle, 1, -2 * stepSize);
            piIfIdle[0] = 0; piIfIdle[1] = 0;
            while (-1 != piIfIdle[0] || -1 != piIfIdle[1])
            {
                zmcaux.ZAux_Direct_GetIfIdle(g_handle, 0, ref piIfIdle[0]);
                zmcaux.ZAux_Direct_GetIfIdle(g_handle, 1, ref piIfIdle[1]);
            }
            //等待机台稳定
            System.Threading.Thread.Sleep(stabTime);

            //相机采集图像
            timeTick = 0;
            //采集前先把标识清除
            IsFrameReady = false;
            while (IsFrameReady == false && ++timeTick < 200)
            {
                System.Threading.Thread.Sleep(10);
            }
            if (IsFrameReady == false)
            {
                this.Text = "图像校验采图超时";
                return -1;
            }

            //执行定位操作
            err = MatchModel();
            if (err != 0) return err;

            //获取图像位置
            curVPosX[3] = matchX;
            curVPosY[3] = matchY;

            //获取机械位置
            zmcaux.ZAux_Direct_GetDpos(g_handle, 0, ref pfValue);
            curMPosX[3] = pfValue;
            zmcaux.ZAux_Direct_GetDpos(g_handle, 1, ref pfValue);
            curMPosY[3] = pfValue;


            //计算每个像素对应的机械距离
            float calibX = 0, calibY = 0;
            for (int idx = 1; idx < 4; ++idx)
            {
                try
                {
                    calibX += (float)Math.Abs((curMPosX[idx] - curMPosX[0]) / (curVPosX[idx] - curVPosX[0]));
                }
                catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.Message); err = -1; }
            }
            for (int idx = 1; idx < 4; ++idx)
            {
                try
                {
                    calibY += (float)Math.Abs((curMPosY[idx] - curMPosY[0]) / (curVPosY[idx] - curVPosY[0]));
                }
                catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.Message); err = -1; }
            }
            //计数出没像素对应的机械距离
            calibrationX = calibX / 3f;
            calibrationY = calibY / 3f;


            //获取图像大小尺寸
            int imageWidth = 1280, imageHeight = 960;
            XTVision_V1.XTGetImageSize(imageNum1, ref imageWidth, ref imageHeight);

            //修正模板机械位置/让模板机械位置刚好在图像中间
            ModelSet.MPosX += (curVPosX[0] - imageWidth * 0.5f) * calibrationX;
            ModelSet.MPosY += (curVPosY[0] - imageHeight * 0.5f) * calibrationY;

            //中心作为模板位置/让模板图像位置刚好在图像中间
            ModelSet.VPosX = imageWidth * 0.5f;
            ModelSet.VPosY = imageHeight * 0.5f;
            ModelSet.VPosA = 0;

            //让电机移动到图像中心处
            zmcaux.ZAux_Direct_Singl_MoveAbs(g_handle, 0, (float)ModelSet.MPosX);
            zmcaux.ZAux_Direct_Singl_MoveAbs(g_handle, 1, (float)ModelSet.MPosY);
            zmcaux.ZAux_Direct_Singl_MoveAbs(g_handle, 2, (float)ModelSet.MPosZ);
            piIfIdle[0] = 0; piIfIdle[1] = 0; piIfIdle[2] = 0;
            while (-1 != piIfIdle[0] || -1 != piIfIdle[1] || -1 != piIfIdle[2])
            {
                zmcaux.ZAux_Direct_GetIfIdle(g_handle, 0, ref piIfIdle[0]);
                zmcaux.ZAux_Direct_GetIfIdle(g_handle, 1, ref piIfIdle[1]);
                zmcaux.ZAux_Direct_GetIfIdle(g_handle, 2, ref piIfIdle[2]);
            }
            //等待机台稳定
            System.Threading.Thread.Sleep(stabTime);

            //相机采集图像
            timeTick = 0;
            //采集前先把标识清除
            IsFrameReady = false;
            while (IsFrameReady == false && ++timeTick < 200)
            {
                System.Threading.Thread.Sleep(10);
            }
            if (IsFrameReady == false)
            {
                this.Text = "图像校验采图超时";
                return -1;
            }

            //执行定位操作/主要用于刷新一下模板框显示
            err = MatchModel();

            return err;
        }

        //程序执行进行拍照
        int ModelMatchWork()
        {
            int err = 0;
            int timeTick = 0;
            int stabTime = Convert.ToInt32(textBoxStableTime.Text);
            float stepSize = Convert.ToSingle(textBoxStableTime.Text);
            int[] piIfIdle = new int[AxisNum];

            //设置移动速度
            zmcaux.ZAux_Direct_SetSpeed(g_handle, 0, (float)20);
            zmcaux.ZAux_Direct_SetAccel(g_handle, 0, (float)20);
            zmcaux.ZAux_Direct_SetDecel(g_handle, 0, (float)20);
            zmcaux.ZAux_Direct_SetSpeed(g_handle, 1, (float)20);
            zmcaux.ZAux_Direct_SetAccel(g_handle, 1, (float)20);
            zmcaux.ZAux_Direct_SetDecel(g_handle, 1, (float)20);
            zmcaux.ZAux_Direct_SetSpeed(g_handle, 2, (float)20);
            zmcaux.ZAux_Direct_SetAccel(g_handle, 2, (float)20);
            zmcaux.ZAux_Direct_SetDecel(g_handle, 2, (float)20);

            //移动到模板设置位置
            zmcaux.ZAux_Direct_Singl_MoveAbs(g_handle, 0, (float)ModelSet.MPosX);
            zmcaux.ZAux_Direct_Singl_MoveAbs(g_handle, 1, (float)ModelSet.MPosY);
            zmcaux.ZAux_Direct_Singl_MoveAbs(g_handle, 2, (float)ModelSet.MPosZ);

            //等待运行结束
            piIfIdle[0] = 0; piIfIdle[1] = 0; piIfIdle[2] = 0;
            while (-1 != piIfIdle[0] || -1 != piIfIdle[1] || -1 != piIfIdle[2])
            {
                zmcaux.ZAux_Direct_GetIfIdle(g_handle, 0, ref piIfIdle[0]);
                zmcaux.ZAux_Direct_GetIfIdle(g_handle, 1, ref piIfIdle[1]);
                zmcaux.ZAux_Direct_GetIfIdle(g_handle, 2, ref piIfIdle[2]);
            }
            //等待平台稳定
            System.Threading.Thread.Sleep(stabTime);

            //相机采集图像
            timeTick = 0;
            //采集前先把标识清除
            IsFrameReady = false;
            while (IsFrameReady == false && ++timeTick < 200)
            {
                System.Threading.Thread.Sleep(10);
            }
            if (IsFrameReady == false)
            {
                this.Text = "图像校验采图超时";
                err = -1;
                return err;
            }

            //执行定位操作
            err = MatchModel();
            if (err != 0) return err;

            //得到定位结果
            ModelRun.VPosX = matchX;
            ModelRun.VPosY = matchY;
            ModelRun.VPosA = matchA;

            //获取当前位置
            float pfValue = 0;
            zmcaux.ZAux_Direct_GetDpos(g_handle, 0, ref pfValue);
            ModelRun.MPosX = pfValue;

            zmcaux.ZAux_Direct_GetDpos(g_handle, 1, ref pfValue);
            ModelRun.MPosY = pfValue;

            zmcaux.ZAux_Direct_GetDpos(g_handle, 2, ref pfValue);
            ModelRun.MPosZ = pfValue;


            return err;
        }

        //定位并对直线进行修正
        int LineRevise()
        {
            int err = 0;
            int stabTime = Convert.ToInt32(textBoxStableTime.Text);
            float stepSize = Convert.ToSingle(textBoxStableTime.Text);
            int[] piIfIdle = new int[AxisNum];

            //获取目标点位坐标
            float workPosX1 = Convert.ToSingle(textBoxWorkPosX1.Text);
            float workPosY1 = Convert.ToSingle(textBoxWorkPosY1.Text);
            float workPosZ1 = Convert.ToSingle(textBoxWorkPosZ1.Text);

            float workPosX2 = Convert.ToSingle(textBoxWorkPosX2.Text);
            float workPosY2 = Convert.ToSingle(textBoxWorkPosY2.Text);
            float workPosZ2 = Convert.ToSingle(textBoxWorkPosZ2.Text);

            //修正目标点位坐标
            float EposX1 = 0, EposY1 = 0;
            float EposX2 = 0, EposY2 = 0;
            if (checkBoxReviseAngle.Checked == true)
            {
                //矫正位置和角度
                CalcVposToMpos1(ModelSet.MPosX, ModelSet.MPosY, ModelSet.VPosX, ModelSet.VPosY, ModelRun.MPosX, ModelRun.MPosY, ModelRun.VPosX, ModelRun.VPosY, ModelRun.VPosA * (float)Math.PI / 180, calibrationX, calibrationY, 1, 1, workPosX1, workPosY1, ref EposX1, ref EposY1);
                CalcVposToMpos1(ModelSet.MPosX, ModelSet.MPosY, ModelSet.VPosX, ModelSet.VPosY, ModelRun.MPosX, ModelRun.MPosY, ModelRun.VPosX, ModelRun.VPosY, ModelRun.VPosA * (float)Math.PI / 180, calibrationX, calibrationY, 1, 1, workPosX2, workPosY2, ref EposX2, ref EposY2);
            }
            else
            {
                //矫正位置忽略角度
                CalcVposToMpos1(ModelSet.MPosX, ModelSet.MPosY, ModelSet.VPosX, ModelSet.VPosY, ModelRun.MPosX, ModelRun.MPosY, ModelRun.VPosX, ModelRun.VPosY, 0, calibrationX, calibrationY, 1, 1, workPosX1, workPosY1, ref EposX1, ref EposY1);
                CalcVposToMpos1(ModelSet.MPosX, ModelSet.MPosY, ModelSet.VPosX, ModelSet.VPosY, ModelRun.MPosX, ModelRun.MPosY, ModelRun.VPosX, ModelRun.VPosY, 0, calibrationX, calibrationY, 1, 1, workPosX2, workPosY2, ref EposX2, ref EposY2);
            }

            //设置移动速度
            zmcaux.ZAux_Direct_SetSpeed(g_handle, 0, (float)20);
            zmcaux.ZAux_Direct_SetAccel(g_handle, 0, (float)20);
            zmcaux.ZAux_Direct_SetDecel(g_handle, 0, (float)20);
            zmcaux.ZAux_Direct_SetSpeed(g_handle, 1, (float)20);
            zmcaux.ZAux_Direct_SetAccel(g_handle, 1, (float)20);
            zmcaux.ZAux_Direct_SetDecel(g_handle, 1, (float)20);
            zmcaux.ZAux_Direct_SetSpeed(g_handle, 2, (float)20);
            zmcaux.ZAux_Direct_SetAccel(g_handle, 2, (float)20);
            zmcaux.ZAux_Direct_SetDecel(g_handle, 2, (float)20);

            //移动矫正后直线开始点位置
            zmcaux.ZAux_Direct_Base(g_handle, 2, new int[] { 0, 1 });
            zmcaux.ZAux_Direct_MoveAbs(g_handle, 2, new float[] { (float)EposX1, (float)EposY1 });      //XY做插补运动
            //等待运行结束
            piIfIdle[0] = 0; piIfIdle[1] = 0; piIfIdle[2] = 0;
            while (-1 != piIfIdle[0] || -1 != piIfIdle[1])
            {
                zmcaux.ZAux_Direct_GetIfIdle(g_handle, 0, ref piIfIdle[0]);
                zmcaux.ZAux_Direct_GetIfIdle(g_handle, 1, ref piIfIdle[1]);
            }
            //Z轴下降
            zmcaux.ZAux_Direct_Singl_MoveAbs(g_handle, 2, (float)workPosZ1);                            //Z做单轴运动
            //等待运行结束
            piIfIdle[0] = 0; piIfIdle[1] = 0; piIfIdle[2] = 0;
            while (-1 != piIfIdle[2])
            {
                zmcaux.ZAux_Direct_GetIfIdle(g_handle, 2, ref piIfIdle[2]);
            }


            //移动矫正后直线结束点位置
            zmcaux.ZAux_Direct_Base(g_handle, 3, new int[] { 0, 1, 2 });                                //建立插补运动组
            zmcaux.ZAux_Direct_MoveAbs(g_handle, 3, new float[] { (float)EposX2, (float)EposY2, (float)workPosZ2 });      //XYZ做插补运动
            //等待运行结束
            piIfIdle[0] = 0; piIfIdle[1] = 0; piIfIdle[2] = 0;
            while (-1 != piIfIdle[0] || -1 != piIfIdle[1] || -1 != piIfIdle[2])
            {
                zmcaux.ZAux_Direct_GetIfIdle(g_handle, 0, ref piIfIdle[0]);
                zmcaux.ZAux_Direct_GetIfIdle(g_handle, 1, ref piIfIdle[1]);
                zmcaux.ZAux_Direct_GetIfIdle(g_handle, 2, ref piIfIdle[2]);
            }

            return err;
        }

        #region 位置矫正函数

        /// <summary>
        /// 目标坐标矫正(单模板模式)
        /// </summary>
        /// <param name="Msx">模板机械坐标X</param>
        /// <param name="Msy">模板机械坐标Y</param>
        /// <param name="Vsx">模板图像坐标X</param>
        /// <param name="Vsy">模板图像坐标Y</param>
        /// <param name="Mrx">定位机械坐标X</param>
        /// <param name="Mry">定位机械坐标Y</param>
        /// <param name="Vrx">定位图像坐标X</param>
        /// <param name="Vry">定位图像坐标Y</param>
        /// <param name="Vra">定位图像角度(弧度)</param>
        /// <param name="CalibX">相机标定值(mm/px)</param>
        /// <param name="CalibY">相机标定值(mm/px)</param>
        /// <param name="MdirX">机械坐标方向(+1/-1)</param>
        /// <param name="MdirY">机械坐标方向(+1/-1)</param>
        /// <param name="SposX">设定目标坐标X</param>
        /// <param name="SposY">设定目标坐标Y</param>
        /// <param name="EposX">矫正目标坐标X</param>
        /// <param name="EposY">矫正目标坐标Y</param>
        /// <returns>非0表示执行错误</returns>
        int CalcVposToMpos1(float Msx, float Msy, float Vsx, float Vsy,
            float Mrx, float Mry, float Vrx, float Vry, float Vra,
            float CalibX, float CalibY, int MdirX, int MdirY,
            float SposX, float SposY, ref float EposX, ref float EposY)
        {
            //方向只有正负
            MdirX = MdirX == -1 ? -1 : 1;
            MdirY = MdirY == -1 ? -1 : 1;

            //设定的目标位置到设定的模板位置距离
            float x = SposX - Msx;
            float y = SposY - Msy;
            float Dis = (float)Math.Sqrt(x * x + y * y);

            //设定的目标位置和模板原始角度
            double SAngle = Math.Atan2(y, x);

            //计算模板校正后的新模板位置
            float PosX = Mrx + (Vrx - Vsx) * CalibX * MdirX;	//模板位置
            float PosY = Mry + (Vry - Vsy) * CalibY * MdirY;	//模板位置

            //综合角度
            SAngle += Vra;

            //校正后的目标位置
            EposX = PosX + Dis * (float)Math.Cos(SAngle);
            EposY = PosY + Dis * (float)Math.Sin(SAngle);

            return 0;
        }

        #endregion
    }

    #region

    //文件读写
    public class Win32
    {
        /// <summary>
        /// 读取配置文件/系统函数
        /// </summary>
        /// <param name="lpAppName">小节名称</param>
        /// <param name="lpKeyName">条目名称</param>
        /// <param name="lpDefault">条目没有找到时返回的默认值</param>
        /// <param name="lpReturnedString">指定一个字串缓冲区，长度至少为nSize</param>
        /// <param name="nSize">指定装载到lpReturnedString缓冲区的最大字符数量</param>
        /// <param name="lpFileName">操作的文件名称，如没有指定一个完整路径名，Windows就在Windows目录中查找文件</param>
        /// <returns></returns>
        [DllImport("kernel32.dll")]
        public static extern int GetPrivateProfileString(string lpAppName, string lpKeyName, string lpDefault, StringBuilder lpReturnedString, int nSize, string lpFileName);

        /// <summary>
        /// 写入配置文件/系统函数
        /// </summary>
        /// <param name="lpAppName">小节名称</param>
        /// <param name="lpKeyName">条目名称</param>
        /// <param name="lpString">条目字符串值</param>
        /// <param name="lpFileName">操作的文件名称，如没有指定一个完整路径名，Windows就在Windows目录中写入文件</param>
        /// <returns></returns>
        [DllImport("kernel32.dll")]
        public static extern int WritePrivateProfileString(string lpAppName, string lpKeyName, string lpString, string lpFileName);
    }

    //轴基本配置
    public class AxisConfig
    {
        //轴基本参数
        public int PPR = 10000;         //电机细分
        public double Pitch = 10;       //丝杆螺距
        public int PulseMode = 0;       //电机脉冲模式(支持八种模式)
        public int AxisType = 0;        //0虚拟轴[对应值1]/1带编码反馈轴[对应值4]
        public int Merge = 1;           //0关闭连续插补/1开启连续插补
        public int LowLimit = 0;        //软下限位
        public int HighLimit = 300;     //软上限位

        //轴复位方式
        public int HomeMode = 4;        //复位模式(3:正向复位，4:反向复位)
        public int SensorMode = 0;      //原点电平(0:低电平，1:高电平)
        public bool UseAxis = true;     //是否启用轴
    }

    //轴基本速度
    public class BSpeed
    {
        public double Speed = 50;       //工作速度
        public double ATime = 100;      //加速时间
        public double DTime = 100;      //减速时间
    }

    #endregion
}
