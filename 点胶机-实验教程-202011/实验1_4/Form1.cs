﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
///视觉库命名空间
using XTVisioncs;

namespace 实验1_4
{
    /// <summary>
    /// 实验1_4：图像预处理操作
    /// 简述：熟悉图像预处理的使用，对于一些噪声比较多的图像，在进行模板定位前，可以先进行预处理操作，能够一定程度上提高定位的准确性
    /// </summary>
    public partial class Form1 : Form
    {
        //每个窗口绑定到一个编号，每个图像绑定到一个图像号

        //给每一个窗口分配一个0--255之间的编号
        int windowNum1 = 0;
        int windowNum2 = 2;
        int windowNum3 = 5;

        //给每一个图像分配一个0--255之间的编号
        //图像号
        int imageNum1 = 0;
        int imageNum2 = 2;
        int imageNum3 = 3;

        public Form1()
        {
            InitializeComponent();
            InitializeForm();
        }

        //窗体辅助初始化
        void InitializeForm()
        {
            //初始化视觉库
            XTVision_V1.XTInitialize(true);

            //视觉窗口背景色
            pictureBox1.BackColor = Color.LightGray;
            pictureBox2.BackColor = Color.LightGray;
            pictureBox3.BackColor = Color.LightGray;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                //从文件中加载图像
                int dispType = 0;   //图像显示方式
                string filePath = @"..\\..\\..\\image\lesson1_4.jpg";
                Size imgSize = Image.FromFile(filePath).Size;
                int imageWidth = imgSize.Width;
                int imageHeight = imgSize.Height;

                //关闭视觉窗口windowNum1
                XTVision_V1.XTCloseWindow(windowNum1);
                //初始化视觉窗口/参数为窗口号，将要绑定到的控件句柄，图像宽，图像高
                XTVision_V1.XTInitWindow(windowNum1, pictureBox1.Handle, imageWidth, imageHeight);
                //从文件中加载图像到图像imageNum1中
                XTVision_V1.XTLoadImage(imageNum1, filePath);
                //将图像imageNum1显示到窗口windowNum1中
                XTVision_V1.XTDisplayPicture(windowNum1, imageNum1, dispType);
            }
            catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.Message); }

            try
            {
                //从文件中加载图像
                int dispType = 0;
                string filePath = @"..\\..\\..\\image\circle_plate_02.png";
                Size imgSize = Image.FromFile(filePath).Size;
                int imageWidth = imgSize.Width;
                int imageHeight = imgSize.Height;

                //关闭视觉窗口windowNum2
                XTVision_V1.XTCloseWindow(windowNum2);
                //初始化视觉窗口/参数为窗口号，将要绑定到的控件句柄，图像宽，图像高
                XTVision_V1.XTInitWindow(windowNum2, pictureBox2.Handle, imageWidth, imageHeight);
                //从文件中加载图像到图像imageNum2中
                XTVision_V1.XTLoadImage(imageNum2, filePath);
                //将图像imageNum2显示到窗口windowNum2中
                XTVision_V1.XTDisplayPicture(windowNum2, imageNum2, dispType);
            }
            catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.Message); }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            //反初始化视觉库/释放资源
            XTVision_V1.XTUninitialize();
        }

        private void buttonLoadImage1_Click(object sender, EventArgs e)
        {
            string resultFile = null;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image Files(*.BMP;*.PNG;*.JPG;*.GIF)|*.BMP;*.PNG;*.JPG;*.GIF|All files(*.*)|*.* ";
            openFileDialog.FilterIndex = 0;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                resultFile = openFileDialog.FileName;
                if (string.IsNullOrEmpty(resultFile)) return;
                try
                {
                    int dispType = 0;   //图像显示方式
                    Size imgSize = Image.FromFile(resultFile).Size;
                    int imageWidth = imgSize.Width;
                    int imageHeight = imgSize.Height;

                    //关闭视觉窗口windowNum1
                    XTVision_V1.XTCloseWindow(windowNum1);
                    //初始化视觉窗口/参数为窗口号，将要绑定到的控件句柄，图像宽，图像高
                    XTVision_V1.XTInitWindow(windowNum1, pictureBox1.Handle, imageWidth, imageHeight);
                    //从文件中加载图像到图像imageNum1中
                    XTVision_V1.XTLoadImage(imageNum1, resultFile);
                    //将图像imageNum1显示到窗口windowNum1中
                    XTVision_V1.XTDisplayPicture(windowNum1, imageNum1, dispType);
                }
                catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.Message); }
            }
        }

        private void buttonLoadImage2_Click(object sender, EventArgs e)
        {
            string resultFile = null;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image Files(*.BMP;*.PNG;*.JPG;*.GIF)|*.BMP;*.PNG;*.JPG;*.GIF|All files(*.*)|*.* ";
            openFileDialog.FilterIndex = 0;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                resultFile = openFileDialog.FileName;
                if (string.IsNullOrEmpty(resultFile)) return;
                try
                {
                    int dispType = 0;   //图像显示方式
                    Size imgSize = Image.FromFile(resultFile).Size;
                    int imageWidth = imgSize.Width;
                    int imageHeight = imgSize.Height;

                    //关闭视觉窗口windowNum2
                    XTVision_V1.XTCloseWindow(windowNum2);
                    //初始化视觉窗口/参数为窗口号，将要绑定到的控件句柄，图像宽，图像高
                    XTVision_V1.XTInitWindow(windowNum2, pictureBox2.Handle, imageWidth, imageHeight);
                    //从文件中加载图像到图像imageNum2中
                    XTVision_V1.XTLoadImage(imageNum2, resultFile);
                    //将图像imageNum2显示到窗口windowNum2中
                    XTVision_V1.XTDisplayPicture(windowNum2, imageNum2, dispType);
                }
                catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.Message); }
            }
        }

        private void buttonEmphasizeImage_Click(object sender, EventArgs e)
        {
            int dispType = 0;   //显示方式
            int imageWidth = 0, imageHeight = 0;
            //获取图像尺寸
            XTVision_V1.XTGetImageSize(imageNum1, ref imageWidth, ref imageHeight);
            //增强图像imageNum1并将增强结果存放到imageNum3
            XTVision_V1.XTEmphasizeImage(imageNum1, imageNum3, 7, 7, 3);

            //关闭视觉窗口windowNum3
            XTVision_V1.XTCloseWindow(windowNum3);
            //初始化视觉窗口/参数为窗口号，将要绑定到的控件句柄，图像宽，图像高
            XTVision_V1.XTInitWindow(windowNum3, pictureBox3.Handle, imageWidth, imageHeight);
            //将图像imageNum3显示到窗口windowNum3中
            XTVision_V1.XTDisplayPicture(windowNum3, imageNum3, dispType);
        }

        private void buttonSmoothImage_Click(object sender, EventArgs e)
        {
            int dispType = 0;   //显示方式
            int imageWidth = 0, imageHeight = 0;
            //获取图像尺寸
            XTVision_V1.XTGetImageSize(imageNum1, ref imageWidth, ref imageHeight);

            //平滑图像imageNum1并将平滑结果存放到imageNum3
            XTVision_V1.XTSmoothImage(imageNum1, imageNum3, 0, 5);

            //关闭视觉窗口windowNum3
            XTVision_V1.XTCloseWindow(windowNum3);
            //初始化视觉窗口/参数为窗口号，将要绑定到的控件句柄，图像宽，图像高
            XTVision_V1.XTInitWindow(windowNum3, pictureBox3.Handle, imageWidth, imageHeight);
            //将图像imageNum3显示到窗口windowNum3中
            XTVision_V1.XTDisplayPicture(windowNum3, imageNum3, dispType);
        }

        private void buttonMedianImage_Click(object sender, EventArgs e)
        {
            int dispType = 0;   //显示方式
            int imageWidth = 0, imageHeight = 0;
            //获取图像尺寸
            XTVision_V1.XTGetImageSize(imageNum1, ref imageWidth, ref imageHeight);

            //中值滤波图像imageNum1并将平滑结果存放到imageNum3
            XTVision_V1.XTMedianImage(imageNum1, imageNum3, 1, 3);

            //关闭视觉窗口windowNum3
            XTVision_V1.XTCloseWindow(windowNum3);
            //初始化视觉窗口/参数为窗口号，将要绑定到的控件句柄，图像宽，图像高
            XTVision_V1.XTInitWindow(windowNum3, pictureBox3.Handle, imageWidth, imageHeight);
            //将图像imageNum3显示到窗口windowNum3中
            XTVision_V1.XTDisplayPicture(windowNum3, imageNum3, dispType);
        }

        private void buttonMeanImage_Click(object sender, EventArgs e)
        {
            int dispType = 0;   //显示方式
            int imageWidth = 0, imageHeight = 0;
            //获取图像尺寸
            XTVision_V1.XTGetImageSize(imageNum1, ref imageWidth, ref imageHeight);

            //均值滤波图像imageNum1并将平滑结果存放到imageNum3
            XTVision_V1.XTMeanImage(imageNum1, imageNum3, 7, 7);

            //关闭视觉窗口windowNum3
            XTVision_V1.XTCloseWindow(windowNum3);
            //初始化视觉窗口/参数为窗口号，将要绑定到的控件句柄，图像宽，图像高
            XTVision_V1.XTInitWindow(windowNum3, pictureBox3.Handle, imageWidth, imageHeight);
            //将图像imageNum3显示到窗口windowNum3中
            XTVision_V1.XTDisplayPicture(windowNum3, imageNum3, dispType);
        }

        private void buttonGaussImage_Click(object sender, EventArgs e)
        {
            int dispType = 0;   //显示方式
            int imageWidth = 0, imageHeight = 0;
            //获取图像尺寸
            XTVision_V1.XTGetImageSize(imageNum1, ref imageWidth, ref imageHeight);

            //高斯滤波图像imageNum1并将平滑结果存放到imageNum3
            XTVision_V1.XTGaussImage(imageNum1, imageNum3, 7);

            //关闭视觉窗口windowNum3
            XTVision_V1.XTCloseWindow(windowNum3);
            //初始化视觉窗口/参数为窗口号，将要绑定到的控件句柄，图像宽，图像高
            XTVision_V1.XTInitWindow(windowNum3, pictureBox3.Handle, imageWidth, imageHeight);
            //将图像imageNum3显示到窗口windowNum3中
            XTVision_V1.XTDisplayPicture(windowNum3, imageNum3, dispType);
        }
    }
}
