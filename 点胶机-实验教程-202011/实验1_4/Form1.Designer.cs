﻿namespace 实验1_4
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonGaussImage = new System.Windows.Forms.Button();
            this.buttonMeanImage = new System.Windows.Forms.Button();
            this.buttonMedianImage = new System.Windows.Forms.Button();
            this.buttonSmoothImage = new System.Windows.Forms.Button();
            this.buttonEmphasizeImage = new System.Windows.Forms.Button();
            this.buttonLoadImage2 = new System.Windows.Forms.Button();
            this.buttonLoadImage1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonGaussImage
            // 
            this.buttonGaussImage.Location = new System.Drawing.Point(850, 148);
            this.buttonGaussImage.Name = "buttonGaussImage";
            this.buttonGaussImage.Size = new System.Drawing.Size(100, 28);
            this.buttonGaussImage.TabIndex = 26;
            this.buttonGaussImage.Text = "高斯滤波";
            this.buttonGaussImage.UseVisualStyleBackColor = true;
            this.buttonGaussImage.Click += new System.EventHandler(this.buttonGaussImage_Click);
            // 
            // buttonMeanImage
            // 
            this.buttonMeanImage.Location = new System.Drawing.Point(850, 114);
            this.buttonMeanImage.Name = "buttonMeanImage";
            this.buttonMeanImage.Size = new System.Drawing.Size(100, 28);
            this.buttonMeanImage.TabIndex = 25;
            this.buttonMeanImage.Text = "均值滤波";
            this.buttonMeanImage.UseVisualStyleBackColor = true;
            this.buttonMeanImage.Click += new System.EventHandler(this.buttonMeanImage_Click);
            // 
            // buttonMedianImage
            // 
            this.buttonMedianImage.Location = new System.Drawing.Point(850, 80);
            this.buttonMedianImage.Name = "buttonMedianImage";
            this.buttonMedianImage.Size = new System.Drawing.Size(100, 28);
            this.buttonMedianImage.TabIndex = 27;
            this.buttonMedianImage.Text = "中值滤波";
            this.buttonMedianImage.UseVisualStyleBackColor = true;
            this.buttonMedianImage.Click += new System.EventHandler(this.buttonMedianImage_Click);
            // 
            // buttonSmoothImage
            // 
            this.buttonSmoothImage.Location = new System.Drawing.Point(850, 46);
            this.buttonSmoothImage.Name = "buttonSmoothImage";
            this.buttonSmoothImage.Size = new System.Drawing.Size(100, 28);
            this.buttonSmoothImage.TabIndex = 30;
            this.buttonSmoothImage.Text = "图像平滑";
            this.buttonSmoothImage.UseVisualStyleBackColor = true;
            this.buttonSmoothImage.Click += new System.EventHandler(this.buttonSmoothImage_Click);
            // 
            // buttonEmphasizeImage
            // 
            this.buttonEmphasizeImage.Location = new System.Drawing.Point(850, 12);
            this.buttonEmphasizeImage.Name = "buttonEmphasizeImage";
            this.buttonEmphasizeImage.Size = new System.Drawing.Size(100, 28);
            this.buttonEmphasizeImage.TabIndex = 29;
            this.buttonEmphasizeImage.Text = "图像增强";
            this.buttonEmphasizeImage.UseVisualStyleBackColor = true;
            this.buttonEmphasizeImage.Click += new System.EventHandler(this.buttonEmphasizeImage_Click);
            // 
            // buttonLoadImage2
            // 
            this.buttonLoadImage2.Location = new System.Drawing.Point(718, 268);
            this.buttonLoadImage2.Name = "buttonLoadImage2";
            this.buttonLoadImage2.Size = new System.Drawing.Size(100, 28);
            this.buttonLoadImage2.TabIndex = 28;
            this.buttonLoadImage2.Text = "载入图像二";
            this.buttonLoadImage2.UseVisualStyleBackColor = true;
            this.buttonLoadImage2.Click += new System.EventHandler(this.buttonLoadImage2_Click);
            // 
            // buttonLoadImage1
            // 
            this.buttonLoadImage1.Location = new System.Drawing.Point(312, 268);
            this.buttonLoadImage1.Name = "buttonLoadImage1";
            this.buttonLoadImage1.Size = new System.Drawing.Size(100, 28);
            this.buttonLoadImage1.TabIndex = 24;
            this.buttonLoadImage1.Text = "载入图像一";
            this.buttonLoadImage1.UseVisualStyleBackColor = true;
            this.buttonLoadImage1.Click += new System.EventHandler(this.buttonLoadImage1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(479, 269);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 22;
            this.label2.Text = "图像二";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(70, 558);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 21;
            this.label3.Text = "图像三";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(70, 269);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 23;
            this.label1.Text = "图像一";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(418, 12);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(400, 250);
            this.pictureBox2.TabIndex = 19;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Location = new System.Drawing.Point(12, 302);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(400, 250);
            this.pictureBox3.TabIndex = 18;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(400, 250);
            this.pictureBox1.TabIndex = 20;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 582);
            this.Controls.Add(this.buttonGaussImage);
            this.Controls.Add(this.buttonMeanImage);
            this.Controls.Add(this.buttonMedianImage);
            this.Controls.Add(this.buttonSmoothImage);
            this.Controls.Add(this.buttonEmphasizeImage);
            this.Controls.Add(this.buttonLoadImage2);
            this.Controls.Add(this.buttonLoadImage1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonGaussImage;
        private System.Windows.Forms.Button buttonMeanImage;
        private System.Windows.Forms.Button buttonMedianImage;
        private System.Windows.Forms.Button buttonSmoothImage;
        private System.Windows.Forms.Button buttonEmphasizeImage;
        private System.Windows.Forms.Button buttonLoadImage2;
        private System.Windows.Forms.Button buttonLoadImage1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

