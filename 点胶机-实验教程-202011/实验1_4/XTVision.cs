﻿/// <summary>
/// XTVision视觉库.NET接口文件
/// </summary>
using System;
using System.Runtime.InteropServices;

/// <summary>
/// XTVision视觉库.NET命名空间
/// </summary>
namespace XTVisioncs
{
    /// <summary>
    /// XTVision视觉库.NET接口
    /// </summary>
    public class XTVision_V1
    {
        //////////////////////////////////////资源编号分配/////////////////////////////////
        #region 资源数量

        /// <summary>
        /// 最大相机编号
        /// </summary>
        public const int MaxCameraNum = 8;

        /// <summary>
        /// 最大图像编号
        /// </summary>
        public const int MaxImageNum = 255;

        /// <summary>
        /// 最大窗口编号
        /// </summary>
        public const int MaxWindowNum = 255;

        /// <summary>
        /// 最大区域编号
        /// </summary>
        public const int MaxRoiNum = 255;

        /// <summary>
        /// 最大模板编号
        /// </summary>
        public const int MaxTemplateNum = 255;

        /// <summary>
        /// 最大定位结果编号
        /// </summary>
        public const int MaxMatchResultNum = 1000;

        /// <summary>
        /// 最大检测结果编号
        /// </summary>
        public const int MaxDetectResultNum = 1000;

        /// <summary>
        /// 最大一维测量结果编号
        /// </summary>
        public const int MaxMeasureNum = 255;

        #endregion

        //////////////////////////////////////图像基础部分/////////////////////////////////
        #region XTBase.h

        /// <summary>
        /// 初始化视觉库
        /// </summary>
        /// <param name="wLog">函数执行错误时输出日志</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTInitialize(bool wLog);

        /// <summary>
        /// 反初始化视觉库
        /// </summary>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTUninitialize();

        /// <summary>
        /// 查看视觉库是否已经初始化
        /// </summary>
        /// <returns>非0表示已经初始化</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTIsInitialized();

        /// <summary>
        /// 设置窗口背景色/在调用XTInitWindow前调用
        /// </summary>
        /// <param name="BackgroundColor">窗口背景色['black', 'red', 'green', 'blue']</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTSetWindowbg(string BackgroundColor);

        /// <summary>
        /// 初始化图像窗口
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="Handle">图像窗口句柄</param>
        /// <param name="ImageWidth">图像宽度</param>
        /// <param name="ImageHeight">图像高度</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTInitWindow(int WindowNum, IntPtr Handle, int ImageWidth, int ImageHeight);

        /// <summary>
        /// 初始化图像窗口
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="Handle">图像窗口句柄</param>
        /// <param name="WindowWidth">窗口宽度</param>
        /// <param name="WindowHeight">窗口高度</param>
        /// <param name="ImageWidth">图像宽度</param>
        /// <param name="ImageHeight">图像高度</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTInitWindow1(int WindowNum, IntPtr Handle, int WindowWidth, int WindowHeight, int ImageWidth, int ImageHeight);

        /// <summary>
        /// 获取图像窗口句柄
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="Handle">图像窗口句柄</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTOSWindowHandle(int WindowNum, ref IntPtr Handle);

        /// <summary>
        /// 释放图像窗口资源
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTUninitWindow(int WindowNum);

        /// <summary>
        /// 清空图像窗口显示
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTClearWindow(int WindowNum);

        /// <summary>
        /// 关闭图像窗口
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTCloseWindow(int WindowNum);

        /// <summary>
        /// 移动图像窗口
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="Row">窗口行开始位置</param>
        /// <param name="Column">窗口列开始位置</param>
        /// <param name="Width">窗口宽度</param>
        /// <param name="Height">窗口高度</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTMoveWindow(int WindowNum, int Row, int Column, int Width, int Height);

        /// <summary>
        /// 设置图像窗口显示区域
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="Row1">行开始坐标</param>
        /// <param name="Col1">列开始坐标</param>
        /// <param name="Row2">行结束坐标</param>
        /// <param name="Col2">列结束坐标</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTSetDisplayPart(int WindowNum, int Row1, int Col1, int Row2, int Col2);

        /// <summary>
        /// 获取当前图像显示区域
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="Row1">行开始坐标</param>
        /// <param name="Col1">列开始坐标</param>
        /// <param name="Row2">行结束坐标</param>
        /// <param name="Col2">列结束坐标</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTGetDisplayPart(int WindowNum, ref int Row1, ref int Col1, ref int Row2, ref int Col2);

        /// <summary>
        /// 将图像数据拷贝到视觉库中
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="ColorType">颜色类型[0灰度/1彩色]</param>
        /// <param name="Channel">图像通道数[1单通道/3三通道]</param>
        /// <param name="ImageWidth">图像宽度</param>
        /// <param name="ImageHeight">图像高度</param>
        /// <param name="SrcFrame">图像数据</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTGrabFrame(int ImageNum, int ColorType, int Channel, int ImageWidth, int ImageHeight, IntPtr SrcFrame);

        /// <summary>
        /// 将图像数据拷贝到视觉库中
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="ColorType">颜色类型[0灰度/1彩色]</param>
        /// <param name="Channel">图像通道数[1单通道/3三通道]</param>
        /// <param name="ImageWidth">图像宽度</param>
        /// <param name="ImageHeight">图像高度</param>
        /// <param name="SrcFrame">图像数据</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll", EntryPoint = "XTGrabFrame")]
        public static extern int XTGrabFrame(int ImageNum, int ColorType, int Channel, int ImageWidth, int ImageHeight, byte[] SrcFrame);

        /// <summary>
        /// 将指定编号图像显示到图像窗口
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="DispType">显示方式[0直接显示/1显示到缓存/2显示缓存图像]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTDisplayPicture(int WindowNum, int ImageNum, int DispType);

        /// <summary>
        /// 图像拷贝操作
        /// </summary>
        /// <param name="SrcImageNum">源图像编号</param>
        /// <param name="DesImageNum">目标图像编号</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTCopyImage(int SrcImageNum, int DesImageNum);

        /// <summary>
        /// 图像裁剪操作
        /// </summary>
        /// <param name="SrcImageNum">源图像编号</param>
        /// <param name="DesImageNum">目标图像编号</param>
        /// <param name="Row1">行开始坐标</param>
        /// <param name="Col1">列开始坐标</param>
        /// <param name="Row2">行结束坐标</param>
        /// <param name="Col2">列结束坐标</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTCropImage(int SrcImageNum, int DesImageNum, int Row1, int Col1, int Row2, int Col2);

        /// <summary>
        /// 将指定编号窗口一部分显示内容拷贝到另一编号图像中
        /// </summary>
        /// <param name="SrcWindowNum">源窗口编号</param>
        /// <param name="DesImageNum">目标图像编号</param>
        /// <param name="Row1">行开始坐标</param>
        /// <param name="Col1">列开始坐标</param>
        /// <param name="Row2">行结束坐标</param>
        /// <param name="Col2">列结束坐标</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTCopyWindow(int SrcWindowNum, int DesImageNum, int Row1, int Col1, int Row2, int Col2);

        /// <summary>
        /// 保存窗口显示内容到文件
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="FileName">文件名称</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTSaveWindow(int WindowNum, string FileName);

        /// <summary>
        /// 转换图像类型
        /// </summary>
        /// <param name="SrcImageNum">源图像编号</param>
        /// <param name="DesImageNum">目标图像编号</param>
        /// <param name="NewType">目标图像类型['byte', 'complex', 'cyclic', 'direction', 'int1', 'int2', 'int4', 'int8', 'real', 'uint2']</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTConvertImageType(int SrcImageNum, int DesImageNum, string NewType);

        /// <summary>
        /// 图像加法
        /// </summary>
        /// <param name="ImageNum1">图像编号</param>
        /// <param name="ImageNum2">图像编号</param>
        /// <param name="ImageResultNum">图像编号</param>
        /// <param name="Mult">灰度值适应因子</param>
        /// <param name="Add">灰度值适应范围</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTAddImage(int ImageNum1, int ImageNum2, int ImageResultNum, float Mult, float Add);

        /// <summary>
        /// 图像减法
        /// </summary>
        /// <param name="ImageNum1">图像编号</param>
        /// <param name="ImageNum2">图像编号</param>
        /// <param name="ImageResultNum">图像编号</param>
        /// <param name="Mult">灰度值适应因子</param>
        /// <param name="Add">灰度值适应范围</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTSubImage(int ImageNum1, int ImageNum2, int ImageResultNum, float Mult, float Add);

        /// <summary>
        /// 图像乘法
        /// </summary>
        /// <param name="ImageNum1">图像编号</param>
        /// <param name="ImageNum2">图像编号</param>
        /// <param name="ImageResultNum">图像编号</param>
        /// <param name="Mult">灰度值适应因子</param>
        /// <param name="Add">灰度值适应范围</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTMultImage(int ImageNum1, int ImageNum2, int ImageResultNum, float Mult, float Add);

        /// <summary>
        /// 图像除法
        /// </summary>
        /// <param name="ImageNum1">图像编号</param>
        /// <param name="ImageNum2">图像编号</param>
        /// <param name="ImageResultNum">图像编号</param>
        /// <param name="Mult">灰度值适应因子</param>
        /// <param name="Add">灰度值适应范围</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTDivImage(int ImageNum1, int ImageNum2, int ImageResultNum, float Mult, float Add);

        /// <summary>
        /// 从文件加载一幅图像
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="FileName">文件名称</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTLoadImage(int ImageNum, string FileName);

        /// <summary>
        /// 保存一幅图像到文件
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="FileName">文件名称（包括路径）</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTSaveImage(int ImageNum, string FileName);

        /// <summary>
        /// 镜像图像
        /// </summary>
        /// <param name="SrcImageNum">源图像编号</param>
        /// <param name="DesImageNum">目标图像编号</param>
        /// <param name="MirrorType">镜像方式[1左右/2上下]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTMirrorImage(int SrcImageNum, int DesImageNum, int MirrorType);

        /// <summary>
        /// 缩放图像
        /// </summary>
        /// <param name="SrcImageNum">源图像编号</param>
        /// <param name="DesImageNum">目标图像编号</param>
        /// <param name="Width">缩放尺寸/缩放因子</param>
        /// <param name="Height">缩放尺寸/缩放因子</param>
        /// <param name="ZoomType">缩放类型[0因子/1尺寸]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTZoomImage(int SrcImageNum, int DesImageNum, float Width, float Height, int ZoomType);

        /// <summary>
        /// 图像增强
        /// </summary>
        /// <param name="SrcImageNum">源图像编号</param>
        /// <param name="DesImageNum">目标图像编号</param>
        /// <param name="MaskWidth">模板宽度(7)</param>
        /// <param name="MaskHeight">模板高度(7)</param>
        /// <param name="Factor">增强因子(1)</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTEmphasizeImage(int SrcImageNum, int DesImageNum, int MaskWidth, int MaskHeight, int Factor);

        /// <summary>
        /// 图像平滑
        /// </summary>
        /// <param name="SrcImageNum">源图像编号</param>
        /// <param name="DesImageNum">目标图像编号</param>
        /// <param name="Filter">过滤器[0 gauss/1 deriche1/2 deriche2/3 shen]（Filter=0时Alpha越大图像越平滑，否则Alpha越小图像越平滑）</param>
        /// <param name="Alpha">平滑因子(0.5)</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTSmoothImage(int SrcImageNum, int DesImageNum, int Filter, float Alpha);

        /// <summary>
        /// 中值滤波
        /// </summary>
        /// <param name="SrcImageNum">源图像编号</param>
        /// <param name="DesImageNum">目标图像编号</param>
        /// <param name="MaskType">MaskType 模板类型[0 circle/1 square]</param>
        /// <param name="Radius">Radius 模板大小(1)</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTMedianImage(int SrcImageNum, int DesImageNum, int MaskType, float Radius);

        /// <summary>
        /// 均值滤波
        /// </summary>
        /// <param name="SrcImageNum">源图像编号</param>
        /// <param name="DesImageNum">目标图像编号</param>
        /// <param name="MaskWidth">模板宽度(7)</param>
        /// <param name="MaskHeight">模板高度(7)</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTMeanImage(int SrcImageNum, int DesImageNum, int MaskWidth, int MaskHeight);

        /// <summary>
        /// 高斯滤波
        /// </summary>
        /// <param name="SrcImageNum">源图像编号</param>
        /// <param name="DesImageNum">目标图像编号</param>
        /// <param name="Size">高斯滤波因子[3,5,7,9]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTGaussImage(int SrcImageNum, int DesImageNum, int Size);

        /// <summary>
        /// 平铺组合图像
        /// </summary>
        /// <param name="ImageNum1">源图像编号</param>
        /// <param name="ImageNum2">源图像编号</param>
        /// <param name="ImageNumO">目标图像编号</param>
        /// <param name="TileType">平铺方式[1左右/2上下]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTTileImage(int ImageNum1, int ImageNum2, int ImageNumO, int TileType);

        /// <summary>
        /// 获取图像尺寸
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="Width">图像宽度</param>
        /// <param name="Height">图像高度</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTGetImageSize(int ImageNum, ref int Width, ref int Height);

        /// <summary>
        /// 获取窗口尺寸
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="Width">窗口宽度</param>
        /// <param name="Height">窗口高度</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTGetWindowSize(int WindowNum, ref int Width, ref int Height);

        /// <summary>
        /// 获取窗口图像尺寸
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="Width">窗口宽度</param>
        /// <param name="Height">窗口高度</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTGetWindowImageSize(int WindowNum, ref int Width, ref int Height);

        /// <summary>
        /// 在窗口上显示大十字架
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="LineWidth">线宽(1)</param>
        /// <param name="DispColor">显示颜色['red', 'green', 'blue']</param>
        /// <param name="DispType">显示方式[0直接显示/1显示到缓存]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTDispBigCross(int WindowNum, int LineWidth, string DispColor, int DispType);

        /// <summary>
        /// 鼠标交互模式画点
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="Row">点行坐标</param>
        /// <param name="Column">点列坐标</param>
        /// <param name="DrawColor">画点颜色['red', 'green', 'blue']</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTDrawPoint(int WindowNum, ref float Row, ref float Column, string DrawColor);

        /// <summary>
        /// 鼠标交互模式画线
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="Row1">线开始行坐标</param>
        /// <param name="Column1">线开始列坐标</param>
        /// <param name="Row2">线结束行坐标</param>
        /// <param name="Column2">线结束列坐标</param>
        /// <param name="DrawColor">画线颜色['red', 'green', 'blue']</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTDrawLine(int WindowNum, ref float Row1, ref float Column1, ref float Row2, ref float Column2, string DrawColor);

        /// <summary>
        /// 显示十字架形式的点
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="Row">点坐标</param>
        /// <param name="Col">点坐标</param>
        /// <param name="Size">点大小</param>
        /// <param name="Angle">十字架角度[0,PI]</param>
        /// <param name="LineWidth">十字线宽(1)</param>
        /// <param name="DispColor">显示颜色['red', 'green', 'blue']</param>
        /// <param name="DispType">显示方式[0直接显示/1显示到缓存]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTDispPoint(int WindowNum, float Row, float Col, int Size, float Angle, int LineWidth, string DispColor, int DispType);

        /// <summary>
        /// 显示直线
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="Row1">开始行坐标</param>
        /// <param name="Col1">开始列坐标</param>
        /// <param name="Row2">结束行坐标</param>
        /// <param name="Col2">结束列坐标</param>
        /// <param name="LineWidth">线宽(1)</param>
        /// <param name="DispColor">显示颜色['red', 'green', 'blue']</param>
        /// <param name="DispType">显示方式[0直接显示/1显示到缓存]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTDispLine(int WindowNum, float Row1, float Col1, float Row2, float Col2, int LineWidth, string DispColor, int DispType);

        /// <summary>
        /// 显示圆形
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="Row">圆心行坐标</param>
        /// <param name="Col">圆心列坐标</param>
        /// <param name="Radius">半径</param>
        /// <param name="CenterSize">圆心大小[0,100](0)</param>
        /// <param name="LineWidth">线宽(1)</param>
        /// <param name="DispColor">显示颜色['red', 'green', 'blue']</param>
        /// <param name="DispType">显示方式[0直接显示/1显示到缓存]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTDispCircle(int WindowNum, float Row, float Col, float Radius, int CenterSize, int LineWidth, string DispColor, int DispType);

        /// <summary>
        /// 显示圆弧
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="Row">圆心行坐标</param>
        /// <param name="Col">圆心列坐标</param>
        /// <param name="Angle">圆弧角度[0,2PI]</param>
        /// <param name="RowB">圆弧开始行坐标</param>
        /// <param name="ColB">圆弧开始列坐标</param>
        /// <param name="CenterSize">圆心大小[0,100](0)</param>
        /// <param name="LineWidth">线宽(1)</param>
        /// <param name="DispColor">显示颜色['red', 'green', 'blue']</param>
        /// <param name="DispType">显示方式[0直接显示/1显示到缓存]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTDispCircleArc(int WindowNum, float Row, float Col, float Angle, float RowB, float ColB, int CenterSize, int LineWidth, string DispColor, int DispType);

        /// <summary>
        /// 显示矩形
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="Row">矩形中心行坐标</param>
        /// <param name="Col">矩形中心列坐标</param>
        /// <param name="Angle">矩形角度[0,2PI]</param>
        /// <param name="Length1">半长轴</param>
        /// <param name="Length2">半短轴</param>
        /// <param name="CenterSize">中心大小[0,100](0)</param>
        /// <param name="LineWidth">线宽(1)</param>
        /// <param name="DispColor">显示颜色['red', 'green', 'blue']</param>
        /// <param name="DispType">显示方式[0直接显示/1显示到缓存]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTDispRectangle(int WindowNum, float Row, float Col, float Angle, float Length1, float Length2, int CenterSize, int LineWidth, string DispColor, int DispType);

        /// <summary>
        /// 显示贝塞尔曲线
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="Rows">曲线行坐标数组</param>
        /// <param name="Cols">曲线列坐标数组</param>
        /// <param name="Number">坐标点数量</param>
        /// <param name="LineColor">线条颜色['red', 'green', 'blue']</param>
        /// <param name="PointColor">点位颜色(null不显示坐标点)</param>
        /// <param name="DispType">显示方式[0直接显示/1显示到缓存]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTDispBezierLine(int WindowNum, float[] Rows, float[] Cols, int Number, string LineColor, string PointColor, int DispType);

        /// <summary>
        /// 以窗口坐标为参考，显示文本文字到窗口
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="Row">文本显示行坐标</param>
        /// <param name="Col">文本显示列坐标</param>
        /// <param name="DispText">文本内容</param>
        /// <param name="FontColor">文本颜色['red', 'green', 'blue']</param>
        /// <param name="FontSize">文字大小(12)</param>
        /// <param name="Bold">字体[0常规/1粗体]</param>
        /// <param name="DispType">显示方式[0直接显示/1显示到缓存]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTDispText(int WindowNum, float Row, float Col, string DispText, string FontColor, int FontSize, int Bold, int DispType);

        /// <summary>
        /// 以图像坐标为参考，显示文本文字到窗口
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="Row">文本显示行坐标</param>
        /// <param name="Col">文本显示列坐标</param>
        /// <param name="DispText">文本内容</param>
        /// <param name="FontColor">文本颜色['red', 'green', 'blue']</param>
        /// <param name="FontSize">文字大小(12)</param>
        /// <param name="Bold">字体[0常规/1粗体]</param>
        /// <param name="DispType">显示方式[0直接显示/1显示到缓存]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTDisplayText(int WindowNum, float Row, float Col, string DispText, string FontColor, int FontSize, int Bold, int DispType);

        /// <summary>
        /// 查看窗口是否已经初始化
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="IsExist">标志值[0窗口还没初始化/1窗口已经初始化]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTIsWindowExist(int WindowNum, ref int IsExist);

        /// <summary>
        /// 查看图像是否已经初始化
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="IsExist">标志值[0图像还没初始化/1图像已经初始化]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTIsImageExist(int ImageNum, ref int IsExist);

        #endregion

        //////////////////////////////////////图像关键区域/////////////////////////////////
        #region XTRegion.h

        /// <summary>
        /// 选择感兴趣区域(鼠标)
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="SelectShape">画图形状[0方形/1圆形/2任意]</param>
        /// <param name="StartColor">画图颜色['red', 'green', 'blue']</param>
        /// <param name="EndColor">结束颜色['red', 'green', 'blue']</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTSelectRegion(int WindowNum, int ImageNum, int RoiNum, int SelectShape, string StartColor, string EndColor);

        /// <summary>
        /// 选择感兴趣区域(数值)
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="AreaShape">画图形状[0方形/1圆形]</param>
        /// <param name="Row1">区域行开始坐标/区域圆心坐标</param>
        /// <param name="Col1">区域列开始坐标/区域圆心坐标</param>
        /// <param name="Row2">区域行结束坐标/区域半径</param>
        /// <param name="Col2">区域列结束坐标/区域半径</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTSelectRegion1(int ImageNum, int RoiNum, int AreaShape, int Row1, int Col1, int Row2, int Col2);

        /// <summary>
        /// 选择感兴趣区域(数值)
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="Rows">行坐标列表[开始-结束]/圆心坐标[圆心-半径]</param>
        /// <param name="Cols">列坐标列表[开始-结束]/圆心坐标[圆心-半径]</param>
        /// <param name="ArrLen">坐标数组长度</param>
        /// <param name="AreaType">坐标数据类型[0方形/1圆形/2多边形]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTSelectRegion2(int ImageNum, int RoiNum, int[] Rows, int[] Cols, int ArrLen, int AreaType);

        /// <summary>
        /// 擦除感兴趣区域
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="SelectShape">画图形状[0方形/1圆形/2任意]</param>
        /// <param name="StartColor">画图颜色['red', 'green', 'blue']</param>
        /// <param name="EndColor">结束颜色['red', 'green', 'blue']</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTEraseRegion(int WindowNum, int ImageNum, int RoiNum, int SelectShape, string StartColor, string EndColor);

        /// <summary>
        /// 添加感兴趣区域
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="SelectShape">画图形状[0方形/1圆形/2任意]</param>
        /// <param name="StartColor">画图颜色['red', 'green', 'blue']</param>
        /// <param name="EndColor">结束颜色['red', 'green', 'blue']</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTResumeRegion(int WindowNum, int ImageNum, int RoiNum, int SelectShape, string StartColor, string EndColor);

        /// <summary>
        /// 移动感兴趣区域
        /// </summary>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="Row">目标区域行坐标</param>
        /// <param name="Column">目标区域列坐标</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTTranslateRegion(int RoiNum, int Row, int Column);

        /// <summary>
        /// 显示感兴趣区域
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="LineWidth">线宽(1)</param>
        /// <param name="DispColor">显示颜色['red', 'green', 'blue']</param>
        /// <param name="DispType">显示模式[0直接显示/1显示到缓存]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTDisplayRegion(int WindowNum, int RoiNum, int LineWidth, string DispColor, int DispType);

        /// <summary>
        /// 获取区域特征
        /// </summary>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="FeatureIndex">区域特征编号[0区域面积/1区域中心X/2区域中心Y/3区域长度W/4区域宽度H/5外接圆半径R]</param>
        /// <param name="FeatureValue">区域特征值</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTRegionFeature(int RoiNum, int FeatureIndex, ref float FeatureValue);

        /// <summary>
        /// 保存感兴趣区域
        /// </summary>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="FileName">文件名称</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTSaveRegion(int RoiNum, string FileName);

        /// <summary>
        /// 加载感兴趣区域
        /// </summary>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="FileName">文件名称</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTLoadRegion(int RoiNum, string FileName);

        /// <summary>
        /// 删除感兴趣区域
        /// </summary>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="FileName">文件名称</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTDeleteRegion(int RoiNum, string FileName);

        /// <summary>
        /// 查看区域是否存在
        /// </summary>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="IsExist">[0区域不存在/1区域存在]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTIsRegionExist(int RoiNum, ref int IsExist);

        /// <summary>
        /// 模板(定位结果)区域到ROI区域转换
        /// </summary>
        /// <param name="TemplateNum">模板编号</param>
        /// <param name="MatchRetNum">定位结果编号,没进行定位则忽略且转换的是模板区域</param>
        /// <param name="RoiNum">转换得到的区域编号</param>
        /// <param name="FillUp">是否对区域进行内填充[0不填充/1填充]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTTemplateAreaToRegion(int TemplateNum, int MatchRetNum, int RoiNum, int FillUp);

        /// <summary>
        /// 选择圆形区域并获取信息
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="StartColor">画图颜色['red', 'green', 'blue']</param>
        /// <param name="EndColor">结束颜色['red', 'green', 'blue']</param>
        /// <param name="CenterX">圆心行坐标</param>
        /// <param name="CenterY">圆心列坐标</param>
        /// <param name="Radius">半径</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTSelectCircleArea(int WindowNum, int ImageNum, int RoiNum, string StartColor, string EndColor, ref float CenterX, ref float CenterY, ref float Radius);

        /// <summary>
        /// 在区域上寻找极值坐标[上/下/左/右]
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="MinGray">最小灰度</param>
        /// <param name="MaxGray">最大灰度</param>
        /// <param name="CoorX">行坐标数组</param>
        /// <param name="CoorY">列坐标数组</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTFindLimitCoordinate(int ImageNum, int RoiNum, int MinGray, int MaxGray, [MarshalAs(UnmanagedType.LPArray)]int[] CoorX, [MarshalAs(UnmanagedType.LPArray)]int[] CoorY);

        #endregion

        //////////////////////////////////////模板定位部分/////////////////////////////////
        #region XTMatch.h

        /// <summary>
        /// 选择模板区域(鼠标)
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="TemplateNum">模板编号</param>
        /// <param name="AreaShape">画图形状[0方形/1圆形/2任意]</param>
        /// <param name="StartColor">画图颜色['red', 'green', 'blue']</param>
        /// <param name="EndColor">结束颜色['red', 'green', 'blue']</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTSelectTemplateArea(int WindowNum, int ImageNum, int TemplateNum, int AreaShape, string StartColor, string EndColor);

        /// <summary>
        /// 选择模板区域(数值)
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="TemplateNum">模板编号</param>
        /// <param name="AreaShape">画图形状[0方形/1圆形]</param>
        /// <param name="Row1">区域行开始坐标/区域圆心坐标</param>
        /// <param name="Col1">区域列开始坐标/区域圆心坐标</param>
        /// <param name="Row2">区域行结束坐标/区域半径</param>
        /// <param name="Col2">区域列结束坐标/区域半径</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTSelectTemplateArea1(int ImageNum, int TemplateNum, int AreaShape, int Row1, int Col1, int Row2, int Col2);

        /// <summary>
        /// 选择模板区域(数值)
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="TemplateNum">模板编号</param>
        /// <param name="Rows">行坐标列表[开始-结束]/圆心坐标[圆心-半径]</param>
        /// <param name="Cols">列坐标列表[开始-结束]/圆心坐标[圆心-半径]</param>
        /// <param name="ArrLen">坐标数组长度</param>
        /// <param name="AreaType">坐标数据类型[0方形/1圆形/2多边形]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTSelectTemplateArea2(int ImageNum, int TemplateNum, int[] Rows, int[] Cols, int ArrLen, int AreaType);

        /// <summary>
        /// 擦除模板区域
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="TemplateNum">模板编号</param>
        /// <param name="AreaShape"> 画图形状[0方形/1圆形/2任意]</param>
        /// <param name="StartColor">画图颜色['red', 'green', 'blue']</param>
        /// <param name="EndColor">结束颜色['red', 'green', 'blue']</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTEraseTemplateArea(int WindowNum, int ImageNum, int TemplateNum, int AreaShape, string StartColor, string EndColor);

        /// <summary>
        /// 添加模板区域
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="TemplateNum">模板编号</param>
        /// <param name="AreaShape">画图形状[0方形/1圆形/2任意]</param>
        /// <param name="StartColor">画图颜色['red', 'green', 'blue']</param>
        /// <param name="EndColor">结束颜色['red', 'green', 'blue']</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTResumeTemplateArea(int WindowNum, int ImageNum, int TemplateNum, int AreaShape, string StartColor, string EndColor);

        /// <summary>
        /// 显示模板区域
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="TemplateNum">模板编号</param>
        /// <param name="LineWidth">线宽(1)</param>
        /// <param name="DispColor">显示颜色['red', 'green', 'blue']</param>
        /// <param name="DispType">显示模式[0直接显示/1显示到缓存]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTDisplayTemplateArea(int WindowNum, int TemplateNum, int LineWidth, string DispColor, int DispType);

        /// <summary>
        /// 获取区域特征
        /// </summary>
        /// <param name="TemplateNum">模板编号</param>
        /// <param name="FeatureIndex">区域特征编号[0区域面积/1区域中心X/2区域中心Y/3区域长度W/4区域宽度H/5外接圆半径]</param>
        /// <param name="FeatureValue">区域特征值</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTTemplateAreaFeature(int TemplateNum, int FeatureIndex, ref float FeatureValue);

        /// <summary>
        /// 显示模板标记
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="TemplateNum">模板编号</param>
        /// <param name="SignNum">标志点编号(-1全部显示)</param>
        /// <param name="LineWidth">线宽(1)</param>
        /// <param name="DispColor">显示颜色['red', 'green', 'blue']</param>
        /// <param name="DispType">显示模式[0直接显示/1显示到缓存]</param>
        /// <returns>非0表示执行错误</returns>/param>
        /// <returns></returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTDisplayTemplateSign(int WindowNum, int TemplateNum, int SignNum, int LineWidth, string DispColor, int DispType);

        /// <summary>
        /// 显示模板图像
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="TemplateNum">模板编号</param>
        /// <param name="DispType">显示模式[0直接显示/1显示到缓存]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTDisplayTemplateImage(int WindowNum, int TemplateNum, int DispType);

        /// <summary>
        /// 添加模板标记点(鼠标)
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="TemplateNum">模板编号</param>
        /// <param name="SignNum">标记点编号</param>
        /// <param name="SignColor">标记点颜色['red', 'green', 'blue']</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTAddTemplateSign(int WindowNum, int TemplateNum, int SignNum, string SignColor);

        /// <summary>
        /// 添加模板标记点(数值)
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="TemplateNum">模板编号</param>
        /// <param name="SignNum">标记点编号</param>
        /// <param name="Row">标记点行坐标</param>
        /// <param name="Col">标记点列坐标</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTAddTemplateSign1(int WindowNum, int TemplateNum, int SignNum, int Row, int Col);

        /// <summary>
        /// 移动模板区域
        /// </summary>
        /// <param name="TemplateNum">模板编号</param>
        /// <param name="Row">目标区域行位置</param>
        /// <param name="Column">目标区域列位置</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTTranslateTemplateArea(int TemplateNum, int Row, int Column);

        /// <summary>
        /// 生成模板匹配模型数据
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="TemplateNum">模板编号</param>
        /// <param name="ModelType">模板类型[0模糊模板/1形状模板/2变形模板]</param>
        /// <param name="AngleStart">开始角度[-PI,PI]</param>
        /// <param name="AngleExtent">旋转范围[0,2PI]</param>
        /// <param name="ScaleMin">最小变形比例[0.1,2.0](ModelType=1,2时有效)</param>
        /// <param name="ScaleMax">最大变形比例[0.1,2.0](ModelType=1,2时有效)</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTCreateTemplate(int ImageNum, int TemplateNum, int ModelType, float AngleStart, float AngleExtent, float ScaleMin, float ScaleMax);

        /// <summary>
        /// 使用模型数据进行模板匹配
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="TemplateNum">模板编号</param>
        /// <param name="AngleStart">开始角度[-PI,PI]</param>
        /// <param name="AngleExtent">旋转范围[0,2PI]</param>
        /// <param name="ScaleMin">最小变形比例[0.1,2.0](ModelType=1,2时有效)</param>
        /// <param name="ScaleMax">最大变形比例[0.1,2.0](ModelType=1,2时有效)</param>
        /// <param name="MinScore">最小分数[0,1000]</param>
        /// <param name="NumMatches">定位数量(1)</param>
        /// <param name="NumFinds">匹配结果数量</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTFindTemplate(int ImageNum, int RoiNum, int TemplateNum, float AngleStart, float AngleExtent, float ScaleMin, float ScaleMax, float MinScore, int NumMatches, ref int NumFinds);

        /// <summary>
        /// 读取模板匹配结果
        /// </summary>
        /// <param name="TemplateNum">模板编号</param>
        /// <param name="Number">匹配结果数量</param>
        /// <param name="MatchRow">匹配结果行坐标数组</param>
        /// <param name="MatchCol">匹配结果列坐标数组</param>
        /// <param name="MatchAngle">匹配结果角度数组</param>
        /// <param name="MatchScore">匹配结果分数数组</param>
        /// <param name="MatchScale">匹配结果形变比例数组</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTReadFindResult(int TemplateNum, int Number, [MarshalAs(UnmanagedType.LPArray)]float[] MatchRow, [MarshalAs(UnmanagedType.LPArray)]float[] MatchCol, [MarshalAs(UnmanagedType.LPArray)]float[] MatchAngle, [MarshalAs(UnmanagedType.LPArray)]float[] MatchScore, [MarshalAs(UnmanagedType.LPArray)]float[] MatchScale);

        /// <summary>
        /// 读取模板匹配结果标志点位置
        /// </summary>
        /// <param name="TemplateNum">模板编号</param>
        /// <param name="SignNum">标记点编号</param>
        /// <param name="Number">模板定位结果数</param>
        /// <param name="Row">标记点行坐标</param>
        /// <param name="Col">标记点列坐标</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTReadFindSign(int TemplateNum, int SignNum, int Number, [MarshalAs(UnmanagedType.LPArray)]float[] Row, [MarshalAs(UnmanagedType.LPArray)]float[] Col);

        /// <summary>
        /// 保存模板匹配模型数据
        /// </summary>
        /// <param name="TemplateNum">模板编号</param>
        /// <param name="FileName">文件名称</param>
        /// <param name="SaveTemplateImage">保存模板图像[0不保存图片/1保存图片]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTSaveTemplate(int TemplateNum, string FileName, int SaveTemplateImage);

        /// <summary>
        /// 加载模板匹配模型数据
        /// </summary>
        /// <param name="TemplateNum">模板编号</param>
        /// <param name="FileName">文件名称</param>
        /// <param name="LoadTemplateImage">是否加载模板图像[0不加载图片/1加载图片]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTLoadTemplate(int TemplateNum, string FileName, int LoadTemplateImage);

        /// <summary>
        /// 删除模板匹配模型数据文件
        /// </summary>
        /// <param name="TemplateNum">模板编号</param>
        /// <param name="FileName">文件名称</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTDeleteTemplate(int TemplateNum, string FileName);

        /// <summary>
        /// 查看模板匹配模型是否存在
        /// </summary>
        /// <param name="TemplateNum">模板编号</param>
        /// <param name="IsExist">存在标志[0模板不存在/1模板区域存在/2模板已经设置]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTIsTemplateExist(int TemplateNum, ref int IsExist);

        /// <summary>
        /// 自动扩展调整模板区域
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="TemplateNum">模板编号</param>
        /// <param name="GuassSigma">阈值分割高斯平滑系数</param>
        /// <param name="Convex">是否转换成最小凸包</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTAdjustTemplateArea(int ImageNum, int TemplateNum, int GuassSigma, int Convex);

        #endregion

        //////////////////////////////////////边沿测量部分/////////////////////////////////
        #region XTMeasure.h

        /// <summary>
        /// 边沿测量
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="MeasureType">测量类型[0垂直于矩形的边沿检测/1垂直于矩形的边沿对检测/2垂直于圆环圆弧的边沿检测/3垂直于圆环圆弧的边沿对检测]</param>
        /// <param name="Sigma">平滑系数[0.4,100](1.5)</param>
        /// <param name="Threshold">最小振幅[1,255](30)</param>
        /// <param name="Transition">要提取的边沿类型[0所有/1暗到亮/2亮到暗](0)</param>
        /// <param name="Select">要提取的边沿部位[/0所有/1最开始/2最末尾](0)</param>
        /// <param name="EdgeNumber">测量得到的边沿或边沿对数量</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTMeasureEdge(int ImageNum, int RoiNum, int MeasureType, float Sigma, int Threshold, int Transition, int Select, ref int EdgeNumber);

        /// <summary>
        /// 显示测量区域
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="MeasureType">测量类型[0垂直于矩形的边沿检测/1垂直于矩形的边沿对检测/2垂直于圆环圆弧的边沿检测/3垂直于圆环圆弧的边沿对检测]</param>
        /// <param name="DispColor">显示颜色['red', 'green', 'blue']</param>
        /// <param name="DispType">显示方式[0直接显示/1显示到缓存]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTDisplayMeasureRegion(int WindowNum, int RoiNum, int MeasureType, string DispColor, int DispType);

        /// <summary>
        /// 显示测量结果
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="MeasureType">测量类型[0垂直于矩形的边沿检测/1垂直于矩形的边沿对检测/2垂直于圆环圆弧的边沿检测/3垂直于圆环圆弧的边沿对检测]</param>
        /// <param name="LineWidth">线条宽度</param>
        /// <param name="Size">坐标点大小</param>
        /// <param name="DispColor">显示颜色['red', 'green', 'blue']</param>
        /// <param name="DispType">显示方式[0直接显示/1显示到缓存]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTDisplayMeasureResult(int WindowNum, int RoiNum, int MeasureType, int LineWidth, int Size, string DispColor, int DispType);

        /// <summary>
        /// 读取边沿测量结果
        /// </summary>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="EdgeNumber">测量得到的边沿编号(-1表示读取全部)</param>
        /// <param name="ResultType">需要获取的结果类型[0边沿X坐标/1边沿Y坐标/2边沿振幅//3第二个边沿X坐标/4第二个边沿Y坐标/5第二个边沿振幅/6边沿或边沿对间距(比边沿数少1)/7边沿对宽度]</param>
        /// <param name="ResultValue">测量得到的结果数值</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTReadMeasureEdgeResult(int RoiNum, int EdgeNumber, int ResultType, [MarshalAs(UnmanagedType.LPArray)]float[] ResultValue);

        #endregion

        //////////////////////////////////////区域分割部分/////////////////////////////////
        #region XTDetect.h

        /// <summary>
        /// 区域分割
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="MinGray">最小灰度</param>
        /// <param name="MaxGray">最大灰度</param>
        /// <param name="MinArea">最小面积</param>
        /// <param name="MaxArea">最大面积</param>
        /// <param name="Number">分割得到的区域数量</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTRegionThreshold(int ImageNum, int RoiNum, int MinGray, int MaxGray, int MinArea, int MaxArea, ref int Number);

        /// <summary>
        /// 读取区域分割结果
        /// </summary>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="ResultNum">结果编号(-1表示获取所有结果)</param>
        /// <param name="AreaSize">区域的面积集合</param>
        /// <param name="CenterX">区域重心集合X</param>
        /// <param name="CenterY">区域重心集合Y</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTReadRegionThresholdResult(int RoiNum, int ResultNum, [MarshalAs(UnmanagedType.LPArray)]float[] AreaSize, [MarshalAs(UnmanagedType.LPArray)]float[] CenterX, [MarshalAs(UnmanagedType.LPArray)]float[] CenterY);

        /// <summary>
        /// 显示区域分割结果
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="ResultNum">结果编号(-1显示所有结果)</param>
        /// <param name="FillUp">FillUp 是否填充显示[0不填充/1填充]</param>
        /// <param name="DispColor">显示颜色['red', 'green', 'blue']</param>
        /// <param name="DispType">显示方式[0直接显示/1显示到缓存]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTDisplayResultRegion(int WindowNum, int RoiNum, int ResultNum, int FillUp, string DispColor, int DispType);

        /// <summary>
        /// 清空区域检测结果
        /// </summary>
        /// <param name="RoiNum">区域编号</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTClearResultRegion(int RoiNum);

        /// <summary>
        /// 区域分割/灯芯检测
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="MinGray">最小灰度</param>
        /// <param name="MaxGray">最大灰度</param>
        /// <param name="MinArea">最小面积</param>
        /// <param name="MaxArea">最大面积</param>
        /// <param name="SelArea">过滤面积/最小面积</param>
        /// <param name="MaxLength">过滤长度/轮廓长度</param>
        /// <param name="Number">分割得到的区域数量</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTRegionThresholdEx(int ImageNum, int RoiNum, int MinGray, int MaxGray, int MinArea, int MaxArea, int SelArea, int MaxLength, ref int Number);

        /// <summary>
        /// 读取区域分割结果/灯芯检测
        /// </summary>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="ResultNum">结果编号(-1表示获取所有结果)</param>
        /// <param name="AreaSize">区域的面积集合</param>
        /// <param name="CenterX">区域重心集合X</param>
        /// <param name="CenterY">区域重心集合Y</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTReadRegionThresholdResultEx(int RoiNum, int ResultNum, [MarshalAs(UnmanagedType.LPArray)]float[] AreaSize, [MarshalAs(UnmanagedType.LPArray)]float[] CenterX, [MarshalAs(UnmanagedType.LPArray)]float[] CenterY);

        /// <summary>
        /// 显示检测结果/灯芯检测
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="ResultNum">结果编号(-1显示所有结果)</param>
        /// <param name="FillUp">FillUp 是否填充显示[0不填充/1填充]</param>
        /// <param name="DispColor">显示颜色['red', 'green', 'blue']</param>
        /// <param name="DispType">显示方式[0直接显示/1显示到缓存]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTDisplayDetectResultEx(int WindowNum, int RoiNum, int ResultNum, int FillUp, string DispColor, int DispType);

        /// <summary>
        /// 清空区域检测结果/灯芯检测
        /// </summary>
        /// <param name="RoiNum">区域编号</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTClearResultRegionEx(int RoiNum);

        /// <summary>
        /// 分割结果区域到ROI区域转换
        /// </summary>
        /// <param name="ResultNum">分割结果号(-1则取全部)</param>
        /// <param name="RoiNum">ROI区域编号</param>
        /// <param name="FillUp">是否对区域进行内填充[0不填充/1填充]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTResultRegionToRoiNum(int ResultNum, int RoiNum, int FillUp);

        /// <summary>
        /// 图像区域轮廓提取
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="MinGray">最小灰度</param>
        /// <param name="MaxGray">最大灰度</param>
        /// <param name="MinSize">最小尺寸</param>
        /// <param name="ContourNumber">提取到的轮廓数量</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTExtractContour(int ImageNum, int RoiNum, int MinGray, int MaxGray, int MinSize, ref int ContourNumber);

        /// <summary>
        /// 读取轮廓极限坐标
        /// </summary>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="ContourNum">结果编号(-1读取所有结果)</param>
        /// <param name="ResultType">结果类型[0最左坐标X/1最左坐标Y/2最右坐标X/3最右坐标Y/4最上坐标X/5最上坐标Y/6最下坐标X/7最下坐标Y]</param>
        /// <param name="ResultValue">结果值数组</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTReadExtractContourResult(int RoiNum, int ContourNum, int ResultType, [MarshalAs(UnmanagedType.LPArray)]float[] ResultValue);

        /// <summary>
        /// 显示提取到的轮廓
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="ContourNum">结果编号(-1显示所有结果)</param>
        /// <param name="DispColor">显示颜色['red', 'green', 'blue']</param>
        /// <param name="DispType">显示方式[0直接显示/1显示到缓存]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTDisplayExtractContour(int WindowNum, int RoiNum, int ContourNum, string DispColor, int DispType);

        /// <summary>
        /// 污点提取操作
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="LightDark">污点亮暗[0亮/1暗]</param>
        /// <param name ="MinSize">最小污点尺寸</param>
        /// <param name="MaxSize">最大污点尺寸</param>
        /// <param name="SpotNumber">提取到的污点数量</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTExtractSpot(int ImageNum, int RoiNum, int LightDark, int MinSize, int MaxSize, ref int SpotNumber);

        /// <summary>
        /// 读取污点极限坐标
        /// </summary>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="SpotNum">结果编号(-1读取所有结果)</param>
        /// <param name="ResultType">结果类型[0最左坐标X/1最左坐标Y/2最右坐标X/3最右坐标Y/4最上坐标X/5最上坐标Y/6最下坐标X/7最下坐标Y/8中心坐标X/9中心左边Y/10污点面积]</param>
        /// <param name="ResultValue">结果值数组</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTReadExtractSpotResult(int RoiNum, int SpotNum, int ResultType, [MarshalAs(UnmanagedType.LPArray)]float[] ResultValue);

        /// <summary>
        /// 显示提取到的污点
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="SpotNum">结果编号(-1显示所有结果)</param>
        /// <param name="DispColor">显示颜色['red', 'green', 'blue']</param>
        /// <param name="DispType">显示方式[0直接显示/1显示到缓存]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTDisplayExtractSpot(int WindowNum, int RoiNum, int SpotNum, string DispColor, int DispType);

        /// <summary>
        /// 直线拟合函数
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="LineIndex">直线编号[0上/1下/2左/3右]</param>
        /// <param name="SmoothFactor">平滑系数[0.2,50](3)</param>
        /// <param name="ThreshLow">边沿提取低通滤波[1,255](20)</param>
        /// <param name="ThreshHigh">边沿提取高通滤波[1,255](40)</param>
        /// <param name="MinLength">线条最小长度(5)</param>
        /// <param name="LinesMaxDist">两条直线间最大距离[0,](10)</param>
        /// <param name="LineNumber">拟合到的直线数量</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTFitLine(int ImageNum, int RoiNum, int LineIndex, float SmoothFactor, int ThreshLow, int ThreshHigh, int MinLength, int LinesMaxDist, ref int LineNumber);

        /// <summary>
        /// 读取拟合到的直线
        /// </summary>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="X1">直线开始坐标X数组</param>
        /// <param name="Y1">直线开始坐标Y数组</param>
        /// <param name="X2">直线结束坐标X数组</param>
        /// <param name="Y2">直线结束坐标Y数组</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTReadFitLine(int RoiNum, ref float X1, ref float Y1, ref float X2, ref float Y2);

        /// <summary>
        /// 拟合直线和圆
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="FitType">拟合类型[0直线/1圆]</param>
        /// <param name="OrienIndex">要提取的边类型[0上边(大圆)/1下边(小圆)/2左边/3右边]</param>
        /// <param name="ThreshScale">二值分割高斯比例系数(0.2)</param>
        /// <param name="ThreshMaskSize">二值分割高斯平滑系数(21)</param>
        /// <param name="MinEdgeLength">边沿最小长度[100]</param>
        /// <param name="MaxEdgeDistAbs">最大边沿距离[200]</param>
        /// <param name="FitNumber">拟合环境[0-dark/1-light]/拟合结果数量</param>
        /// <returns></returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTFitLineCircle(int ImageNum, int RoiNum, int FitType, int OrienIndex, float ThreshScale, float ThreshMaskSize, int MinEdgeLength, int MaxEdgeDistAbs, ref int FitNumber);

        /// <summary>
        /// 读取拟合到的直线或圆
        /// </summary>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="FitType">拟合类型[0直线/1圆]</param>
        /// <param name="Row1">直线行开始坐标/圆心行坐标</param>
        /// <param name="Col1">直线列开始坐标/圆心列坐标</param>
        /// <param name="Row2">直线行结束坐标/圆半径</param>
        /// <param name="Col2">直线列结束坐标/圆弧中点角度(弧度)</param>
        /// <returns></returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTReadFitLineCircle(int RoiNum, int FitType, ref float Row1, ref float Col1, ref float Row2, ref float Col2);

        /// <summary>
        /// 寻找内圆或外接圆
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="LightDark">寻找区域[0暗区域/1亮区域]</param>
        /// <param name="InnerOuter">寻找内外圆[0内圆/1外圆]</param>
        /// <param name="CenterX">圆心X</param>
        /// <param name="CenterY">圆心Y</param>
        /// <param name="Radius">半径R</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTFindCircle(int ImageNum, int RoiNum, int LightDark, int InnerOuter, ref float CenterX, ref float CenterY, ref float Radius);

        /// <summary>
        /// 寻找区域面积中心
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="LightDark">寻找区域[0暗区域/1亮区域]</param>
        /// <param name="FindType">寻找类型[0最外突区域/1所有找到区域/2最大连通区域]</param>
        /// <param name="Area">面积A</param>
        /// <param name="CenterX">中心X</param>
        /// <param name="CenterY">中心Y</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTFindAreaCenter(int ImageNum, int RoiNum, int LightDark, int FindType, ref float Area, ref float CenterX, ref float CenterY);


        /// <summary>
        /// 给定引导线查找直线
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="Row1">直线起点行坐标</param>
        /// <param name="Column1">直线起点列坐标</param>
        /// <param name="Row2">直线终点行坐标</param>
        /// <param name="Column2">直线终点列坐标</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTLineDetection(int ImageNum, ref float Row1, ref float Column1, ref float Row2, ref float Column2);

        /// <summary>
        /// 给定引导线查找直线交点
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="Row11">直线1起点行坐标</param>
        /// <param name="Column11">直线1起点列坐标</param>
        /// <param name="Row21">直线1终点行坐标</param>
        /// <param name="Column21">直线1终点列坐标</param>
        /// <param name="Row12">直线2起点行坐标</param>
        /// <param name="Column12">直线2起点列坐标</param>
        /// <param name="Row22">直线2终点行坐标</param>
        /// <param name="Column22">直线2终点列坐标</param>
        /// <param name="RowInter">直线交点行坐标</param>
        /// <param name="ColInter">直线交点列坐标</param>
        /// <param name="PhiInter">两直线夹角[-PI,PI]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTFindInterPoint(int ImageNum, float Row11, float Column11, float Row21, float Column21, float Row12, float Column12, float Row22, float Column22, ref float RowInter, ref float ColInter, ref float PhiInter);

        /// <summary>
        /// 寻找区域边沿点/2020051401
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="FitType">要提取的边类型[0上边/1下边/2左边/3右边/4内圆/5外圆]</param>
        /// <param name="MinGray">最小灰度</param>
        /// <param name="MaxGray">最大灰度</param>
        /// <param name="MinArea">最小面积</param>
        /// <param name="MaxArea">最大面积</param>
        /// <param name="Row">目标位置行坐标</param>
        /// <param name="Column">目标位置列坐标</param>
        /// <returns></returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTFindEdge(int ImageNum, int RoiNum, int FitType, int MinGray, int MaxGray, int MinArea, int MaxArea, ref float Row, ref float Column);

        /// <summary>
        /// 灯板检测提取/(专用)2020070801
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="MinGray">最小灰度</param>
        /// <param name="MaxGray">最大灰度</param>
        /// <param name="MinArea">最小面积</param>
        /// <param name="MaxArea">最大面积</param>
        /// <param name="Area">区域面积数组</param>
        /// <param name="Row">区域中心行坐标数组</param>
        /// <param name="Column">区域中心列坐标数组</param>
        /// <param name="Number">结果数量</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTFindBoard(int ImageNum, int RoiNum, int MinGray, int MaxGray, int MinArea, int MaxArea, [MarshalAs(UnmanagedType.LPArray)]float[] Area, [MarshalAs(UnmanagedType.LPArray)]float[] Row, [MarshalAs(UnmanagedType.LPArray)]float[] Column, ref int Number);

        /// <summary>
        /// 查找灯珠芯片(专用)
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="MaskWidth">掩码宽度</param>
        /// <param name="MaskHeight">掩码高度</param>
        /// <param name="AbsThreshold">与平均值的最小灰度差异</param>
        /// <param name="LightDark">寻找区域[0暗区域/1亮区域]</param>
        /// <param name="MinArea">最小面积</param>
        /// <param name="MaxArea">最大面积</param>
        /// <param name="Number">区域数量</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTFindLampChip(int ImageNum, int RoiNum, int MaskWidth, int MaskHeight, int AbsThreshold, int LightDark, int MinArea, int MaxArea, ref int Number);

        #endregion

        //////////////////////////////////////区域分割部分/////////////////////////////////
        #region XTDetectSp.h

        // 查找端子
        [DllImport("XTVisionc.dll")]
        public static extern int XTDetectTerminal(int ImageNum, int RoiNum, int MeasureTopLine, int TerminalMinGray, float TerminalSize, float EmphasizeLevel, ref int TerminalNumber, ref float MaxDistance);

        //端子查找结果
        [DllImport("XTVisionc.dll")]
        public static extern int XTReadDetectTerminalResult(ref float MaxDistanceX, ref float MaxDistanceY, ref float RefLineX1, ref float RefLineY1, ref float RefLineX2, ref float RefLineY2, ref int TerminalIndex);

        //血管增强显示
        [DllImport("XTVisionc.dll")]
        public static extern int XTAngioEnhance(int ImageNum, int RoiNum, int DetectNum, int AngioMinGray, int AngioMaxGray, int MaskSize, float EmphLevel, float SmoothLevel, int MinLineSize);

        //方形摄像头镜片检测（专用）
        [DllImport("XTVisionc.dll")]
        public static extern int XTRectLensDetect(int WindowNum, int ImageNum, int RoiNum, int MaxMorphAngle, int GaussThreshold, int HLineThreshold, int HLAngleGap, int HLDistGap, float TrimPixel, int CollapseMinGray, int DirtyThresholdValue, float CollapseLength1, float CollapseLength2, float DirtySize, ref int Result, ref float LensWidth, ref float LensHeight);

        //陶瓷圆度检测（专用）
        [DllImport("XTVisionc.dll")]
        public static extern int XTCeramicsDetect(int ImageNum, int RoiNum, int EmphaFactor, int MinGray, int MaxGray, float GraySmooth, ref float Area, ref float InnerRadius, ref float MeanRadius, ref float OuterRadius, ref float Circularity);

        //手机面板胶点位置确定（专用）
        [DllImport("XTVisionc.dll")]
        public static extern int XTCheckDisp(int WindowNum, int ImageNum, int GlueMinGray, int GlueMaxGray, int GlueMinArea, int GlueMaxArea, int PanelBoxMinGray, int PanelBoxMaxGray, int PanelBoxWidth, int PanelBoxHeight);


        /// <summary>
        /// 编带机产品检测算法/专用
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="GlassMinGray">玻璃柱最小灰度</param>
        /// <param name="GlassMaxGray">玻璃柱最大灰度</param>
        /// <param name="CenterMinGray">中心区最小灰度</param>
        /// <param name="CenterMaxGray">中心区最大灰度</param>
        /// <param name="CenterArea">中心区面积</param>
        /// <param name="OuterRadius">中心区外半径</param>
        /// <param name="Circularity">中心区圆度</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTGlassPillar(int ImageNum, int RoiNum, int GlassMinGray, int GlassMaxGray, int CenterMinGray, int CenterMaxGray, ref float CenterArea, ref float OuterRadius, ref float Circularity);

        /// <summary>
        /// 石膏板孔洞检测
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="LightDard">寻找区域[0暗区域/1亮区域]</param>
        /// <param name="ThreshScale">区域分割比例(越小细节越多:0.2)</param>
        /// <param name="MinRadius">最小半径</param>
        /// <param name="MaxRadius">最大半径</param>
        /// <param name="BlackHoleMaxGray">深孔最大灰度</param>
        /// <param name="NumRegionHoles">区域结果数量</param>
        /// <param name="NumBlackHoles">深孔结果数量</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int GypsumBoardDetect(int ImageNum, int RoiNum, int LightDard, float ThreshScale, float MinRadius, float MaxRadius, int BlackHoleMaxGray, ref int NumRegionHoles, ref int NumBlackHoles);

        /// <summary>
        /// 石膏板孔洞检测结果
        /// </summary>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="HoleType">孔径类型[0内圆/1深圆]</param>
        /// <param name="ResultNum">结果编号(-1表示所有)</param>
        /// <param name="CenterX">内接圆心X</param>
        /// <param name="CenterY">内接圆心Y</param>
        /// <param name="Radius">内接圆半径R</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int GypsumBoardResult(int RoiNum, int HoleType, int ResultNum, [MarshalAs(UnmanagedType.LPArray)]float[] CenterX, [MarshalAs(UnmanagedType.LPArray)]float[] CenterY, [MarshalAs(UnmanagedType.LPArray)]float[] Radius);

        [DllImport("XTVisionc.dll")]
        public static extern int GypsumBoardHoldsNum(int RoiNum, int HoleType, ref int NumHoles);

        [DllImport("XTVisionc.dll")]
        public static extern int CypsumBoardAddResult(int RoiNum, float CenterX, float CenterY, float Radius, int BlackHole);

        [DllImport("XTVisionc.dll")]
        public static extern int CypsumBoardDelResult(int RoiNum, float CenterX, float CenterY, float Radius, int BlackHole);

        /// <summary>
        /// 圆检测和边沿精确提取
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="Row">圆心坐标</param>
        /// <param name="Column">圆心坐标</param>
        /// <param name="Radius">圆形半径</param>
        /// <returns></returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTFindPreciseCircle(int ImageNum, int RoiNum, ref float Row, ref float Column, ref float Radius);

        /// <summary>
        /// 提取圆形
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="MinRingThickness">圆环最小厚度</param>
        /// <param name="MaxRingThickness">圆环最大厚度</param>
        /// <param name="Number">圆形个数</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTFindMeasureCircleNew(int ImageNum, int RoiNum, float MinRingThickness, float MaxRingThickness, ref int Number);

        /// <summary>
        /// 提取圆形
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="Number">圆形个数</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTFindMeasureCircle(int ImageNum, int RoiNum, ref int Number);

        /// <summary>
        /// 提取圆测量结果
        /// </summary>
        /// <param name="Number">提取数量</param>
        /// <param name="Row">圆心坐标数组</param>
        /// <param name="Column">圆心坐标数组</param>
        /// <param name="Radius">圆形半径数组</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTReadMeasureCircle(int Number, [MarshalAs(UnmanagedType.LPArray)]float[] Row, [MarshalAs(UnmanagedType.LPArray)]float[] Column, [MarshalAs(UnmanagedType.LPArray)]float[] Radius);

        /// <summary>
        /// 提取圆测量结果
        /// </summary>
        /// <param name="Number">提取数量</param>
        /// <param name="Row">圆心坐标数组</param>
        /// <param name="Column">圆心坐标数组</param>
        /// <param name="Radius">圆形半径数组</param>
        /// <param name="RingThickness">圆环厚度</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTReadMeasureCircleNew(int Number, [MarshalAs(UnmanagedType.LPArray)]float[] Row, [MarshalAs(UnmanagedType.LPArray)]float[] Column, [MarshalAs(UnmanagedType.LPArray)]float[] Radius, [MarshalAs(UnmanagedType.LPArray)]float[] RingThickness);

        /// <summary>
        /// 直角两侧直线提取
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="PixelMm">像素精度</param>
        /// <param name="PhysiL12">产品宽度</param>
        /// <param name="PhysiL14">产品长度</param>
        /// <param name="ReferanceCamera">是否是参考相机</param>
        /// <param name="ReturnValue">找线结果/0找线失败/2找线成功</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTCeramicPlateMeasure(int ImageNum, int RoiNum, float PixelMm, float PhysiL12, float PhysiL14, int ReferanceCamera, ref int ReturnValue);

        /// <summary>
        /// 直角两侧直线提取
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="PixelMm">像素精度</param>
        /// <param name="PhysiL12">产品宽度</param>
        /// <param name="PhysiL14">产品长度</param>
        /// <param name="CameraIndex">相机编号</param>
        /// <param name="DefectArea">缺口面积</param>
        /// <param name="ReturnValue">找线结果/0找线失败/2找线成功/大于2部分表示缺口数量</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTCeramicPlateMeasureNew(int ImageNum, int RoiNum, float PixelMm, float PhysiL12, float PhysiL14, int CameraIndex, float DefectArea, ref int ReturnValue);

        /// <summary>
        /// 获取直线提取结果
        /// </summary>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="RowInter">角点坐标/长度为4</param>
        /// <param name="ColInter">角点坐标/长度为4</param>
        /// <param name="LineH">水平直线坐标(01是开始点/12是结束点)/长度为4</param>
        /// <param name="LineV">垂直直线坐标(01是开始点/12是结束点)/长度为4</param>
        /// <returns></returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTReadCeramicPlateSize(int RoiNum, [MarshalAs(UnmanagedType.LPArray)]float[] RowInter, [MarshalAs(UnmanagedType.LPArray)]float[] ColInter, [MarshalAs(UnmanagedType.LPArray)]float[] LineH, [MarshalAs(UnmanagedType.LPArray)]float[] LineV);

        /// <summary>
        /// 获取直线提取结果
        /// </summary>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="RowInter">角点坐标/长度为4</param>
        /// <param name="ColInter">角点坐标/长度为4</param>
        /// <param name="LineH">水平直线坐标(01是开始点/12是结束点)/长度为4</param>
        /// <param name="LineV">垂直直线坐标(01是开始点/12是结束点)/长度为4</param>
        /// <param name="Angle">边线夹角</param>
        /// <returns></returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTReadCeramicPlateSizeNew(int RoiNum, [MarshalAs(UnmanagedType.LPArray)]float[] RowInter, [MarshalAs(UnmanagedType.LPArray)]float[] ColInter, [MarshalAs(UnmanagedType.LPArray)]float[] LineH, [MarshalAs(UnmanagedType.LPArray)]float[] LineV, ref float Angle);

        /// <summary>
        /// 喇叭寻找直线
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="LineLength">直线长度</param>
        /// <param name="LineNumer">直线数量</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTHornFindLine(int ImageNum, int RoiNum, int LineLength, ref int LineNumer);

        /// <summary>
        /// 二极管寻找焊盘中心
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="MinArea">最小面积</param>
        /// <param name="MaxArea">最大面积</param>
        /// <param name="Row">焊盘位置</param>
        /// <param name="Column">焊盘位置</param>
        /// <param name="Number">焊盘数量</param>
        /// <returns></returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTFindPadCenter(int ImageNum, int RoiNum, int MinArea, int MaxArea, [MarshalAs(UnmanagedType.LPArray)]float[] Row, [MarshalAs(UnmanagedType.LPArray)]float[] Column, ref int Number);

        #endregion

        //////////////////////////////////////相机标定部分/////////////////////////////////
        #region XTCalibration.h

        /// <summary>
        /// 生成标定描述文件
        /// </summary>
        /// <param name="XNum">X方向标记数量</param>
        /// <param name="YNum">Y方向标记数量</param>
        /// <param name="MarkDist">标记之间距离(m)</param>
        /// <param name="DiameterRatio">标记直径与标记距离的比率</param>
        /// <param name="CalPlateDescr">校准板说明文件名</param>
        /// <param name="CalPlatePSFile">校准板脚本文件名</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int GenCalibrationPlate(int XNum, int YNum, float MarkDist, float DiameterRatio, string CalPlateDescr, string CalPlatePSFile);

        /// <summary>
        /// 相机标定,要求第一张图像标定板的角在图像的左上方
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="CameraNum">相机编号</param>
        /// <param name="ImageFile">图像文件名(函数自动加编号，编号范围[001,099])</param>
        /// <param name="ImageCount">图像数量[11,99]</param>
        /// <param name="CalibrationFile">标定描述文件</param>
        /// <param name="Focus">焦距F，单位mm(8.0mm)</param>
        /// <param name="Sx">像元宽Sx，单位um(8.3um)</param>
        /// <param name="Sy">像元宽Sy，单位um(8.3um)</param>
        /// <param name="StdZ">标定板厚度，单位mm</param>
        /// <param name="Precise">精准标定模式[0普通标定/1精准标定]</param>
        /// <param name="Errors">标定平均误差(应该在[0.0,1.0]Pixel之间)</param>
        /// <param name="PixelDist">像素距离(mm/pix)</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTCameraCalibration(int WindowNum, int CameraNum, string ImageFile, int ImageCount, string CalibrationFile,
            float Focus, float Sx, float Sy, float StdZ, bool Precise, ref float Errors, ref float PixelDist);

        /// <summary>
        /// 双相机标定，要求第一张图像标定板的角在图像的左上方
        /// </summary>
        /// <param name="WindowNumL">左窗口编号</param>
        /// <param name="WindowNumR">右窗口编号</param>
        /// <param name="CameraNumL">左相机编号</param>
        /// <param name="CameraNumR">右相机编号</param>
        /// <param name="ImageFileL">左图像文件名(带编号，编号范围[001,099])</param>
        /// <param name="ImageFileR">右图像文件名(带编号，编号范围[001,099])</param>
        /// <param name="ImageCount">图像数量[011,099]</param>
        /// <param name="CalibrationFile">标定描述文件</param>
        /// <param name="Focus">焦距F，单位mm(8.0mm)</param>
        /// <param name="Sx">像元宽Sx，单位um(8.3um)</param>
        /// <param name="Sy">像元宽Sy，单位um(8.3um)</param>
        /// <param name="StdZ">标定板厚度Thickness(1.0mm)</param>
        /// <param name="Errors">标定平均误差(应该在[0.0,1.0]Pixel之间)</param>
        /// <param name="PixelDist">像素距离(mm/pix)</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTCameraCalibration2(int WindowNumL, int WindowNumR, int CameraNumL, int CameraNumR, string ImageFileL, string ImageFileR, int ImageCount, string CalibrationFile,
            float Focus, float Sx, float Sy, float StdZ, ref float Errors, ref float PixelDist);

        /// <summary>
        /// 获取标定板圆孔中心坐标
        /// </summary>
        /// <param name="CameraNum">相机编号</param>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="RoiNum">结果存放区域编号</param>
        /// <param name="CalibrationFile">标定文件</param>
        /// <param name="Focus">焦距F，单位mm(8.0mm)</param>
        /// <param name="Sx">像元宽Sx，单位um(8.3um)</param>
        /// <param name="Sy">像元宽Sy，单位um(8.3um)</param>
        /// <param name="StdZ">标定板厚度Thickness(1.0mm)</param>
        /// <param name="ParamType">参数类型[0理论参数/1内部参数/2MAP参数]</param>
        /// <param name="Row">圆孔行坐标数组</param>
        /// <param name="Column">圆孔列坐标数组</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTGetCalibObservPoints(int CameraNum, int ImageNum, int RoiNum, string CalibrationFile,
            float Focus, float Sx, float Sy, float StdZ, int ParamType, [MarshalAs(UnmanagedType.LPArray)]float[] Row, [MarshalAs(UnmanagedType.LPArray)]float[] Column);


        /// <summary>
        /// 使用标定结果校正图像
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="CameraNum">相机编号</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTMapCameraImage(int ImageNum, int CameraNum);

        /// <summary>
        /// 保存相机标定结果
        /// </summary>
        /// <param name="CameraNum">相机编号</param>
        /// <param name="FileName">文件名称</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTSaveCalibration(int CameraNum, string FileName);

        /// <summary>
        /// 加载相机标定结果
        /// </summary>
        /// <param name="CameraNum">相机编号</param>
        /// <param name="FileName">文件名称</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTLoadCalibration(int CameraNum, string FileName);

        /// <summary>
        /// 读取标定像素距离
        /// </summary>
        /// <param name="CameraNum">相机编号</param>
        /// <param name="Error">标定平均误差</param>
        /// <param name="PixelDist">像素距离[mm/px]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTReadPixelSize(int CameraNum, ref float Error, ref float PixelDist);

        /// <summary>
        /// 计算双目视差,在使用XTCalibration2TransPoint进行位置计算前调用
        /// </summary>
        /// <param name="CameraNumL">左相机编号</param>
        /// <param name="CameraNumR">右相机编号</param>
        /// <param name="ImageNumL">左图像编号</param>
        /// <param name="ImageNumR">右图像编号</param>
        /// <param name="RoiNumL">左区域编号</param>
        /// <param name="RoiNumR">右区域编号</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTBinocularDisparity(int CameraNumL, int CameraNumR, int ImageNumL, int ImageNumR, int RoiNumL, int RoiNumR);

        /// <summary>
        /// 视差位置提取,给定图像坐标获取相机3D位置信息
        /// </summary>
        /// <param name="CameraNumL">左相机编号</param>
        /// <param name="CameraNumR">右相机编号</param>
        /// <param name="ImageNumL">左图像编号</param>
        /// <param name="ImageNumR">右图像编号</param>
        /// <param name="RowL">左行坐标位置</param>
        /// <param name="ColumnL">左列坐标位置</param>
        /// <param name="RowR">右行坐标位置</param>
        /// <param name="ColumnR">右行坐标位置</param>
        /// <param name="EposX">三维坐标X</param>
        /// <param name="EposY">三维坐标Y</param>
        /// <param name="EposZ">三维坐标Z</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTCalibration2TransPoint(int CameraNumL, int CameraNumR,
            float RowL, float ColumnL, float RowR, float ColumnR, ref float EposX, ref float EposY, ref float EposZ);

        [DllImport("XTVisionc.dll")]
        public static extern int XTReadTransPoint([MarshalAs(UnmanagedType.LPArray)]float[] EposX, [MarshalAs(UnmanagedType.LPArray)]float[] EposY, [MarshalAs(UnmanagedType.LPArray)]float[] EposZ);


        /// <summary>
        /// 图像镶嵌拼接建模
        /// </summary>
        /// <param name="ImageNumL">左图像编号</param>
        /// <param name="ImageNumR">右图像编号</param>
        /// <param name="ImageNumO">拼接结果图像编号</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTImageMosaicTrain(int ImageNumL, int ImageNumR, int ImageNumO);

        /// <summary>
        /// 图像镶嵌拼接工作
        /// </summary>
        /// <param name="ImageNumL">左图像编号</param>
        /// <param name="ImageNumR">右图像编号</param>
        /// <param name="ImageNumO">拼接结果图像编号</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTImageMosaicWork(int ImageNumL, int ImageNumR, int ImageNumO);

        /// <summary>
        /// 图像镶嵌参数加载
        /// </summary>
        /// <param name="ImageNumO">拼接结果图像编号</param>
        /// <param name="FileName">文件名称(不含扩展名)</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTLoadMosaicParam(int ImageNumO, string FileName);

        /// <summary>
        /// 图像镶嵌参数保存
        /// </summary>
        /// <param name="ImageNumO">拼接结果图像编号</param>
        /// <param name="FileName">文件名称(不含扩展名)</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTSaveMosaicParam(int ImageNumO, string FileName);

        /// <summary>
        /// 清理图像镶嵌参数(不清理本地文件)
        /// </summary>
        /// <param name="ImageNumO">拼接结果图像编号</param>
        /// <returns></returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTClearMosaicParam(int ImageNumO);

        #endregion

        //////////////////////////////////////辅助工具类目/////////////////////////////////
        #region XTTools.h

        /// <summary>
        /// 获取鼠标当前位置
        /// </summary>
        /// <param name="WindowNum">窗口编号</param>
        /// <param name="PosX">鼠标所在位置图像坐标X</param>
        /// <param name="PosY">鼠标所在位置图像坐标Y</param>
        /// <param name="Button">当前鼠标按下按键[0无/1左键/2中键/4右键]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTGetMposition(int WindowNum, ref int PosX, ref int PosY, ref int Button);

        /// <summary>
        /// 图像仿射变换(平移、缩放、旋转图像或区域)
        /// </summary>
        /// <param name="SrcNum">源编号(图像编号/区域编号/模板编号)</param>
        /// <param name="DesNum">目标编号(图像编号/区域编号/模板编号)</param>
        /// <param name="Px">参照点X坐标</param>
        /// <param name="Py">参照点Y坐标</param>
        /// <param name="Tx">X方向平移值(0)</param>
        /// <param name="Ty">Y方向平移值(0)</param>
        /// <param name="Sx">X方向缩放比例(1)</param>
        /// <param name="Sy">Y方向缩放比例(1)</param>
        /// <param name="Phi">旋转角度[0,PI]</param>
        /// <param name="TransType">变换类型[0窗口图像/1ROI 区域/2模板区域]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTAffineTrans(int SrcNum, int DesNum, float Px, float Py, float Tx, float Ty, float Sx, float Sy, float Phi, int TransType);

        /// <summary>
        /// 计算点到直线距离
        /// </summary>
        /// <param name="X">点X坐标</param>
        /// <param name="Y">点Y坐标</param>
        /// <param name="X1">直线开始X坐标</param>
        /// <param name="Y1">直线开始Y坐标</param>
        /// <param name="X2">直线结束X坐标</param>
        /// <param name="Y2">直线结束Y坐标</param>
        /// <param name="Distance">点到直线距离</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTDistancePl(float X, float Y, float X1, float Y1, float X2, float Y2, ref float Distance);

        /// <summary>
        /// 图像清晰度评估,方差值越大表示纹理越清晰
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="RoiNum">区域编号,-1评估图像中心</param>
        /// <param name="Method">评估方法['deviation', 'laplace', 'energy', 'brenner', 'tenegrad']</param>
        /// <param name="MeanValue">均值</param>
        /// <param name="Deviation">方差</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTImageFocus(int ImageNum, int RoiNum, string Method, ref float MeanValue, ref float Deviation);

        /// <summary>
        /// 相机自动对焦算法
        /// </summary>
        /// <param name="ImageNum">图像编号</param>
        /// <param name="RoiNum">区域编号</param>
        /// <param name="MeanValue">均值</param>
        /// <param name="Deviation">方差</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTCameraFocus(int ImageNum, int RoiNum, ref float MeanValue, ref float Deviation);

        /// <summary>
        /// 目标坐标矫正(单模板模式)
        /// </summary>
        /// <param name="Msx">模板机械坐标X</param>
        /// <param name="Msy">模板机械坐标Y</param>
        /// <param name="Vsx">模板图像坐标X</param>
        /// <param name="Vsy">模板图像坐标Y</param>
        /// <param name="Mrx">定位机械坐标X</param>
        /// <param name="Mry">定位机械坐标Y</param>
        /// <param name="Vrx">定位图像坐标X</param>
        /// <param name="Vry">定位图像坐标Y</param>
        /// <param name="Vra">定位图像角度(弧度)</param>
        /// <param name="CalibX">相机标定值(mm/px)</param>
        /// <param name="CalibY">相机标定值(mm/px)</param>
        /// <param name="MdirX">机械坐标方向(+1/-1)</param>
        /// <param name="MdirY">机械坐标方向(+1/-1)</param>
        /// <param name="SposX">设定目标坐标X</param>
        /// <param name="SposY">设定目标坐标Y</param>
        /// <param name="EposX">矫正目标坐标X</param>
        /// <param name="EposY">矫正目标坐标Y</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int CalcVposToMpos1(float Msx, float Msy, float Vsx, float Vsy,
            float Mrx, float Mry, float Vrx, float Vry, float Vra,
            float CalibX, float CalibY, int MdirX, int MdirY,
            float SposX, float SposY, ref float EposX, ref float EposY);

        /// <summary>
        /// 目标坐标矫正(双模板模式)
        /// </summary>
        /// <param name="Msx1">模板机械坐标X1</param>
        /// <param name="Msy1">模板机械坐标Y1</param>
        /// <param name="Vsx1">模板图像坐标X1</param>
        /// <param name="Vsy1">模板图像坐标Y1</param>
        /// <param name="Msx2">模板机械坐标X2</param>
        /// <param name="Msy2">模板机械坐标Y2</param>
        /// <param name="Vsx2">模板图像坐标X2</param>
        /// <param name="Vsy2">模板图像坐标Y2</param>
        /// <param name="Mrx1">定位机械坐标X1</param>
        /// <param name="Mry1">定位机械坐标Y1</param>
        /// <param name="Vrx1">定位图像坐标X1</param>
        /// <param name="Vry1">定位图像坐标Y1</param>
        /// <param name="Mrx2">定位机械坐标X2</param>
        /// <param name="Mry2">定位机械坐标Y2</param>
        /// <param name="Vrx2">定位图像坐标X2</param>
        /// <param name="Vry2">定位图像坐标Y2</param>
        /// <param name="CalibX">相机标定值(mm/px)</param>
        /// <param name="CalibY">相机标定值(mm/px)</param>
        /// <param name="MdirX">机械坐标方向(+1/-1)</param>
        /// <param name="MdirY">机械坐标方向(+1/-1)</param>
        /// <param name="SposX">设定目标坐标X</param>
        /// <param name="SposY">设定目标坐标Y</param>
        /// <param name="EposX">矫正目标坐标X</param>
        /// <param name="EposY">矫正目标坐标Y</param>
        /// <param name="EposA">定位角度差值A</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int CalcVposToMpos2A(float Msx1, float Msy1, float Vsx1, float Vsy1, float Msx2, float Msy2, float Vsx2, float Vsy2,
            float Mrx1, float Mry1, float Vrx1, float Vry1, float Mrx2, float Mry2, float Vrx2, float Vry2,
            float CalibX, float CalibY, int MdirX, int MdirY,
            float SposX, float SposY, ref float EposX, ref float EposY, ref float EposA);

        /// <summary>
        /// 目标坐标矫正(双模板模式)
        /// </summary>
        /// <param name="Msx1">设定机械坐标X1</param>
        /// <param name="Msy1">设定机械坐标Y1</param>
        /// <param name="Msx2">设定机械坐标X2</param>
        /// <param name="Msy2">设定机械坐标Y2</param>
        /// <param name="Mrx1">修正机械坐标X1</param>
        /// <param name="Mry1">修正机械坐标Y1</param>
        /// <param name="Mrx2">修正机械坐标X2</param>
        /// <param name="Mry2">修正机械坐标Y2</param>
        /// <param name="SposX">设定目标坐标X</param>
        /// <param name="SposY">设定目标坐标Y</param>
        /// <param name="EposX">矫正目标坐标X</param>
        /// <param name="EposY">矫正目标坐标Y</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int CalcVposToMpos2R(float Msx1, float Msy1, float Msx2, float Msy2, float Mrx1, float Mry1, float Mrx2, float Mry2, float SposX, float SposY, ref float EposX, ref float EposY);

        /// <summary>
        /// 给定平面上三点计算圆心和半径
        /// </summary>
        /// <param name="X1">开始坐标X</param>
        /// <param name="Y1">开始坐标Y</param>
        /// <param name="X2">中间坐标X</param>
        /// <param name="Y2">中间坐标Y</param>
        /// <param name="X3">结束坐标X</param>
        /// <param name="Y3">结束坐标Y</param>
        /// <param name="Cx">圆心坐标X</param>
        /// <param name="Cy">圆心坐标Y</param>
        /// <param name="R">圆半径</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int CalcCenterRadius(float X1, float Y1, float X2, float Y2, float X3, float Y3, ref float Cx, ref float Cy, ref float R);

        /// <summary>
        /// 利用向量叉乘判断旋转方向
        /// </summary>
        /// <param name="X1">开始坐标X</param>
        /// <param name="Y1">开始坐标Y</param>
        /// <param name="X2">中间坐标X</param>
        /// <param name="Y2">中间坐标Y</param>
        /// <param name="X3">结束坐标X</param>
        /// <param name="Y3">结束坐标Y</param>
        /// <param name="Sign">旋转方向[1顺时针/-1逆时针]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int CalcArcCross(float X1, float Y1, float X2, float Y2, float X3, float Y3, ref int Sign);

        /// <summary>
        /// 给出圆心和三个点计算角度
        /// </summary>
        /// <param name="X1">开始坐标X</param>
        /// <param name="Y1">开始坐标Y</param>
        /// <param name="X2">中间坐标X</param>
        /// <param name="Y2">中间坐标Y</param>
        /// <param name="X3">结束坐标X</param>
        /// <param name="Y3">结束坐标Y</param>
        /// <param name="Cx">圆心坐标X</param>
        /// <param name="Cy">圆心坐标Y</param>
        /// <param name="Deg">角度值(弧度)</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int CalcArcAngle(float X1, float Y1, float X2, float Y2, float X3, float Y3, float Cx, float Cy, ref float Deg);

        /// <summary>
        /// 给出坐标集合计算最小外接矩形/圆
        /// </summary>
        /// <param name="XList">给定X坐标集合</param>
        /// <param name="YList">给定Y坐标集合</param>
        /// <param name="ListSize">坐标集合大小</param>
        /// <param name="ShapeType">计算类型[0矩形/1圆形/2带角度矩形]</param>
        /// <param name="CenterX">矩形中心坐标X/圆中心坐标X</param>
        /// <param name="CenterY">矩形中心坐标Y/圆中心坐标Y</param>
        /// <param name="Length1">矩形半长/矩形长轴</param>
        /// <param name="Length2">矩形半高/矩形短轴</param>
        /// <param name="Deg">矩形角度(弧度)</param>
        /// <param name="Radius">圆半径</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int CalcSmallestShape(float[] XList, float[] YList, int ListSize, int ShapeType, ref float CenterX, ref float CenterY, ref float Length1, ref float Length2, ref float Deg, ref float Radius);

        /// <summary>
        /// 计算两条直线的交点
        /// </summary>
        /// <param name="Line1Row1">直线1行开始坐标</param>
        /// <param name="Line1Column1">直线1列开始坐标</param>
        /// <param name="Line1Row2">直线1行结束坐标</param>
        /// <param name="Line1Column2">直线1列结束坐标</param>
        /// <param name="Line2Row1">直线2行开始坐标</param>
        /// <param name="Line2Column1">直线2列开始坐标</param>
        /// <param name="Line2Row2">直线2行结束坐标</param>
        /// <param name="Line2Column2">直线2列结束坐标</param>
        /// <param name="Row">直线交点行坐标</param>
        /// <param name="Column">直线交点列坐标</param>
        /// <param name="Phi">直线之间夹角[-PI,PI]</param>
        /// <param name="IsOverlapping">直线是否平行[0不平行/1平行]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int CalcLinesIntersection(float Line1Row1, float Line1Column1, float Line1Row2, float Line1Column2, float Line2Row1, float Line2Column1, float Line2Row2, float Line2Column2, ref float Row, ref float Column, ref float Phi, ref int IsOverlapping);

        /// <summary>
        /// 计算直线的方向
        /// </summary>
        /// <param name="LineX1">直线开始点坐标X</param>
        /// <param name="LineY1">直线开始点坐标Y</param>
        /// <param name="LineX2">直线结束点坐标X</param>
        /// <param name="LineY2">直线结束点坐标Y</param>
        /// <param name="LineDeg">直线角度[-PI,PI]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int CalcLineOrientation(float LineX1, float LineY1, float LineX2, float LineY2, ref float LineDeg);

        /// <summary>
        /// 最小二值法拟合圆
        /// </summary>
        /// <param name="X">位置坐标数组X</param>
        /// <param name="Y">位置坐标数组Y</param>
        /// <param name="Len">数组长度L</param>
        /// <param name="CenterX">圆心X</param>
        /// <param name="CenterY">圆心X</param>
        /// <param name="Radius">半径R</param>
        /// <param name="Error">综合误差</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int LeastSquaresFitting(float[] X, float[] Y, int Len, ref float CenterX, ref float CenterY, ref float Radius, ref float Error);

        /// <summary>
        /// 计算平面3点构成圆的圆心和半径
        /// </summary>
        /// <param name="X1">第1点X</param>
        /// <param name="Y1">第1点Y</param>
        /// <param name="X2">第2点X</param>
        /// <param name="Y2">第2点Y</param>
        /// <param name="X3">第3点X</param>
        /// <param name="Y3">第3点Y</param>
        /// <param name="Cx">圆心X</param>
        /// <param name="Cy">圆心Y</param>
        /// <param name="Cr">半径R</param>
        /// <returns></returns>
        [DllImport("xTVisionc.dll")]
        public static extern int SolveCenterPointOfCircle2(float X1, float Y1, float X2, float Y2, float X3, float Y3, ref float Cx, ref float Cy, ref float Cr);

        /// <summary>
        /// 计算空间3点构成圆的圆心和半径
        /// </summary>
        /// <param name="X1">第1点X</param>
        /// <param name="Y1">第1点Y</param>
        /// <param name="Z1">第1点Z</param>
        /// <param name="X2">第2点X</param>
        /// <param name="Y2">第2点Y</param>
        /// <param name="Z2">第2点Z</param>
        /// <param name="X3">第3点X</param>
        /// <param name="Y3">第3点Y</param>
        /// <param name="Z3">第3点Z</param>
        /// <param name="Cx">圆心X</param>
        /// <param name="Cy">圆心Y</param>
        /// <param name="Cz">圆心Z</param>
        /// <param name="Cr">半径R</param>
        /// <returns></returns>
        [DllImport("xTVisionc.dll")]
        public static extern int SolveCenterPointOfCircle3(float X1, float Y1, float Z1, float X2, float Y2, float Z2, float X3, float Y3, float Z3, ref float Cx, ref float Cy, ref float Cz, ref float Cr);

        /// <summary>
        /// 计算两条直线夹角/由第一条直线逆时针旋转到第二条直线的角度
        /// </summary>
        /// <param name="LineAX1">直线A开始坐标X</param>
        /// <param name="LineAY1">直线A开始坐标Y</param>
        /// <param name="LineAX2">直线A结束坐标X</param>
        /// <param name="LineAY2">直线A结束坐标Y</param>
        /// <param name="LineBX1">直线B开始坐标X</param>
        /// <param name="LineBY1">直线B开始坐标Y</param>
        /// <param name="LineBX2">直线B结束坐标X</param>
        /// <param name="LineBY2">直线B结束坐标Y</param>
        /// <param name="Angle">直线的夹角[-PI,PI]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int AngleLines(float LineAX1, float LineAY1, float LineAX2, float LineAY2, float LineBX1, float LineBY1, float LineBX2, float LineBY2, ref float Angle);

        /// <summary>
        /// 生成两个坐标系的对应关系
        /// </summary>
        /// <param name="HomMatNum">仿射编号</param>
        /// <param name="Px">坐标系1坐标X样本集</param>
        /// <param name="Py">坐标系1坐标Y样本集</param>
        /// <param name="Qx">坐标系2坐标X样本集</param>
        /// <param name="Qy">坐标系2坐标Y样本集</param>
        /// <param name="ArrayLen">样本数组长度(不能小于3)</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTVectorToHomMat2D(int HomMatNum, [MarshalAs(UnmanagedType.LPArray)]float[] Px, [MarshalAs(UnmanagedType.LPArray)]float[] Py, [MarshalAs(UnmanagedType.LPArray)]float[] Qx, [MarshalAs(UnmanagedType.LPArray)]float[] Qy, int ArrayLen);

        /// <summary>
        /// 获取仿射矩阵综合参数
        /// </summary>
        /// <param name="HomMatNum">仿射矩阵编号</param>
        /// <param name="Sx">缩变比例X</param>
        /// <param name="Sy">缩变比例Y</param>
        /// <param name="Phi">旋转角度A</param>
        /// <param name="Theta">倾斜角度A</param>
        /// <param name="Tx">X方向平移大小</param>
        /// <param name="Ty">Y方向平移大小</param>
        /// <returns></returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTHomMat2DToAffine(int HomMatNum, ref float Sx, ref float Sy, ref float Phi, ref float Theta, ref float Tx, ref float Ty);

        /// <summary>
        /// 保存变换关系模型
        /// </summary>
        /// <param name="HomMatNum">仿射编号</param>
        /// <param name="FileName">文件名称</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTSaveHomMat2D(int HomMatNum, string FileName);

        /// <summary>
        /// 加载变换关系模型
        /// </summary>
        /// <param name="HomMatNum">仿射编号</param>
        /// <param name="FileName">文件名称</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTLoadHomMat2D(int HomMatNum, string FileName);

        /// <summary>
        /// 查看变换关系模型是否存在
        /// </summary>
        /// <param name="HomMatNum">仿射编号</param>
        /// <param name="IsExist">IsExist 存在标志[0矩阵不存在/1矩阵存在]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTIsHomMatExist(int HomMatNum, ref int IsExist);

        /// <summary>
        /// 利用变换关系模型计算出目标坐标
        /// </summary>
        /// <param name="HomMatNum">矩阵编号</param>
        /// <param name="Px">坐标系1坐标X</param>
        /// <param name="Py">坐标系1坐标Y</param>
        /// <param name="Qx">坐标系2坐标X</param>
        /// <param name="Qy">坐标系2坐标Y</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTImagePtToMachinePt(int HomMatNum, float Px, float Py, ref float Qx, ref float Qy);

        /// <summary>
        /// 利用变换关系模型计算出源坐标
        /// </summary>
        /// <param name="HomMatNum">矩阵编号</param>
        /// <param name="Px">坐标系2坐标X</param>
        /// <param name="Py">坐标系2坐标Y</param>
        /// <param name="Qx">坐标系1坐标X</param>
        /// <param name="Qy">坐标系1坐标Y</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTMachinePtToImagePt(int HomMatNum, float Px, float Py, ref float Qx, ref float Qy);

        /// <summary>
        /// 保存矩阵数据
        /// </summary>
        /// <param name="TData">矩阵数据列表</param>
        /// <param name="TLen">数据长度</param>
        /// <param name="FileName">文件名称</param>
        /// <returns></returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTWriteTuple(float[] TData, int TLen, string FileName);

        /// <summary>
        /// 最小二乘法拟合球体
        /// </summary>
        /// <param name="PointsX">坐标数组X</param>
        /// <param name="PointsY">坐标数组Y</param>
        /// <param name="PointsZ">坐标数组Z</param>
        /// <param name="PointsN">数组长度</param>
        /// <param name="CenterX">球体中心坐标X</param>
        /// <param name="CenterY">球体中心坐标Y</param>
        /// <param name="CenterZ">球体中心坐标Z</param>
        /// <param name="Radius">球体半径R</param>
        /// <param name="Error">综合误差</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int XTSphereLeastFit([MarshalAs(UnmanagedType.LPArray)]double[] PointsX, [MarshalAs(UnmanagedType.LPArray)]double[] PointsY, [MarshalAs(UnmanagedType.LPArray)]double[] PointsZ, int PointsN,
            ref double CenterX, ref double CenterY, ref double CenterZ, ref double Radius, ref double Error);

        #endregion

        //////////////////////////////////////授权信息部分/////////////////////////////////
        #region Author.h

        /// <summary>
        /// 获取可传输机器码
        /// </summary>
        /// <param name="buffer_ptr">存放机器码的数组地址</param>
        /// <param name="buffer_len">机器码长度(12)[CODE_SIZE * 2]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int GetMCodeHex([MarshalAs(UnmanagedType.LPArray)]byte[] buffer_ptr, int buffer_len);

        /// <summary>
        /// 获取可传输机器码(机器码2)
        /// </summary>
        /// <param name="buffer_ptr">存放机器码的数组地址</param>
        /// <param name="buffer_len">机器码长度(12)[CODE_SIZE * 2]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int GetMCodeHex1([MarshalAs(UnmanagedType.LPArray)]byte[] buffer_ptr, int buffer_len);

        /// <summary>
        /// 写入授权信息到系统
        /// </summary>
        /// <param name="buffer_author_hex">授权码字符数组</param>
        /// <param name="buffer_len">授权码长度(12)[CODE_SIZE * 2]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int SetAuthorInfo([MarshalAs(UnmanagedType.LPArray)]byte[] buffer_author_hex, int buffer_len);

        /// <summary>
        /// 写入授权信息到系统(授权码2)
        /// </summary>
        /// <param name="buffer_author_hex">授权码字符数组</param>
        /// <param name="buffer_len">授权码长度(12)[CODE_SIZE * 2]</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int SetAuthorInfo1([MarshalAs(UnmanagedType.LPArray)]byte[] buffer_author_hex, int buffer_len);

        /// <summary>
        /// 获取系统授权信息
        /// </summary>
        /// <param name="recode_year">上次授权时间-年</param>
        /// <param name="recode_month">上次授权时间-月</param>
        /// <param name="recode_day">上次授权时间-日</param>
        /// <param name="author_day">剩余授权天数(大于3600表示已永久授权)</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int GetAuthorInfo(ref int recode_year, ref int recode_month, ref int recode_day, ref int author_day);

        /// <summary>
        /// 获取系统授权信息(授权码2)
        /// </summary>
        /// <param name="recode_year">上次授权时间-年</param>
        /// <param name="recode_month">上次授权时间-月</param>
        /// <param name="recode_day">上次授权时间-日</param>
        /// <param name="author_day">剩余授权天数(大于3600表示已永久授权)</param>
        /// <returns>非0表示执行错误</returns>
        [DllImport("XTVisionc.dll")]
        public static extern int GetAuthorInfo1(ref int recode_year, ref int recode_month, ref int recode_day, ref int author_day);

        #endregion
    }
}
