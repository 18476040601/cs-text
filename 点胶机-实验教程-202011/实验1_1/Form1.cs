﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
///视觉库命名空间
using XTVisioncs;

namespace 实验1_1
{
    /// <summary>
    /// 实验1_1：机器视觉基础入门操作
    /// 简述：熟悉使用图像库进行加减乘除镜像等操作
    /// </summary>
    public partial class Form1 : Form
    {
        //每个窗口绑定到一个编号，每个图像绑定到一个图像号

        //给每一个窗口分配一个0--255之间的编号
        //视觉窗口编号
        int windowNum1 = 0;
        int windowNum2 = 2;
        int windowNum3 = 5;

        //给每一个图像分配一个0--255之间的编号
        //视觉图像编号
        int imageNum1 = 0;
        int imageNum2 = 2;
        int imageNum3 = 3;

        public Form1()
        {
            InitializeComponent();
            InitializeForm();
        }

        //窗体辅助初始化
        void InitializeForm()
        {
            //初始化视觉库
            XTVision_V1.XTInitialize(true);

            //视觉窗口背景色
            pictureBox1.BackColor = Color.LightGray;
            pictureBox2.BackColor = Color.LightGray;
            pictureBox3.BackColor = Color.LightGray;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                //从文件中加载图像
                int dispType = 0;   //图像显示方式
                string filePath = @"..\\..\\..\\image\circle_plate_01.png";
                Size imgSize = Image.FromFile(filePath).Size;
                int imageWidth = imgSize.Width;
                int imageHeight = imgSize.Height;

                //关闭视觉窗口windowNum1
                XTVision_V1.XTCloseWindow(windowNum1);
                //初始化视觉窗口/参数为窗口号，将要绑定到的控件句柄，图像宽，图像高
                XTVision_V1.XTInitWindow(windowNum1, pictureBox1.Handle, imageWidth, imageHeight);
                //从文件中加载图像到图像imageNum1中
                XTVision_V1.XTLoadImage(imageNum1, filePath);
                //将图像imageNum1显示到窗口windowNum1中
                XTVision_V1.XTDisplayPicture(windowNum1, imageNum1, dispType);
            }
            catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.Message); }

            try
            {
                //从文件中加载图像
                int dispType = 0;
                string filePath = @"..\\..\\..\\image\circle_plate_02.png";
                Size imgSize = Image.FromFile(filePath).Size;
                int imageWidth = imgSize.Width;
                int imageHeight = imgSize.Height;

                //关闭视觉窗口windowNum2
                XTVision_V1.XTCloseWindow(windowNum2);
                //初始化视觉窗口/参数为窗口号，将要绑定到的控件句柄，图像宽，图像高
                XTVision_V1.XTInitWindow(windowNum2, pictureBox2.Handle, imageWidth, imageHeight);
                //从文件中加载图像到图像imageNum2中
                XTVision_V1.XTLoadImage(imageNum2, filePath);
                //将图像imageNum2显示到窗口windowNum2中
                XTVision_V1.XTDisplayPicture(windowNum2, imageNum2, dispType);
            }
            catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.Message); }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            //反初始化视觉库/释放资源
            XTVision_V1.XTUninitialize();
        }

        private void buttonLoadImage1_Click(object sender, EventArgs e)
        {
            string resultFile = null;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image Files(*.BMP;*.PNG;*.JPG;*.GIF)|*.BMP;*.PNG;*.JPG;*.GIF|All files(*.*)|*.* ";
            openFileDialog.FilterIndex = 0;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                resultFile = openFileDialog.FileName;
                if (string.IsNullOrEmpty(resultFile)) return;
                try
                {
                    int dispType = 0;   //图像显示方式
                    Size imgSize = Image.FromFile(resultFile).Size;
                    int imageWidth = imgSize.Width;
                    int imageHeight = imgSize.Height;

                    //关闭视觉窗口windowNum1
                    XTVision_V1.XTCloseWindow(windowNum1);
                    //初始化视觉窗口/参数为窗口号，将要绑定到的控件句柄，图像宽，图像高
                    XTVision_V1.XTInitWindow(windowNum1, pictureBox1.Handle, imageWidth, imageHeight);
                    //从文件中加载图像到图像imageNum1中
                    XTVision_V1.XTLoadImage(imageNum1, resultFile);
                    //将图像imageNum1显示到窗口windowNum1中
                    XTVision_V1.XTDisplayPicture(windowNum1, imageNum1, dispType);
                }
                catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.Message); }
            }
        }

        private void buttonLoadImage2_Click(object sender, EventArgs e)
        {
            string resultFile = null;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image Files(*.BMP;*.PNG;*.JPG;*.GIF)|*.BMP;*.PNG;*.JPG;*.GIF|All files(*.*)|*.* ";
            openFileDialog.FilterIndex = 0;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                resultFile = openFileDialog.FileName;
                if (string.IsNullOrEmpty(resultFile)) return;
                try
                {
                    int dispType = 0;   //图像显示方式
                    Size imgSize = Image.FromFile(resultFile).Size;
                    int imageWidth = imgSize.Width;
                    int imageHeight = imgSize.Height;

                    //关闭视觉窗口windowNum2
                    XTVision_V1.XTCloseWindow(windowNum2);
                    //初始化视觉窗口/参数为窗口号，将要绑定到的控件句柄，图像宽，图像高
                    XTVision_V1.XTInitWindow(windowNum2, pictureBox2.Handle, imageWidth, imageHeight);
                    //从文件中加载图像到图像imageNum2中
                    XTVision_V1.XTLoadImage(imageNum2, resultFile);
                    //将图像imageNum2显示到窗口windowNum2中
                    XTVision_V1.XTDisplayPicture(windowNum2, imageNum2, dispType);
                }
                catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.Message); }
            }
        }

        private void buttonCopyImage_Click(object sender, EventArgs e)
        {
            int dispType = 0;   //图像显示方式
            int imageWidth = 0, imageHeight = 0;
            //获取图像尺寸
            XTVision_V1.XTGetImageSize(imageNum1, ref imageWidth, ref imageHeight);
            //将图像imageNum1拷贝到图像imageNum2
            XTVision_V1.XTCopyImage(imageNum1, imageNum3);

            //关闭视觉窗口windowNum3
            XTVision_V1.XTCloseWindow(windowNum3);
            //初始化视觉窗口/参数为窗口号，将要绑定到的控件句柄，图像宽，图像高
            XTVision_V1.XTInitWindow(windowNum3, pictureBox3.Handle, imageWidth, imageHeight);
            //将图像imageNum3显示到窗口windowNum3中
            XTVision_V1.XTDisplayPicture(windowNum3, imageNum3, dispType);
        }

        private void buttonAddImage_Click(object sender, EventArgs e)
        {
            int dispType = 0;   //图像显示方式
            int imageWidth1 = 0, imageHeight1 = 0;
            int imageWidth2 = 0, imageHeight2 = 0;
            //获取图像尺寸
            XTVision_V1.XTGetImageSize(imageNum1, ref imageWidth1, ref imageHeight1);
            XTVision_V1.XTGetImageSize(imageNum2, ref imageWidth2, ref imageHeight2);

            //首先判断图像是否尺寸相等
            if (imageWidth1 == imageWidth2 && imageHeight1 == imageHeight2)
            {
                //imageNum1图像和imageNum2进行相加操作，结果放到图像imageNum3中
                XTVision_V1.XTAddImage(imageNum1, imageNum2, imageNum3, 1, 0);

                //关闭视觉窗口windowNum3
                XTVision_V1.XTCloseWindow(windowNum3);
                //初始化视觉窗口/参数为窗口号，将要绑定到的控件句柄，图像宽，图像高
                XTVision_V1.XTInitWindow(windowNum3, pictureBox3.Handle, imageWidth1, imageHeight1);
                //将图像imageNum3显示到窗口windowNum3中
                XTVision_V1.XTDisplayPicture(windowNum3, imageNum3, dispType);
            }
        }

        private void buttonSubImage_Click(object sender, EventArgs e)
        {
            int dispType = 0;   //图像显示方式
            int imageWidth1 = 0, imageHeight1 = 0;
            int imageWidth2 = 0, imageHeight2 = 0;
            //获取图像尺寸
            XTVision_V1.XTGetImageSize(imageNum1, ref imageWidth1, ref imageHeight1);
            XTVision_V1.XTGetImageSize(imageNum2, ref imageWidth2, ref imageHeight2);

            //首先判断图像是否尺寸相等
            if (imageWidth1 == imageWidth2 && imageHeight1 == imageHeight2)
            {
                //imageNum1图像和imageNum2进行相减操作，结果放到图像imageNum3中
                XTVision_V1.XTSubImage(imageNum1, imageNum2, imageNum3, 1, 0);

                //关闭视觉窗口windowNum3
                XTVision_V1.XTCloseWindow(windowNum3);
                //初始化视觉窗口/参数为窗口号，将要绑定到的控件句柄，图像宽，图像高
                XTVision_V1.XTInitWindow(windowNum3, pictureBox3.Handle, imageWidth1, imageHeight1);
                //将图像imageNum3显示到窗口windowNum3中
                XTVision_V1.XTDisplayPicture(windowNum3, imageNum3, dispType);
            }
        }

        private void buttonMultImage_Click(object sender, EventArgs e)
        {
            int dispType = 0;   //图像显示方式
            int imageWidth1 = 0, imageHeight1 = 0;
            int imageWidth2 = 0, imageHeight2 = 0;
            //获取图像尺寸
            XTVision_V1.XTGetImageSize(imageNum1, ref imageWidth1, ref imageHeight1);
            XTVision_V1.XTGetImageSize(imageNum2, ref imageWidth2, ref imageHeight2);

            //首先判断图像是否尺寸相等
            if (imageWidth1 == imageWidth2 && imageHeight1 == imageHeight2)
            {
                //imageNum1图像和imageNum2进行相乘操作，结果放到图像imageNum3中
                XTVision_V1.XTMultImage(imageNum1, imageNum2, imageNum3, 1, 0);

                //关闭视觉窗口windowNum3
                XTVision_V1.XTCloseWindow(windowNum3);
                //初始化视觉窗口/参数为窗口号，将要绑定到的控件句柄，图像宽，图像高
                XTVision_V1.XTInitWindow(windowNum3, pictureBox3.Handle, imageWidth1, imageHeight1);
                //将图像imageNum3显示到窗口windowNum3中
                XTVision_V1.XTDisplayPicture(windowNum3, imageNum3, dispType);
            }
        }

        private void buttonDivImage_Click(object sender, EventArgs e)
        {
            int dispType = 0;   //图像显示方式
            int imageWidth1 = 0, imageHeight1 = 0;
            int imageWidth2 = 0, imageHeight2 = 0;
            //获取图像尺寸
            XTVision_V1.XTGetImageSize(imageNum1, ref imageWidth1, ref imageHeight1);
            XTVision_V1.XTGetImageSize(imageNum2, ref imageWidth2, ref imageHeight2);

            //首先判断图像是否尺寸相等
            if (imageWidth1 == imageWidth2 && imageHeight1 == imageHeight2)
            {
                //imageNum1图像和imageNum2进行相除操作，结果放到图像imageNum3中
                XTVision_V1.XTDivImage(imageNum1, imageNum2, imageNum3, 1, 0);

                //关闭视觉窗口windowNum3
                XTVision_V1.XTCloseWindow(windowNum3);
                //初始化视觉窗口/参数为窗口号，将要绑定到的控件句柄，图像宽，图像高
                XTVision_V1.XTInitWindow(windowNum3, pictureBox3.Handle, imageWidth1, imageHeight1);
                //将图像imageNum3显示到窗口windowNum3中
                XTVision_V1.XTDisplayPicture(windowNum3, imageNum3, dispType);
            }
        }

        private void buttonMirrorImage_Click(object sender, EventArgs e)
        {
            int dispType = 0;   //图像显示方式
            int imageWidth = 0, imageHeight = 0;
            //获取图像尺寸
            XTVision_V1.XTGetImageSize(imageNum1, ref imageWidth, ref imageHeight);
            //对图像imageNum1进行镜像，结果放到图像imageNum3中
            XTVision_V1.XTMirrorImage(imageNum1, imageNum3, 1);     //左右镜像
            //XTVision_V1.XTMirrorImage(imageNum1, imageNum3, 2);     //上下镜像

            //关闭视觉窗口windowNum3
            XTVision_V1.XTCloseWindow(windowNum3);
            //初始化视觉窗口/参数为窗口号，将要绑定到的控件句柄，图像宽，图像高
            XTVision_V1.XTInitWindow(windowNum3, pictureBox3.Handle, imageWidth, imageHeight);
            //将图像imageNum3显示到窗口windowNum3中
            XTVision_V1.XTDisplayPicture(windowNum3, imageNum3, dispType);
        }

        private void buttonZoomImage_Click(object sender, EventArgs e)
        {
            int dispType = 0;   //图像显示方式
            int imageWidth = 0, imageHeight = 0;
            //获取图像尺寸
            XTVision_V1.XTGetImageSize(imageNum1, ref imageWidth, ref imageHeight);
            //对图像imageNum1进行缩放，结果放到图像imageNum3中
            XTVision_V1.XTZoomImage(imageNum1, imageNum3, 0.8f, 0.8f, 0);     //0.8倍缩放

            //关闭视觉窗口windowNum3
            XTVision_V1.XTCloseWindow(windowNum3);
            //初始化视觉窗口/参数为窗口号，将要绑定到的控件句柄，图像宽，图像高
            XTVision_V1.XTInitWindow(windowNum3, pictureBox3.Handle, imageWidth, imageHeight);
            //将图像imageNum3显示到窗口windowNum3中
            XTVision_V1.XTDisplayPicture(windowNum3, imageNum3, dispType);
        }
    }
}
