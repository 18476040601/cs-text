﻿namespace 实验1_1
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonZoomImage = new System.Windows.Forms.Button();
            this.buttonMirrorImage = new System.Windows.Forms.Button();
            this.buttonDivImage = new System.Windows.Forms.Button();
            this.buttonMultImage = new System.Windows.Forms.Button();
            this.buttonSubImage = new System.Windows.Forms.Button();
            this.buttonAddImage = new System.Windows.Forms.Button();
            this.buttonCopyImage = new System.Windows.Forms.Button();
            this.buttonLoadImage2 = new System.Windows.Forms.Button();
            this.buttonLoadImage1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonZoomImage
            // 
            this.buttonZoomImage.Location = new System.Drawing.Point(850, 216);
            this.buttonZoomImage.Name = "buttonZoomImage";
            this.buttonZoomImage.Size = new System.Drawing.Size(100, 28);
            this.buttonZoomImage.TabIndex = 10;
            this.buttonZoomImage.Text = "图像缩放";
            this.buttonZoomImage.UseVisualStyleBackColor = true;
            this.buttonZoomImage.Click += new System.EventHandler(this.buttonZoomImage_Click);
            // 
            // buttonMirrorImage
            // 
            this.buttonMirrorImage.Location = new System.Drawing.Point(850, 182);
            this.buttonMirrorImage.Name = "buttonMirrorImage";
            this.buttonMirrorImage.Size = new System.Drawing.Size(100, 28);
            this.buttonMirrorImage.TabIndex = 13;
            this.buttonMirrorImage.Text = "图像镜像";
            this.buttonMirrorImage.UseVisualStyleBackColor = true;
            this.buttonMirrorImage.Click += new System.EventHandler(this.buttonMirrorImage_Click);
            // 
            // buttonDivImage
            // 
            this.buttonDivImage.Location = new System.Drawing.Point(850, 148);
            this.buttonDivImage.Name = "buttonDivImage";
            this.buttonDivImage.Size = new System.Drawing.Size(100, 28);
            this.buttonDivImage.TabIndex = 12;
            this.buttonDivImage.Text = "图像相除";
            this.buttonDivImage.UseVisualStyleBackColor = true;
            this.buttonDivImage.Click += new System.EventHandler(this.buttonDivImage_Click);
            // 
            // buttonMultImage
            // 
            this.buttonMultImage.Location = new System.Drawing.Point(850, 114);
            this.buttonMultImage.Name = "buttonMultImage";
            this.buttonMultImage.Size = new System.Drawing.Size(100, 28);
            this.buttonMultImage.TabIndex = 11;
            this.buttonMultImage.Text = "图像相乘";
            this.buttonMultImage.UseVisualStyleBackColor = true;
            this.buttonMultImage.Click += new System.EventHandler(this.buttonMultImage_Click);
            // 
            // buttonSubImage
            // 
            this.buttonSubImage.Location = new System.Drawing.Point(850, 80);
            this.buttonSubImage.Name = "buttonSubImage";
            this.buttonSubImage.Size = new System.Drawing.Size(100, 28);
            this.buttonSubImage.TabIndex = 14;
            this.buttonSubImage.Text = "图像相减";
            this.buttonSubImage.UseVisualStyleBackColor = true;
            this.buttonSubImage.Click += new System.EventHandler(this.buttonSubImage_Click);
            // 
            // buttonAddImage
            // 
            this.buttonAddImage.Location = new System.Drawing.Point(850, 46);
            this.buttonAddImage.Name = "buttonAddImage";
            this.buttonAddImage.Size = new System.Drawing.Size(100, 28);
            this.buttonAddImage.TabIndex = 17;
            this.buttonAddImage.Text = "图像相加";
            this.buttonAddImage.UseVisualStyleBackColor = true;
            this.buttonAddImage.Click += new System.EventHandler(this.buttonAddImage_Click);
            // 
            // buttonCopyImage
            // 
            this.buttonCopyImage.Location = new System.Drawing.Point(850, 12);
            this.buttonCopyImage.Name = "buttonCopyImage";
            this.buttonCopyImage.Size = new System.Drawing.Size(100, 28);
            this.buttonCopyImage.TabIndex = 16;
            this.buttonCopyImage.Text = "图像拷贝";
            this.buttonCopyImage.UseVisualStyleBackColor = true;
            this.buttonCopyImage.Click += new System.EventHandler(this.buttonCopyImage_Click);
            // 
            // buttonLoadImage2
            // 
            this.buttonLoadImage2.Location = new System.Drawing.Point(718, 268);
            this.buttonLoadImage2.Name = "buttonLoadImage2";
            this.buttonLoadImage2.Size = new System.Drawing.Size(100, 28);
            this.buttonLoadImage2.TabIndex = 15;
            this.buttonLoadImage2.Text = "载入图像二";
            this.buttonLoadImage2.UseVisualStyleBackColor = true;
            this.buttonLoadImage2.Click += new System.EventHandler(this.buttonLoadImage2_Click);
            // 
            // buttonLoadImage1
            // 
            this.buttonLoadImage1.Location = new System.Drawing.Point(312, 268);
            this.buttonLoadImage1.Name = "buttonLoadImage1";
            this.buttonLoadImage1.Size = new System.Drawing.Size(100, 28);
            this.buttonLoadImage1.TabIndex = 9;
            this.buttonLoadImage1.Text = "载入图像一";
            this.buttonLoadImage1.UseVisualStyleBackColor = true;
            this.buttonLoadImage1.Click += new System.EventHandler(this.buttonLoadImage1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(479, 269);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 7;
            this.label2.Text = "图像二";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(70, 558);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "图像三";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(70, 269);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 8;
            this.label1.Text = "图像一";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(418, 12);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(400, 250);
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Location = new System.Drawing.Point(12, 302);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(400, 250);
            this.pictureBox3.TabIndex = 3;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(400, 250);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 582);
            this.Controls.Add(this.buttonZoomImage);
            this.Controls.Add(this.buttonMirrorImage);
            this.Controls.Add(this.buttonDivImage);
            this.Controls.Add(this.buttonMultImage);
            this.Controls.Add(this.buttonSubImage);
            this.Controls.Add(this.buttonAddImage);
            this.Controls.Add(this.buttonCopyImage);
            this.Controls.Add(this.buttonLoadImage2);
            this.Controls.Add(this.buttonLoadImage1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonZoomImage;
        private System.Windows.Forms.Button buttonMirrorImage;
        private System.Windows.Forms.Button buttonDivImage;
        private System.Windows.Forms.Button buttonMultImage;
        private System.Windows.Forms.Button buttonSubImage;
        private System.Windows.Forms.Button buttonAddImage;
        private System.Windows.Forms.Button buttonCopyImage;
        private System.Windows.Forms.Button buttonLoadImage2;
        private System.Windows.Forms.Button buttonLoadImage1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

