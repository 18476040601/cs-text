综合实验的例子，需要连接相机，并且需要检查图像显示是否需要进行镜像处理，东西进入相机视野的方向需要和实际方向保持一致

把需要用到的库文件拷贝到运行目录下面
运动相关的：zmotion.dll  zauxdll.dll
视觉相关的：XTBase.dll XTDraws.dll XTVisionp.dll XTVisionc.dll XSvision.dll

相机使用需要在工程中引用DVPCameraCS.dll库文件，该文件位于相机SDK安装目录下
