﻿namespace 实验2_1
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.radioButtonAxisU = new System.Windows.Forms.RadioButton();
            this.radioButtonAxisZ = new System.Windows.Forms.RadioButton();
            this.radioButtonAxisY = new System.Windows.Forms.RadioButton();
            this.radioButtonAxisX = new System.Windows.Forms.RadioButton();
            this.numericUpDownPitch = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownHighLimit = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownLowLimit = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDownPPR = new System.Windows.Forms.NumericUpDown();
            this.comboBoxSensorMode = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBoxAxisType = new System.Windows.Forms.ComboBox();
            this.comboBoxPulseMode = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonStop = new System.Windows.Forms.Button();
            this.buttonHome = new System.Windows.Forms.Button();
            this.buttonZero = new System.Windows.Forms.Button();
            this.buttonMoveAdd = new System.Windows.Forms.Button();
            this.buttonSaveInfo = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.buttonMoveSub = new System.Windows.Forms.Button();
            this.radioButtonLow = new System.Windows.Forms.RadioButton();
            this.radioButtonHigh = new System.Windows.Forms.RadioButton();
            this.comboBoxCardip = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonCardClose = new System.Windows.Forms.Button();
            this.buttonCardLink = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label_U = new System.Windows.Forms.Label();
            this.label_Z = new System.Windows.Forms.Label();
            this.label_Y = new System.Windows.Forms.Label();
            this.label_X = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.timerMotor = new System.Windows.Forms.Timer(this.components);
            this.timerStatus = new System.Windows.Forms.Timer(this.components);
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.numericUpDownDTime = new System.Windows.Forms.NumericUpDown();
            this.label18 = new System.Windows.Forms.Label();
            this.numericUpDownATime = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.numericUpDownSpeed = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPitch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHighLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLowLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPPR)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownATime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpeed)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.radioButtonAxisU);
            this.groupBox3.Controls.Add(this.radioButtonAxisZ);
            this.groupBox3.Controls.Add(this.radioButtonAxisY);
            this.groupBox3.Controls.Add(this.radioButtonAxisX);
            this.groupBox3.Controls.Add(this.numericUpDownPitch);
            this.groupBox3.Controls.Add(this.numericUpDownHighLimit);
            this.groupBox3.Controls.Add(this.numericUpDownLowLimit);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.numericUpDownPPR);
            this.groupBox3.Controls.Add(this.comboBoxSensorMode);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.comboBoxAxisType);
            this.groupBox3.Controls.Add(this.comboBoxPulseMode);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(12, 110);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(300, 197);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "运动轴参数";
            // 
            // radioButtonAxisU
            // 
            this.radioButtonAxisU.AutoSize = true;
            this.radioButtonAxisU.Location = new System.Drawing.Point(236, 20);
            this.radioButtonAxisU.Name = "radioButtonAxisU";
            this.radioButtonAxisU.Size = new System.Drawing.Size(41, 16);
            this.radioButtonAxisU.TabIndex = 6;
            this.radioButtonAxisU.TabStop = true;
            this.radioButtonAxisU.Text = "U轴";
            this.radioButtonAxisU.UseVisualStyleBackColor = true;
            this.radioButtonAxisU.CheckedChanged += new System.EventHandler(this.radioButtonAxisU_CheckedChanged);
            // 
            // radioButtonAxisZ
            // 
            this.radioButtonAxisZ.AutoSize = true;
            this.radioButtonAxisZ.Location = new System.Drawing.Point(182, 20);
            this.radioButtonAxisZ.Name = "radioButtonAxisZ";
            this.radioButtonAxisZ.Size = new System.Drawing.Size(41, 16);
            this.radioButtonAxisZ.TabIndex = 6;
            this.radioButtonAxisZ.TabStop = true;
            this.radioButtonAxisZ.Text = "Z轴";
            this.radioButtonAxisZ.UseVisualStyleBackColor = true;
            this.radioButtonAxisZ.CheckedChanged += new System.EventHandler(this.radioButtonAxisZ_CheckedChanged);
            // 
            // radioButtonAxisY
            // 
            this.radioButtonAxisY.AutoSize = true;
            this.radioButtonAxisY.Location = new System.Drawing.Point(127, 20);
            this.radioButtonAxisY.Name = "radioButtonAxisY";
            this.radioButtonAxisY.Size = new System.Drawing.Size(41, 16);
            this.radioButtonAxisY.TabIndex = 6;
            this.radioButtonAxisY.TabStop = true;
            this.radioButtonAxisY.Text = "Y轴";
            this.radioButtonAxisY.UseVisualStyleBackColor = true;
            this.radioButtonAxisY.CheckedChanged += new System.EventHandler(this.radioButtonAxisY_CheckedChanged);
            // 
            // radioButtonAxisX
            // 
            this.radioButtonAxisX.AutoSize = true;
            this.radioButtonAxisX.Location = new System.Drawing.Point(72, 21);
            this.radioButtonAxisX.Name = "radioButtonAxisX";
            this.radioButtonAxisX.Size = new System.Drawing.Size(41, 16);
            this.radioButtonAxisX.TabIndex = 6;
            this.radioButtonAxisX.TabStop = true;
            this.radioButtonAxisX.Text = "X轴";
            this.radioButtonAxisX.UseVisualStyleBackColor = true;
            this.radioButtonAxisX.CheckedChanged += new System.EventHandler(this.radioButtonAxisX_CheckedChanged);
            // 
            // numericUpDownPitch
            // 
            this.numericUpDownPitch.Location = new System.Drawing.Point(206, 125);
            this.numericUpDownPitch.Name = "numericUpDownPitch";
            this.numericUpDownPitch.Size = new System.Drawing.Size(65, 21);
            this.numericUpDownPitch.TabIndex = 2;
            // 
            // numericUpDownHighLimit
            // 
            this.numericUpDownHighLimit.Location = new System.Drawing.Point(206, 155);
            this.numericUpDownHighLimit.Name = "numericUpDownHighLimit";
            this.numericUpDownHighLimit.Size = new System.Drawing.Size(65, 21);
            this.numericUpDownHighLimit.TabIndex = 2;
            // 
            // numericUpDownLowLimit
            // 
            this.numericUpDownLowLimit.Location = new System.Drawing.Point(71, 155);
            this.numericUpDownLowLimit.Name = "numericUpDownLowLimit";
            this.numericUpDownLowLimit.Size = new System.Drawing.Size(52, 21);
            this.numericUpDownLowLimit.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "原点电平";
            // 
            // numericUpDownPPR
            // 
            this.numericUpDownPPR.Location = new System.Drawing.Point(71, 125);
            this.numericUpDownPPR.Name = "numericUpDownPPR";
            this.numericUpDownPPR.Size = new System.Drawing.Size(52, 21);
            this.numericUpDownPPR.TabIndex = 2;
            // 
            // comboBoxSensorMode
            // 
            this.comboBoxSensorMode.FormattingEnabled = true;
            this.comboBoxSensorMode.Location = new System.Drawing.Point(71, 98);
            this.comboBoxSensorMode.Name = "comboBoxSensorMode";
            this.comboBoxSensorMode.Size = new System.Drawing.Size(210, 20);
            this.comboBoxSensorMode.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(151, 160);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 0;
            this.label7.Text = "行程上限";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(277, 134);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(17, 12);
            this.label11.TabIndex = 0;
            this.label11.Text = "mm";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(277, 163);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(17, 12);
            this.label10.TabIndex = 0;
            this.label10.Text = "mm";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(129, 163);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(17, 12);
            this.label9.TabIndex = 0;
            this.label9.Text = "mm";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(129, 134);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(23, 12);
            this.label8.TabIndex = 0;
            this.label8.Text = "P/R";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(171, 130);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 12);
            this.label5.TabIndex = 0;
            this.label5.Text = "螺距";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 160);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 0;
            this.label6.Text = "行程下限";
            // 
            // comboBoxAxisType
            // 
            this.comboBoxAxisType.FormattingEnabled = true;
            this.comboBoxAxisType.Location = new System.Drawing.Point(71, 72);
            this.comboBoxAxisType.Name = "comboBoxAxisType";
            this.comboBoxAxisType.Size = new System.Drawing.Size(210, 20);
            this.comboBoxAxisType.TabIndex = 1;
            // 
            // comboBoxPulseMode
            // 
            this.comboBoxPulseMode.FormattingEnabled = true;
            this.comboBoxPulseMode.Location = new System.Drawing.Point(71, 46);
            this.comboBoxPulseMode.Name = "comboBoxPulseMode";
            this.comboBoxPulseMode.Size = new System.Drawing.Size(210, 20);
            this.comboBoxPulseMode.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "电机细分";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(12, 76);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(53, 12);
            this.label20.TabIndex = 0;
            this.label20.Text = "轴 类 型";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "脉冲方式";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "运动轴号";
            // 
            // buttonStop
            // 
            this.buttonStop.Location = new System.Drawing.Point(350, 58);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(100, 32);
            this.buttonStop.TabIndex = 11;
            this.buttonStop.Text = "停止";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // buttonHome
            // 
            this.buttonHome.Location = new System.Drawing.Point(350, 20);
            this.buttonHome.Name = "buttonHome";
            this.buttonHome.Size = new System.Drawing.Size(100, 32);
            this.buttonHome.TabIndex = 12;
            this.buttonHome.Text = "回零";
            this.buttonHome.UseVisualStyleBackColor = true;
            this.buttonHome.Click += new System.EventHandler(this.buttonHome_Click);
            // 
            // buttonZero
            // 
            this.buttonZero.Location = new System.Drawing.Point(350, 96);
            this.buttonZero.Name = "buttonZero";
            this.buttonZero.Size = new System.Drawing.Size(100, 32);
            this.buttonZero.TabIndex = 10;
            this.buttonZero.Text = "位置清零";
            this.buttonZero.UseVisualStyleBackColor = true;
            this.buttonZero.Click += new System.EventHandler(this.buttonZero_Click);
            // 
            // buttonMoveAdd
            // 
            this.buttonMoveAdd.Location = new System.Drawing.Point(12, 44);
            this.buttonMoveAdd.Name = "buttonMoveAdd";
            this.buttonMoveAdd.Size = new System.Drawing.Size(100, 32);
            this.buttonMoveAdd.TabIndex = 3;
            this.buttonMoveAdd.Text = "正向运动";
            this.buttonMoveAdd.UseVisualStyleBackColor = true;
            this.buttonMoveAdd.MouseDown += new System.Windows.Forms.MouseEventHandler(this.buttonMoveAdd_MouseDown);
            this.buttonMoveAdd.MouseUp += new System.Windows.Forms.MouseEventHandler(this.buttonMoveAdd_MouseUp);
            // 
            // buttonSaveInfo
            // 
            this.buttonSaveInfo.Location = new System.Drawing.Point(212, 526);
            this.buttonSaveInfo.Name = "buttonSaveInfo";
            this.buttonSaveInfo.Size = new System.Drawing.Size(100, 32);
            this.buttonSaveInfo.TabIndex = 9;
            this.buttonSaveInfo.Text = "保存参数";
            this.buttonSaveInfo.UseVisualStyleBackColor = true;
            this.buttonSaveInfo.Click += new System.EventHandler(this.buttonSaveInfo_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 24);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 12);
            this.label12.TabIndex = 0;
            this.label12.Text = "控制器IP";
            // 
            // buttonMoveSub
            // 
            this.buttonMoveSub.Location = new System.Drawing.Point(12, 82);
            this.buttonMoveSub.Name = "buttonMoveSub";
            this.buttonMoveSub.Size = new System.Drawing.Size(100, 32);
            this.buttonMoveSub.TabIndex = 3;
            this.buttonMoveSub.Text = "反向运动";
            this.buttonMoveSub.UseVisualStyleBackColor = true;
            this.buttonMoveSub.MouseDown += new System.Windows.Forms.MouseEventHandler(this.buttonMoveSub_MouseDown);
            this.buttonMoveSub.MouseUp += new System.Windows.Forms.MouseEventHandler(this.buttonMoveSub_MouseUp);
            // 
            // radioButtonLow
            // 
            this.radioButtonLow.AutoSize = true;
            this.radioButtonLow.Location = new System.Drawing.Point(12, 20);
            this.radioButtonLow.Name = "radioButtonLow";
            this.radioButtonLow.Size = new System.Drawing.Size(47, 16);
            this.radioButtonLow.TabIndex = 5;
            this.radioButtonLow.TabStop = true;
            this.radioButtonLow.Text = "低速";
            this.radioButtonLow.UseVisualStyleBackColor = true;
            // 
            // radioButtonHigh
            // 
            this.radioButtonHigh.AutoSize = true;
            this.radioButtonHigh.Location = new System.Drawing.Point(65, 20);
            this.radioButtonHigh.Name = "radioButtonHigh";
            this.radioButtonHigh.Size = new System.Drawing.Size(47, 16);
            this.radioButtonHigh.TabIndex = 5;
            this.radioButtonHigh.TabStop = true;
            this.radioButtonHigh.Text = "高速";
            this.radioButtonHigh.UseVisualStyleBackColor = true;
            // 
            // comboBoxCardip
            // 
            this.comboBoxCardip.FormattingEnabled = true;
            this.comboBoxCardip.Location = new System.Drawing.Point(71, 20);
            this.comboBoxCardip.Name = "comboBoxCardip";
            this.comboBoxCardip.Size = new System.Drawing.Size(210, 20);
            this.comboBoxCardip.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboBoxCardip);
            this.groupBox2.Controls.Add(this.buttonCardClose);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.buttonCardLink);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(300, 92);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "控制器连接";
            // 
            // buttonCardClose
            // 
            this.buttonCardClose.Location = new System.Drawing.Point(20, 46);
            this.buttonCardClose.Name = "buttonCardClose";
            this.buttonCardClose.Size = new System.Drawing.Size(100, 32);
            this.buttonCardClose.TabIndex = 3;
            this.buttonCardClose.Text = "断开";
            this.buttonCardClose.UseVisualStyleBackColor = true;
            this.buttonCardClose.Click += new System.EventHandler(this.buttonCardClose_Click);
            // 
            // buttonCardLink
            // 
            this.buttonCardLink.Location = new System.Drawing.Point(181, 46);
            this.buttonCardLink.Name = "buttonCardLink";
            this.buttonCardLink.Size = new System.Drawing.Size(100, 32);
            this.buttonCardLink.TabIndex = 3;
            this.buttonCardLink.Text = "连接";
            this.buttonCardLink.UseVisualStyleBackColor = true;
            this.buttonCardLink.Click += new System.EventHandler(this.buttonCardLink_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label_U);
            this.groupBox1.Controls.Add(this.label_Z);
            this.groupBox1.Controls.Add(this.label_Y);
            this.groupBox1.Controls.Add(this.label_X);
            this.groupBox1.Location = new System.Drawing.Point(12, 429);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(300, 91);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "轴状态显示";
            // 
            // label_U
            // 
            this.label_U.AutoSize = true;
            this.label_U.Location = new System.Drawing.Point(171, 55);
            this.label_U.Name = "label_U";
            this.label_U.Size = new System.Drawing.Size(77, 12);
            this.label_U.TabIndex = 0;
            this.label_U.Text = "U 轴停止 ：0";
            // 
            // label_Z
            // 
            this.label_Z.AutoSize = true;
            this.label_Z.Location = new System.Drawing.Point(18, 55);
            this.label_Z.Name = "label_Z";
            this.label_Z.Size = new System.Drawing.Size(77, 12);
            this.label_Z.TabIndex = 0;
            this.label_Z.Text = "Z 轴停止 ：0";
            // 
            // label_Y
            // 
            this.label_Y.AutoSize = true;
            this.label_Y.Location = new System.Drawing.Point(171, 26);
            this.label_Y.Name = "label_Y";
            this.label_Y.Size = new System.Drawing.Size(77, 12);
            this.label_Y.TabIndex = 0;
            this.label_Y.Text = "Y 轴停止 ：0";
            // 
            // label_X
            // 
            this.label_X.AutoSize = true;
            this.label_X.Location = new System.Drawing.Point(18, 26);
            this.label_X.Name = "label_X";
            this.label_X.Size = new System.Drawing.Size(77, 12);
            this.label_X.TabIndex = 0;
            this.label_X.Text = "X 轴停止 ：0";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.buttonMoveAdd);
            this.groupBox4.Controls.Add(this.radioButtonHigh);
            this.groupBox4.Controls.Add(this.buttonMoveSub);
            this.groupBox4.Controls.Add(this.radioButtonLow);
            this.groupBox4.Location = new System.Drawing.Point(339, 134);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(124, 128);
            this.groupBox4.TabIndex = 14;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "电机移动控制";
            // 
            // timerMotor
            // 
            this.timerMotor.Interval = 200;
            this.timerMotor.Tick += new System.EventHandler(this.timerMotor_Tick);
            // 
            // timerStatus
            // 
            this.timerStatus.Interval = 500;
            this.timerStatus.Tick += new System.EventHandler(this.timerStatus_Tick);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.numericUpDownDTime);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Controls.Add(this.numericUpDownATime);
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.Controls.Add(this.numericUpDownSpeed);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Location = new System.Drawing.Point(12, 313);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(300, 110);
            this.groupBox5.TabIndex = 15;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "轴速度设置";
            // 
            // numericUpDownDTime
            // 
            this.numericUpDownDTime.Location = new System.Drawing.Point(95, 74);
            this.numericUpDownDTime.Name = "numericUpDownDTime";
            this.numericUpDownDTime.Size = new System.Drawing.Size(65, 21);
            this.numericUpDownDTime.TabIndex = 2;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(36, 79);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 12);
            this.label18.TabIndex = 0;
            this.label18.Text = "减速时间";
            // 
            // numericUpDownATime
            // 
            this.numericUpDownATime.Location = new System.Drawing.Point(95, 47);
            this.numericUpDownATime.Name = "numericUpDownATime";
            this.numericUpDownATime.Size = new System.Drawing.Size(65, 21);
            this.numericUpDownATime.TabIndex = 2;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(36, 52);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 12);
            this.label16.TabIndex = 0;
            this.label16.Text = "加速时间";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(166, 79);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(17, 12);
            this.label17.TabIndex = 0;
            this.label17.Text = "ms";
            // 
            // numericUpDownSpeed
            // 
            this.numericUpDownSpeed.Location = new System.Drawing.Point(95, 20);
            this.numericUpDownSpeed.Name = "numericUpDownSpeed";
            this.numericUpDownSpeed.Size = new System.Drawing.Size(65, 21);
            this.numericUpDownSpeed.TabIndex = 2;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(166, 52);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(17, 12);
            this.label15.TabIndex = 0;
            this.label15.Text = "ms";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(36, 25);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 12);
            this.label13.TabIndex = 0;
            this.label13.Text = "工作速度";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(166, 25);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(29, 12);
            this.label14.TabIndex = 0;
            this.label14.Text = "mm/s";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 582);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.buttonStop);
            this.Controls.Add(this.buttonHome);
            this.Controls.Add(this.buttonZero);
            this.Controls.Add(this.buttonSaveInfo);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPitch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHighLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLowLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPPR)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownATime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpeed)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton radioButtonAxisU;
        private System.Windows.Forms.RadioButton radioButtonAxisZ;
        private System.Windows.Forms.RadioButton radioButtonAxisY;
        private System.Windows.Forms.RadioButton radioButtonAxisX;
        private System.Windows.Forms.NumericUpDown numericUpDownPitch;
        private System.Windows.Forms.NumericUpDown numericUpDownHighLimit;
        private System.Windows.Forms.NumericUpDown numericUpDownLowLimit;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericUpDownPPR;
        private System.Windows.Forms.ComboBox comboBoxSensorMode;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBoxAxisType;
        private System.Windows.Forms.ComboBox comboBoxPulseMode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.Button buttonHome;
        private System.Windows.Forms.Button buttonZero;
        private System.Windows.Forms.Button buttonMoveAdd;
        private System.Windows.Forms.Button buttonSaveInfo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button buttonMoveSub;
        private System.Windows.Forms.RadioButton radioButtonLow;
        private System.Windows.Forms.RadioButton radioButtonHigh;
        private System.Windows.Forms.ComboBox comboBoxCardip;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonCardClose;
        private System.Windows.Forms.Button buttonCardLink;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label_U;
        private System.Windows.Forms.Label label_Z;
        private System.Windows.Forms.Label label_Y;
        private System.Windows.Forms.Label label_X;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Timer timerMotor;
        private System.Windows.Forms.Timer timerStatus;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.NumericUpDown numericUpDownSpeed;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown numericUpDownDTime;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.NumericUpDown numericUpDownATime;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label15;
    }
}

