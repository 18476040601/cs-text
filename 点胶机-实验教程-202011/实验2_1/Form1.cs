﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;
///运动控制库命名空间
using zmcauxcs;

namespace 实验2_1
{
    /// <summary>
    /// 实验2_1：单轴回零实验
    /// 简述：学会正确设置运动控制器参数，驱动电机移动；了解电机回零方式方法，让平台正确回零
    /// </summary>
    public partial class Form1 : Form
    {
        //控制器句柄
        IntPtr g_handle;

        //控制器轴数量
        int AxisNum = 4;

        //轴参数配置数组
        AxisConfig[] AxisConfigList;

        //轴速度配置数组
        BSpeed[] AxisSpeedList;

        //鼠标按下标志
        bool AddDown = false;

        //鼠标按下标志
        bool SubDown = false;

        //持续运动标志
        bool ContiWork = false;

        //构造函数
        public Form1()
        {
            InitializeComponent();
            InitializeForm();
        }

        //读取配置文件
        void ReadIni()
        {
            //运行目录下的card.ini文件
            string filePath = Application.StartupPath + "\\card.ini";
            //可变字符串
            StringBuilder temp = new StringBuilder(128);

            //读取轴参数
            for (int idx = 0; idx < AxisNum; ++idx)
            {
                Win32.GetPrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "PPR", "10000", temp, 128, filePath);
                AxisConfigList[idx].PPR = Convert.ToInt32(temp.ToString());

                Win32.GetPrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "Pitch", "10", temp, 128, filePath);
                AxisConfigList[idx].Pitch = Convert.ToDouble(temp.ToString());

                Win32.GetPrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "PulseMode", "0", temp, 128, filePath);
                AxisConfigList[idx].PulseMode = Convert.ToInt32(temp.ToString());

                Win32.GetPrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "AxisType", "0", temp, 128, filePath);
                AxisConfigList[idx].AxisType = Convert.ToInt32(temp.ToString());

                Win32.GetPrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "Merge", "1", temp, 128, filePath);
                AxisConfigList[idx].Merge = Convert.ToInt32(temp.ToString());

                Win32.GetPrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "LowLimit", "0", temp, 128, filePath);
                AxisConfigList[idx].LowLimit = Convert.ToInt32(temp.ToString());

                Win32.GetPrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "HighLimit", "300", temp, 128, filePath);
                AxisConfigList[idx].HighLimit = Convert.ToInt32(temp.ToString());

                Win32.GetPrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "HomeDir", "4", temp, 128, filePath);
                AxisConfigList[idx].HomeMode = Convert.ToInt32(temp.ToString());

                Win32.GetPrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "SensorMode", "0", temp, 128, filePath);
                AxisConfigList[idx].SensorMode = Convert.ToInt32(temp.ToString());
            }

            //读取轴速度
            for (int idx = 0; idx < AxisNum; ++idx)
            {
                Win32.GetPrivateProfileString(string.Format("AXIS_SPEED{0}", idx), "SPEED", "10", temp, 128, filePath);
                AxisSpeedList[idx].Speed = Convert.ToDouble(temp.ToString());

                Win32.GetPrivateProfileString(string.Format("AXIS_SPEED{0}", idx), "ATime", "10", temp, 128, filePath);
                AxisSpeedList[idx].ATime = Convert.ToDouble(temp.ToString());

                Win32.GetPrivateProfileString(string.Format("AXIS_SPEED{0}", idx), "DTime", "10", temp, 128, filePath);
                AxisSpeedList[idx].DTime = Convert.ToDouble(temp.ToString());
            }
        }

        //写入配置文件
        void SaveIni()
        {
            //运行目录下的card.ini文件
            string filePath = Application.StartupPath + "\\card.ini";

            //保存轴参数
            for (int idx = 0; idx < AxisNum; ++idx)
            {
                Win32.WritePrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "PPR", Convert.ToString(AxisConfigList[idx].PPR), filePath);
                Win32.WritePrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "Pitch", Convert.ToString(AxisConfigList[idx].Pitch), filePath);
                Win32.WritePrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "PulseMode", Convert.ToString(AxisConfigList[idx].PulseMode), filePath);
                Win32.WritePrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "AxisType", Convert.ToString(AxisConfigList[idx].AxisType), filePath);
                Win32.WritePrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "Merge", Convert.ToString(AxisConfigList[idx].Merge), filePath);
                Win32.WritePrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "LowLimit", Convert.ToString(AxisConfigList[idx].LowLimit), filePath);
                Win32.WritePrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "HighLimit", Convert.ToString(AxisConfigList[idx].HighLimit), filePath);
                Win32.WritePrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "HomeDir", Convert.ToString(AxisConfigList[idx].HomeMode), filePath);
                Win32.WritePrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "SensorMode", Convert.ToString(AxisConfigList[idx].SensorMode), filePath);
            }

            //保存轴速度
            for (int idx = 0; idx < AxisNum; ++idx)
            {
                Win32.WritePrivateProfileString(string.Format("AXIS_SPEED{0}", idx), "SPEED", Convert.ToString(AxisSpeedList[idx].Speed), filePath);
                Win32.WritePrivateProfileString(string.Format("AXIS_SPEED{0}", idx), "ATime", Convert.ToString(AxisSpeedList[idx].ATime), filePath);
                Win32.WritePrivateProfileString(string.Format("AXIS_SPEED{0}", idx), "DTime", Convert.ToString(AxisSpeedList[idx].DTime), filePath);
            }
        }

        //保存配置数据
        void SaveForm()
        {
            int axisIndex = 0;
            if (radioButtonAxisX.Checked == true) axisIndex = 0;
            if (radioButtonAxisY.Checked == true) axisIndex = 1;
            if (radioButtonAxisZ.Checked == true) axisIndex = 2;
            if (radioButtonAxisU.Checked == true) axisIndex = 3;

            AxisConfigList[axisIndex].PulseMode = comboBoxPulseMode.SelectedIndex;
            AxisConfigList[axisIndex].AxisType = comboBoxAxisType.SelectedIndex;
            AxisConfigList[axisIndex].SensorMode = comboBoxSensorMode.SelectedIndex;
            AxisConfigList[axisIndex].PPR = (int)numericUpDownPPR.Value;
            AxisConfigList[axisIndex].Pitch = (double)numericUpDownPitch.Value;
            AxisConfigList[axisIndex].LowLimit = (int)numericUpDownLowLimit.Value;
            AxisConfigList[axisIndex].HighLimit = (int)numericUpDownHighLimit.Value;

            AxisSpeedList[axisIndex].Speed = (int)numericUpDownSpeed.Value;
            AxisSpeedList[axisIndex].ATime = (int)numericUpDownATime.Value;
            AxisSpeedList[axisIndex].DTime = (int)numericUpDownDTime.Value;
        }

        //更新配置界面
        void UpdateForm()
        {
            int axisIndex = 0;
            if (radioButtonAxisX.Checked == true) axisIndex = 0;
            if (radioButtonAxisY.Checked == true) axisIndex = 1;
            if (radioButtonAxisZ.Checked == true) axisIndex = 2;
            if (radioButtonAxisU.Checked == true) axisIndex = 3;

            comboBoxPulseMode.SelectedIndex = AxisConfigList[axisIndex].PulseMode;
            comboBoxAxisType.SelectedIndex = AxisConfigList[axisIndex].AxisType;
            comboBoxSensorMode.SelectedIndex = AxisConfigList[axisIndex].SensorMode;
            numericUpDownPPR.Value = AxisConfigList[axisIndex].PPR;
            numericUpDownPitch.Value = (decimal)AxisConfigList[axisIndex].Pitch;
            numericUpDownLowLimit.Value = AxisConfigList[axisIndex].LowLimit;
            numericUpDownHighLimit.Value = AxisConfigList[axisIndex].HighLimit;

            numericUpDownSpeed.Value = (int)AxisSpeedList[axisIndex].Speed;
            numericUpDownATime.Value = (int)AxisSpeedList[axisIndex].ATime;
            numericUpDownDTime.Value = (int)AxisSpeedList[axisIndex].DTime;
        }

        //窗口辅助初始化
        void InitializeForm()
        {
            //控制器IP
            comboBoxCardip.Items.Add("192.168.0.11");
            comboBoxCardip.Items.Add("127.0.0.1");
            comboBoxCardip.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxCardip.SelectedIndex = 0;

            //脉冲方式
            comboBoxPulseMode.Items.Add("脉冲方向:下跳沿有效");
            comboBoxPulseMode.Items.Add("脉冲方向:上跳沿有效");
            comboBoxPulseMode.Items.Add("脉冲方向:下跳沿有效,方向线反转");
            comboBoxPulseMode.Items.Add("脉冲方向:上跳沿有效,方向线反转");
            comboBoxPulseMode.Items.Add("双脉冲:常态低电平,缺省电平");
            comboBoxPulseMode.Items.Add("双脉冲:常态高电平,缺省电平");
            comboBoxPulseMode.Items.Add("双脉冲:常态低电平");
            comboBoxPulseMode.Items.Add("双脉冲:常态高电平");
            comboBoxPulseMode.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxPulseMode.SelectedIndex = 0;

            //轴类型
            comboBoxAxisType.Items.Add("脉冲轴:无编码反馈");
            comboBoxAxisType.Items.Add("编码轴:带编码反馈");
            comboBoxAxisType.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxAxisType.SelectedIndex = 0;

            //原点电平
            comboBoxSensorMode.Items.Add("低电平");
            comboBoxSensorMode.Items.Add("高电平");
            comboBoxSensorMode.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxSensorMode.SelectedIndex = 0;

            //电机细分
            numericUpDownPPR.Minimum = 1;
            numericUpDownPPR.Maximum = 100000;
            numericUpDownPPR.Value = 1000;

            //丝杆导程
            numericUpDownPitch.Minimum = 1;
            numericUpDownPitch.Maximum = 1000;
            numericUpDownPitch.Value = 10;
            numericUpDownPitch.DecimalPlaces = 3;
            numericUpDownPitch.Increment = 0.001M;

            //轴软限位
            numericUpDownLowLimit.Minimum = -200000000;
            numericUpDownLowLimit.Maximum = 200000000;
            numericUpDownLowLimit.Value = 0;

            //轴软限位
            numericUpDownHighLimit.Minimum = -200000000;
            numericUpDownHighLimit.Maximum = 200000000;
            numericUpDownHighLimit.Value = 300;

            //初始化轴配置参数数组
            AxisConfigList = new AxisConfig[AxisNum];
            for (int idx = 0; idx < AxisNum; ++idx)
            {
                AxisConfigList[idx] = new AxisConfig();
            }

            //初始化轴速度参数数组
            AxisSpeedList = new BSpeed[AxisNum];
            for (int idx = 0; idx < AxisNum; ++idx)
            {
                AxisSpeedList[idx] = new BSpeed();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //读取配置
            ReadIni();

            //更新界面
            UpdateForm();

            //默认选中X轴
            radioButtonAxisX.Checked = true;

            //默认低速运动
            radioButtonLow.Checked = true;

            //启动定时器，监控按钮是否被按下
            timerMotor.Start();

            //启动定时器，显示当前轴参数信息
            timerStatus.Start();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            //断开链接/关闭运动控制器
            zmcaux.ZAux_Close(g_handle);
            g_handle = (IntPtr)0;

            this.Text = "控制器已经断开";
        }

        private void buttonCardLink_Click(object sender, EventArgs e)
        {
            //链接控制器 
            string ipaddr = comboBoxCardip.Text;
            zmcaux.ZAux_OpenEth(ipaddr, out g_handle);

            //判断句柄是否有效
            if ((int)g_handle != 0)
            {
                this.Text = "控制器链接成功";
            }
            else
            {
                this.Text = "控制器链接失败";
            }

            if ((int)g_handle != 0)
            {
                int err = 0;

                //设置脉冲输出类型：脉冲+方向/双脉冲
                for (int Cnt = 0; Cnt < AxisNum; Cnt++)
                {
                    AxisConfig aParam = AxisConfigList[Cnt];
                    int PulseMode = aParam.PulseMode;

                    //设置脉冲方式
                    //0 脉冲方向：下跳沿有效
                    //1 脉冲方向：上跳沿有效
                    //2 脉冲方向方式，下跳沿有效，方向线反转
                    //3 脉冲方向方式，上跳沿有效，方向线反转
                    //4 双脉冲常态低，缺省电平
                    //5 双脉冲常态高，缺省电平
                    //6 双脉冲常态低
                    //7 双脉冲常态高
                    err = zmcaux.ZAux_Direct_SetInvertStep(g_handle, Cnt, PulseMode);

                    //设置轴类型
                    int AxisType = 1;                               //轴类型：脉冲方向方式的步进或伺服
                    if (aParam.AxisType == 0) { AxisType = 1; }     //脉冲轴:无编码反馈
                    if (aParam.AxisType == 1) { AxisType = 4; }     //编码轴:带编码反馈
                    err = zmcaux.ZAux_Direct_SetAtype(g_handle, Cnt, AxisType);

                    if (err != 0) return;
                }

                //设定脉冲当量：每转脉冲数/丝杆导程
                for (int Cnt = 0; Cnt < AxisNum; Cnt++)
                {
                    AxisConfig aParam = AxisConfigList[Cnt];
                    float PPR = aParam.PPR;                 //电机细分
                    float Pitch = (float)aParam.Pitch;      //丝杆导程
                    err = zmcaux.ZAux_Direct_SetUnits(g_handle, Cnt, PPR / Pitch);

                    if (err != 0) return;
                }

                //设置正负软限位
                for (int Cnt = 0; Cnt < AxisNum; ++Cnt)
                {
                    AxisConfig aParam = AxisConfigList[Cnt];
                    float LowLimit = aParam.LowLimit;
                    float HighLimit = aParam.HighLimit;

                    err = zmcaux.ZAux_Direct_SetRsLimit(g_handle, Cnt, LowLimit);	//设置负向软限位位置/需要取消是设置成特别小的数即可，如-200000000
                    err = zmcaux.ZAux_Direct_SetFsLimit(g_handle, Cnt, HighLimit);	//设置正向软限位位置/需要取消是设置成特别大的数即可，如+200000000

                    if (err != 0) return;
                }

                //取消正负硬限位/当不接正负硬限位传感器时可以取消
                for (int Cnt = 0; Cnt < AxisNum; ++Cnt)
                {
                    err = zmcaux.ZAux_Direct_SetRevIn(g_handle, Cnt, -1);           //取消负向硬限位位置
                    err = zmcaux.ZAux_Direct_SetFwdIn(g_handle, Cnt, -1);           //取消正向硬限位位置

                    if (err != 0) return;
                }

                //取消轴报警信号/当不接轴报警信号到控制器时可以取消
                for (int Cnt = 0; Cnt < AxisNum; ++Cnt)
                {
                    err = zmcaux.ZAux_Direct_SetAlmIn(g_handle, Cnt, -1);           //取消轴报警信号

                    if (err != 0) return;
                }

                //设置运动速度以及加速度和减速度
                for (int Cnt = 0; Cnt < AxisNum; Cnt++)
                {
                    double speed = AxisSpeedList[Cnt].Speed;
                    double accel = speed / AxisSpeedList[Cnt].ATime * 1000;
                    double decel = speed / AxisSpeedList[Cnt].DTime * 1000;

                    err = zmcaux.ZAux_Direct_SetSpeed(g_handle, Cnt, (float)speed); //设置轴速度
                    err = zmcaux.ZAux_Direct_SetAccel(g_handle, Cnt, (float)accel); //设置轴加速度
                    err = zmcaux.ZAux_Direct_SetDecel(g_handle, Cnt, (float)decel); //设置轴减速度
                }
            }
            else
            {
                this.Text = "控制器尚未链接";
            }
        }

        private void buttonCardClose_Click(object sender, EventArgs e)
        {
            //断开链接/关闭卡
            zmcaux.ZAux_Close(g_handle);
            g_handle = (IntPtr)0;

            this.Text = "控制器已经断开";
        }

        private void buttonSaveInfo_Click(object sender, EventArgs e)
        {
            SaveForm();
            SaveIni();
        }

        private void buttonHome_Click(object sender, EventArgs e)
        {
            //判断句柄是否有效
            if ((int)g_handle == 0)
            {
                MessageBox.Show("未链接到控制器!", "提示");
            }
            else
            {
                int nAxis = 0;
                if (radioButtonAxisX.Checked == true) nAxis = 0;
                if (radioButtonAxisY.Checked == true) nAxis = 1;
                if (radioButtonAxisZ.Checked == true) nAxis = 2;
                if (radioButtonAxisU.Checked == true) nAxis = 3;

                int SensorMode = AxisConfigList[nAxis].SensorMode;
                double speed = AxisSpeedList[nAxis].Speed;
                double accel = speed / AxisSpeedList[nAxis].ATime * 1000;
                double decel = speed / AxisSpeedList[nAxis].DTime * 1000;

                //设置零点传感器输入端口/平台接线IN20为X轴原点端口
                zmcaux.ZAux_Direct_SetDatumIn(g_handle, nAxis, nAxis + 20);
                //设置零点输入口电平有效模式/默认低电平有效，当SensorMode=1时表示将输入口反转，变为高电平有效
                zmcaux.ZAux_Direct_SetInvertIn(g_handle, nAxis + 20, SensorMode);
                //设置运动速度
                zmcaux.ZAux_Direct_SetSpeed(g_handle, nAxis, (float)speed);
                //设置运动加速度
                zmcaux.ZAux_Direct_SetAccel(g_handle, nAxis, (float)accel);
                //设置运动减速度
                zmcaux.ZAux_Direct_SetDecel(g_handle, nAxis, (float)decel);
                //设置零点出入口信号有效后，反向爬行速度
                zmcaux.ZAux_Direct_SetCreep(g_handle, nAxis, (float)5);
                
                //开始调用回零函数，执行回零运动
                zmcaux.ZAux_Direct_Singl_Datum(g_handle, nAxis, AxisConfigList[nAxis].HomeMode);
            }
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            //查看控制器句柄是否有效
            if ((int)g_handle == 0)
            {
                MessageBox.Show("未链接到控制器!", "提示");
            }
            else
            {
                int nAxis = 0;
                if (radioButtonAxisX.Checked == true) nAxis = 0;
                if (radioButtonAxisY.Checked == true) nAxis = 1;
                if (radioButtonAxisZ.Checked == true) nAxis = 2;
                if (radioButtonAxisU.Checked == true) nAxis = 3;

                //取消轴所有运动
                zmcaux.ZAux_Direct_Singl_Cancel(g_handle, nAxis, 2);
            }
        }

        private void buttonZero_Click(object sender, EventArgs e)
        {
            //查看控制器句柄是否有效
            if ((int)g_handle == 0)
            {
                MessageBox.Show("未链接到控制器!", "提示");
            }
            else
            {
                for (int i = 0; i < AxisNum; i++)
                {
                    //将轴当前位置修改为0
                    zmcaux.ZAux_Direct_SetDpos(g_handle, i, 0);
                }
            }
        }

        private void buttonMoveAdd_MouseDown(object sender, MouseEventArgs e)
        {
            AddDown = true;
        }

        private void buttonMoveAdd_MouseUp(object sender, MouseEventArgs e)
        {
            AddDown = false;
        }

        private void buttonMoveSub_MouseDown(object sender, MouseEventArgs e)
        {
            SubDown = true;
        }

        private void buttonMoveSub_MouseUp(object sender, MouseEventArgs e)
        {
            SubDown = false;
        }

        private void radioButtonAxisX_CheckedChanged(object sender, EventArgs e)
        {
            UpdateForm();
        }

        private void radioButtonAxisY_CheckedChanged(object sender, EventArgs e)
        {
            UpdateForm();
        }

        private void radioButtonAxisZ_CheckedChanged(object sender, EventArgs e)
        {
            UpdateForm();
        }

        private void radioButtonAxisU_CheckedChanged(object sender, EventArgs e)
        {
            UpdateForm();
        }

        private void timerMotor_Tick(object sender, EventArgs e)
        {
            //判断运动控制器句柄是否有效
            if ((int)g_handle == 0) return;

            //判断是否有按钮被按下
            if (SubDown || AddDown)
            {
                //判断当前选择的是哪个轴
                int nAxis = 0;
                if (radioButtonAxisX.Checked == true) nAxis = 0;
                if (radioButtonAxisY.Checked == true) nAxis = 1;
                if (radioButtonAxisZ.Checked == true) nAxis = 2;
                if (radioButtonAxisU.Checked == true) nAxis = 3;

                //设置速度/高速
                if (radioButtonLow.Checked)
                {
                    zmcaux.ZAux_Direct_SetSpeed(g_handle, nAxis, 10);
                    ContiWork = true;   //设置持续运动标志
                }
                //设置速度/低速
                if (radioButtonHigh.Checked)
                {
                    zmcaux.ZAux_Direct_SetSpeed(g_handle, nAxis, 50);
                    ContiWork = true;   //设置持续运动标志
                }

                //持续运动标志
                if (ContiWork)
                {
                    if (AddDown)
                    {
                        //正向持续运动
                        zmcaux.ZAux_Direct_Singl_Vmove(g_handle, nAxis, 1);
                    }
                    else if (SubDown)
                    {
                        //反向持续运动
                        zmcaux.ZAux_Direct_Singl_Vmove(g_handle, nAxis, -1);
                    }
                }
            }
            else
            {
                //持续运动标志
                if (ContiWork)
                {
                    //判断当前选择的是哪个轴
                    int nAxis = 0;
                    if (radioButtonAxisX.Checked == true) nAxis = 0;
                    if (radioButtonAxisY.Checked == true) nAxis = 1;
                    if (radioButtonAxisZ.Checked == true) nAxis = 2;
                    if (radioButtonAxisU.Checked == true) nAxis = 3;

                    //反置持续运动标志
                    ContiWork = false;

                    //取消轴所有运动
                    zmcaux.ZAux_Direct_Singl_Cancel(g_handle, nAxis, 2);
                }
            }
        }

        private void timerStatus_Tick(object sender, EventArgs e)
        {
            //判断控制器句柄是否有效
            if ((int)g_handle != 0)
            {
                int[] runstate = new int[AxisNum];          //轴的运行状态数组
                float[] curdpos = new float[AxisNum];       //轴的当前位置数组/虚拟位置
                float[] curmpos = new float[AxisNum];       //轴的编码位置数组/开环接法时同curdpos

                for (int i = 0; i < AxisNum; i++)
                {

                    //获取当前轴是否空闲
                    zmcaux.ZAux_Direct_GetIfIdle(g_handle, i, ref runstate[i]);
                    //获取当前轴的虚拟位置
                    zmcaux.ZAux_Direct_GetDpos(g_handle, i, ref curdpos[i]);
                    //获取当前轴的编码位置/闭环接法有效
                    zmcaux.ZAux_Direct_GetMpos(g_handle, i, ref curmpos[i]);
                }

                label_X.Text = "X " + Convert.ToString(runstate[0] == 0 ? " 运行中 " : " 停止中 ") + curdpos[0];
                label_Y.Text = "Y " + Convert.ToString(runstate[1] == 0 ? " 运行中 " : " 停止中 ") + curdpos[1];
                label_Z.Text = "Z " + Convert.ToString(runstate[2] == 0 ? " 运行中 " : " 停止中 ") + curdpos[2];
                label_U.Text = "U " + Convert.ToString(runstate[3] == 0 ? " 运行中 " : " 停止中 ") + curdpos[3];
            }
        }
    }

    #region

    //文件读写
    public class Win32
    {
        /// <summary>
        /// 读取配置文件/系统函数
        /// </summary>
        /// <param name="lpAppName">小节名称</param>
        /// <param name="lpKeyName">条目名称</param>
        /// <param name="lpDefault">条目没有找到时返回的默认值</param>
        /// <param name="lpReturnedString">指定一个字串缓冲区，长度至少为nSize</param>
        /// <param name="nSize">指定装载到lpReturnedString缓冲区的最大字符数量</param>
        /// <param name="lpFileName">操作的文件名称，如没有指定一个完整路径名，Windows就在Windows目录中查找文件</param>
        /// <returns></returns>
        [DllImport("kernel32.dll")]
        public static extern int GetPrivateProfileString(string lpAppName, string lpKeyName, string lpDefault, StringBuilder lpReturnedString, int nSize, string lpFileName);

        /// <summary>
        /// 写入配置文件/系统函数
        /// </summary>
        /// <param name="lpAppName">小节名称</param>
        /// <param name="lpKeyName">条目名称</param>
        /// <param name="lpString">条目字符串值</param>
        /// <param name="lpFileName">操作的文件名称，如没有指定一个完整路径名，Windows就在Windows目录中写入文件</param>
        /// <returns></returns>
        [DllImport("kernel32.dll")]
        public static extern int WritePrivateProfileString(string lpAppName, string lpKeyName, string lpString, string lpFileName);
    }

    //轴基本配置
    public class AxisConfig
    {
        //轴基本参数
        public int PPR = 10000;         //电机细分
        public double Pitch = 10;       //丝杆螺距
        public int PulseMode = 0;       //电机脉冲模式(支持八种模式)
        public int AxisType = 0;        //0虚拟轴[对应值1]/1带编码反馈轴[对应值4]
        public int Merge = 1;           //0关闭连续插补/1开启连续插补
        public int LowLimit = 0;        //软下限位
        public int HighLimit = 300;     //软上限位

        //轴复位方式
        public int HomeMode = 4;        //复位模式(3:正向复位，4:反向复位)
        public int SensorMode = 0;      //原点电平(0:低电平，1:高电平)
        public bool UseAxis = true;     //是否启用轴
    }

    //轴基本速度
    public class BSpeed
    {
        public double Speed = 50;       //工作速度
        public double ATime = 100;      //加速时间
        public double DTime = 100;      //减速时间
    }

    #endregion
}
