﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
///视觉库命名空间
using XTVisioncs;

namespace 实验1_2
{
    /// <summary>
    /// 实验1_2：模板设定和模板匹配实验
    /// 简述：熟悉使用图像库进行模板区域选择、模板设定、模板匹配等操作
    /// </summary>
    public partial class Form1 : Form
    {
        //每个窗口绑定到一个编号，每个图像绑定到一个图像号

        //给每一个窗口分配一个0--255之间的编号
        //窗口号
        int windowNum1 = 10;

        //给每一个图像分配一个0--255之间的编号
        //图像号
        int imageNum1 = 12;

        //给每一个模板分配一个0--255之间的编号
        //模板号
        int templateNum1 = 1;

        //给每一个搜索区域分配一个0--255之间的编号
        //搜索号
        int roiNum1 = 4;

        public Form1()
        {
            InitializeComponent();
            InitializeForm();
        }

        //窗体辅助初始化
        void InitializeForm()
        {
            //初始化视觉库
            XTVision_V1.XTInitialize(true);

            //视觉窗口背景色
            pictureBox1.BackColor = Color.LightGray;

            //定位参数
            textBoxMaxAngle.Text = "30";                //旋转角度
            textBoxMinScore.Text = "700";               //定位分数
            textBoxLightValue.Text = "0";               //灯光亮度/光源控制器的值

            //定位方式
            comboBoxModelType.Items.Add("模糊定位");    //速度适中，适用于大多数情况
            comboBoxModelType.Items.Add("形状定位");    //速度较快，适用于轮廓比较清晰的情况，允许产品有轻微的形变，对光照变化比较敏感
            //comboBoxModelType.Items.Add("形变定位");    //速度较快，适用于轮廓比较清晰的情况，要求产品没有产生形变，对光照变化比较敏感
            comboBoxModelType.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxModelType.SelectedIndex = 0;

            //区域形状
            comboBoxAreaShape.Items.Add("矩形形状");
            comboBoxAreaShape.Items.Add("圆形形状");
            comboBoxAreaShape.Items.Add("任意形状");
            comboBoxAreaShape.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxAreaShape.SelectedIndex = 0;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                //从文件中加载图像
                int dispType = 0;   //图像显示方式
                string filePath = @"..\\..\\..\\image\circle_plate_01.png";
                Size imgSize = Image.FromFile(filePath).Size;
                int imageWidth = imgSize.Width;
                int imageHeight = imgSize.Height;

                //关闭视觉窗口windowNum1
                XTVision_V1.XTCloseWindow(windowNum1);
                //初始化视觉窗口/参数为窗口号，将要绑定到的控件句柄，图像宽，图像高
                XTVision_V1.XTInitWindow(windowNum1, pictureBox1.Handle, imageWidth, imageHeight);
                //从文件中加载图像到图像imageNum1中
                XTVision_V1.XTLoadImage(imageNum1, filePath);
                //将图像imageNum1显示到窗口windowNum1中
                XTVision_V1.XTDisplayPicture(windowNum1, imageNum1, dispType);

                //加载之前保存的模板
                buttonLoadTemplate_Click(sender, e);

                //将搜索区域roiNum1显示到窗口windowNum1中，区域颜色为blue
                XTVision_V1.XTDisplayRegion(windowNum1, roiNum1, 1, "blue", dispType);
                //将模板区域templateNum1显示到窗口windowNum1中，区域颜色为green
                XTVision_V1.XTDisplayTemplateArea(windowNum1, templateNum1, 1, "green", dispType);
            }
            catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.Message); }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            //反初始化视觉库/释放资源
            XTVision_V1.XTUninitialize();
        }

        private void buttonLoadImage1_Click(object sender, EventArgs e)
        {
            string resultFile = null;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image Files(*.BMP;*.PNG;*.JPG;*.GIF)|*.BMP;*.PNG;*.JPG;*.GIF|All files(*.*)|*.* ";
            openFileDialog.FilterIndex = 0;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                resultFile = openFileDialog.FileName;
                if (string.IsNullOrEmpty(resultFile)) return;
                try
                {
                    int dispType = 0;   //图像显示方式
                    Size imgSize = Image.FromFile(resultFile).Size;
                    int imageWidth = imgSize.Width;
                    int imageHeight = imgSize.Height;

                    //关闭视觉窗口windowNum1
                    XTVision_V1.XTCloseWindow(windowNum1);
                    //初始化视觉窗口/参数为窗口号，将要绑定到的控件句柄，图像宽，图像高
                    XTVision_V1.XTInitWindow(windowNum1, pictureBox1.Handle, imageWidth, imageHeight);
                    //从文件中加载图像到图像imageNum1中
                    XTVision_V1.XTLoadImage(imageNum1, resultFile);
                    //将图像imageNum1显示到窗口windowNum1中
                    XTVision_V1.XTDisplayPicture(windowNum1, imageNum1, dispType);
                }
                catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.Message); }
            }
        }

        private void buttonSelectTemplateArea_Click(object sender, EventArgs e)
        {
            //将焦点转移到图像控件上
            pictureBox1.Focus();
            //鼠标画图形状0-矩形 1-圆形 2-任意形状
            int AreaShape = comboBoxAreaShape.SelectedIndex;

            //将图像imageNum1显示到窗口windowNum1中/缓存方式
            XTVision_V1.XTDisplayPicture(windowNum1, imageNum1, 1);
            //在windowNum1窗口中显示大十字线/缓存方式
            XTVision_V1.XTDispBigCross(windowNum1, 1, "green", 1);
            //将缓存图像imageNum1显示到窗口windowNum1中
            XTVision_V1.XTDisplayPicture(windowNum1, imageNum1, 2);
            //调用以上三个函数实现刷新窗口图像显示功能

            labelMessage.Text = @"提示：请用鼠标左键在图像上选择和修改区域，右键结束修改！";

            //调用“选择模板区域”函数/每调用一次函数，都需要鼠标在窗口上做画图操作并以右键结束
            int err = XTVision_V1.XTSelectTemplateArea(windowNum1, imageNum1, templateNum1, AreaShape, "red", "green");

            labelMessage.Text = @"信息提示";
        }

        private void buttonEraseTemplateArea_Click(object sender, EventArgs e)
        {
            if (DialogResult.Cancel == MessageBox.Show("擦除模板区域是在已选模板区域基础上擦除掉一部分!\n是否继续?", "", MessageBoxButtons.OKCancel))
            {
                return;
            }

            //将焦点转移到图像控件上
            pictureBox1.Focus();
            //鼠标画图形状0-矩形 1-圆形 2-任意形状
            int AreaShape = comboBoxAreaShape.SelectedIndex;

            labelMessage.Text = @"提示：请用鼠标左键在图像上选择和修改区域，右键结束修改！";

            //调用“擦除模板区域”函数/每调用一次函数，都需要鼠标在窗口上做画图操作并以右键结束
            int err = XTVision_V1.XTEraseTemplateArea(windowNum1, imageNum1, templateNum1, AreaShape, "red", "blue");

            labelMessage.Text = @"信息提示";
        }

        private void buttonResumeTemplateArea_Click(object sender, EventArgs e)
        {
            if (DialogResult.Cancel == MessageBox.Show("增加模板区域是在已选模板区域基础上再增加一部分!\n是否继续?", "", MessageBoxButtons.OKCancel))
            {
                return;
            }

            //将焦点转移到图像控件上
            pictureBox1.Focus();
            //鼠标画图形状0-矩形 1-圆形 2-任意形状
            int AreaShape = comboBoxAreaShape.SelectedIndex;

            labelMessage.Text = @"提示：请用鼠标左键在图像上选择和修改区域，右键结束修改！";

            //调用“添加模板区域”函数/每调用一次函数，都需要鼠标在窗口上做画图操作并以右键结束
            int err = XTVision_V1.XTResumeTemplateArea(windowNum1, imageNum1, templateNum1, AreaShape, "red", "blue");

            labelMessage.Text = @"信息提示";
        }

        private void buttonProcessTemplate_Click(object sender, EventArgs e)
        {
            labelMessage.Text = @"提示：正在设置模板，请稍后。。。";

            int modelType = comboBoxModelType.SelectedIndex;            //定位类型：0-模糊定位 1-形状定位 2-形变定位
            float maxAngle = Convert.ToSingle(textBoxMaxAngle.Text);    //定位角度

            maxAngle = maxAngle * (float)Math.PI / 180;
            float angleStart = -1 * maxAngle;                           //开始角度
            float angleRange = 2 * maxAngle;                            //旋转角度
            float scaleMin = 0.8f;                                      //最小缩放比例80%
            float scaleMax = 1.1f;                                      //最大缩放比例110%
            //调用“设定模板”函数，训练好一个模板用于后续定位使用
            int err = XTVision_V1.XTCreateTemplate(imageNum1, templateNum1, modelType, angleStart, angleRange, scaleMin, scaleMax);
            if (err == 0)
            {
                //创建模板成功则刷新窗口/0表示直接显示图像
                XTVision_V1.XTDisplayPicture(windowNum1, imageNum1, 0);
            }

            labelMessage.Text = @"信息提示";
        }

        private void buttonSelectSearchArea_Click(object sender, EventArgs e)
        {
            //将焦点转移到图像控件上
            pictureBox1.Focus();
            //鼠标画图形状0-矩形 1-圆形 2-任意形状
            int AreaShape = comboBoxAreaShape.SelectedIndex;

            labelMessage.Text = @"提示：请用鼠标左键在图像上选择和修改区域，右键结束修改！";

            //调用“选择搜索区域”函数/每调用一次函数，都需要鼠标在窗口上做画图操作并以右键结束
            int err = XTVision_V1.XTSelectRegion(windowNum1, imageNum1, roiNum1, AreaShape, "red", "green");

            labelMessage.Text = @"信息提示";
        }

        private void buttonProcessImage_Click(object sender, EventArgs e)
        {
            float maxAngle = Convert.ToSingle(textBoxMaxAngle.Text);    //旋转角度
            float minScore = Convert.ToSingle(textBoxMinScore.Text);    //最小分数
            int findNumber = 1;                                         //寻找模板数量
            int matchNumber = 0;                                        //定位结果数量

            maxAngle = maxAngle * (float)Math.PI / 180;
            float angleStart = -1 * maxAngle;                           //开始角度
            float angleRange = 2 * maxAngle;                            //旋转角度
            float scaleMin = 0.8f;                                      //最小缩放比例80%
            float scaleMax = 1.1f;                                      //最大缩放比例110%
            //调用“图像定位”函数，使用训练好的模板在图像中寻找匹配度最好的图像区域，并得到该区域的图像坐标以及实际匹配度(匹配分数)
            int err = XTVision_V1.XTFindTemplate(imageNum1, roiNum1, templateNum1, angleStart, angleRange, scaleMin, scaleMax, minScore, findNumber, ref matchNumber);

            //函数执行没有错误，并且寻找模板数量和定位结果数量
            if (err == 0 && matchNumber == findNumber)
            {
                if (matchNumber > 0)
                {
                    //读取定位结果
                    float[] MatchX = new float[matchNumber];
                    float[] MatchY = new float[matchNumber];
                    float[] MatchAngle = new float[matchNumber];
                    float[] MatchScore = new float[matchNumber];
                    float[] MatchScale = new float[matchNumber];
                    XTVision_V1.XTReadFindResult(templateNum1, matchNumber, MatchY, MatchX, MatchAngle, MatchScore, MatchScale);
                    for (int idx = 0; idx < matchNumber; ++idx)
                    {
                        //转换角度单位
                        MatchAngle[idx] = MatchAngle[idx] * 180 / (float)Math.PI;
                    }

                    int dispType = 0;   //显示方式
                    //在windowNum1窗口上显示roiNum1搜索区域
                    XTVision_V1.XTDisplayRegion(windowNum1, roiNum1, 1, "blue", dispType);
                    //在windowNum1窗口上显示templateNum1模板区域(在定位结果位置显示)
                    XTVision_V1.XTDisplayTemplateArea(windowNum1, templateNum1, 1, "green", dispType);

                    //显示定位结果/文字
                    string strMsg = null;
                    strMsg += string.Format("坐标X:{0:N2} ", MatchX[0]);
                    strMsg += string.Format("坐标Y:{0:N2} ", MatchY[0]);
                    strMsg += string.Format("角度A:{0:N2} ", MatchAngle[0]);
                    strMsg += string.Format("分数S:{0:N2} ", MatchScore[0]);
                    labelMessage.Text = strMsg;
                }
            }
            else
            {
                labelMessage.Text = @"提示：模板定位失败";
            }
        }

        private void buttonSaveTemplate_Click(object sender, EventArgs e)
        {
            try
            {
                //如果目录不存在则创建文件夹
                string fileDir = "实验1_2";
                if (Directory.Exists(fileDir) == false)
                {
                    Directory.CreateDirectory(fileDir);
                }

                string filePath = null;
                int saveTemplateImage = 1;
                filePath = fileDir + "/" + "roi" + roiNum1;
                //保存搜索区域roiNum1到文件"roi" + roiNum1中
                XTVision_V1.XTSaveRegion(roiNum1, filePath);
                filePath = fileDir + "/" + "tmp" + templateNum1;
                //保存模板到templateNum1到文件"tmp" + templateNum1中
                XTVision_V1.XTSaveTemplate(templateNum1, filePath, saveTemplateImage);
            }
            catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.Message); }
        }

        private void buttonLoadTemplate_Click(object sender, EventArgs e)
        {
            try
            {
                //如果目录不存在则创建文件夹
                string fileDir = "实验1_2";
                if (Directory.Exists(fileDir) == false)
                {
                    Directory.CreateDirectory(fileDir);
                }

                string filePath = null;
                int loadTemplateImage = 1;
                filePath = fileDir + "/" + "roi" + roiNum1;
                //从文件"roi" + roiNum1中加载搜索区到roiNum1中
                XTVision_V1.XTLoadRegion(roiNum1, filePath);
                filePath = fileDir + "/" + "tmp" + templateNum1;
                //从文件"tmp" + templateNum1中加载模板到templateNum1中
                XTVision_V1.XTLoadTemplate(templateNum1, filePath, loadTemplateImage);
            }
            catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.Message); }
        }
    }
}
