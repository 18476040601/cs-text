﻿namespace 实验1_2
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxLightValue = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxMinScore = new System.Windows.Forms.TextBox();
            this.comboBoxModelType = new System.Windows.Forms.ComboBox();
            this.textBoxMaxAngle = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoxAreaShape = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.buttonSelectTemplateArea = new System.Windows.Forms.Button();
            this.buttonProcessImage = new System.Windows.Forms.Button();
            this.buttonEraseTemplateArea = new System.Windows.Forms.Button();
            this.buttonSelectSearchArea = new System.Windows.Forms.Button();
            this.buttonResumeTemplateArea = new System.Windows.Forms.Button();
            this.buttonProcessTemplate = new System.Windows.Forms.Button();
            this.labelMessage = new System.Windows.Forms.Label();
            this.buttonSaveTemplate = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonLoadImage1 = new System.Windows.Forms.Button();
            this.buttonLoadTemplate = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxLightValue
            // 
            this.textBoxLightValue.Location = new System.Drawing.Point(127, 98);
            this.textBoxLightValue.Name = "textBoxLightValue";
            this.textBoxLightValue.Size = new System.Drawing.Size(51, 21);
            this.textBoxLightValue.TabIndex = 15;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxLightValue);
            this.groupBox1.Controls.Add(this.textBoxMinScore);
            this.groupBox1.Controls.Add(this.comboBoxModelType);
            this.groupBox1.Controls.Add(this.textBoxMaxAngle);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(778, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(194, 136);
            this.groupBox1.TabIndex = 25;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "模板参数";
            // 
            // textBoxMinScore
            // 
            this.textBoxMinScore.Location = new System.Drawing.Point(127, 71);
            this.textBoxMinScore.Name = "textBoxMinScore";
            this.textBoxMinScore.Size = new System.Drawing.Size(51, 21);
            this.textBoxMinScore.TabIndex = 15;
            // 
            // comboBoxModelType
            // 
            this.comboBoxModelType.FormattingEnabled = true;
            this.comboBoxModelType.Location = new System.Drawing.Point(79, 18);
            this.comboBoxModelType.Name = "comboBoxModelType";
            this.comboBoxModelType.Size = new System.Drawing.Size(99, 20);
            this.comboBoxModelType.TabIndex = 1;
            // 
            // textBoxMaxAngle
            // 
            this.textBoxMaxAngle.Location = new System.Drawing.Point(127, 44);
            this.textBoxMaxAngle.Name = "textBoxMaxAngle";
            this.textBoxMaxAngle.Size = new System.Drawing.Size(51, 21);
            this.textBoxMaxAngle.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "定位类型";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(20, 101);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(101, 12);
            this.label27.TabIndex = 8;
            this.label27.Text = "灯光亮度(0--255)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "旋转角度(0--180)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "定位分数(0-1000)";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(760, 520);
            this.pictureBox1.TabIndex = 19;
            this.pictureBox1.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 2;
            this.label5.Text = "区域形状";
            // 
            // comboBoxAreaShape
            // 
            this.comboBoxAreaShape.FormattingEnabled = true;
            this.comboBoxAreaShape.Location = new System.Drawing.Point(79, 20);
            this.comboBoxAreaShape.Name = "comboBoxAreaShape";
            this.comboBoxAreaShape.Size = new System.Drawing.Size(99, 20);
            this.comboBoxAreaShape.TabIndex = 3;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.comboBoxAreaShape);
            this.groupBox4.Controls.Add(this.buttonSelectTemplateArea);
            this.groupBox4.Controls.Add(this.buttonProcessImage);
            this.groupBox4.Controls.Add(this.buttonEraseTemplateArea);
            this.groupBox4.Controls.Add(this.buttonSelectSearchArea);
            this.groupBox4.Controls.Add(this.buttonResumeTemplateArea);
            this.groupBox4.Controls.Add(this.buttonProcessTemplate);
            this.groupBox4.Location = new System.Drawing.Point(778, 154);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(194, 269);
            this.groupBox4.TabIndex = 26;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "模板设置";
            // 
            // buttonSelectTemplateArea
            // 
            this.buttonSelectTemplateArea.Location = new System.Drawing.Point(50, 50);
            this.buttonSelectTemplateArea.Name = "buttonSelectTemplateArea";
            this.buttonSelectTemplateArea.Size = new System.Drawing.Size(100, 28);
            this.buttonSelectTemplateArea.TabIndex = 8;
            this.buttonSelectTemplateArea.Text = "选择模板区域";
            this.buttonSelectTemplateArea.UseVisualStyleBackColor = true;
            this.buttonSelectTemplateArea.Click += new System.EventHandler(this.buttonSelectTemplateArea_Click);
            // 
            // buttonProcessImage
            // 
            this.buttonProcessImage.Location = new System.Drawing.Point(50, 220);
            this.buttonProcessImage.Name = "buttonProcessImage";
            this.buttonProcessImage.Size = new System.Drawing.Size(100, 28);
            this.buttonProcessImage.TabIndex = 11;
            this.buttonProcessImage.Text = "定位测试";
            this.buttonProcessImage.UseVisualStyleBackColor = true;
            this.buttonProcessImage.Click += new System.EventHandler(this.buttonProcessImage_Click);
            // 
            // buttonEraseTemplateArea
            // 
            this.buttonEraseTemplateArea.Location = new System.Drawing.Point(50, 84);
            this.buttonEraseTemplateArea.Name = "buttonEraseTemplateArea";
            this.buttonEraseTemplateArea.Size = new System.Drawing.Size(100, 28);
            this.buttonEraseTemplateArea.TabIndex = 7;
            this.buttonEraseTemplateArea.Text = "擦除模板区域";
            this.buttonEraseTemplateArea.UseVisualStyleBackColor = true;
            this.buttonEraseTemplateArea.Click += new System.EventHandler(this.buttonEraseTemplateArea_Click);
            // 
            // buttonSelectSearchArea
            // 
            this.buttonSelectSearchArea.Location = new System.Drawing.Point(50, 186);
            this.buttonSelectSearchArea.Name = "buttonSelectSearchArea";
            this.buttonSelectSearchArea.Size = new System.Drawing.Size(100, 28);
            this.buttonSelectSearchArea.TabIndex = 12;
            this.buttonSelectSearchArea.Text = "选择搜索区域";
            this.buttonSelectSearchArea.UseVisualStyleBackColor = true;
            this.buttonSelectSearchArea.Click += new System.EventHandler(this.buttonSelectSearchArea_Click);
            // 
            // buttonResumeTemplateArea
            // 
            this.buttonResumeTemplateArea.Location = new System.Drawing.Point(50, 118);
            this.buttonResumeTemplateArea.Name = "buttonResumeTemplateArea";
            this.buttonResumeTemplateArea.Size = new System.Drawing.Size(100, 28);
            this.buttonResumeTemplateArea.TabIndex = 6;
            this.buttonResumeTemplateArea.Text = "增加模板区域";
            this.buttonResumeTemplateArea.UseVisualStyleBackColor = true;
            this.buttonResumeTemplateArea.Click += new System.EventHandler(this.buttonResumeTemplateArea_Click);
            // 
            // buttonProcessTemplate
            // 
            this.buttonProcessTemplate.Location = new System.Drawing.Point(50, 152);
            this.buttonProcessTemplate.Name = "buttonProcessTemplate";
            this.buttonProcessTemplate.Size = new System.Drawing.Size(100, 28);
            this.buttonProcessTemplate.TabIndex = 9;
            this.buttonProcessTemplate.Text = "设定模板";
            this.buttonProcessTemplate.UseVisualStyleBackColor = true;
            this.buttonProcessTemplate.Click += new System.EventHandler(this.buttonProcessTemplate_Click);
            // 
            // labelMessage
            // 
            this.labelMessage.AutoSize = true;
            this.labelMessage.Location = new System.Drawing.Point(12, 543);
            this.labelMessage.Name = "labelMessage";
            this.labelMessage.Size = new System.Drawing.Size(53, 12);
            this.labelMessage.TabIndex = 24;
            this.labelMessage.Text = "信息提示";
            // 
            // buttonSaveTemplate
            // 
            this.buttonSaveTemplate.Location = new System.Drawing.Point(828, 470);
            this.buttonSaveTemplate.Name = "buttonSaveTemplate";
            this.buttonSaveTemplate.Size = new System.Drawing.Size(100, 28);
            this.buttonSaveTemplate.TabIndex = 22;
            this.buttonSaveTemplate.Text = "保存模板";
            this.buttonSaveTemplate.UseVisualStyleBackColor = true;
            this.buttonSaveTemplate.Click += new System.EventHandler(this.buttonSaveTemplate_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(470, 543);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 20;
            this.label1.Text = "图像一";
            // 
            // buttonLoadImage1
            // 
            this.buttonLoadImage1.Location = new System.Drawing.Point(672, 538);
            this.buttonLoadImage1.Name = "buttonLoadImage1";
            this.buttonLoadImage1.Size = new System.Drawing.Size(100, 28);
            this.buttonLoadImage1.TabIndex = 21;
            this.buttonLoadImage1.Text = "载入图像一";
            this.buttonLoadImage1.UseVisualStyleBackColor = true;
            this.buttonLoadImage1.Click += new System.EventHandler(this.buttonLoadImage1_Click);
            // 
            // buttonLoadTemplate
            // 
            this.buttonLoadTemplate.Location = new System.Drawing.Point(828, 504);
            this.buttonLoadTemplate.Name = "buttonLoadTemplate";
            this.buttonLoadTemplate.Size = new System.Drawing.Size(100, 28);
            this.buttonLoadTemplate.TabIndex = 23;
            this.buttonLoadTemplate.Text = "加载模板";
            this.buttonLoadTemplate.UseVisualStyleBackColor = true;
            this.buttonLoadTemplate.Click += new System.EventHandler(this.buttonLoadTemplate_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 582);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.labelMessage);
            this.Controls.Add(this.buttonSaveTemplate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonLoadImage1);
            this.Controls.Add(this.buttonLoadTemplate);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxLightValue;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBoxMinScore;
        private System.Windows.Forms.ComboBox comboBoxModelType;
        private System.Windows.Forms.TextBox textBoxMaxAngle;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBoxAreaShape;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button buttonSelectTemplateArea;
        private System.Windows.Forms.Button buttonProcessImage;
        private System.Windows.Forms.Button buttonEraseTemplateArea;
        private System.Windows.Forms.Button buttonSelectSearchArea;
        private System.Windows.Forms.Button buttonResumeTemplateArea;
        private System.Windows.Forms.Button buttonProcessTemplate;
        private System.Windows.Forms.Label labelMessage;
        private System.Windows.Forms.Button buttonSaveTemplate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonLoadImage1;
        private System.Windows.Forms.Button buttonLoadTemplate;
    }
}

