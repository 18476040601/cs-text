﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
///视觉库命名空间
using XTVisioncs;

namespace 实验1_5
{
    /// <summary>
    /// 实验1_5：图像测量基础
    /// 了解关于图像测量的基础知识，经过标定得到图像和世界坐标对应关系后，使用图像测量可以获知物体的实际大小
    /// </summary>
    public partial class Form1 : Form
    {
        //每个窗口绑定到一个编号，每个图像绑定到一个图像号

        //给每一个窗口分配一个0--255之间的编号
        int windowNum1 = 10;

        //给每一个图像分配一个0--255之间的编号
        //图像号
        int imageNum1 = 12;

        //给每一个测量分配一个0--255之间的编号
        //搜索号
        int roiNum1 = 4;

        public Form1()
        {
            InitializeComponent();
            InitializeForm();
        }

        //窗体辅助初始化
        void InitializeForm()
        {
            //初始化视觉库
            XTVision_V1.XTInitialize(true);

            //视觉窗口背景色
            pictureBox1.BackColor = Color.LightGray;

            //区域形状
            comboBoxAreaShape.Items.Add("矩形形状");
            comboBoxAreaShape.Items.Add("圆形形状");
            comboBoxAreaShape.Items.Add("任意形状");
            comboBoxAreaShape.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxAreaShape.SelectedIndex = 0;

            //测量类型
            comboBoxMeasureType.Items.Add("垂直于矩形边沿");
            comboBoxMeasureType.Items.Add("垂直于矩形边沿对");
            comboBoxMeasureType.Items.Add("垂直于圆形边沿");
            comboBoxMeasureType.Items.Add("垂直于圆形边沿对");
            comboBoxMeasureType.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxMeasureType.SelectedIndex = 0;

            //边沿类型
            comboBoxEdgeType.Items.Add("所有边沿类型");
            comboBoxEdgeType.Items.Add("由暗到亮边沿");
            comboBoxEdgeType.Items.Add("由亮到暗边沿");
            comboBoxEdgeType.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxEdgeType.SelectedIndex = 0;

            //提取部位
            comboBoxSelectType.Items.Add("提取所有边沿");
            comboBoxSelectType.Items.Add("提取最开始边沿");
            comboBoxSelectType.Items.Add("提取最末尾边沿");
            comboBoxSelectType.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxSelectType.SelectedIndex = 0;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                //从文件中加载图像
                int dispType = 0;   //图像显示方式
                string filePath = @"..\\..\\..\\image\circle_plate_01.png";
                Size imgSize = Image.FromFile(filePath).Size;
                int imageWidth = imgSize.Width;
                int imageHeight = imgSize.Height;

                //关闭视觉窗口windowNum1
                XTVision_V1.XTCloseWindow(windowNum1);
                //初始化视觉窗口/参数为窗口号，将要绑定到的控件句柄，图像宽，图像高
                XTVision_V1.XTInitWindow(windowNum1, pictureBox1.Handle, imageWidth, imageHeight);
                //从文件中加载图像到图像imageNum1中
                XTVision_V1.XTLoadImage(imageNum1, filePath);
                //将图像imageNum1显示到窗口windowNum1中
                XTVision_V1.XTDisplayPicture(windowNum1, imageNum1, dispType);

                //总文件中加载测量区域
                buttonLoadMeasureArea_Click(sender, e);
                //显示测量区域roiNum1到窗口windowNum1中
                XTVision_V1.XTDisplayRegion(windowNum1, roiNum1, 1, "blue", dispType);
            }
            catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.Message); }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            //反初始化视觉库/释放资源
            XTVision_V1.XTUninitialize();
        }

        private void buttonLoadImage1_Click(object sender, EventArgs e)
        {
            string resultFile = null;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image Files(*.BMP;*.PNG;*.JPG;*.GIF)|*.BMP;*.PNG;*.JPG;*.GIF|All files(*.*)|*.* ";
            openFileDialog.FilterIndex = 0;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                resultFile = openFileDialog.FileName;
                if (string.IsNullOrEmpty(resultFile)) return;
                try
                {
                    int dispType = 0;   //图像显示方式
                    Size imgSize = Image.FromFile(resultFile).Size;
                    int imageWidth = imgSize.Width;
                    int imageHeight = imgSize.Height;

                    //关闭视觉窗口windowNum1
                    XTVision_V1.XTCloseWindow(windowNum1);
                    //初始化视觉窗口/参数为窗口号，将要绑定到的控件句柄，图像宽，图像高
                    XTVision_V1.XTInitWindow(windowNum1, pictureBox1.Handle, imageWidth, imageHeight);
                    //从文件中加载图像到图像imageNum1中
                    XTVision_V1.XTLoadImage(imageNum1, resultFile);
                    //将图像imageNum1显示到窗口windowNum1中
                    XTVision_V1.XTDisplayPicture(windowNum1, imageNum1, dispType);
                }
                catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.Message); }
            }
        }

        private void buttonSelectMeasureArea_Click(object sender, EventArgs e)
        {
            //将焦点转移到图像控件上
            pictureBox1.Focus();
            //鼠标画图形状0-矩形 1-圆形 2-任意形状
            int AreaShape = comboBoxAreaShape.SelectedIndex;

            labelMessage.Text = @"提示：请用鼠标左键在图像上选择和修改区域，右键结束修改！";

            //调用“选择测量区域”函数/每调用一次函数，都需要鼠标在窗口上做画图操作并以右键结束
            int err = XTVision_V1.XTSelectRegion(windowNum1, imageNum1, roiNum1, AreaShape, "red", "blue");

            labelMessage.Text = @"信息提示";
        }

        private void buttonMeasureImage_Click(object sender, EventArgs e)
        {
            int measureType = comboBoxMeasureType.SelectedIndex;    //测量类型[0垂直于矩形的边沿检测/1垂直于矩形的边沿对检测/2垂直于圆环圆弧的边沿检测/3垂直于圆环圆弧的边沿对检测]
            float sigma = 1.5f;                                     //高斯平滑系数[0.4,100](1.5)
            int threshold = 30;                                     //最小边沿振幅[1,255](30)
            int transition = comboBoxEdgeType.SelectedIndex;        //要提取的边沿类型[0所有/1暗到亮/2亮到暗](0)
            int select = comboBoxSelectType.SelectedIndex;          //要提取的边沿部位[/0所有/1最开始/2最末尾](0)
            int edgeNumber = 0;                                     //提取到的边沿或边沿对数量
            int err = XTVision_V1.XTMeasureEdge(imageNum1, roiNum1, measureType, sigma, threshold, transition, select, ref edgeNumber);

            //读取测量结果
            if (edgeNumber > 0)
            {
                //如果进行的是边沿检测
                if (measureType == 0 || measureType == 2)
                {
                    float[] edgeX = new float[edgeNumber];                  //边沿X坐标
                    float[] edgeY = new float[edgeNumber];                  //边沿Y坐标
                    float[] amplitude = new float[edgeNumber];              //边沿振幅

                    float[] distance = new float[edgeNumber - 1];           //边沿间距

                    //获取边沿对测量结果/结果类型[0边沿X坐标/1边沿Y坐标/2边沿振幅//3第二个边沿X坐标/4第二个边沿Y坐标/5第二个边沿振幅/6边沿或边沿对间距(比边沿数少1)/7边沿对宽度]
                    XTVision_V1.XTReadMeasureEdgeResult(roiNum1, edgeNumber, 0, edgeX);
                    XTVision_V1.XTReadMeasureEdgeResult(roiNum1, edgeNumber, 1, edgeY);
                    XTVision_V1.XTReadMeasureEdgeResult(roiNum1, edgeNumber, 2, amplitude);
                    XTVision_V1.XTReadMeasureEdgeResult(roiNum1, edgeNumber, 6, distance);
                }
                //如果进行的是边沿对检测
                if (measureType == 1 || measureType == 3)
                {
                    float[] edgeX1 = new float[edgeNumber];                 //边沿X坐标1
                    float[] edgeY1 = new float[edgeNumber];                 //边沿Y坐标1
                    float[] amplitude1 = new float[edgeNumber];             //边沿振幅1
                    float[] edgeX2 = new float[edgeNumber];                 //边沿X坐标2
                    float[] edgeY2 = new float[edgeNumber];                 //边沿Y坐标2
                    float[] amplitude2 = new float[edgeNumber];             //边沿振幅2

                    float[] Intradistance = new float[edgeNumber];          //边沿对宽度
                    float[] Interdistance = new float[edgeNumber - 1];      //边沿对间距

                    //获取边沿对测量结果/结果类型[0边沿X坐标/1边沿Y坐标/2边沿振幅//3第二个边沿X坐标/4第二个边沿Y坐标/5第二个边沿振幅/6边沿或边沿对间距(比边沿数少1)/7边沿对宽度]
                    XTVision_V1.XTReadMeasureEdgeResult(roiNum1, edgeNumber, 0, edgeX1);
                    XTVision_V1.XTReadMeasureEdgeResult(roiNum1, edgeNumber, 1, edgeY1);
                    XTVision_V1.XTReadMeasureEdgeResult(roiNum1, edgeNumber, 2, amplitude1);

                    XTVision_V1.XTReadMeasureEdgeResult(roiNum1, edgeNumber, 3, edgeX2);
                    XTVision_V1.XTReadMeasureEdgeResult(roiNum1, edgeNumber, 4, edgeY2);
                    XTVision_V1.XTReadMeasureEdgeResult(roiNum1, edgeNumber, 5, amplitude2);

                    XTVision_V1.XTReadMeasureEdgeResult(roiNum1, edgeNumber, 6, Interdistance);
                    XTVision_V1.XTReadMeasureEdgeResult(roiNum1, edgeNumber, 7, Intradistance);
                }
            }

            //更新图像显示
            XTVision_V1.XTDisplayPicture(windowNum1, imageNum1, 0);
            //显示测量区域(搜索区)
            XTVision_V1.XTDisplayMeasureRegion(windowNum1, roiNum1, measureType, "green", 0);
            //显示测量结果区域
            XTVision_V1.XTDisplayMeasureResult(windowNum1, roiNum1, measureType, 1, 35, "red", 0);

            //如果进行的是边沿检测
            if (measureType == 0 || measureType == 2)
            {
                labelMessage.Text = string.Format("提示：总共测量到 {0} 个边沿", edgeNumber);
            }
            //如果进行的是边沿对检测
            if (measureType == 1 || measureType == 3)
            {
                labelMessage.Text = string.Format("提示：总共测量到 {0} 对边沿", edgeNumber);
            }
        }

        private void buttonSaveMeasureArea_Click(object sender, EventArgs e)
        {
            try
            {
                string fileDir = "实验1_5";
                if (Directory.Exists(fileDir) == false)
                {
                    Directory.CreateDirectory(fileDir);
                }

                string filePath = null;
                filePath = fileDir + "/" + "roi" + roiNum1;
                //将测量区域保存到"roi" + roiNum1文件中
                XTVision_V1.XTSaveRegion(roiNum1, filePath);
            }
            catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.Message); }
        }

        private void buttonLoadMeasureArea_Click(object sender, EventArgs e)
        {
            try
            {
                string fileDir = "实验1_5";
                if (Directory.Exists(fileDir) == false)
                {
                    Directory.CreateDirectory(fileDir);
                }

                string filePath = null;
                filePath = fileDir + "/" + "roi" + roiNum1;
                //从"roi" + roiNum1文件中加载测量区域
                XTVision_V1.XTLoadRegion(roiNum1, filePath);
            }
            catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.Message); }
        }
    }
}
