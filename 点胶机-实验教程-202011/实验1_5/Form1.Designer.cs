﻿namespace 实验1_5
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxEdgeType = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBoxMeasureType = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonMeasureImage = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxSelectType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoxAreaShape = new System.Windows.Forms.ComboBox();
            this.buttonSelectMeasureArea = new System.Windows.Forms.Button();
            this.labelMessage = new System.Windows.Forms.Label();
            this.buttonSaveMeasureArea = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonLoadMeasureArea = new System.Windows.Forms.Button();
            this.buttonLoadImage1 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxEdgeType
            // 
            this.comboBoxEdgeType.FormattingEnabled = true;
            this.comboBoxEdgeType.Location = new System.Drawing.Point(71, 46);
            this.comboBoxEdgeType.Name = "comboBoxEdgeType";
            this.comboBoxEdgeType.Size = new System.Drawing.Size(110, 20);
            this.comboBoxEdgeType.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboBoxMeasureType);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.buttonMeasureImage);
            this.groupBox1.Controls.Add(this.comboBoxEdgeType);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.comboBoxSelectType);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(778, 118);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 155);
            this.groupBox1.TabIndex = 44;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "测量参数";
            // 
            // comboBoxMeasureType
            // 
            this.comboBoxMeasureType.FormattingEnabled = true;
            this.comboBoxMeasureType.Location = new System.Drawing.Point(71, 20);
            this.comboBoxMeasureType.Name = "comboBoxMeasureType";
            this.comboBoxMeasureType.Size = new System.Drawing.Size(110, 20);
            this.comboBoxMeasureType.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 2;
            this.label4.Text = "提取部位";
            // 
            // buttonMeasureImage
            // 
            this.buttonMeasureImage.Location = new System.Drawing.Point(50, 109);
            this.buttonMeasureImage.Name = "buttonMeasureImage";
            this.buttonMeasureImage.Size = new System.Drawing.Size(100, 28);
            this.buttonMeasureImage.TabIndex = 11;
            this.buttonMeasureImage.Text = "测量测试";
            this.buttonMeasureImage.UseVisualStyleBackColor = true;
            this.buttonMeasureImage.Click += new System.EventHandler(this.buttonMeasureImage_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "边沿类型";
            // 
            // comboBoxSelectType
            // 
            this.comboBoxSelectType.FormattingEnabled = true;
            this.comboBoxSelectType.Location = new System.Drawing.Point(71, 72);
            this.comboBoxSelectType.Name = "comboBoxSelectType";
            this.comboBoxSelectType.Size = new System.Drawing.Size(110, 20);
            this.comboBoxSelectType.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "测量类型";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.comboBoxAreaShape);
            this.groupBox4.Controls.Add(this.buttonSelectMeasureArea);
            this.groupBox4.Location = new System.Drawing.Point(778, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(194, 100);
            this.groupBox4.TabIndex = 43;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "区域选择";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 2;
            this.label5.Text = "区域形状";
            // 
            // comboBoxAreaShape
            // 
            this.comboBoxAreaShape.FormattingEnabled = true;
            this.comboBoxAreaShape.Location = new System.Drawing.Point(71, 20);
            this.comboBoxAreaShape.Name = "comboBoxAreaShape";
            this.comboBoxAreaShape.Size = new System.Drawing.Size(110, 20);
            this.comboBoxAreaShape.TabIndex = 3;
            // 
            // buttonSelectMeasureArea
            // 
            this.buttonSelectMeasureArea.Location = new System.Drawing.Point(50, 56);
            this.buttonSelectMeasureArea.Name = "buttonSelectMeasureArea";
            this.buttonSelectMeasureArea.Size = new System.Drawing.Size(100, 28);
            this.buttonSelectMeasureArea.TabIndex = 12;
            this.buttonSelectMeasureArea.Text = "选择测量区域";
            this.buttonSelectMeasureArea.UseVisualStyleBackColor = true;
            this.buttonSelectMeasureArea.Click += new System.EventHandler(this.buttonSelectMeasureArea_Click);
            // 
            // labelMessage
            // 
            this.labelMessage.AutoSize = true;
            this.labelMessage.Location = new System.Drawing.Point(12, 543);
            this.labelMessage.Name = "labelMessage";
            this.labelMessage.Size = new System.Drawing.Size(53, 12);
            this.labelMessage.TabIndex = 42;
            this.labelMessage.Text = "信息提示";
            // 
            // buttonSaveMeasureArea
            // 
            this.buttonSaveMeasureArea.Location = new System.Drawing.Point(828, 470);
            this.buttonSaveMeasureArea.Name = "buttonSaveMeasureArea";
            this.buttonSaveMeasureArea.Size = new System.Drawing.Size(100, 28);
            this.buttonSaveMeasureArea.TabIndex = 40;
            this.buttonSaveMeasureArea.Text = "保存区域";
            this.buttonSaveMeasureArea.UseVisualStyleBackColor = true;
            this.buttonSaveMeasureArea.Click += new System.EventHandler(this.buttonSaveMeasureArea_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(760, 520);
            this.pictureBox1.TabIndex = 37;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(470, 543);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 38;
            this.label1.Text = "图像一";
            // 
            // buttonLoadMeasureArea
            // 
            this.buttonLoadMeasureArea.Location = new System.Drawing.Point(828, 504);
            this.buttonLoadMeasureArea.Name = "buttonLoadMeasureArea";
            this.buttonLoadMeasureArea.Size = new System.Drawing.Size(100, 28);
            this.buttonLoadMeasureArea.TabIndex = 41;
            this.buttonLoadMeasureArea.Text = "加载区域";
            this.buttonLoadMeasureArea.UseVisualStyleBackColor = true;
            this.buttonLoadMeasureArea.Click += new System.EventHandler(this.buttonLoadMeasureArea_Click);
            // 
            // buttonLoadImage1
            // 
            this.buttonLoadImage1.Location = new System.Drawing.Point(672, 538);
            this.buttonLoadImage1.Name = "buttonLoadImage1";
            this.buttonLoadImage1.Size = new System.Drawing.Size(100, 28);
            this.buttonLoadImage1.TabIndex = 39;
            this.buttonLoadImage1.Text = "载入图像一";
            this.buttonLoadImage1.UseVisualStyleBackColor = true;
            this.buttonLoadImage1.Click += new System.EventHandler(this.buttonLoadImage1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 582);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.labelMessage);
            this.Controls.Add(this.buttonSaveMeasureArea);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonLoadMeasureArea);
            this.Controls.Add(this.buttonLoadImage1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxEdgeType;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox comboBoxMeasureType;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonMeasureImage;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxSelectType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBoxAreaShape;
        private System.Windows.Forms.Button buttonSelectMeasureArea;
        private System.Windows.Forms.Label labelMessage;
        private System.Windows.Forms.Button buttonSaveMeasureArea;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonLoadMeasureArea;
        private System.Windows.Forms.Button buttonLoadImage1;
    }
}

