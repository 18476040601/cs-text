﻿namespace 实验3_3
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.radioButtonAxisU = new System.Windows.Forms.RadioButton();
            this.radioButtonAxisZ = new System.Windows.Forms.RadioButton();
            this.radioButtonAxisY = new System.Windows.Forms.RadioButton();
            this.radioButtonAxisX = new System.Windows.Forms.RadioButton();
            this.numericUpDownPitch = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownHighLimit = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownLowLimit = new System.Windows.Forms.NumericUpDown();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.numericUpDownDTime = new System.Windows.Forms.NumericUpDown();
            this.label18 = new System.Windows.Forms.Label();
            this.numericUpDownATime = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.numericUpDownSpeed = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDownPPR = new System.Windows.Forms.NumericUpDown();
            this.comboBoxSensorMode = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.numericUpDownLightVaue = new System.Windows.Forms.NumericUpDown();
            this.buttonHome = new System.Windows.Forms.Button();
            this.buttonMoveAdd = new System.Windows.Forms.Button();
            this.buttonLight = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxLightValue = new System.Windows.Forms.TextBox();
            this.comboBoxModelType = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label28 = new System.Windows.Forms.Label();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.buttonOpenCom = new System.Windows.Forms.Button();
            this.buttonCloseCom = new System.Windows.Forms.Button();
            this.radioButtonHigh = new System.Windows.Forms.RadioButton();
            this.buttonStop = new System.Windows.Forms.Button();
            this.comboBoxCardip = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.buttonSaveInfo = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label_U = new System.Windows.Forms.Label();
            this.buttonCardClose = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonCardLink = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label_Z = new System.Windows.Forms.Label();
            this.label_Y = new System.Windows.Forms.Label();
            this.label_X = new System.Windows.Forms.Label();
            this.buttonMoveSub = new System.Windows.Forms.Button();
            this.radioButtonLow = new System.Windows.Forms.RadioButton();
            this.timerStatus = new System.Windows.Forms.Timer(this.components);
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.timerMotor = new System.Windows.Forms.Timer(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.comboBoxAxisType = new System.Windows.Forms.ComboBox();
            this.comboBoxPulseMode = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonZero = new System.Windows.Forms.Button();
            this.textBoxMinScore = new System.Windows.Forms.TextBox();
            this.buttonCalcPixelDist = new System.Windows.Forms.Button();
            this.buttonToTemplatePos = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.buttonLoadTemplate = new System.Windows.Forms.Button();
            this.label29 = new System.Windows.Forms.Label();
            this.textBoxCalibStepSize = new System.Windows.Forms.TextBox();
            this.textBoxStableTime = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.buttonGetCurrentPos1 = new System.Windows.Forms.Button();
            this.textBoxWorkPosY1 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.textBoxWorkPosZ1 = new System.Windows.Forms.TextBox();
            this.buttonSaveTemplate = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.buttonCameraParam = new System.Windows.Forms.Button();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.checkBoxReviseAngle = new System.Windows.Forms.CheckBox();
            this.buttonCircleRevise = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.textBoxMaxAngle = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.buttonGetCurrentPos3 = new System.Windows.Forms.Button();
            this.buttonGetCurrentPos2 = new System.Windows.Forms.Button();
            this.textBoxWorkPosY3 = new System.Windows.Forms.TextBox();
            this.textBoxWorkPosZ3 = new System.Windows.Forms.TextBox();
            this.textBoxWorkPosY2 = new System.Windows.Forms.TextBox();
            this.textBoxWorkPosZ2 = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.textBoxWorkPosX3 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.textBoxWorkPosX2 = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.textBoxWorkPosX1 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label23 = new System.Windows.Forms.Label();
            this.comboBoxAreaShape = new System.Windows.Forms.ComboBox();
            this.buttonSelectTemplateArea = new System.Windows.Forms.Button();
            this.buttonProcessImage = new System.Windows.Forms.Button();
            this.buttonSelectSearchArea = new System.Windows.Forms.Button();
            this.buttonProcessTemplate = new System.Windows.Forms.Button();
            this.timerFrame = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPitch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHighLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLowLimit)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownATime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPPR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLightVaue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox10.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.SuspendLayout();
            // 
            // radioButtonAxisU
            // 
            this.radioButtonAxisU.AutoSize = true;
            this.radioButtonAxisU.Location = new System.Drawing.Point(236, 20);
            this.radioButtonAxisU.Name = "radioButtonAxisU";
            this.radioButtonAxisU.Size = new System.Drawing.Size(41, 16);
            this.radioButtonAxisU.TabIndex = 6;
            this.radioButtonAxisU.TabStop = true;
            this.radioButtonAxisU.Text = "U轴";
            this.radioButtonAxisU.UseVisualStyleBackColor = true;
            this.radioButtonAxisU.Click += new System.EventHandler(this.radioButtonAxisU_CheckedChanged);
            // 
            // radioButtonAxisZ
            // 
            this.radioButtonAxisZ.AutoSize = true;
            this.radioButtonAxisZ.Location = new System.Drawing.Point(182, 20);
            this.radioButtonAxisZ.Name = "radioButtonAxisZ";
            this.radioButtonAxisZ.Size = new System.Drawing.Size(41, 16);
            this.radioButtonAxisZ.TabIndex = 6;
            this.radioButtonAxisZ.TabStop = true;
            this.radioButtonAxisZ.Text = "Z轴";
            this.radioButtonAxisZ.UseVisualStyleBackColor = true;
            this.radioButtonAxisZ.Click += new System.EventHandler(this.radioButtonAxisZ_CheckedChanged);
            // 
            // radioButtonAxisY
            // 
            this.radioButtonAxisY.AutoSize = true;
            this.radioButtonAxisY.Location = new System.Drawing.Point(127, 20);
            this.radioButtonAxisY.Name = "radioButtonAxisY";
            this.radioButtonAxisY.Size = new System.Drawing.Size(41, 16);
            this.radioButtonAxisY.TabIndex = 6;
            this.radioButtonAxisY.TabStop = true;
            this.radioButtonAxisY.Text = "Y轴";
            this.radioButtonAxisY.UseVisualStyleBackColor = true;
            this.radioButtonAxisY.Click += new System.EventHandler(this.radioButtonAxisY_CheckedChanged);
            // 
            // radioButtonAxisX
            // 
            this.radioButtonAxisX.AutoSize = true;
            this.radioButtonAxisX.Location = new System.Drawing.Point(72, 21);
            this.radioButtonAxisX.Name = "radioButtonAxisX";
            this.radioButtonAxisX.Size = new System.Drawing.Size(41, 16);
            this.radioButtonAxisX.TabIndex = 6;
            this.radioButtonAxisX.TabStop = true;
            this.radioButtonAxisX.Text = "X轴";
            this.radioButtonAxisX.UseVisualStyleBackColor = true;
            this.radioButtonAxisX.Click += new System.EventHandler(this.radioButtonAxisX_CheckedChanged);
            // 
            // numericUpDownPitch
            // 
            this.numericUpDownPitch.Location = new System.Drawing.Point(206, 125);
            this.numericUpDownPitch.Name = "numericUpDownPitch";
            this.numericUpDownPitch.Size = new System.Drawing.Size(65, 21);
            this.numericUpDownPitch.TabIndex = 2;
            // 
            // numericUpDownHighLimit
            // 
            this.numericUpDownHighLimit.Location = new System.Drawing.Point(206, 155);
            this.numericUpDownHighLimit.Name = "numericUpDownHighLimit";
            this.numericUpDownHighLimit.Size = new System.Drawing.Size(65, 21);
            this.numericUpDownHighLimit.TabIndex = 2;
            // 
            // numericUpDownLowLimit
            // 
            this.numericUpDownLowLimit.Location = new System.Drawing.Point(71, 155);
            this.numericUpDownLowLimit.Name = "numericUpDownLowLimit";
            this.numericUpDownLowLimit.Size = new System.Drawing.Size(52, 21);
            this.numericUpDownLowLimit.TabIndex = 2;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.numericUpDownDTime);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Controls.Add(this.numericUpDownATime);
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.Controls.Add(this.numericUpDownSpeed);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Location = new System.Drawing.Point(12, 313);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(300, 110);
            this.groupBox5.TabIndex = 82;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "轴速度设置";
            // 
            // numericUpDownDTime
            // 
            this.numericUpDownDTime.Location = new System.Drawing.Point(95, 74);
            this.numericUpDownDTime.Name = "numericUpDownDTime";
            this.numericUpDownDTime.Size = new System.Drawing.Size(65, 21);
            this.numericUpDownDTime.TabIndex = 2;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(36, 79);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 12);
            this.label18.TabIndex = 0;
            this.label18.Text = "减速时间";
            // 
            // numericUpDownATime
            // 
            this.numericUpDownATime.Location = new System.Drawing.Point(95, 47);
            this.numericUpDownATime.Name = "numericUpDownATime";
            this.numericUpDownATime.Size = new System.Drawing.Size(65, 21);
            this.numericUpDownATime.TabIndex = 2;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(36, 52);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 12);
            this.label16.TabIndex = 0;
            this.label16.Text = "加速时间";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(166, 79);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(17, 12);
            this.label17.TabIndex = 0;
            this.label17.Text = "ms";
            // 
            // numericUpDownSpeed
            // 
            this.numericUpDownSpeed.Location = new System.Drawing.Point(95, 20);
            this.numericUpDownSpeed.Name = "numericUpDownSpeed";
            this.numericUpDownSpeed.Size = new System.Drawing.Size(65, 21);
            this.numericUpDownSpeed.TabIndex = 2;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(166, 52);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(17, 12);
            this.label15.TabIndex = 0;
            this.label15.Text = "ms";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(36, 25);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 12);
            this.label13.TabIndex = 0;
            this.label13.Text = "工作速度";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(166, 25);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(29, 12);
            this.label14.TabIndex = 0;
            this.label14.Text = "mm/s";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "原点电平";
            // 
            // numericUpDownPPR
            // 
            this.numericUpDownPPR.Location = new System.Drawing.Point(71, 125);
            this.numericUpDownPPR.Name = "numericUpDownPPR";
            this.numericUpDownPPR.Size = new System.Drawing.Size(52, 21);
            this.numericUpDownPPR.TabIndex = 2;
            // 
            // comboBoxSensorMode
            // 
            this.comboBoxSensorMode.FormattingEnabled = true;
            this.comboBoxSensorMode.Location = new System.Drawing.Point(71, 98);
            this.comboBoxSensorMode.Name = "comboBoxSensorMode";
            this.comboBoxSensorMode.Size = new System.Drawing.Size(210, 20);
            this.comboBoxSensorMode.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(151, 160);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 0;
            this.label7.Text = "行程上限";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(277, 134);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(17, 12);
            this.label11.TabIndex = 0;
            this.label11.Text = "mm";
            // 
            // serialPort1
            // 
            this.serialPort1.BaudRate = 19200;
            this.serialPort1.PortName = "COM5";
            // 
            // numericUpDownLightVaue
            // 
            this.numericUpDownLightVaue.Location = new System.Drawing.Point(46, 20);
            this.numericUpDownLightVaue.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDownLightVaue.Name = "numericUpDownLightVaue";
            this.numericUpDownLightVaue.Size = new System.Drawing.Size(65, 21);
            this.numericUpDownLightVaue.TabIndex = 2;
            this.numericUpDownLightVaue.ValueChanged += new System.EventHandler(this.numericUpDownLightValue_ValueChanged);
            // 
            // buttonHome
            // 
            this.buttonHome.Location = new System.Drawing.Point(350, 20);
            this.buttonHome.Name = "buttonHome";
            this.buttonHome.Size = new System.Drawing.Size(100, 32);
            this.buttonHome.TabIndex = 79;
            this.buttonHome.Text = "回零";
            this.buttonHome.UseVisualStyleBackColor = true;
            this.buttonHome.Click += new System.EventHandler(this.buttonHome_Click);
            // 
            // buttonMoveAdd
            // 
            this.buttonMoveAdd.Location = new System.Drawing.Point(11, 90);
            this.buttonMoveAdd.Name = "buttonMoveAdd";
            this.buttonMoveAdd.Size = new System.Drawing.Size(100, 32);
            this.buttonMoveAdd.TabIndex = 3;
            this.buttonMoveAdd.Text = "正向运动";
            this.buttonMoveAdd.UseVisualStyleBackColor = true;
            this.buttonMoveAdd.Click += new System.EventHandler(this.buttonMoveAdd_Click);
            this.buttonMoveAdd.MouseDown += new System.Windows.Forms.MouseEventHandler(this.buttonMoveAdd_MouseDown);
            this.buttonMoveAdd.MouseUp += new System.Windows.Forms.MouseEventHandler(this.buttonMoveAdd_MouseUp);
            // 
            // buttonLight
            // 
            this.buttonLight.Location = new System.Drawing.Point(163, 96);
            this.buttonLight.Name = "buttonLight";
            this.buttonLight.Size = new System.Drawing.Size(24, 23);
            this.buttonLight.TabIndex = 61;
            this.buttonLight.Text = "L";
            this.buttonLight.UseVisualStyleBackColor = true;
            this.buttonLight.Click += new System.EventHandler(this.buttonLight_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(277, 163);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(17, 12);
            this.label10.TabIndex = 0;
            this.label10.Text = "mm";
            // 
            // textBoxLightValue
            // 
            this.textBoxLightValue.Location = new System.Drawing.Point(127, 98);
            this.textBoxLightValue.Name = "textBoxLightValue";
            this.textBoxLightValue.Size = new System.Drawing.Size(33, 21);
            this.textBoxLightValue.TabIndex = 15;
            // 
            // comboBoxModelType
            // 
            this.comboBoxModelType.FormattingEnabled = true;
            this.comboBoxModelType.Location = new System.Drawing.Point(79, 18);
            this.comboBoxModelType.Name = "comboBoxModelType";
            this.comboBoxModelType.Size = new System.Drawing.Size(99, 20);
            this.comboBoxModelType.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(6, 20);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(295, 224);
            this.pictureBox1.TabIndex = 55;
            this.pictureBox1.TabStop = false;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(11, 25);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(29, 12);
            this.label28.TabIndex = 62;
            this.label28.Text = "亮度";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(12, 42);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(65, 16);
            this.radioButton1.TabIndex = 5;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "单步1mm";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.BackColor = System.Drawing.SystemColors.ControlDark;
            this.groupBox10.Controls.Add(this.label28);
            this.groupBox10.Controls.Add(this.buttonOpenCom);
            this.groupBox10.Controls.Add(this.buttonCloseCom);
            this.groupBox10.Controls.Add(this.numericUpDownLightVaue);
            this.groupBox10.Location = new System.Drawing.Point(339, 313);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(124, 139);
            this.groupBox10.TabIndex = 84;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "光源控制";
            // 
            // buttonOpenCom
            // 
            this.buttonOpenCom.Location = new System.Drawing.Point(11, 92);
            this.buttonOpenCom.Name = "buttonOpenCom";
            this.buttonOpenCom.Size = new System.Drawing.Size(100, 32);
            this.buttonOpenCom.TabIndex = 61;
            this.buttonOpenCom.Text = "打开光源";
            this.buttonOpenCom.UseVisualStyleBackColor = true;
            this.buttonOpenCom.Click += new System.EventHandler(this.buttonOpenCom_Click);
            // 
            // buttonCloseCom
            // 
            this.buttonCloseCom.Location = new System.Drawing.Point(11, 55);
            this.buttonCloseCom.Name = "buttonCloseCom";
            this.buttonCloseCom.Size = new System.Drawing.Size(100, 32);
            this.buttonCloseCom.TabIndex = 61;
            this.buttonCloseCom.Text = "关闭光源";
            this.buttonCloseCom.UseVisualStyleBackColor = true;
            this.buttonCloseCom.Click += new System.EventHandler(this.buttonCloseCom_Click);
            // 
            // radioButtonHigh
            // 
            this.radioButtonHigh.AutoSize = true;
            this.radioButtonHigh.Location = new System.Drawing.Point(65, 20);
            this.radioButtonHigh.Name = "radioButtonHigh";
            this.radioButtonHigh.Size = new System.Drawing.Size(47, 16);
            this.radioButtonHigh.TabIndex = 5;
            this.radioButtonHigh.TabStop = true;
            this.radioButtonHigh.Text = "高速";
            this.radioButtonHigh.UseVisualStyleBackColor = true;
            // 
            // buttonStop
            // 
            this.buttonStop.Location = new System.Drawing.Point(350, 58);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(100, 32);
            this.buttonStop.TabIndex = 78;
            this.buttonStop.Text = "停止";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // comboBoxCardip
            // 
            this.comboBoxCardip.FormattingEnabled = true;
            this.comboBoxCardip.Location = new System.Drawing.Point(71, 20);
            this.comboBoxCardip.Name = "comboBoxCardip";
            this.comboBoxCardip.Size = new System.Drawing.Size(210, 20);
            this.comboBoxCardip.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 24);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 12);
            this.label12.TabIndex = 0;
            this.label12.Text = "控制器IP";
            // 
            // buttonSaveInfo
            // 
            this.buttonSaveInfo.Location = new System.Drawing.Point(212, 526);
            this.buttonSaveInfo.Name = "buttonSaveInfo";
            this.buttonSaveInfo.Size = new System.Drawing.Size(100, 32);
            this.buttonSaveInfo.TabIndex = 76;
            this.buttonSaveInfo.Text = "保存参数";
            this.buttonSaveInfo.UseVisualStyleBackColor = true;
            this.buttonSaveInfo.Click += new System.EventHandler(this.buttonSaveInfo_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(129, 163);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(17, 12);
            this.label9.TabIndex = 0;
            this.label9.Text = "mm";
            // 
            // label_U
            // 
            this.label_U.AutoSize = true;
            this.label_U.Location = new System.Drawing.Point(171, 55);
            this.label_U.Name = "label_U";
            this.label_U.Size = new System.Drawing.Size(77, 12);
            this.label_U.TabIndex = 0;
            this.label_U.Text = "U 轴停止 ：0";
            // 
            // buttonCardClose
            // 
            this.buttonCardClose.Location = new System.Drawing.Point(20, 46);
            this.buttonCardClose.Name = "buttonCardClose";
            this.buttonCardClose.Size = new System.Drawing.Size(100, 32);
            this.buttonCardClose.TabIndex = 3;
            this.buttonCardClose.Text = "断开";
            this.buttonCardClose.UseVisualStyleBackColor = true;
            this.buttonCardClose.Click += new System.EventHandler(this.buttonCardClose_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboBoxCardip);
            this.groupBox2.Controls.Add(this.buttonCardClose);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.buttonCardLink);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(300, 92);
            this.groupBox2.TabIndex = 80;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "控制器连接";
            // 
            // buttonCardLink
            // 
            this.buttonCardLink.Location = new System.Drawing.Point(181, 46);
            this.buttonCardLink.Name = "buttonCardLink";
            this.buttonCardLink.Size = new System.Drawing.Size(100, 32);
            this.buttonCardLink.TabIndex = 3;
            this.buttonCardLink.Text = "连接";
            this.buttonCardLink.UseVisualStyleBackColor = true;
            this.buttonCardLink.Click += new System.EventHandler(this.buttonCardLink_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label_U);
            this.groupBox1.Controls.Add(this.label_Z);
            this.groupBox1.Controls.Add(this.label_Y);
            this.groupBox1.Controls.Add(this.label_X);
            this.groupBox1.Location = new System.Drawing.Point(12, 429);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(300, 91);
            this.groupBox1.TabIndex = 75;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "轴状态显示";
            // 
            // label_Z
            // 
            this.label_Z.AutoSize = true;
            this.label_Z.Location = new System.Drawing.Point(18, 55);
            this.label_Z.Name = "label_Z";
            this.label_Z.Size = new System.Drawing.Size(77, 12);
            this.label_Z.TabIndex = 0;
            this.label_Z.Text = "Z 轴停止 ：0";
            // 
            // label_Y
            // 
            this.label_Y.AutoSize = true;
            this.label_Y.Location = new System.Drawing.Point(171, 26);
            this.label_Y.Name = "label_Y";
            this.label_Y.Size = new System.Drawing.Size(77, 12);
            this.label_Y.TabIndex = 0;
            this.label_Y.Text = "Y 轴停止 ：0";
            // 
            // label_X
            // 
            this.label_X.AutoSize = true;
            this.label_X.Location = new System.Drawing.Point(18, 26);
            this.label_X.Name = "label_X";
            this.label_X.Size = new System.Drawing.Size(77, 12);
            this.label_X.TabIndex = 0;
            this.label_X.Text = "X 轴停止 ：0";
            // 
            // buttonMoveSub
            // 
            this.buttonMoveSub.Location = new System.Drawing.Point(11, 128);
            this.buttonMoveSub.Name = "buttonMoveSub";
            this.buttonMoveSub.Size = new System.Drawing.Size(100, 32);
            this.buttonMoveSub.TabIndex = 3;
            this.buttonMoveSub.Text = "反向运动";
            this.buttonMoveSub.UseVisualStyleBackColor = true;
            this.buttonMoveSub.Click += new System.EventHandler(this.buttonMoveSub_Click);
            this.buttonMoveSub.MouseDown += new System.Windows.Forms.MouseEventHandler(this.buttonMoveSub_MouseDown);
            this.buttonMoveSub.MouseUp += new System.Windows.Forms.MouseEventHandler(this.buttonMoveSub_MouseUp);
            // 
            // radioButtonLow
            // 
            this.radioButtonLow.AutoSize = true;
            this.radioButtonLow.Location = new System.Drawing.Point(12, 20);
            this.radioButtonLow.Name = "radioButtonLow";
            this.radioButtonLow.Size = new System.Drawing.Size(47, 16);
            this.radioButtonLow.TabIndex = 5;
            this.radioButtonLow.TabStop = true;
            this.radioButtonLow.Text = "低速";
            this.radioButtonLow.UseVisualStyleBackColor = true;
            // 
            // timerStatus
            // 
            this.timerStatus.Interval = 500;
            this.timerStatus.Tick += new System.EventHandler(this.timerStatus_Tick);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.buttonMoveAdd);
            this.groupBox4.Controls.Add(this.radioButton2);
            this.groupBox4.Controls.Add(this.radioButton1);
            this.groupBox4.Controls.Add(this.radioButtonHigh);
            this.groupBox4.Controls.Add(this.buttonMoveSub);
            this.groupBox4.Controls.Add(this.radioButtonLow);
            this.groupBox4.Location = new System.Drawing.Point(339, 134);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(124, 173);
            this.groupBox4.TabIndex = 81;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "电机移动控制";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(12, 64);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(77, 16);
            this.radioButton2.TabIndex = 5;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "单步0.1mm";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(171, 130);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 12);
            this.label5.TabIndex = 0;
            this.label5.Text = "螺距";
            // 
            // timerMotor
            // 
            this.timerMotor.Interval = 200;
            this.timerMotor.Tick += new System.EventHandler(this.timerMotor_Tick);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(129, 134);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(23, 12);
            this.label8.TabIndex = 0;
            this.label8.Text = "P/R";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 160);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 0;
            this.label6.Text = "行程下限";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.radioButtonAxisU);
            this.groupBox3.Controls.Add(this.radioButtonAxisZ);
            this.groupBox3.Controls.Add(this.radioButtonAxisY);
            this.groupBox3.Controls.Add(this.radioButtonAxisX);
            this.groupBox3.Controls.Add(this.numericUpDownPitch);
            this.groupBox3.Controls.Add(this.numericUpDownHighLimit);
            this.groupBox3.Controls.Add(this.numericUpDownLowLimit);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.numericUpDownPPR);
            this.groupBox3.Controls.Add(this.comboBoxSensorMode);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.comboBoxAxisType);
            this.groupBox3.Controls.Add(this.comboBoxPulseMode);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(12, 110);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(300, 197);
            this.groupBox3.TabIndex = 74;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "运动轴参数";
            // 
            // comboBoxAxisType
            // 
            this.comboBoxAxisType.FormattingEnabled = true;
            this.comboBoxAxisType.Location = new System.Drawing.Point(71, 72);
            this.comboBoxAxisType.Name = "comboBoxAxisType";
            this.comboBoxAxisType.Size = new System.Drawing.Size(210, 20);
            this.comboBoxAxisType.TabIndex = 1;
            // 
            // comboBoxPulseMode
            // 
            this.comboBoxPulseMode.FormattingEnabled = true;
            this.comboBoxPulseMode.Location = new System.Drawing.Point(71, 46);
            this.comboBoxPulseMode.Name = "comboBoxPulseMode";
            this.comboBoxPulseMode.Size = new System.Drawing.Size(210, 20);
            this.comboBoxPulseMode.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "电机细分";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(12, 76);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(53, 12);
            this.label20.TabIndex = 0;
            this.label20.Text = "轴 类 型";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "脉冲方式";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "运动轴号";
            // 
            // buttonZero
            // 
            this.buttonZero.Location = new System.Drawing.Point(350, 96);
            this.buttonZero.Name = "buttonZero";
            this.buttonZero.Size = new System.Drawing.Size(100, 32);
            this.buttonZero.TabIndex = 77;
            this.buttonZero.Text = "位置清零";
            this.buttonZero.UseVisualStyleBackColor = true;
            this.buttonZero.Click += new System.EventHandler(this.buttonZero_Click);
            // 
            // textBoxMinScore
            // 
            this.textBoxMinScore.Location = new System.Drawing.Point(127, 71);
            this.textBoxMinScore.Name = "textBoxMinScore";
            this.textBoxMinScore.Size = new System.Drawing.Size(51, 21);
            this.textBoxMinScore.TabIndex = 15;
            // 
            // buttonCalcPixelDist
            // 
            this.buttonCalcPixelDist.Location = new System.Drawing.Point(189, 44);
            this.buttonCalcPixelDist.Name = "buttonCalcPixelDist";
            this.buttonCalcPixelDist.Size = new System.Drawing.Size(75, 23);
            this.buttonCalcPixelDist.TabIndex = 21;
            this.buttonCalcPixelDist.Text = "图像校正";
            this.buttonCalcPixelDist.UseVisualStyleBackColor = true;
            this.buttonCalcPixelDist.Click += new System.EventHandler(this.buttonCalcPixelDist_Click);
            // 
            // buttonToTemplatePos
            // 
            this.buttonToTemplatePos.Location = new System.Drawing.Point(357, 426);
            this.buttonToTemplatePos.Name = "buttonToTemplatePos";
            this.buttonToTemplatePos.Size = new System.Drawing.Size(100, 28);
            this.buttonToTemplatePos.TabIndex = 60;
            this.buttonToTemplatePos.Text = "移动到模板位置";
            this.buttonToTemplatePos.UseVisualStyleBackColor = true;
            this.buttonToTemplatePos.Click += new System.EventHandler(this.buttonToTemplatePos_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(136, 53);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(17, 12);
            this.label24.TabIndex = 20;
            this.label24.Text = "ms";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(20, 20);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(53, 12);
            this.label25.TabIndex = 8;
            this.label25.Text = "校正步距";
            // 
            // buttonLoadTemplate
            // 
            this.buttonLoadTemplate.Location = new System.Drawing.Point(357, 392);
            this.buttonLoadTemplate.Name = "buttonLoadTemplate";
            this.buttonLoadTemplate.Size = new System.Drawing.Size(100, 28);
            this.buttonLoadTemplate.TabIndex = 60;
            this.buttonLoadTemplate.Text = "加载模板";
            this.buttonLoadTemplate.UseVisualStyleBackColor = true;
            this.buttonLoadTemplate.Click += new System.EventHandler(this.buttonLoadTemplate_Click);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(12, 27);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(17, 12);
            this.label29.TabIndex = 57;
            this.label29.Text = "X1";
            // 
            // textBoxCalibStepSize
            // 
            this.textBoxCalibStepSize.Location = new System.Drawing.Point(79, 17);
            this.textBoxCalibStepSize.Name = "textBoxCalibStepSize";
            this.textBoxCalibStepSize.Size = new System.Drawing.Size(51, 21);
            this.textBoxCalibStepSize.TabIndex = 15;
            // 
            // textBoxStableTime
            // 
            this.textBoxStableTime.Location = new System.Drawing.Point(79, 44);
            this.textBoxStableTime.Name = "textBoxStableTime";
            this.textBoxStableTime.Size = new System.Drawing.Size(51, 21);
            this.textBoxStableTime.TabIndex = 19;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(136, 26);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(17, 12);
            this.label31.TabIndex = 17;
            this.label31.Text = "mm";
            // 
            // buttonGetCurrentPos1
            // 
            this.buttonGetCurrentPos1.Location = new System.Drawing.Point(22, 101);
            this.buttonGetCurrentPos1.Name = "buttonGetCurrentPos1";
            this.buttonGetCurrentPos1.Size = new System.Drawing.Size(100, 32);
            this.buttonGetCurrentPos1.TabIndex = 59;
            this.buttonGetCurrentPos1.Text = "设置当前位置为圆第一点";
            this.buttonGetCurrentPos1.UseVisualStyleBackColor = true;
            this.buttonGetCurrentPos1.Click += new System.EventHandler(this.buttonGetCurrentPos1_Click);
            // 
            // textBoxWorkPosY1
            // 
            this.textBoxWorkPosY1.Location = new System.Drawing.Point(121, 24);
            this.textBoxWorkPosY1.Name = "textBoxWorkPosY1";
            this.textBoxWorkPosY1.Size = new System.Drawing.Size(50, 21);
            this.textBoxWorkPosY1.TabIndex = 58;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(20, 47);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(53, 12);
            this.label26.TabIndex = 18;
            this.label26.Text = "稳定时间";
            // 
            // textBoxWorkPosZ1
            // 
            this.textBoxWorkPosZ1.Location = new System.Drawing.Point(213, 24);
            this.textBoxWorkPosZ1.Name = "textBoxWorkPosZ1";
            this.textBoxWorkPosZ1.Size = new System.Drawing.Size(50, 21);
            this.textBoxWorkPosZ1.TabIndex = 58;
            // 
            // buttonSaveTemplate
            // 
            this.buttonSaveTemplate.Location = new System.Drawing.Point(357, 358);
            this.buttonSaveTemplate.Name = "buttonSaveTemplate";
            this.buttonSaveTemplate.Size = new System.Drawing.Size(100, 28);
            this.buttonSaveTemplate.TabIndex = 59;
            this.buttonSaveTemplate.Text = "保存模板";
            this.buttonSaveTemplate.UseVisualStyleBackColor = true;
            this.buttonSaveTemplate.Click += new System.EventHandler(this.buttonSaveTemplate_Click);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(104, 27);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(17, 12);
            this.label30.TabIndex = 57;
            this.label30.Text = "Y1";
            // 
            // buttonCameraParam
            // 
            this.buttonCameraParam.Location = new System.Drawing.Point(189, 15);
            this.buttonCameraParam.Name = "buttonCameraParam";
            this.buttonCameraParam.Size = new System.Drawing.Size(75, 23);
            this.buttonCameraParam.TabIndex = 61;
            this.buttonCameraParam.Text = "相机参数";
            this.buttonCameraParam.UseVisualStyleBackColor = true;
            this.buttonCameraParam.Click += new System.EventHandler(this.buttonCameraParam_Click);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.buttonCameraParam);
            this.groupBox9.Controls.Add(this.buttonCalcPixelDist);
            this.groupBox9.Controls.Add(this.label24);
            this.groupBox9.Controls.Add(this.textBoxCalibStepSize);
            this.groupBox9.Controls.Add(this.label25);
            this.groupBox9.Controls.Add(this.textBoxStableTime);
            this.groupBox9.Controls.Add(this.label31);
            this.groupBox9.Controls.Add(this.label26);
            this.groupBox9.Location = new System.Drawing.Point(6, 250);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(295, 76);
            this.groupBox9.TabIndex = 58;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "图像校正";
            // 
            // checkBoxReviseAngle
            // 
            this.checkBoxReviseAngle.AutoSize = true;
            this.checkBoxReviseAngle.Location = new System.Drawing.Point(213, 111);
            this.checkBoxReviseAngle.Name = "checkBoxReviseAngle";
            this.checkBoxReviseAngle.Size = new System.Drawing.Size(72, 16);
            this.checkBoxReviseAngle.TabIndex = 62;
            this.checkBoxReviseAngle.Text = "修正角度";
            this.checkBoxReviseAngle.UseVisualStyleBackColor = true;
            // 
            // buttonCircleRevise
            // 
            this.buttonCircleRevise.Location = new System.Drawing.Point(179, 176);
            this.buttonCircleRevise.Name = "buttonCircleRevise";
            this.buttonCircleRevise.Size = new System.Drawing.Size(100, 32);
            this.buttonCircleRevise.TabIndex = 59;
            this.buttonCircleRevise.Text = "定位并执行实际圆形轨迹";
            this.buttonCircleRevise.UseVisualStyleBackColor = true;
            this.buttonCircleRevise.Click += new System.EventHandler(this.buttonCircleRevise_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.buttonLight);
            this.groupBox7.Controls.Add(this.textBoxLightValue);
            this.groupBox7.Controls.Add(this.textBoxMinScore);
            this.groupBox7.Controls.Add(this.comboBoxModelType);
            this.groupBox7.Controls.Add(this.textBoxMaxAngle);
            this.groupBox7.Controls.Add(this.label19);
            this.groupBox7.Controls.Add(this.label27);
            this.groupBox7.Controls.Add(this.label21);
            this.groupBox7.Controls.Add(this.label22);
            this.groupBox7.Location = new System.Drawing.Point(307, 20);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(187, 136);
            this.groupBox7.TabIndex = 56;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "模板参数";
            // 
            // textBoxMaxAngle
            // 
            this.textBoxMaxAngle.Location = new System.Drawing.Point(127, 44);
            this.textBoxMaxAngle.Name = "textBoxMaxAngle";
            this.textBoxMaxAngle.Size = new System.Drawing.Size(51, 21);
            this.textBoxMaxAngle.TabIndex = 15;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(20, 22);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(53, 12);
            this.label19.TabIndex = 0;
            this.label19.Text = "定位类型";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(20, 101);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(101, 12);
            this.label27.TabIndex = 8;
            this.label27.Text = "灯光亮度(0--255)";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(20, 47);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(101, 12);
            this.label21.TabIndex = 4;
            this.label21.Text = "旋转角度(0--180)";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(20, 74);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(101, 12);
            this.label22.TabIndex = 6;
            this.label22.Text = "定位分数(0-1000)";
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.SystemColors.ControlDark;
            this.groupBox6.Controls.Add(this.groupBox11);
            this.groupBox6.Controls.Add(this.buttonSaveTemplate);
            this.groupBox6.Controls.Add(this.buttonToTemplatePos);
            this.groupBox6.Controls.Add(this.buttonLoadTemplate);
            this.groupBox6.Controls.Add(this.groupBox9);
            this.groupBox6.Controls.Add(this.groupBox8);
            this.groupBox6.Controls.Add(this.groupBox7);
            this.groupBox6.Controls.Add(this.pictureBox1);
            this.groupBox6.Location = new System.Drawing.Point(472, 12);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(500, 558);
            this.groupBox6.TabIndex = 83;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "视觉相关";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.checkBoxReviseAngle);
            this.groupBox11.Controls.Add(this.buttonCircleRevise);
            this.groupBox11.Controls.Add(this.buttonGetCurrentPos3);
            this.groupBox11.Controls.Add(this.buttonGetCurrentPos2);
            this.groupBox11.Controls.Add(this.buttonGetCurrentPos1);
            this.groupBox11.Controls.Add(this.textBoxWorkPosY3);
            this.groupBox11.Controls.Add(this.textBoxWorkPosZ3);
            this.groupBox11.Controls.Add(this.textBoxWorkPosY2);
            this.groupBox11.Controls.Add(this.textBoxWorkPosZ2);
            this.groupBox11.Controls.Add(this.label47);
            this.groupBox11.Controls.Add(this.textBoxWorkPosY1);
            this.groupBox11.Controls.Add(this.label41);
            this.groupBox11.Controls.Add(this.label46);
            this.groupBox11.Controls.Add(this.textBoxWorkPosZ1);
            this.groupBox11.Controls.Add(this.label40);
            this.groupBox11.Controls.Add(this.label45);
            this.groupBox11.Controls.Add(this.label29);
            this.groupBox11.Controls.Add(this.label39);
            this.groupBox11.Controls.Add(this.label44);
            this.groupBox11.Controls.Add(this.label30);
            this.groupBox11.Controls.Add(this.label38);
            this.groupBox11.Controls.Add(this.label43);
            this.groupBox11.Controls.Add(this.label35);
            this.groupBox11.Controls.Add(this.label37);
            this.groupBox11.Controls.Add(this.textBoxWorkPosX3);
            this.groupBox11.Controls.Add(this.label34);
            this.groupBox11.Controls.Add(this.textBoxWorkPosX2);
            this.groupBox11.Controls.Add(this.label42);
            this.groupBox11.Controls.Add(this.label33);
            this.groupBox11.Controls.Add(this.label36);
            this.groupBox11.Controls.Add(this.textBoxWorkPosX1);
            this.groupBox11.Controls.Add(this.label32);
            this.groupBox11.Location = new System.Drawing.Point(6, 332);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(295, 214);
            this.groupBox11.TabIndex = 61;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "圆形校正";
            // 
            // buttonGetCurrentPos3
            // 
            this.buttonGetCurrentPos3.Location = new System.Drawing.Point(22, 176);
            this.buttonGetCurrentPos3.Name = "buttonGetCurrentPos3";
            this.buttonGetCurrentPos3.Size = new System.Drawing.Size(100, 32);
            this.buttonGetCurrentPos3.TabIndex = 59;
            this.buttonGetCurrentPos3.Text = "设置当前位置为圆第三点";
            this.buttonGetCurrentPos3.UseVisualStyleBackColor = true;
            this.buttonGetCurrentPos3.Click += new System.EventHandler(this.buttonGetCurrentPos3_Click);
            // 
            // buttonGetCurrentPos2
            // 
            this.buttonGetCurrentPos2.Location = new System.Drawing.Point(22, 138);
            this.buttonGetCurrentPos2.Name = "buttonGetCurrentPos2";
            this.buttonGetCurrentPos2.Size = new System.Drawing.Size(100, 32);
            this.buttonGetCurrentPos2.TabIndex = 59;
            this.buttonGetCurrentPos2.Text = "设置当前位置为圆第二点";
            this.buttonGetCurrentPos2.UseVisualStyleBackColor = true;
            this.buttonGetCurrentPos2.Click += new System.EventHandler(this.buttonGetCurrentPos2_Click);
            // 
            // textBoxWorkPosY3
            // 
            this.textBoxWorkPosY3.Location = new System.Drawing.Point(121, 78);
            this.textBoxWorkPosY3.Name = "textBoxWorkPosY3";
            this.textBoxWorkPosY3.Size = new System.Drawing.Size(50, 21);
            this.textBoxWorkPosY3.TabIndex = 58;
            // 
            // textBoxWorkPosZ3
            // 
            this.textBoxWorkPosZ3.Location = new System.Drawing.Point(213, 78);
            this.textBoxWorkPosZ3.Name = "textBoxWorkPosZ3";
            this.textBoxWorkPosZ3.Size = new System.Drawing.Size(50, 21);
            this.textBoxWorkPosZ3.TabIndex = 58;
            // 
            // textBoxWorkPosY2
            // 
            this.textBoxWorkPosY2.Location = new System.Drawing.Point(121, 51);
            this.textBoxWorkPosY2.Name = "textBoxWorkPosY2";
            this.textBoxWorkPosY2.Size = new System.Drawing.Size(50, 21);
            this.textBoxWorkPosY2.TabIndex = 58;
            // 
            // textBoxWorkPosZ2
            // 
            this.textBoxWorkPosZ2.Location = new System.Drawing.Point(213, 51);
            this.textBoxWorkPosZ2.Name = "textBoxWorkPosZ2";
            this.textBoxWorkPosZ2.Size = new System.Drawing.Size(50, 21);
            this.textBoxWorkPosZ2.TabIndex = 58;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(12, 81);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(17, 12);
            this.label47.TabIndex = 57;
            this.label47.Text = "X3";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(12, 54);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(17, 12);
            this.label41.TabIndex = 57;
            this.label41.Text = "X2";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(104, 81);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(17, 12);
            this.label46.TabIndex = 57;
            this.label46.Text = "Y3";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(104, 54);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(17, 12);
            this.label40.TabIndex = 57;
            this.label40.Text = "Y2";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(269, 82);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(17, 12);
            this.label45.TabIndex = 17;
            this.label45.Text = "mm";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(269, 55);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(17, 12);
            this.label39.TabIndex = 17;
            this.label39.Text = "mm";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(177, 82);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(17, 12);
            this.label44.TabIndex = 17;
            this.label44.Text = "mm";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(177, 55);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(17, 12);
            this.label38.TabIndex = 17;
            this.label38.Text = "mm";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(85, 82);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(17, 12);
            this.label43.TabIndex = 17;
            this.label43.Text = "mm";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(269, 28);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(17, 12);
            this.label35.TabIndex = 17;
            this.label35.Text = "mm";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(85, 55);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(17, 12);
            this.label37.TabIndex = 17;
            this.label37.Text = "mm";
            // 
            // textBoxWorkPosX3
            // 
            this.textBoxWorkPosX3.Location = new System.Drawing.Point(29, 78);
            this.textBoxWorkPosX3.Name = "textBoxWorkPosX3";
            this.textBoxWorkPosX3.Size = new System.Drawing.Size(50, 21);
            this.textBoxWorkPosX3.TabIndex = 58;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(177, 28);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(17, 12);
            this.label34.TabIndex = 17;
            this.label34.Text = "mm";
            // 
            // textBoxWorkPosX2
            // 
            this.textBoxWorkPosX2.Location = new System.Drawing.Point(29, 51);
            this.textBoxWorkPosX2.Name = "textBoxWorkPosX2";
            this.textBoxWorkPosX2.Size = new System.Drawing.Size(50, 21);
            this.textBoxWorkPosX2.TabIndex = 58;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(196, 81);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(17, 12);
            this.label42.TabIndex = 57;
            this.label42.Text = "Z3";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(85, 28);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(17, 12);
            this.label33.TabIndex = 17;
            this.label33.Text = "mm";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(196, 54);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(17, 12);
            this.label36.TabIndex = 57;
            this.label36.Text = "Z2";
            // 
            // textBoxWorkPosX1
            // 
            this.textBoxWorkPosX1.Location = new System.Drawing.Point(29, 24);
            this.textBoxWorkPosX1.Name = "textBoxWorkPosX1";
            this.textBoxWorkPosX1.Size = new System.Drawing.Size(50, 21);
            this.textBoxWorkPosX1.TabIndex = 58;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(196, 27);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(17, 12);
            this.label32.TabIndex = 57;
            this.label32.Text = "Z1";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label23);
            this.groupBox8.Controls.Add(this.comboBoxAreaShape);
            this.groupBox8.Controls.Add(this.buttonSelectTemplateArea);
            this.groupBox8.Controls.Add(this.buttonProcessImage);
            this.groupBox8.Controls.Add(this.buttonSelectSearchArea);
            this.groupBox8.Controls.Add(this.buttonProcessTemplate);
            this.groupBox8.Location = new System.Drawing.Point(307, 162);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(187, 190);
            this.groupBox8.TabIndex = 57;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "模板设置";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(20, 23);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(53, 12);
            this.label23.TabIndex = 2;
            this.label23.Text = "区域形状";
            // 
            // comboBoxAreaShape
            // 
            this.comboBoxAreaShape.FormattingEnabled = true;
            this.comboBoxAreaShape.Location = new System.Drawing.Point(79, 20);
            this.comboBoxAreaShape.Name = "comboBoxAreaShape";
            this.comboBoxAreaShape.Size = new System.Drawing.Size(99, 20);
            this.comboBoxAreaShape.TabIndex = 3;
            // 
            // buttonSelectTemplateArea
            // 
            this.buttonSelectTemplateArea.Location = new System.Drawing.Point(50, 50);
            this.buttonSelectTemplateArea.Name = "buttonSelectTemplateArea";
            this.buttonSelectTemplateArea.Size = new System.Drawing.Size(100, 28);
            this.buttonSelectTemplateArea.TabIndex = 8;
            this.buttonSelectTemplateArea.Text = "选择模板区域";
            this.buttonSelectTemplateArea.UseVisualStyleBackColor = true;
            this.buttonSelectTemplateArea.Click += new System.EventHandler(this.buttonSelectTemplateArea_Click);
            // 
            // buttonProcessImage
            // 
            this.buttonProcessImage.Location = new System.Drawing.Point(50, 152);
            this.buttonProcessImage.Name = "buttonProcessImage";
            this.buttonProcessImage.Size = new System.Drawing.Size(100, 28);
            this.buttonProcessImage.TabIndex = 11;
            this.buttonProcessImage.Text = "定位测试";
            this.buttonProcessImage.UseVisualStyleBackColor = true;
            this.buttonProcessImage.Click += new System.EventHandler(this.buttonProcessImage_Click);
            // 
            // buttonSelectSearchArea
            // 
            this.buttonSelectSearchArea.Location = new System.Drawing.Point(50, 118);
            this.buttonSelectSearchArea.Name = "buttonSelectSearchArea";
            this.buttonSelectSearchArea.Size = new System.Drawing.Size(100, 28);
            this.buttonSelectSearchArea.TabIndex = 12;
            this.buttonSelectSearchArea.Text = "选择搜索区域";
            this.buttonSelectSearchArea.UseVisualStyleBackColor = true;
            this.buttonSelectSearchArea.Click += new System.EventHandler(this.buttonSelectSearchArea_Click);
            // 
            // buttonProcessTemplate
            // 
            this.buttonProcessTemplate.Location = new System.Drawing.Point(50, 84);
            this.buttonProcessTemplate.Name = "buttonProcessTemplate";
            this.buttonProcessTemplate.Size = new System.Drawing.Size(100, 28);
            this.buttonProcessTemplate.TabIndex = 9;
            this.buttonProcessTemplate.Text = "设定模板";
            this.buttonProcessTemplate.UseVisualStyleBackColor = true;
            this.buttonProcessTemplate.Click += new System.EventHandler(this.buttonProcessTemplate_Click);
            // 
            // timerFrame
            // 
            this.timerFrame.Interval = 200;
            this.timerFrame.Tick += new System.EventHandler(this.timerFrame_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 582);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.buttonHome);
            this.Controls.Add(this.groupBox10);
            this.Controls.Add(this.buttonStop);
            this.Controls.Add(this.buttonSaveInfo);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.buttonZero);
            this.Controls.Add(this.groupBox6);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPitch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHighLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLowLimit)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownATime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPPR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLightVaue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RadioButton radioButtonAxisU;
        private System.Windows.Forms.RadioButton radioButtonAxisZ;
        private System.Windows.Forms.RadioButton radioButtonAxisY;
        private System.Windows.Forms.RadioButton radioButtonAxisX;
        private System.Windows.Forms.NumericUpDown numericUpDownPitch;
        private System.Windows.Forms.NumericUpDown numericUpDownHighLimit;
        private System.Windows.Forms.NumericUpDown numericUpDownLowLimit;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.NumericUpDown numericUpDownDTime;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.NumericUpDown numericUpDownATime;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown numericUpDownSpeed;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericUpDownPPR;
        private System.Windows.Forms.ComboBox comboBoxSensorMode;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label11;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.NumericUpDown numericUpDownLightVaue;
        private System.Windows.Forms.Button buttonHome;
        private System.Windows.Forms.Button buttonMoveAdd;
        private System.Windows.Forms.Button buttonLight;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxLightValue;
        private System.Windows.Forms.ComboBox comboBoxModelType;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Button buttonOpenCom;
        private System.Windows.Forms.Button buttonCloseCom;
        private System.Windows.Forms.RadioButton radioButtonHigh;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.ComboBox comboBoxCardip;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button buttonSaveInfo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label_U;
        private System.Windows.Forms.Button buttonCardClose;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonCardLink;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label_Z;
        private System.Windows.Forms.Label label_Y;
        private System.Windows.Forms.Label label_X;
        private System.Windows.Forms.Button buttonMoveSub;
        private System.Windows.Forms.RadioButton radioButtonLow;
        private System.Windows.Forms.Timer timerStatus;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Timer timerMotor;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox comboBoxAxisType;
        private System.Windows.Forms.ComboBox comboBoxPulseMode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonZero;
        private System.Windows.Forms.TextBox textBoxMinScore;
        private System.Windows.Forms.Button buttonCalcPixelDist;
        private System.Windows.Forms.Button buttonToTemplatePos;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button buttonLoadTemplate;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox textBoxCalibStepSize;
        private System.Windows.Forms.TextBox textBoxStableTime;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button buttonGetCurrentPos1;
        private System.Windows.Forms.TextBox textBoxWorkPosY1;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textBoxWorkPosZ1;
        private System.Windows.Forms.Button buttonSaveTemplate;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button buttonCameraParam;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.CheckBox checkBoxReviseAngle;
        private System.Windows.Forms.Button buttonCircleRevise;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox textBoxMaxAngle;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Button buttonGetCurrentPos2;
        private System.Windows.Forms.TextBox textBoxWorkPosY2;
        private System.Windows.Forms.TextBox textBoxWorkPosZ2;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox textBoxWorkPosX2;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox textBoxWorkPosX1;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox comboBoxAreaShape;
        private System.Windows.Forms.Button buttonSelectTemplateArea;
        private System.Windows.Forms.Button buttonProcessImage;
        private System.Windows.Forms.Button buttonSelectSearchArea;
        private System.Windows.Forms.Button buttonProcessTemplate;
        private System.Windows.Forms.Timer timerFrame;
        private System.Windows.Forms.Button buttonGetCurrentPos3;
        private System.Windows.Forms.TextBox textBoxWorkPosY3;
        private System.Windows.Forms.TextBox textBoxWorkPosZ3;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox textBoxWorkPosX3;
        private System.Windows.Forms.Label label42;
    }
}

