﻿实验3_3：图像标定和圆形校正实验
简述：图像标定实验用于计算出每个图像像素对应电机走动实际距离mm/px
	通过图像标定，可以将图像坐标系和运动平台坐标系关联起来
	在算出每像素对应实际机械距离后，结合两次图像定位位置的差值计算出新的圆的三点位置

使用：鼠标右键点击"实验3_3"==>"设为启动项目"