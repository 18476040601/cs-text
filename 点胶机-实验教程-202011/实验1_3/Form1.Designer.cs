﻿namespace 实验1_3
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBoxPixelSize = new System.Windows.Forms.TextBox();
            this.textBoxObjectDist = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.buttonCalibration = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelMessage = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoxAreaShape = new System.Windows.Forms.ComboBox();
            this.textBoxLightValue = new System.Windows.Forms.TextBox();
            this.buttonSaveTemplate = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonSelectTemplateArea = new System.Windows.Forms.Button();
            this.buttonLoadTemplate = new System.Windows.Forms.Button();
            this.textBoxMinScore = new System.Windows.Forms.TextBox();
            this.comboBoxModelType = new System.Windows.Forms.ComboBox();
            this.buttonSelectSearchArea = new System.Windows.Forms.Button();
            this.textBoxMaxAngle = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonProcessImage = new System.Windows.Forms.Button();
            this.buttonLoadImage1 = new System.Windows.Forms.Button();
            this.buttonProcessTemplate = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBoxPixelSize);
            this.groupBox2.Controls.Add(this.textBoxObjectDist);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.buttonCalibration);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Location = new System.Drawing.Point(778, 350);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(194, 114);
            this.groupBox2.TabIndex = 34;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "标定参数";
            // 
            // textBoxPixelSize
            // 
            this.textBoxPixelSize.Location = new System.Drawing.Point(91, 47);
            this.textBoxPixelSize.Name = "textBoxPixelSize";
            this.textBoxPixelSize.Size = new System.Drawing.Size(87, 21);
            this.textBoxPixelSize.TabIndex = 15;
            // 
            // textBoxObjectDist
            // 
            this.textBoxObjectDist.Location = new System.Drawing.Point(91, 20);
            this.textBoxObjectDist.Name = "textBoxObjectDist";
            this.textBoxObjectDist.Size = new System.Drawing.Size(65, 21);
            this.textBoxObjectDist.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(20, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 12);
            this.label8.TabIndex = 4;
            this.label8.Text = "标的物间距";
            // 
            // buttonCalibration
            // 
            this.buttonCalibration.Location = new System.Drawing.Point(50, 74);
            this.buttonCalibration.Name = "buttonCalibration";
            this.buttonCalibration.Size = new System.Drawing.Size(100, 28);
            this.buttonCalibration.TabIndex = 11;
            this.buttonCalibration.Text = "标定测试";
            this.buttonCalibration.UseVisualStyleBackColor = true;
            this.buttonCalibration.Click += new System.EventHandler(this.buttonCalibration_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(162, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 12);
            this.label6.TabIndex = 6;
            this.label6.Text = "mm";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(20, 50);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 12);
            this.label9.TabIndex = 6;
            this.label9.Text = "每像素大小";
            // 
            // labelMessage
            // 
            this.labelMessage.AutoSize = true;
            this.labelMessage.Location = new System.Drawing.Point(12, 543);
            this.labelMessage.Name = "labelMessage";
            this.labelMessage.Size = new System.Drawing.Size(53, 12);
            this.labelMessage.TabIndex = 32;
            this.labelMessage.Text = "信息提示";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(760, 520);
            this.pictureBox1.TabIndex = 27;
            this.pictureBox1.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 2;
            this.label5.Text = "区域形状";
            // 
            // comboBoxAreaShape
            // 
            this.comboBoxAreaShape.FormattingEnabled = true;
            this.comboBoxAreaShape.Location = new System.Drawing.Point(79, 20);
            this.comboBoxAreaShape.Name = "comboBoxAreaShape";
            this.comboBoxAreaShape.Size = new System.Drawing.Size(99, 20);
            this.comboBoxAreaShape.TabIndex = 3;
            // 
            // textBoxLightValue
            // 
            this.textBoxLightValue.Location = new System.Drawing.Point(127, 98);
            this.textBoxLightValue.Name = "textBoxLightValue";
            this.textBoxLightValue.Size = new System.Drawing.Size(51, 21);
            this.textBoxLightValue.TabIndex = 15;
            // 
            // buttonSaveTemplate
            // 
            this.buttonSaveTemplate.Location = new System.Drawing.Point(828, 470);
            this.buttonSaveTemplate.Name = "buttonSaveTemplate";
            this.buttonSaveTemplate.Size = new System.Drawing.Size(100, 28);
            this.buttonSaveTemplate.TabIndex = 30;
            this.buttonSaveTemplate.Text = "保存模板";
            this.buttonSaveTemplate.UseVisualStyleBackColor = true;
            this.buttonSaveTemplate.Click += new System.EventHandler(this.buttonSaveTemplate_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(470, 543);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 28;
            this.label1.Text = "图像一";
            // 
            // buttonSelectTemplateArea
            // 
            this.buttonSelectTemplateArea.Location = new System.Drawing.Point(50, 50);
            this.buttonSelectTemplateArea.Name = "buttonSelectTemplateArea";
            this.buttonSelectTemplateArea.Size = new System.Drawing.Size(100, 28);
            this.buttonSelectTemplateArea.TabIndex = 8;
            this.buttonSelectTemplateArea.Text = "选择模板区域";
            this.buttonSelectTemplateArea.UseVisualStyleBackColor = true;
            this.buttonSelectTemplateArea.Click += new System.EventHandler(this.buttonSelectTemplateArea_Click);
            // 
            // buttonLoadTemplate
            // 
            this.buttonLoadTemplate.Location = new System.Drawing.Point(828, 504);
            this.buttonLoadTemplate.Name = "buttonLoadTemplate";
            this.buttonLoadTemplate.Size = new System.Drawing.Size(100, 28);
            this.buttonLoadTemplate.TabIndex = 31;
            this.buttonLoadTemplate.Text = "加载模板";
            this.buttonLoadTemplate.UseVisualStyleBackColor = true;
            this.buttonLoadTemplate.Click += new System.EventHandler(this.buttonLoadTemplate_Click);
            // 
            // textBoxMinScore
            // 
            this.textBoxMinScore.Location = new System.Drawing.Point(127, 71);
            this.textBoxMinScore.Name = "textBoxMinScore";
            this.textBoxMinScore.Size = new System.Drawing.Size(51, 21);
            this.textBoxMinScore.TabIndex = 15;
            // 
            // comboBoxModelType
            // 
            this.comboBoxModelType.FormattingEnabled = true;
            this.comboBoxModelType.Location = new System.Drawing.Point(79, 18);
            this.comboBoxModelType.Name = "comboBoxModelType";
            this.comboBoxModelType.Size = new System.Drawing.Size(99, 20);
            this.comboBoxModelType.TabIndex = 1;
            // 
            // buttonSelectSearchArea
            // 
            this.buttonSelectSearchArea.Location = new System.Drawing.Point(50, 118);
            this.buttonSelectSearchArea.Name = "buttonSelectSearchArea";
            this.buttonSelectSearchArea.Size = new System.Drawing.Size(100, 28);
            this.buttonSelectSearchArea.TabIndex = 12;
            this.buttonSelectSearchArea.Text = "选择搜索区域";
            this.buttonSelectSearchArea.UseVisualStyleBackColor = true;
            this.buttonSelectSearchArea.Click += new System.EventHandler(this.buttonSelectSearchArea_Click);
            // 
            // textBoxMaxAngle
            // 
            this.textBoxMaxAngle.Location = new System.Drawing.Point(127, 44);
            this.textBoxMaxAngle.Name = "textBoxMaxAngle";
            this.textBoxMaxAngle.Size = new System.Drawing.Size(51, 21);
            this.textBoxMaxAngle.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "定位类型";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxLightValue);
            this.groupBox1.Controls.Add(this.textBoxMinScore);
            this.groupBox1.Controls.Add(this.comboBoxModelType);
            this.groupBox1.Controls.Add(this.textBoxMaxAngle);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(778, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(194, 136);
            this.groupBox1.TabIndex = 33;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "模板参数";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(20, 101);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(101, 12);
            this.label27.TabIndex = 8;
            this.label27.Text = "灯光亮度(0--255)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "旋转角度(0--180)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "定位分数(0-1000)";
            // 
            // buttonProcessImage
            // 
            this.buttonProcessImage.Location = new System.Drawing.Point(50, 152);
            this.buttonProcessImage.Name = "buttonProcessImage";
            this.buttonProcessImage.Size = new System.Drawing.Size(100, 28);
            this.buttonProcessImage.TabIndex = 11;
            this.buttonProcessImage.Text = "定位测试";
            this.buttonProcessImage.UseVisualStyleBackColor = true;
            this.buttonProcessImage.Click += new System.EventHandler(this.buttonProcessImage_Click);
            // 
            // buttonLoadImage1
            // 
            this.buttonLoadImage1.Location = new System.Drawing.Point(672, 538);
            this.buttonLoadImage1.Name = "buttonLoadImage1";
            this.buttonLoadImage1.Size = new System.Drawing.Size(100, 28);
            this.buttonLoadImage1.TabIndex = 29;
            this.buttonLoadImage1.Text = "载入图像一";
            this.buttonLoadImage1.UseVisualStyleBackColor = true;
            this.buttonLoadImage1.Click += new System.EventHandler(this.buttonLoadImage1_Click);
            // 
            // buttonProcessTemplate
            // 
            this.buttonProcessTemplate.Location = new System.Drawing.Point(50, 84);
            this.buttonProcessTemplate.Name = "buttonProcessTemplate";
            this.buttonProcessTemplate.Size = new System.Drawing.Size(100, 28);
            this.buttonProcessTemplate.TabIndex = 9;
            this.buttonProcessTemplate.Text = "设定模板";
            this.buttonProcessTemplate.UseVisualStyleBackColor = true;
            this.buttonProcessTemplate.Click += new System.EventHandler(this.buttonProcessTemplate_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.comboBoxAreaShape);
            this.groupBox4.Controls.Add(this.buttonSelectTemplateArea);
            this.groupBox4.Controls.Add(this.buttonProcessImage);
            this.groupBox4.Controls.Add(this.buttonSelectSearchArea);
            this.groupBox4.Controls.Add(this.buttonProcessTemplate);
            this.groupBox4.Location = new System.Drawing.Point(778, 154);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(194, 190);
            this.groupBox4.TabIndex = 35;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "模板设置";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 582);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.labelMessage);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.buttonSaveTemplate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonLoadTemplate);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonLoadImage1);
            this.Controls.Add(this.groupBox4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBoxPixelSize;
        private System.Windows.Forms.TextBox textBoxObjectDist;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button buttonCalibration;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelMessage;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBoxAreaShape;
        private System.Windows.Forms.TextBox textBoxLightValue;
        private System.Windows.Forms.Button buttonSaveTemplate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonSelectTemplateArea;
        private System.Windows.Forms.Button buttonLoadTemplate;
        private System.Windows.Forms.TextBox textBoxMinScore;
        private System.Windows.Forms.ComboBox comboBoxModelType;
        private System.Windows.Forms.Button buttonSelectSearchArea;
        private System.Windows.Forms.TextBox textBoxMaxAngle;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonProcessImage;
        private System.Windows.Forms.Button buttonLoadImage1;
        private System.Windows.Forms.Button buttonProcessTemplate;
        private System.Windows.Forms.GroupBox groupBox4;
    }
}

