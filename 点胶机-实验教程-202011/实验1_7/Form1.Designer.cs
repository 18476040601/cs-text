﻿namespace 实验1_7
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.labelMessage = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoxAreaShape = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.buttonSelectExtractArea = new System.Windows.Forms.Button();
            this.comboBoxSpotType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonExtractSpot = new System.Windows.Forms.Button();
            this.buttonSaveExtractArea = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonLoadExtractArea = new System.Windows.Forms.Button();
            this.buttonLoadImage1 = new System.Windows.Forms.Button();
            this.groupBox4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelMessage
            // 
            this.labelMessage.AutoSize = true;
            this.labelMessage.Location = new System.Drawing.Point(12, 543);
            this.labelMessage.Name = "labelMessage";
            this.labelMessage.Size = new System.Drawing.Size(53, 12);
            this.labelMessage.TabIndex = 58;
            this.labelMessage.Text = "信息提示";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 2;
            this.label5.Text = "区域形状";
            // 
            // comboBoxAreaShape
            // 
            this.comboBoxAreaShape.FormattingEnabled = true;
            this.comboBoxAreaShape.Location = new System.Drawing.Point(71, 20);
            this.comboBoxAreaShape.Name = "comboBoxAreaShape";
            this.comboBoxAreaShape.Size = new System.Drawing.Size(110, 20);
            this.comboBoxAreaShape.TabIndex = 3;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.comboBoxAreaShape);
            this.groupBox4.Controls.Add(this.buttonSelectExtractArea);
            this.groupBox4.Location = new System.Drawing.Point(778, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(194, 100);
            this.groupBox4.TabIndex = 59;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "区域选择";
            // 
            // buttonSelectExtractArea
            // 
            this.buttonSelectExtractArea.Location = new System.Drawing.Point(50, 56);
            this.buttonSelectExtractArea.Name = "buttonSelectExtractArea";
            this.buttonSelectExtractArea.Size = new System.Drawing.Size(100, 28);
            this.buttonSelectExtractArea.TabIndex = 12;
            this.buttonSelectExtractArea.Text = "选择提取区域";
            this.buttonSelectExtractArea.UseVisualStyleBackColor = true;
            this.buttonSelectExtractArea.Click += new System.EventHandler(this.buttonSelectExtractArea_Click);
            // 
            // comboBoxSpotType
            // 
            this.comboBoxSpotType.FormattingEnabled = true;
            this.comboBoxSpotType.Location = new System.Drawing.Point(71, 20);
            this.comboBoxSpotType.Name = "comboBoxSpotType";
            this.comboBoxSpotType.Size = new System.Drawing.Size(110, 20);
            this.comboBoxSpotType.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 13;
            this.label2.Text = "污点类型";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboBoxSpotType);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.buttonExtractSpot);
            this.groupBox1.Location = new System.Drawing.Point(778, 118);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 155);
            this.groupBox1.TabIndex = 60;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "提取参数";
            // 
            // buttonExtractSpot
            // 
            this.buttonExtractSpot.Location = new System.Drawing.Point(50, 109);
            this.buttonExtractSpot.Name = "buttonExtractSpot";
            this.buttonExtractSpot.Size = new System.Drawing.Size(100, 28);
            this.buttonExtractSpot.TabIndex = 11;
            this.buttonExtractSpot.Text = "污点提取";
            this.buttonExtractSpot.UseVisualStyleBackColor = true;
            this.buttonExtractSpot.Click += new System.EventHandler(this.buttonExtractSpot_Click);
            // 
            // buttonSaveExtractArea
            // 
            this.buttonSaveExtractArea.Location = new System.Drawing.Point(828, 470);
            this.buttonSaveExtractArea.Name = "buttonSaveExtractArea";
            this.buttonSaveExtractArea.Size = new System.Drawing.Size(100, 28);
            this.buttonSaveExtractArea.TabIndex = 56;
            this.buttonSaveExtractArea.Text = "保存区域";
            this.buttonSaveExtractArea.UseVisualStyleBackColor = true;
            this.buttonSaveExtractArea.Click += new System.EventHandler(this.buttonSaveExtractArea_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(760, 520);
            this.pictureBox1.TabIndex = 53;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(470, 543);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 54;
            this.label1.Text = "图像一";
            // 
            // buttonLoadExtractArea
            // 
            this.buttonLoadExtractArea.Location = new System.Drawing.Point(828, 504);
            this.buttonLoadExtractArea.Name = "buttonLoadExtractArea";
            this.buttonLoadExtractArea.Size = new System.Drawing.Size(100, 28);
            this.buttonLoadExtractArea.TabIndex = 57;
            this.buttonLoadExtractArea.Text = "加载区域";
            this.buttonLoadExtractArea.UseVisualStyleBackColor = true;
            this.buttonLoadExtractArea.Click += new System.EventHandler(this.buttonLoadExtractArea_Click);
            // 
            // buttonLoadImage1
            // 
            this.buttonLoadImage1.Location = new System.Drawing.Point(672, 538);
            this.buttonLoadImage1.Name = "buttonLoadImage1";
            this.buttonLoadImage1.Size = new System.Drawing.Size(100, 28);
            this.buttonLoadImage1.TabIndex = 55;
            this.buttonLoadImage1.Text = "载入图像一";
            this.buttonLoadImage1.UseVisualStyleBackColor = true;
            this.buttonLoadImage1.Click += new System.EventHandler(this.buttonLoadImage1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 582);
            this.Controls.Add(this.labelMessage);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonSaveExtractArea);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonLoadExtractArea);
            this.Controls.Add(this.buttonLoadImage1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelMessage;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBoxAreaShape;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button buttonSelectExtractArea;
        private System.Windows.Forms.ComboBox comboBoxSpotType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonExtractSpot;
        private System.Windows.Forms.Button buttonSaveExtractArea;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonLoadExtractArea;
        private System.Windows.Forms.Button buttonLoadImage1;
    }
}

