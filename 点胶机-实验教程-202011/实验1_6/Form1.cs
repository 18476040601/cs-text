﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
///视觉库命名空间
using XTVisioncs;

namespace 实验1_6
{
    /// <summary>
    /// 实验1_6：轮廓提取实验
    /// 从图像中提取物体轮廓并给出轮廓四个方向的极值坐标点
    /// </summary>
    public partial class Form1 : Form
    {
        //每个窗口绑定到一个编号，每个图像绑定到一个图像号

        //给每一个窗口分配一个0--255之间的编号
        int windowNum1 = 10;

        //给每一个图像分配一个0--255之间的编号
        //图像号
        int imageNum1 = 12;

        //给每一个区域分配一个0--255之间的编号
        //搜索号
        int roiNum1 = 4;

        public Form1()
        {
            InitializeComponent();
            InitializeForm();
        }

        //窗体辅助初始化
        void InitializeForm()
        {
            //初始化视觉库
            XTVision_V1.XTInitialize(true);

            //视觉窗口背景色
            pictureBox1.BackColor = Color.LightGray;

            //区域形状
            comboBoxAreaShape.Items.Add("矩形形状");
            comboBoxAreaShape.Items.Add("圆形形状");
            comboBoxAreaShape.Items.Add("任意形状");
            comboBoxAreaShape.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxAreaShape.SelectedIndex = 0;

            //提取参数
            textBoxMinGray.Text = "0";
            textBoxMaxGray.Text = "100";
            textBoxMinSize.Text = "300";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                //从文件中加载图像
                int dispType = 0;   //图像显示方式
                string filePath = @"..\\..\\..\\image\circle_plate_01.png";
                Size imgSize = Image.FromFile(filePath).Size;
                int imageWidth = imgSize.Width;
                int imageHeight = imgSize.Height;

                //关闭视觉窗口windowNum1
                XTVision_V1.XTCloseWindow(windowNum1);
                //初始化视觉窗口/参数为窗口号，将要绑定到的控件句柄，图像宽，图像高
                XTVision_V1.XTInitWindow(windowNum1, pictureBox1.Handle, imageWidth, imageHeight);
                //从文件中加载图像到图像imageNum1中
                XTVision_V1.XTLoadImage(imageNum1, filePath);
                //将图像imageNum1显示到窗口windowNum1中
                XTVision_V1.XTDisplayPicture(windowNum1, imageNum1, dispType);

                //总文件中加载提取区域
                buttonLoadExtractArea_Click(sender, e);
                //显示提取区域roiNum1到窗口windowNum1中
                XTVision_V1.XTDisplayRegion(windowNum1, roiNum1, 1, "blue", dispType);
            }
            catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.Message); }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            //反初始化视觉库/释放资源
            XTVision_V1.XTUninitialize();
        }

        private void buttonLoadImage1_Click(object sender, EventArgs e)
        {
            string resultFile = null;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image Files(*.BMP;*.PNG;*.JPG;*.GIF)|*.BMP;*.PNG;*.JPG;*.GIF|All files(*.*)|*.* ";
            openFileDialog.FilterIndex = 0;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                resultFile = openFileDialog.FileName;
                if (string.IsNullOrEmpty(resultFile)) return;
                try
                {
                    int dispType = 0;   //图像显示方式
                    Size imgSize = Image.FromFile(resultFile).Size;
                    int imageWidth = imgSize.Width;
                    int imageHeight = imgSize.Height;

                    //关闭视觉窗口windowNum1
                    XTVision_V1.XTCloseWindow(windowNum1);
                    //初始化视觉窗口/参数为窗口号，将要绑定到的控件句柄，图像宽，图像高
                    XTVision_V1.XTInitWindow(windowNum1, pictureBox1.Handle, imageWidth, imageHeight);
                    //从文件中加载图像到图像imageNum1中
                    XTVision_V1.XTLoadImage(imageNum1, resultFile);
                    //将图像imageNum1显示到窗口windowNum1中
                    XTVision_V1.XTDisplayPicture(windowNum1, imageNum1, dispType);
                }
                catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.Message); }
            }
        }

        private void buttonSelectExtractArea_Click(object sender, EventArgs e)
        {
            //将焦点转移到图像控件上
            pictureBox1.Focus();
            //鼠标画图形状0-矩形 1-圆形 2-任意形状
            int AreaShape = comboBoxAreaShape.SelectedIndex;

            labelMessage.Text = @"提示：请用鼠标左键在图像上选择和修改区域，右键结束修改！";

            //调用“选择提取区域”函数/每调用一次函数，都需要鼠标在窗口上做画图操作并以右键结束
            int err = XTVision_V1.XTSelectRegion(windowNum1, imageNum1, roiNum1, AreaShape, "red", "blue");

            labelMessage.Text = @"信息提示";
        }

        private void buttonExtractContours_Click(object sender, EventArgs e)
        {
            int minGray = Convert.ToInt32(textBoxMinGray.Text);     //最小灰度
            int maxGray = Convert.ToInt32(textBoxMaxGray.Text);     //最大灰度
            int minSize = Convert.ToInt32(textBoxMinSize.Text);     //最小尺寸
            int contourNumber = 0;                                  //提取到的轮廓数量
            int err = XTVision_V1.XTExtractContour(imageNum1, roiNum1, minGray, maxGray, minSize, ref contourNumber);

            //更新图像显示
            XTVision_V1.XTDisplayPicture(windowNum1, imageNum1, 0);
            //显示提取区域(搜索区)
            XTVision_V1.XTDisplayExtractContour(windowNum1, roiNum1, -1, "green", 0);

            //获取轮廓上极值点
            float[] CoorXL = new float[contourNumber];
            float[] CoorYL = new float[contourNumber];
            float[] CoorXR = new float[contourNumber];
            float[] CoorYR = new float[contourNumber];
            float[] CoorXT = new float[contourNumber];
            float[] CoorYT = new float[contourNumber];
            float[] CoorXB = new float[contourNumber];
            float[] CoorYB = new float[contourNumber];

            //获取轮廓上极值点/结果类型[0最左坐标X/1最左坐标Y/2最右坐标X/3最右坐标Y/4最上坐标X/5最上坐标Y/6最下坐标X/7最下坐标Y]
            XTVision_V1.XTReadExtractContourResult(roiNum1, -1, 0, CoorXL);
            XTVision_V1.XTReadExtractContourResult(roiNum1, -1, 1, CoorYL);
            XTVision_V1.XTReadExtractContourResult(roiNum1, -1, 2, CoorXR);
            XTVision_V1.XTReadExtractContourResult(roiNum1, -1, 3, CoorYR);
            XTVision_V1.XTReadExtractContourResult(roiNum1, -1, 4, CoorXT);
            XTVision_V1.XTReadExtractContourResult(roiNum1, -1, 5, CoorYT);
            XTVision_V1.XTReadExtractContourResult(roiNum1, -1, 6, CoorXB);
            XTVision_V1.XTReadExtractContourResult(roiNum1, -1, 7, CoorYB);

            //显示轮廓上极值点
            for (int idx = 0; idx < contourNumber; ++idx)
            {
                XTVision_V1.XTDispPoint(windowNum1, CoorYL[idx], CoorXL[idx], 10, 45, 1, "red", 0);
                XTVision_V1.XTDispPoint(windowNum1, CoorYR[idx], CoorXR[idx], 10, 45, 1, "red", 0);
                XTVision_V1.XTDispPoint(windowNum1, CoorYT[idx], CoorXT[idx], 10, 45, 1, "red", 0);
                XTVision_V1.XTDispPoint(windowNum1, CoorYB[idx], CoorXB[idx], 10, 45, 1, "red", 0);
            }

            labelMessage.Text = string.Format("提示：总共提取到 {0} 个轮廓", contourNumber);
        }

        private void buttonSaveExtractArea_Click(object sender, EventArgs e)
        {
            try
            {
                string fileDir = "实验1_6";
                if (Directory.Exists(fileDir) == false)
                {
                    Directory.CreateDirectory(fileDir);
                }

                string filePath = null;
                filePath = fileDir + "/" + "roi" + roiNum1;
                //将提取区域保存到"roi" + roiNum1文件中
                XTVision_V1.XTSaveRegion(roiNum1, filePath);
            }
            catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.Message); }
        }

        private void buttonLoadExtractArea_Click(object sender, EventArgs e)
        {
            try
            {
                string fileDir = "实验1_6";
                if (Directory.Exists(fileDir) == false)
                {
                    Directory.CreateDirectory(fileDir);
                }

                string filePath = null;
                filePath = fileDir + "/" + "roi" + roiNum1;
                //从"roi" + roiNum1文件中加载提取区域
                XTVision_V1.XTLoadRegion(roiNum1, filePath);
            }
            catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.Message); }
        }
    }
}
