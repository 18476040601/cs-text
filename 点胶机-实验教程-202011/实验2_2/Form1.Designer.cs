﻿namespace 实验2_2
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.checkBoxInvertIn23 = new System.Windows.Forms.CheckBox();
            this.In23 = new System.Windows.Forms.CheckBox();
            this.In15 = new System.Windows.Forms.CheckBox();
            this.In7 = new System.Windows.Forms.CheckBox();
            this.In22 = new System.Windows.Forms.CheckBox();
            this.In14 = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.labelAxisU = new System.Windows.Forms.Label();
            this.labelAxisZ = new System.Windows.Forms.Label();
            this.labelAxisY = new System.Windows.Forms.Label();
            this.labelAxisX = new System.Windows.Forms.Label();
            this.In6 = new System.Windows.Forms.CheckBox();
            this.In21 = new System.Windows.Forms.CheckBox();
            this.labelIn23 = new System.Windows.Forms.Label();
            this.comboBoxCardip = new System.Windows.Forms.ComboBox();
            this.In13 = new System.Windows.Forms.CheckBox();
            this.buttonCardClose = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.labelIn22 = new System.Windows.Forms.Label();
            this.checkBoxInvertIn22 = new System.Windows.Forms.CheckBox();
            this.labelIn21 = new System.Windows.Forms.Label();
            this.checkBoxInvertIn21 = new System.Windows.Forms.CheckBox();
            this.labelIn20 = new System.Windows.Forms.Label();
            this.checkBoxInvertIn20 = new System.Windows.Forms.CheckBox();
            this.labelIn19 = new System.Windows.Forms.Label();
            this.checkBoxInvertIn19 = new System.Windows.Forms.CheckBox();
            this.labelIn18 = new System.Windows.Forms.Label();
            this.checkBoxInvertIn18 = new System.Windows.Forms.CheckBox();
            this.buttonCardLink = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.In5 = new System.Windows.Forms.CheckBox();
            this.In20 = new System.Windows.Forms.CheckBox();
            this.In12 = new System.Windows.Forms.CheckBox();
            this.In4 = new System.Windows.Forms.CheckBox();
            this.In19 = new System.Windows.Forms.CheckBox();
            this.In11 = new System.Windows.Forms.CheckBox();
            this.In3 = new System.Windows.Forms.CheckBox();
            this.In18 = new System.Windows.Forms.CheckBox();
            this.In10 = new System.Windows.Forms.CheckBox();
            this.In2 = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.In17 = new System.Windows.Forms.CheckBox();
            this.In9 = new System.Windows.Forms.CheckBox();
            this.In1 = new System.Windows.Forms.CheckBox();
            this.In16 = new System.Windows.Forms.CheckBox();
            this.In8 = new System.Windows.Forms.CheckBox();
            this.In0 = new System.Windows.Forms.CheckBox();
            this.Out0 = new System.Windows.Forms.CheckBox();
            this.Out6 = new System.Windows.Forms.CheckBox();
            this.Out7 = new System.Windows.Forms.CheckBox();
            this.Out5 = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Out4 = new System.Windows.Forms.CheckBox();
            this.Out3 = new System.Windows.Forms.CheckBox();
            this.Out2 = new System.Windows.Forms.CheckBox();
            this.Out1 = new System.Windows.Forms.CheckBox();
            this.timerStatus = new System.Windows.Forms.Timer(this.components);
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkBoxInvertIn23
            // 
            this.checkBoxInvertIn23.AutoSize = true;
            this.checkBoxInvertIn23.Location = new System.Drawing.Point(586, 49);
            this.checkBoxInvertIn23.Name = "checkBoxInvertIn23";
            this.checkBoxInvertIn23.Size = new System.Drawing.Size(48, 16);
            this.checkBoxInvertIn23.TabIndex = 0;
            this.checkBoxInvertIn23.Text = "In23";
            this.checkBoxInvertIn23.UseVisualStyleBackColor = true;
            this.checkBoxInvertIn23.CheckedChanged += new System.EventHandler(this.checkBoxInvertIn23_CheckedChanged);
            // 
            // In23
            // 
            this.In23.AutoSize = true;
            this.In23.Location = new System.Drawing.Point(376, 213);
            this.In23.Name = "In23";
            this.In23.Size = new System.Drawing.Size(48, 16);
            this.In23.TabIndex = 0;
            this.In23.Text = "In23";
            this.In23.UseVisualStyleBackColor = true;
            // 
            // In15
            // 
            this.In15.AutoSize = true;
            this.In15.Location = new System.Drawing.Point(200, 213);
            this.In15.Name = "In15";
            this.In15.Size = new System.Drawing.Size(48, 16);
            this.In15.TabIndex = 0;
            this.In15.Text = "In15";
            this.In15.UseVisualStyleBackColor = true;
            // 
            // In7
            // 
            this.In7.AutoSize = true;
            this.In7.Location = new System.Drawing.Point(24, 213);
            this.In7.Name = "In7";
            this.In7.Size = new System.Drawing.Size(48, 16);
            this.In7.TabIndex = 0;
            this.In7.Text = "In07";
            this.In7.UseVisualStyleBackColor = true;
            // 
            // In22
            // 
            this.In22.AutoSize = true;
            this.In22.Location = new System.Drawing.Point(376, 186);
            this.In22.Name = "In22";
            this.In22.Size = new System.Drawing.Size(48, 16);
            this.In22.TabIndex = 0;
            this.In22.Text = "In22";
            this.In22.UseVisualStyleBackColor = true;
            // 
            // In14
            // 
            this.In14.AutoSize = true;
            this.In14.Location = new System.Drawing.Point(200, 186);
            this.In14.Name = "In14";
            this.In14.Size = new System.Drawing.Size(48, 16);
            this.In14.TabIndex = 0;
            this.In14.Text = "In14";
            this.In14.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.labelAxisU);
            this.groupBox3.Controls.Add(this.labelAxisZ);
            this.groupBox3.Controls.Add(this.labelAxisY);
            this.groupBox3.Controls.Add(this.labelAxisX);
            this.groupBox3.Location = new System.Drawing.Point(12, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(720, 77);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "运动轴状态";
            // 
            // labelAxisU
            // 
            this.labelAxisU.AutoSize = true;
            this.labelAxisU.Location = new System.Drawing.Point(374, 47);
            this.labelAxisU.Name = "labelAxisU";
            this.labelAxisU.Size = new System.Drawing.Size(83, 12);
            this.labelAxisU.TabIndex = 0;
            this.labelAxisU.Text = "U轴状态：正常";
            // 
            // labelAxisZ
            // 
            this.labelAxisZ.AutoSize = true;
            this.labelAxisZ.Location = new System.Drawing.Point(374, 24);
            this.labelAxisZ.Name = "labelAxisZ";
            this.labelAxisZ.Size = new System.Drawing.Size(83, 12);
            this.labelAxisZ.TabIndex = 0;
            this.labelAxisZ.Text = "Z轴状态：正常";
            // 
            // labelAxisY
            // 
            this.labelAxisY.AutoSize = true;
            this.labelAxisY.Location = new System.Drawing.Point(22, 47);
            this.labelAxisY.Name = "labelAxisY";
            this.labelAxisY.Size = new System.Drawing.Size(83, 12);
            this.labelAxisY.TabIndex = 0;
            this.labelAxisY.Text = "Y轴状态：正常";
            // 
            // labelAxisX
            // 
            this.labelAxisX.AutoSize = true;
            this.labelAxisX.Location = new System.Drawing.Point(23, 24);
            this.labelAxisX.Name = "labelAxisX";
            this.labelAxisX.Size = new System.Drawing.Size(83, 12);
            this.labelAxisX.TabIndex = 0;
            this.labelAxisX.Text = "X轴状态：正常";
            // 
            // In6
            // 
            this.In6.AutoSize = true;
            this.In6.Location = new System.Drawing.Point(24, 186);
            this.In6.Name = "In6";
            this.In6.Size = new System.Drawing.Size(48, 16);
            this.In6.TabIndex = 0;
            this.In6.Text = "In06";
            this.In6.UseVisualStyleBackColor = true;
            // 
            // In21
            // 
            this.In21.AutoSize = true;
            this.In21.Location = new System.Drawing.Point(376, 159);
            this.In21.Name = "In21";
            this.In21.Size = new System.Drawing.Size(48, 16);
            this.In21.TabIndex = 0;
            this.In21.Text = "In21";
            this.In21.UseVisualStyleBackColor = true;
            // 
            // labelIn23
            // 
            this.labelIn23.AutoSize = true;
            this.labelIn23.Location = new System.Drawing.Point(584, 24);
            this.labelIn23.Name = "labelIn23";
            this.labelIn23.Size = new System.Drawing.Size(29, 12);
            this.labelIn23.TabIndex = 1;
            this.labelIn23.Text = "In23";
            // 
            // comboBoxCardip
            // 
            this.comboBoxCardip.FormattingEnabled = true;
            this.comboBoxCardip.Location = new System.Drawing.Point(71, 20);
            this.comboBoxCardip.Name = "comboBoxCardip";
            this.comboBoxCardip.Size = new System.Drawing.Size(210, 20);
            this.comboBoxCardip.TabIndex = 1;
            // 
            // In13
            // 
            this.In13.AutoSize = true;
            this.In13.Location = new System.Drawing.Point(200, 159);
            this.In13.Name = "In13";
            this.In13.Size = new System.Drawing.Size(48, 16);
            this.In13.TabIndex = 0;
            this.In13.Text = "In13";
            this.In13.UseVisualStyleBackColor = true;
            // 
            // buttonCardClose
            // 
            this.buttonCardClose.Location = new System.Drawing.Point(20, 46);
            this.buttonCardClose.Name = "buttonCardClose";
            this.buttonCardClose.Size = new System.Drawing.Size(100, 32);
            this.buttonCardClose.TabIndex = 3;
            this.buttonCardClose.Text = "断开";
            this.buttonCardClose.UseVisualStyleBackColor = true;
            this.buttonCardClose.Click += new System.EventHandler(this.buttonCardClose_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 24);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 12);
            this.label12.TabIndex = 0;
            this.label12.Text = "控制器IP";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.labelIn23);
            this.groupBox5.Controls.Add(this.checkBoxInvertIn23);
            this.groupBox5.Controls.Add(this.labelIn22);
            this.groupBox5.Controls.Add(this.checkBoxInvertIn22);
            this.groupBox5.Controls.Add(this.labelIn21);
            this.groupBox5.Controls.Add(this.checkBoxInvertIn21);
            this.groupBox5.Controls.Add(this.labelIn20);
            this.groupBox5.Controls.Add(this.checkBoxInvertIn20);
            this.groupBox5.Controls.Add(this.labelIn19);
            this.groupBox5.Controls.Add(this.checkBoxInvertIn19);
            this.groupBox5.Controls.Add(this.labelIn18);
            this.groupBox5.Controls.Add(this.checkBoxInvertIn18);
            this.groupBox5.Location = new System.Drawing.Point(12, 351);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(720, 81);
            this.groupBox5.TabIndex = 12;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "输入口配置";
            // 
            // labelIn22
            // 
            this.labelIn22.AutoSize = true;
            this.labelIn22.Location = new System.Drawing.Point(468, 24);
            this.labelIn22.Name = "labelIn22";
            this.labelIn22.Size = new System.Drawing.Size(29, 12);
            this.labelIn22.TabIndex = 1;
            this.labelIn22.Text = "In22";
            // 
            // checkBoxInvertIn22
            // 
            this.checkBoxInvertIn22.AutoSize = true;
            this.checkBoxInvertIn22.Location = new System.Drawing.Point(470, 49);
            this.checkBoxInvertIn22.Name = "checkBoxInvertIn22";
            this.checkBoxInvertIn22.Size = new System.Drawing.Size(48, 16);
            this.checkBoxInvertIn22.TabIndex = 0;
            this.checkBoxInvertIn22.Text = "In22";
            this.checkBoxInvertIn22.UseVisualStyleBackColor = true;
            this.checkBoxInvertIn22.CheckedChanged += new System.EventHandler(this.checkBoxInvertIn22_CheckedChanged);
            // 
            // labelIn21
            // 
            this.labelIn21.AutoSize = true;
            this.labelIn21.Location = new System.Drawing.Point(356, 24);
            this.labelIn21.Name = "labelIn21";
            this.labelIn21.Size = new System.Drawing.Size(29, 12);
            this.labelIn21.TabIndex = 1;
            this.labelIn21.Text = "In21";
            // 
            // checkBoxInvertIn21
            // 
            this.checkBoxInvertIn21.AutoSize = true;
            this.checkBoxInvertIn21.Location = new System.Drawing.Point(358, 49);
            this.checkBoxInvertIn21.Name = "checkBoxInvertIn21";
            this.checkBoxInvertIn21.Size = new System.Drawing.Size(48, 16);
            this.checkBoxInvertIn21.TabIndex = 0;
            this.checkBoxInvertIn21.Text = "In21";
            this.checkBoxInvertIn21.UseVisualStyleBackColor = true;
            this.checkBoxInvertIn21.CheckedChanged += new System.EventHandler(this.checkBoxInvertIn21_CheckedChanged);
            // 
            // labelIn20
            // 
            this.labelIn20.AutoSize = true;
            this.labelIn20.Location = new System.Drawing.Point(244, 24);
            this.labelIn20.Name = "labelIn20";
            this.labelIn20.Size = new System.Drawing.Size(29, 12);
            this.labelIn20.TabIndex = 1;
            this.labelIn20.Text = "In20";
            // 
            // checkBoxInvertIn20
            // 
            this.checkBoxInvertIn20.AutoSize = true;
            this.checkBoxInvertIn20.Location = new System.Drawing.Point(246, 49);
            this.checkBoxInvertIn20.Name = "checkBoxInvertIn20";
            this.checkBoxInvertIn20.Size = new System.Drawing.Size(48, 16);
            this.checkBoxInvertIn20.TabIndex = 0;
            this.checkBoxInvertIn20.Text = "In20";
            this.checkBoxInvertIn20.UseVisualStyleBackColor = true;
            this.checkBoxInvertIn20.CheckedChanged += new System.EventHandler(this.checkBoxInvertIn20_CheckedChanged);
            // 
            // labelIn19
            // 
            this.labelIn19.AutoSize = true;
            this.labelIn19.Location = new System.Drawing.Point(132, 24);
            this.labelIn19.Name = "labelIn19";
            this.labelIn19.Size = new System.Drawing.Size(29, 12);
            this.labelIn19.TabIndex = 1;
            this.labelIn19.Text = "In19";
            // 
            // checkBoxInvertIn19
            // 
            this.checkBoxInvertIn19.AutoSize = true;
            this.checkBoxInvertIn19.Location = new System.Drawing.Point(134, 49);
            this.checkBoxInvertIn19.Name = "checkBoxInvertIn19";
            this.checkBoxInvertIn19.Size = new System.Drawing.Size(48, 16);
            this.checkBoxInvertIn19.TabIndex = 0;
            this.checkBoxInvertIn19.Text = "In19";
            this.checkBoxInvertIn19.UseVisualStyleBackColor = true;
            this.checkBoxInvertIn19.CheckedChanged += new System.EventHandler(this.checkBoxInvertIn19_CheckedChanged);
            // 
            // labelIn18
            // 
            this.labelIn18.AutoSize = true;
            this.labelIn18.Location = new System.Drawing.Point(22, 24);
            this.labelIn18.Name = "labelIn18";
            this.labelIn18.Size = new System.Drawing.Size(29, 12);
            this.labelIn18.TabIndex = 1;
            this.labelIn18.Text = "In18";
            // 
            // checkBoxInvertIn18
            // 
            this.checkBoxInvertIn18.AutoSize = true;
            this.checkBoxInvertIn18.Location = new System.Drawing.Point(22, 49);
            this.checkBoxInvertIn18.Name = "checkBoxInvertIn18";
            this.checkBoxInvertIn18.Size = new System.Drawing.Size(48, 16);
            this.checkBoxInvertIn18.TabIndex = 0;
            this.checkBoxInvertIn18.Text = "In18";
            this.checkBoxInvertIn18.UseVisualStyleBackColor = true;
            this.checkBoxInvertIn18.CheckedChanged += new System.EventHandler(this.checkBoxInvertIn18_CheckedChanged);
            // 
            // buttonCardLink
            // 
            this.buttonCardLink.Location = new System.Drawing.Point(181, 46);
            this.buttonCardLink.Name = "buttonCardLink";
            this.buttonCardLink.Size = new System.Drawing.Size(100, 32);
            this.buttonCardLink.TabIndex = 3;
            this.buttonCardLink.Text = "连接";
            this.buttonCardLink.UseVisualStyleBackColor = true;
            this.buttonCardLink.Click += new System.EventHandler(this.buttonCardLink_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.comboBoxCardip);
            this.groupBox4.Controls.Add(this.buttonCardClose);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.buttonCardLink);
            this.groupBox4.Location = new System.Drawing.Point(12, 438);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(300, 92);
            this.groupBox4.TabIndex = 11;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "控制器连接";
            // 
            // In5
            // 
            this.In5.AutoSize = true;
            this.In5.Location = new System.Drawing.Point(24, 159);
            this.In5.Name = "In5";
            this.In5.Size = new System.Drawing.Size(48, 16);
            this.In5.TabIndex = 0;
            this.In5.Text = "In05";
            this.In5.UseVisualStyleBackColor = true;
            // 
            // In20
            // 
            this.In20.AutoSize = true;
            this.In20.Location = new System.Drawing.Point(376, 132);
            this.In20.Name = "In20";
            this.In20.Size = new System.Drawing.Size(48, 16);
            this.In20.TabIndex = 0;
            this.In20.Text = "In20";
            this.In20.UseVisualStyleBackColor = true;
            // 
            // In12
            // 
            this.In12.AutoSize = true;
            this.In12.Location = new System.Drawing.Point(200, 132);
            this.In12.Name = "In12";
            this.In12.Size = new System.Drawing.Size(48, 16);
            this.In12.TabIndex = 0;
            this.In12.Text = "In12";
            this.In12.UseVisualStyleBackColor = true;
            // 
            // In4
            // 
            this.In4.AutoSize = true;
            this.In4.Location = new System.Drawing.Point(24, 132);
            this.In4.Name = "In4";
            this.In4.Size = new System.Drawing.Size(48, 16);
            this.In4.TabIndex = 0;
            this.In4.Text = "In04";
            this.In4.UseVisualStyleBackColor = true;
            // 
            // In19
            // 
            this.In19.AutoSize = true;
            this.In19.Location = new System.Drawing.Point(376, 105);
            this.In19.Name = "In19";
            this.In19.Size = new System.Drawing.Size(48, 16);
            this.In19.TabIndex = 0;
            this.In19.Text = "In19";
            this.In19.UseVisualStyleBackColor = true;
            // 
            // In11
            // 
            this.In11.AutoSize = true;
            this.In11.Location = new System.Drawing.Point(200, 105);
            this.In11.Name = "In11";
            this.In11.Size = new System.Drawing.Size(48, 16);
            this.In11.TabIndex = 0;
            this.In11.Text = "In11";
            this.In11.UseVisualStyleBackColor = true;
            // 
            // In3
            // 
            this.In3.AutoSize = true;
            this.In3.Location = new System.Drawing.Point(24, 105);
            this.In3.Name = "In3";
            this.In3.Size = new System.Drawing.Size(48, 16);
            this.In3.TabIndex = 0;
            this.In3.Text = "In03";
            this.In3.UseVisualStyleBackColor = true;
            // 
            // In18
            // 
            this.In18.AutoSize = true;
            this.In18.Location = new System.Drawing.Point(376, 78);
            this.In18.Name = "In18";
            this.In18.Size = new System.Drawing.Size(48, 16);
            this.In18.TabIndex = 0;
            this.In18.Text = "In18";
            this.In18.UseVisualStyleBackColor = true;
            // 
            // In10
            // 
            this.In10.AutoSize = true;
            this.In10.Location = new System.Drawing.Point(200, 78);
            this.In10.Name = "In10";
            this.In10.Size = new System.Drawing.Size(48, 16);
            this.In10.TabIndex = 0;
            this.In10.Text = "In10";
            this.In10.UseVisualStyleBackColor = true;
            // 
            // In2
            // 
            this.In2.AutoSize = true;
            this.In2.Location = new System.Drawing.Point(24, 78);
            this.In2.Name = "In2";
            this.In2.Size = new System.Drawing.Size(48, 16);
            this.In2.TabIndex = 0;
            this.In2.Text = "In02";
            this.In2.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.In23);
            this.groupBox1.Controls.Add(this.In15);
            this.groupBox1.Controls.Add(this.In7);
            this.groupBox1.Controls.Add(this.In22);
            this.groupBox1.Controls.Add(this.In14);
            this.groupBox1.Controls.Add(this.In6);
            this.groupBox1.Controls.Add(this.In21);
            this.groupBox1.Controls.Add(this.In13);
            this.groupBox1.Controls.Add(this.In5);
            this.groupBox1.Controls.Add(this.In20);
            this.groupBox1.Controls.Add(this.In12);
            this.groupBox1.Controls.Add(this.In4);
            this.groupBox1.Controls.Add(this.In19);
            this.groupBox1.Controls.Add(this.In11);
            this.groupBox1.Controls.Add(this.In3);
            this.groupBox1.Controls.Add(this.In18);
            this.groupBox1.Controls.Add(this.In10);
            this.groupBox1.Controls.Add(this.In2);
            this.groupBox1.Controls.Add(this.In17);
            this.groupBox1.Controls.Add(this.In9);
            this.groupBox1.Controls.Add(this.In1);
            this.groupBox1.Controls.Add(this.In16);
            this.groupBox1.Controls.Add(this.In8);
            this.groupBox1.Controls.Add(this.In0);
            this.groupBox1.Location = new System.Drawing.Point(12, 95);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(556, 250);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "输入口状态";
            // 
            // In17
            // 
            this.In17.AutoSize = true;
            this.In17.Location = new System.Drawing.Point(376, 51);
            this.In17.Name = "In17";
            this.In17.Size = new System.Drawing.Size(48, 16);
            this.In17.TabIndex = 0;
            this.In17.Text = "In17";
            this.In17.UseVisualStyleBackColor = true;
            // 
            // In9
            // 
            this.In9.AutoSize = true;
            this.In9.Location = new System.Drawing.Point(200, 51);
            this.In9.Name = "In9";
            this.In9.Size = new System.Drawing.Size(48, 16);
            this.In9.TabIndex = 0;
            this.In9.Text = "In09";
            this.In9.UseVisualStyleBackColor = true;
            // 
            // In1
            // 
            this.In1.AutoSize = true;
            this.In1.Location = new System.Drawing.Point(24, 51);
            this.In1.Name = "In1";
            this.In1.Size = new System.Drawing.Size(48, 16);
            this.In1.TabIndex = 0;
            this.In1.Text = "In01";
            this.In1.UseVisualStyleBackColor = true;
            // 
            // In16
            // 
            this.In16.AutoSize = true;
            this.In16.Location = new System.Drawing.Point(376, 24);
            this.In16.Name = "In16";
            this.In16.Size = new System.Drawing.Size(48, 16);
            this.In16.TabIndex = 0;
            this.In16.Text = "In16";
            this.In16.UseVisualStyleBackColor = true;
            // 
            // In8
            // 
            this.In8.AutoSize = true;
            this.In8.Location = new System.Drawing.Point(200, 24);
            this.In8.Name = "In8";
            this.In8.Size = new System.Drawing.Size(48, 16);
            this.In8.TabIndex = 0;
            this.In8.Text = "In08";
            this.In8.UseVisualStyleBackColor = true;
            // 
            // In0
            // 
            this.In0.AutoSize = true;
            this.In0.Location = new System.Drawing.Point(24, 24);
            this.In0.Name = "In0";
            this.In0.Size = new System.Drawing.Size(48, 16);
            this.In0.TabIndex = 0;
            this.In0.Text = "In00";
            this.In0.UseVisualStyleBackColor = true;
            // 
            // Out0
            // 
            this.Out0.AutoSize = true;
            this.Out0.Location = new System.Drawing.Point(24, 24);
            this.Out0.Name = "Out0";
            this.Out0.Size = new System.Drawing.Size(54, 16);
            this.Out0.TabIndex = 0;
            this.Out0.Text = "Out00";
            this.Out0.UseVisualStyleBackColor = true;
            this.Out0.CheckedChanged += new System.EventHandler(this.Out0_CheckedChanged);
            // 
            // Out6
            // 
            this.Out6.AutoSize = true;
            this.Out6.Location = new System.Drawing.Point(24, 186);
            this.Out6.Name = "Out6";
            this.Out6.Size = new System.Drawing.Size(54, 16);
            this.Out6.TabIndex = 0;
            this.Out6.Text = "Out06";
            this.Out6.UseVisualStyleBackColor = true;
            this.Out6.CheckedChanged += new System.EventHandler(this.Out6_CheckedChanged);
            // 
            // Out7
            // 
            this.Out7.AutoSize = true;
            this.Out7.Location = new System.Drawing.Point(24, 213);
            this.Out7.Name = "Out7";
            this.Out7.Size = new System.Drawing.Size(54, 16);
            this.Out7.TabIndex = 0;
            this.Out7.Text = "Out07";
            this.Out7.UseVisualStyleBackColor = true;
            this.Out7.CheckedChanged += new System.EventHandler(this.Out7_CheckedChanged);
            // 
            // Out5
            // 
            this.Out5.AutoSize = true;
            this.Out5.Location = new System.Drawing.Point(24, 159);
            this.Out5.Name = "Out5";
            this.Out5.Size = new System.Drawing.Size(54, 16);
            this.Out5.TabIndex = 0;
            this.Out5.Text = "Out05";
            this.Out5.UseVisualStyleBackColor = true;
            this.Out5.CheckedChanged += new System.EventHandler(this.Out5_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.Out7);
            this.groupBox2.Controls.Add(this.Out6);
            this.groupBox2.Controls.Add(this.Out5);
            this.groupBox2.Controls.Add(this.Out4);
            this.groupBox2.Controls.Add(this.Out3);
            this.groupBox2.Controls.Add(this.Out2);
            this.groupBox2.Controls.Add(this.Out1);
            this.groupBox2.Controls.Add(this.Out0);
            this.groupBox2.Location = new System.Drawing.Point(574, 95);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(158, 250);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "输出口设置";
            // 
            // Out4
            // 
            this.Out4.AutoSize = true;
            this.Out4.Location = new System.Drawing.Point(24, 132);
            this.Out4.Name = "Out4";
            this.Out4.Size = new System.Drawing.Size(54, 16);
            this.Out4.TabIndex = 0;
            this.Out4.Text = "Out04";
            this.Out4.UseVisualStyleBackColor = true;
            this.Out4.CheckedChanged += new System.EventHandler(this.Out4_CheckedChanged);
            // 
            // Out3
            // 
            this.Out3.AutoSize = true;
            this.Out3.Location = new System.Drawing.Point(24, 105);
            this.Out3.Name = "Out3";
            this.Out3.Size = new System.Drawing.Size(54, 16);
            this.Out3.TabIndex = 0;
            this.Out3.Text = "Out03";
            this.Out3.UseVisualStyleBackColor = true;
            this.Out3.CheckedChanged += new System.EventHandler(this.Out3_CheckedChanged);
            // 
            // Out2
            // 
            this.Out2.AutoSize = true;
            this.Out2.Location = new System.Drawing.Point(24, 78);
            this.Out2.Name = "Out2";
            this.Out2.Size = new System.Drawing.Size(54, 16);
            this.Out2.TabIndex = 0;
            this.Out2.Text = "Out02";
            this.Out2.UseVisualStyleBackColor = true;
            this.Out2.CheckedChanged += new System.EventHandler(this.Out2_CheckedChanged);
            // 
            // Out1
            // 
            this.Out1.AutoSize = true;
            this.Out1.Location = new System.Drawing.Point(24, 51);
            this.Out1.Name = "Out1";
            this.Out1.Size = new System.Drawing.Size(54, 16);
            this.Out1.TabIndex = 0;
            this.Out1.Text = "Out01";
            this.Out1.UseVisualStyleBackColor = true;
            this.Out1.CheckedChanged += new System.EventHandler(this.Out1_CheckedChanged);
            // 
            // timerStatus
            // 
            this.timerStatus.Tick += new System.EventHandler(this.timerStatus_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 582);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBoxInvertIn23;
        private System.Windows.Forms.CheckBox In23;
        private System.Windows.Forms.CheckBox In15;
        private System.Windows.Forms.CheckBox In7;
        private System.Windows.Forms.CheckBox In22;
        private System.Windows.Forms.CheckBox In14;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label labelAxisU;
        private System.Windows.Forms.Label labelAxisZ;
        private System.Windows.Forms.Label labelAxisY;
        private System.Windows.Forms.Label labelAxisX;
        private System.Windows.Forms.CheckBox In6;
        private System.Windows.Forms.CheckBox In21;
        private System.Windows.Forms.Label labelIn23;
        private System.Windows.Forms.ComboBox comboBoxCardip;
        private System.Windows.Forms.CheckBox In13;
        private System.Windows.Forms.Button buttonCardClose;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label labelIn22;
        private System.Windows.Forms.CheckBox checkBoxInvertIn22;
        private System.Windows.Forms.Label labelIn21;
        private System.Windows.Forms.CheckBox checkBoxInvertIn21;
        private System.Windows.Forms.Label labelIn20;
        private System.Windows.Forms.CheckBox checkBoxInvertIn20;
        private System.Windows.Forms.Label labelIn19;
        private System.Windows.Forms.CheckBox checkBoxInvertIn19;
        private System.Windows.Forms.Label labelIn18;
        private System.Windows.Forms.CheckBox checkBoxInvertIn18;
        private System.Windows.Forms.Button buttonCardLink;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox In5;
        private System.Windows.Forms.CheckBox In20;
        private System.Windows.Forms.CheckBox In12;
        private System.Windows.Forms.CheckBox In4;
        private System.Windows.Forms.CheckBox In19;
        private System.Windows.Forms.CheckBox In11;
        private System.Windows.Forms.CheckBox In3;
        private System.Windows.Forms.CheckBox In18;
        private System.Windows.Forms.CheckBox In10;
        private System.Windows.Forms.CheckBox In2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox In17;
        private System.Windows.Forms.CheckBox In9;
        private System.Windows.Forms.CheckBox In1;
        private System.Windows.Forms.CheckBox In16;
        private System.Windows.Forms.CheckBox In8;
        private System.Windows.Forms.CheckBox In0;
        private System.Windows.Forms.CheckBox Out0;
        private System.Windows.Forms.CheckBox Out6;
        private System.Windows.Forms.CheckBox Out7;
        private System.Windows.Forms.CheckBox Out5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox Out4;
        private System.Windows.Forms.CheckBox Out3;
        private System.Windows.Forms.CheckBox Out2;
        private System.Windows.Forms.CheckBox Out1;
        private System.Windows.Forms.Timer timerStatus;
    }
}

