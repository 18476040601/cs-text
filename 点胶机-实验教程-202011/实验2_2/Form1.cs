﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
///运动控制库命名空间
using zmcauxcs;

namespace 实验2_2
{
    /// <summary>
    /// 实验2_2：运动控制器IO端口查看和控制
    /// 简述：通过API接口查看运动控制器对应输入端口是否有信号；通过输出端口实现对气缸、电磁阀等外设的控制；了解传感器常开常闭的接法并能通过控制器内部反转输入信号来适应不同的接线方式
    /// </summary>
    public partial class Form1 : Form
    {
        //控制器句柄
        IntPtr g_handle;

        //控制器轴数量
        int AxisNum = 4;

        //轴参数配置数组
        AxisConfig[] AxisConfigList;

        //轴速度配置数组
        BSpeed[] AxisSpeedList;

        //构造函数
        public Form1()
        {
            InitializeComponent();
            InitializeForm();
        }

        //读取配置文件
        void ReadIni()
        {
            //运行目录下的card.ini文件
            string filePath = Application.StartupPath + "\\card.ini";
            //可变字符串
            StringBuilder temp = new StringBuilder(128);

            //读取轴参数
            for (int idx = 0; idx < AxisNum; ++idx)
            {
                Win32.GetPrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "PPR", "10000", temp, 128, filePath);
                AxisConfigList[idx].PPR = Convert.ToInt32(temp.ToString());

                Win32.GetPrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "Pitch", "10", temp, 128, filePath);
                AxisConfigList[idx].Pitch = Convert.ToDouble(temp.ToString());

                Win32.GetPrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "PulseMode", "0", temp, 128, filePath);
                AxisConfigList[idx].PulseMode = Convert.ToInt32(temp.ToString());

                Win32.GetPrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "AxisType", "0", temp, 128, filePath);
                AxisConfigList[idx].AxisType = Convert.ToInt32(temp.ToString());

                Win32.GetPrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "Merge", "1", temp, 128, filePath);
                AxisConfigList[idx].Merge = Convert.ToInt32(temp.ToString());

                Win32.GetPrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "LowLimit", "0", temp, 128, filePath);
                AxisConfigList[idx].LowLimit = Convert.ToInt32(temp.ToString());

                Win32.GetPrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "HighLimit", "300", temp, 128, filePath);
                AxisConfigList[idx].HighLimit = Convert.ToInt32(temp.ToString());

                Win32.GetPrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "HomeDir", "4", temp, 128, filePath);
                AxisConfigList[idx].HomeMode = Convert.ToInt32(temp.ToString());

                Win32.GetPrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "SensorMode", "0", temp, 128, filePath);
                AxisConfigList[idx].SensorMode = Convert.ToInt32(temp.ToString());
            }

            //读取轴速度
            for (int idx = 0; idx < AxisNum; ++idx)
            {
                Win32.GetPrivateProfileString(string.Format("AXIS_SPEED{0}", idx), "SPEED", "10", temp, 128, filePath);
                AxisSpeedList[idx].Speed = Convert.ToDouble(temp.ToString());

                Win32.GetPrivateProfileString(string.Format("AXIS_SPEED{0}", idx), "ATime", "10", temp, 128, filePath);
                AxisSpeedList[idx].ATime = Convert.ToDouble(temp.ToString());

                Win32.GetPrivateProfileString(string.Format("AXIS_SPEED{0}", idx), "DTime", "10", temp, 128, filePath);
                AxisSpeedList[idx].DTime = Convert.ToDouble(temp.ToString());
            }
        }

        //写入配置文件
        void SaveIni()
        {
            //运行目录下的card.ini文件
            string filePath = Application.StartupPath + "\\card.ini";

            //保存轴参数
            for (int idx = 0; idx < AxisNum; ++idx)
            {
                Win32.WritePrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "PPR", Convert.ToString(AxisConfigList[idx].PPR), filePath);
                Win32.WritePrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "Pitch", Convert.ToString(AxisConfigList[idx].Pitch), filePath);
                Win32.WritePrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "PulseMode", Convert.ToString(AxisConfigList[idx].PulseMode), filePath);
                Win32.WritePrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "AxisType", Convert.ToString(AxisConfigList[idx].AxisType), filePath);
                Win32.WritePrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "Merge", Convert.ToString(AxisConfigList[idx].Merge), filePath);
                Win32.WritePrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "LowLimit", Convert.ToString(AxisConfigList[idx].LowLimit), filePath);
                Win32.WritePrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "HighLimit", Convert.ToString(AxisConfigList[idx].HighLimit), filePath);
                Win32.WritePrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "HomeDir", Convert.ToString(AxisConfigList[idx].HomeMode), filePath);
                Win32.WritePrivateProfileString(string.Format("AXIS_CONFIG{0}", idx), "SensorMode", Convert.ToString(AxisConfigList[idx].SensorMode), filePath);
            }

            //保存轴速度
            for (int idx = 0; idx < AxisNum; ++idx)
            {
                Win32.WritePrivateProfileString(string.Format("AXIS_SPEED{0}", idx), "SPEED", Convert.ToString(AxisSpeedList[idx].Speed), filePath);
                Win32.WritePrivateProfileString(string.Format("AXIS_SPEED{0}", idx), "ATime", Convert.ToString(AxisSpeedList[idx].ATime), filePath);
                Win32.WritePrivateProfileString(string.Format("AXIS_SPEED{0}", idx), "DTime", Convert.ToString(AxisSpeedList[idx].DTime), filePath);
            }
        }

        //窗口辅助初始化
        void InitializeForm()
        {
            //控制器IP
            comboBoxCardip.Items.Add("192.168.0.11");
            comboBoxCardip.Items.Add("127.0.0.1");
            comboBoxCardip.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxCardip.SelectedIndex = 0;

            labelIn18.Text = "X轴负限位(In18)";
            labelIn19.Text = "X轴正限位(In19)";
            labelIn20.Text = "X轴原点(In20)";
            labelIn21.Text = "Y轴原点(In21)";
            labelIn22.Text = "X轴告警(In22)";
            labelIn23.Text = "Y轴告警(In23)";

            checkBoxInvertIn18.Text = "常开";
            checkBoxInvertIn19.Text = "常开";
            checkBoxInvertIn20.Text = "常开";
            checkBoxInvertIn21.Text = "常开";
            checkBoxInvertIn22.Text = "常开";
            checkBoxInvertIn23.Text = "常开";

            In18.Text = "In18(X轴负硬限位)";
            In19.Text = "In19(X轴正硬限位)";
            In20.Text = "In20(X轴原点)";
            In21.Text = "In21(Y轴原点)";
            In22.Text = "In22(X轴告警)";
            In23.Text = "In23(Y轴告警)";

            Out0.Text = "Out00(关闭)";
            Out1.Text = "Out01(关闭)";
            Out2.Text = "Out02(关闭)";
            Out3.Text = "Out03(关闭)";
            Out4.Text = "Out04(关闭)";
            Out5.Text = "Out05(关闭)";
            Out6.Text = "Out06(关闭)";
            Out7.Text = "Out07(关闭)";

            //轴参数
            AxisConfigList = new AxisConfig[AxisNum];
            for (int idx = 0; idx < AxisNum; ++idx)
            {
                AxisConfigList[idx] = new AxisConfig();
            }

            //轴速度
            AxisSpeedList = new BSpeed[AxisNum];
            for (int idx = 0; idx < AxisNum; ++idx)
            {
                AxisSpeedList[idx] = new BSpeed();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //读取配置
            ReadIni();

            //启动定时器，显示当前轴参数信息
            timerStatus.Start();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            //断开链接/关闭运动控制器
            zmcaux.ZAux_Close(g_handle);
            g_handle = (IntPtr)0;

            this.Text = "控制器已经断开";
        }

        private void buttonCardLink_Click(object sender, EventArgs e)
        {
            //链接控制器 
            string ipaddr = comboBoxCardip.Text;
            zmcaux.ZAux_OpenEth(ipaddr, out g_handle);

            //判断句柄是否有效
            if ((int)g_handle != 0)
            {
                this.Text = "控制器链接成功";
            }
            else
            {
                this.Text = "控制器链接失败";
            }

            if ((int)g_handle != 0)
            {
                int err = 0;

                //设置脉冲输出类型：脉冲+方向/双脉冲
                for (int Cnt = 0; Cnt < AxisNum; Cnt++)
                {
                    AxisConfig aParam = AxisConfigList[Cnt];
                    int PulseMode = aParam.PulseMode;

                    //设置脉冲方式
                    //0 脉冲方向：下跳沿有效
                    //1 脉冲方向：上跳沿有效
                    //2 脉冲方向方式，下跳沿有效，方向线反转
                    //3 脉冲方向方式，上跳沿有效，方向线反转
                    //4 双脉冲常态低，缺省电平
                    //5 双脉冲常态高，缺省电平
                    //6 双脉冲常态低
                    //7 双脉冲常态高
                    err = zmcaux.ZAux_Direct_SetInvertStep(g_handle, Cnt, PulseMode);

                    //设置轴类型
                    int AxisType = 1;                               //轴类型：脉冲方向方式的步进或伺服
                    if (aParam.AxisType == 0) { AxisType = 1; }     //脉冲轴:无编码反馈
                    if (aParam.AxisType == 1) { AxisType = 4; }     //编码轴:带编码反馈
                    err = zmcaux.ZAux_Direct_SetAtype(g_handle, Cnt, AxisType);

                    if (err != 0) return;
                }

                //设定脉冲当量：每转脉冲数/丝杆导程
                for (int Cnt = 0; Cnt < AxisNum; Cnt++)
                {
                    AxisConfig aParam = AxisConfigList[Cnt];
                    float PPR = aParam.PPR;                 //电机细分
                    float Pitch = (float)aParam.Pitch;      //丝杆导程
                    err = zmcaux.ZAux_Direct_SetUnits(g_handle, Cnt, PPR / Pitch);

                    if (err != 0) return;
                }

                //设置正负软限位
                for (int Cnt = 0; Cnt < AxisNum; ++Cnt)
                {
                    AxisConfig aParam = AxisConfigList[Cnt];
                    float LowLimit = aParam.LowLimit;
                    float HighLimit = aParam.HighLimit;

                    err = zmcaux.ZAux_Direct_SetRsLimit(g_handle, Cnt, LowLimit);	//设置负向软限位位置/需要取消是设置成特别小的数即可，如-200000000
                    err = zmcaux.ZAux_Direct_SetFsLimit(g_handle, Cnt, HighLimit);	//设置正向软限位位置/需要取消是设置成特别大的数即可，如+200000000

                    if (err != 0) return;
                }

                //取消正负硬限位/当不接正负硬限位传感器时可以取消
                for (int Cnt = 0; Cnt < AxisNum; ++Cnt)
                {
                    err = zmcaux.ZAux_Direct_SetRevIn(g_handle, Cnt, -1);           //取消负向硬限位位置
                    err = zmcaux.ZAux_Direct_SetFwdIn(g_handle, Cnt, -1);           //取消正向硬限位位置

                    if (err != 0) return;
                }

                //取消轴报警信号/当不接轴报警信号到控制器时可以取消
                for (int Cnt = 0; Cnt < AxisNum; ++Cnt)
                {
                    err = zmcaux.ZAux_Direct_SetAlmIn(g_handle, Cnt, -1);           //取消轴报警信号

                    if (err != 0) return;
                }

                //设置运动速度以及加速度和减速度
                for (int Cnt = 0; Cnt < AxisNum; Cnt++)
                {
                    double speed = AxisSpeedList[Cnt].Speed;
                    double accel = speed / AxisSpeedList[Cnt].ATime * 1000;
                    double decel = speed / AxisSpeedList[Cnt].DTime * 1000;

                    err = zmcaux.ZAux_Direct_SetSpeed(g_handle, Cnt, (float)speed); //设置轴速度
                    err = zmcaux.ZAux_Direct_SetAccel(g_handle, Cnt, (float)accel); //设置轴加速度
                    err = zmcaux.ZAux_Direct_SetDecel(g_handle, Cnt, (float)decel); //设置轴减速度
                }
            }
            else
            {
                this.Text = "控制器尚未链接";
            }
        }

        private void buttonCardClose_Click(object sender, EventArgs e)
        {
            //断开链接/关闭卡
            zmcaux.ZAux_Close(g_handle);
            g_handle = (IntPtr)0;

            this.Text = "控制器已经断开";
        }

        private void timerStatus_Tick(object sender, EventArgs e)
        {
            if ((int)g_handle != 0)
            {
                //轴状态读数组
                int[] axisStatus = new int[AxisNum];
                for (int idx = 0; idx < AxisNum; ++idx)
                {
                    //读取当前轴状态
                    zmcaux.ZAux_Direct_GetAxisStatus(g_handle, idx, ref axisStatus[idx]);
                }

                if (axisStatus[0] == 0)
                    labelAxisX.Text = "X轴状态：正常";
                else
                    labelAxisX.Text = "X轴状态：异常" + axisStatus[0];

                if (axisStatus[1] == 0)
                    labelAxisY.Text = "Y轴状态：正常";
                else
                    labelAxisY.Text = "Y轴状态：异常" + axisStatus[1];

                if (axisStatus[2] == 0)
                    labelAxisZ.Text = "Z轴状态：正常";
                else
                    labelAxisZ.Text = "Z轴状态：异常" + axisStatus[2];

                if (axisStatus[3] == 0)
                    labelAxisU.Text = "U轴状态：正常";
                else
                    labelAxisU.Text = "U轴状态：异常" + axisStatus[3];


                //多选框集合
                CheckBox[] checkBoxIn = new CheckBox[] { 
                In0, In1, In2, In3, In4, In5, In6, In7,
                In8, In9, In10, In11, In12, In13, In14, In15, 
                In16, In17, In18, In19, In20, In21, In22, In23 };

                //IO状态读取
                for (int idx = 0; idx < checkBoxIn.Length; ++idx)
                {
                    uint uIo = 0;
                    //读取输入口状态
                    zmcaux.ZAux_Direct_GetIn(g_handle, idx, ref uIo);
                    checkBoxIn[idx].Checked = uIo == 1 ? true : false;
                }
            }
        }

        private void Out0_CheckedChanged(object sender, EventArgs e)
        {
            if ((int)g_handle == 0)
            {
                MessageBox.Show("未链接到控制器!", "提示");
                return;
            }
            if (Out0.Checked)
            {
                Out0.Text = "Out00(打开)";
            }
            else
            {
                Out0.Text = "Out00(关闭)";
            }
            //设置输出口状态
            zmcaux.ZAux_Direct_SetOp(g_handle, 0, (uint)(Out0.Checked == true ? 1 : 0));
        }

        private void Out1_CheckedChanged(object sender, EventArgs e)
        {
            if ((int)g_handle == 0)
            {
                MessageBox.Show("未链接到控制器!", "提示");
                return;
            }
            if (Out1.Checked)
            {
                Out1.Text = "Out01(打开)";
            }
            else
            {
                Out1.Text = "Out01(关闭)";
            }
            //设置输出口状态
            zmcaux.ZAux_Direct_SetOp(g_handle, 1, (uint)(Out1.Checked == true ? 1 : 0));
        }

        private void Out2_CheckedChanged(object sender, EventArgs e)
        {
            if ((int)g_handle == 0)
            {
                MessageBox.Show("未链接到控制器!", "提示");
                return;
            }
            if (Out2.Checked)
            {
                Out2.Text = "Out02(打开)";
            }
            else
            {
                Out2.Text = "Out02(关闭)";
            }
            //设置输出口状态
            zmcaux.ZAux_Direct_SetOp(g_handle, 2, (uint)(Out2.Checked == true ? 1 : 0));
        }

        private void Out3_CheckedChanged(object sender, EventArgs e)
        {
            if ((int)g_handle == 0)
            {
                MessageBox.Show("未链接到控制器!", "提示");
                return;
            }
            if (Out3.Checked)
            {
                Out3.Text = "Out03(打开)";
            }
            else
            {
                Out3.Text = "Out03(关闭)";
            }
            //设置输出口状态
            zmcaux.ZAux_Direct_SetOp(g_handle, 3, (uint)(Out3.Checked == true ? 1 : 0));
        }

        private void Out4_CheckedChanged(object sender, EventArgs e)
        {
            if ((int)g_handle == 0)
            {
                MessageBox.Show("未链接到控制器!", "提示");
                return;
            }
            if (Out4.Checked)
            {
                Out4.Text = "Out04(打开)";
            }
            else
            {
                Out4.Text = "Out04(关闭)";
            }
            //设置输出口状态
            zmcaux.ZAux_Direct_SetOp(g_handle, 4, (uint)(Out4.Checked == true ? 1 : 0));
        }

        private void Out5_CheckedChanged(object sender, EventArgs e)
        {
            if ((int)g_handle == 0)
            {
                MessageBox.Show("未链接到控制器!", "提示");
                return;
            }
            if (Out5.Checked)
            {
                Out5.Text = "Out05(打开)";
            }
            else
            {
                Out5.Text = "Out05(关闭)";
            }
            //设置输出口状态
            zmcaux.ZAux_Direct_SetOp(g_handle, 5, (uint)(Out5.Checked == true ? 1 : 0));
        }

        private void Out6_CheckedChanged(object sender, EventArgs e)
        {
            if ((int)g_handle == 0)
            {
                MessageBox.Show("未链接到控制器!", "提示");
                return;
            }
            if (Out6.Checked)
            {
                Out6.Text = "Out06(打开)";
            }
            else
            {
                Out6.Text = "Out06(关闭)";
            }
            //设置输出口状态
            zmcaux.ZAux_Direct_SetOp(g_handle, 6, (uint)(Out6.Checked == true ? 1 : 0));
        }

        private void Out7_CheckedChanged(object sender, EventArgs e)
        {
            if ((int)g_handle == 0)
            {
                MessageBox.Show("未链接到控制器!", "提示");
                return;
            }
            if (Out7.Checked)
            {
                Out7.Text = "Out07(打开)";
            }
            else
            {
                Out7.Text = "Out07(关闭)";
            }
            //设置输出口状态
            zmcaux.ZAux_Direct_SetOp(g_handle, 7, (uint)(Out7.Checked == true ? 1 : 0));
        }

        private void checkBoxInvertIn18_CheckedChanged(object sender, EventArgs e)
        {
            if ((int)g_handle == 0)
            {
                MessageBox.Show("未链接到控制器!", "提示");
                return;
            }
            if (checkBoxInvertIn18.Checked)
            {
                //反转输入端口状态
                zmcaux.ZAux_Direct_SetInvertIn(g_handle, 18, 1);
                checkBoxInvertIn18.Text = "常闭";
            }
            else
            {
                //取消输入端口反转状态
                zmcaux.ZAux_Direct_SetInvertIn(g_handle, 18, 0);
                checkBoxInvertIn18.Text = "常开";
            }
        }

        private void checkBoxInvertIn19_CheckedChanged(object sender, EventArgs e)
        {
            if ((int)g_handle == 0)
            {
                MessageBox.Show("未链接到控制器!", "提示");
                return;
            }
            if (checkBoxInvertIn19.Checked)
            {
                //反转输入端口状态
                zmcaux.ZAux_Direct_SetInvertIn(g_handle, 19, 1);
                checkBoxInvertIn19.Text = "常闭";
            }
            else
            {
                //取消输入端口反转状态
                zmcaux.ZAux_Direct_SetInvertIn(g_handle, 19, 0);
                checkBoxInvertIn19.Text = "常开";
            }
        }

        private void checkBoxInvertIn20_CheckedChanged(object sender, EventArgs e)
        {
            if ((int)g_handle == 0)
            {
                MessageBox.Show("未链接到控制器!", "提示");
                return;
            }
            if (checkBoxInvertIn20.Checked)
            {
                //反转输入端口状态
                zmcaux.ZAux_Direct_SetInvertIn(g_handle, 20, 1);
                checkBoxInvertIn20.Text = "常闭";
            }
            else
            {
                //取消输入端口反转状态
                zmcaux.ZAux_Direct_SetInvertIn(g_handle, 20, 0);
                checkBoxInvertIn20.Text = "常开";
            }
        }

        private void checkBoxInvertIn21_CheckedChanged(object sender, EventArgs e)
        {
            if ((int)g_handle == 0)
            {
                MessageBox.Show("未链接到控制器!", "提示");
                return;
            }
            if (checkBoxInvertIn21.Checked)
            {
                //反转输入端口状态
                zmcaux.ZAux_Direct_SetInvertIn(g_handle, 21, 1);
                checkBoxInvertIn21.Text = "常闭";
            }
            else
            {
                //取消输入端口反转状态
                zmcaux.ZAux_Direct_SetInvertIn(g_handle, 12, 0);
                checkBoxInvertIn21.Text = "常开";
            }
        }

        private void checkBoxInvertIn22_CheckedChanged(object sender, EventArgs e)
        {
            if ((int)g_handle == 0)
            {
                MessageBox.Show("未链接到控制器!", "提示");
                return;
            }
            if (checkBoxInvertIn22.Checked)
            {
                //反转输入端口状态
                zmcaux.ZAux_Direct_SetInvertIn(g_handle, 22, 1);
                checkBoxInvertIn22.Text = "常闭";
            }
            else
            {
                //取消输入端口反转状态
                zmcaux.ZAux_Direct_SetInvertIn(g_handle, 22, 0);
                checkBoxInvertIn22.Text = "常开";
            }
        }

        private void checkBoxInvertIn23_CheckedChanged(object sender, EventArgs e)
        {
            if ((int)g_handle == 0)
            {
                MessageBox.Show("未链接到控制器!", "提示");
                return;
            }
            if (checkBoxInvertIn23.Checked)
            {
                //反转输入端口状态
                zmcaux.ZAux_Direct_SetInvertIn(g_handle, 23, 1);
                checkBoxInvertIn23.Text = "常闭";
            }
            else
            {
                //取消输入端口反转状态
                zmcaux.ZAux_Direct_SetInvertIn(g_handle, 23, 0);
                checkBoxInvertIn23.Text = "常开";
            }
        }
    }

    #region

    //文件读写
    public class Win32
    {
        /// <summary>
        /// 读取配置文件/系统函数
        /// </summary>
        /// <param name="lpAppName">小节名称</param>
        /// <param name="lpKeyName">条目名称</param>
        /// <param name="lpDefault">条目没有找到时返回的默认值</param>
        /// <param name="lpReturnedString">指定一个字串缓冲区，长度至少为nSize</param>
        /// <param name="nSize">指定装载到lpReturnedString缓冲区的最大字符数量</param>
        /// <param name="lpFileName">操作的文件名称，如没有指定一个完整路径名，Windows就在Windows目录中查找文件</param>
        /// <returns></returns>
        [DllImport("kernel32.dll")]
        public static extern int GetPrivateProfileString(string lpAppName, string lpKeyName, string lpDefault, StringBuilder lpReturnedString, int nSize, string lpFileName);

        /// <summary>
        /// 写入配置文件/系统函数
        /// </summary>
        /// <param name="lpAppName">小节名称</param>
        /// <param name="lpKeyName">条目名称</param>
        /// <param name="lpString">条目字符串值</param>
        /// <param name="lpFileName">操作的文件名称，如没有指定一个完整路径名，Windows就在Windows目录中写入文件</param>
        /// <returns></returns>
        [DllImport("kernel32.dll")]
        public static extern int WritePrivateProfileString(string lpAppName, string lpKeyName, string lpString, string lpFileName);
    }

    //轴基本配置
    public class AxisConfig
    {
        //轴基本参数
        public int PPR = 10000;         //电机细分
        public double Pitch = 10;       //丝杆螺距
        public int PulseMode = 0;       //电机脉冲模式(支持八种模式)
        public int AxisType = 0;        //0虚拟轴[对应值1]/1带编码反馈轴[对应值4]
        public int Merge = 1;           //0关闭连续插补/1开启连续插补
        public int LowLimit = 0;        //软下限位
        public int HighLimit = 300;     //软上限位

        //轴复位方式
        public int HomeMode = 4;        //复位模式(3:正向复位，4:反向复位)
        public int SensorMode = 0;      //原点电平(0:低电平，1:高电平)
        public bool UseAxis = true;     //是否启用轴
    }

    //轴基本速度
    public class BSpeed
    {
        public double Speed = 50;       //工作速度
        public double ATime = 100;      //加速时间
        public double DTime = 100;      //减速时间
    }

    #endregion
}
