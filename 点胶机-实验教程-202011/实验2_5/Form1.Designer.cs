﻿namespace 实验2_5
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonMoveAdd = new System.Windows.Forms.Button();
            this.buttonHome = new System.Windows.Forms.Button();
            this.comboBoxCardip = new System.Windows.Forms.ComboBox();
            this.buttonStop = new System.Windows.Forms.Button();
            this.radioButtonAxisZ = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.radioButtonAxisU = new System.Windows.Forms.RadioButton();
            this.radioButtonAxisY = new System.Windows.Forms.RadioButton();
            this.radioButtonAxisX = new System.Windows.Forms.RadioButton();
            this.numericUpDownPitch = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownHighLimit = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownLowLimit = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDownPPR = new System.Windows.Forms.NumericUpDown();
            this.comboBoxSensorMode = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBoxAxisType = new System.Windows.Forms.ComboBox();
            this.comboBoxPulseMode = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonCardClose = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.radioButtonHigh = new System.Windows.Forms.RadioButton();
            this.buttonMoveSub = new System.Windows.Forms.Button();
            this.radioButtonLow = new System.Windows.Forms.RadioButton();
            this.label_U = new System.Windows.Forms.Label();
            this.buttonZero = new System.Windows.Forms.Button();
            this.buttonSaveInfo = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonCardLink = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label_Z = new System.Windows.Forms.Label();
            this.label_Y = new System.Windows.Forms.Label();
            this.label_X = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.textBoxDistanceZ = new System.Windows.Forms.TextBox();
            this.textBoxDistanceY = new System.Windows.Forms.TextBox();
            this.textBoxDistanceX = new System.Windows.Forms.TextBox();
            this.buttonMultiMoveAbs = new System.Windows.Forms.Button();
            this.buttonMultiMoveInc = new System.Windows.Forms.Button();
            this.label29 = new System.Windows.Forms.Label();
            this.timerMotor = new System.Windows.Forms.Timer(this.components);
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.textBoxSpeed = new System.Windows.Forms.TextBox();
            this.textBoxATime = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.textBoxX2 = new System.Windows.Forms.TextBox();
            this.textBoxX1 = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.textBoxZ2 = new System.Windows.Forms.TextBox();
            this.textBoxZ1 = new System.Windows.Forms.TextBox();
            this.textBoxY2 = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.textBoxY1 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.label23 = new System.Windows.Forms.Label();
            this.textBoxDTime = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.numericUpDownATime = new System.Windows.Forms.NumericUpDown();
            this.timerStatus = new System.Windows.Forms.Timer(this.components);
            this.numericUpDownDTime = new System.Windows.Forms.NumericUpDown();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.numericUpDownSpeed = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPitch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHighLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLowLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPPR)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownATime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDTime)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpeed)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonMoveAdd
            // 
            this.buttonMoveAdd.Location = new System.Drawing.Point(12, 44);
            this.buttonMoveAdd.Name = "buttonMoveAdd";
            this.buttonMoveAdd.Size = new System.Drawing.Size(100, 32);
            this.buttonMoveAdd.TabIndex = 3;
            this.buttonMoveAdd.Text = "正向运动";
            this.buttonMoveAdd.UseVisualStyleBackColor = true;
            this.buttonMoveAdd.MouseDown += new System.Windows.Forms.MouseEventHandler(this.buttonMoveAdd_MouseDown);
            this.buttonMoveAdd.MouseUp += new System.Windows.Forms.MouseEventHandler(this.buttonMoveAdd_MouseUp);
            // 
            // buttonHome
            // 
            this.buttonHome.Location = new System.Drawing.Point(350, 20);
            this.buttonHome.Name = "buttonHome";
            this.buttonHome.Size = new System.Drawing.Size(100, 32);
            this.buttonHome.TabIndex = 41;
            this.buttonHome.Text = "回零";
            this.buttonHome.UseVisualStyleBackColor = true;
            this.buttonHome.Click += new System.EventHandler(this.buttonHome_Click);
            // 
            // comboBoxCardip
            // 
            this.comboBoxCardip.FormattingEnabled = true;
            this.comboBoxCardip.Location = new System.Drawing.Point(71, 20);
            this.comboBoxCardip.Name = "comboBoxCardip";
            this.comboBoxCardip.Size = new System.Drawing.Size(210, 20);
            this.comboBoxCardip.TabIndex = 1;
            // 
            // buttonStop
            // 
            this.buttonStop.Location = new System.Drawing.Point(350, 58);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(100, 32);
            this.buttonStop.TabIndex = 40;
            this.buttonStop.Text = "停止";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // radioButtonAxisZ
            // 
            this.radioButtonAxisZ.AutoSize = true;
            this.radioButtonAxisZ.Location = new System.Drawing.Point(182, 20);
            this.radioButtonAxisZ.Name = "radioButtonAxisZ";
            this.radioButtonAxisZ.Size = new System.Drawing.Size(41, 16);
            this.radioButtonAxisZ.TabIndex = 6;
            this.radioButtonAxisZ.TabStop = true;
            this.radioButtonAxisZ.Text = "Z轴";
            this.radioButtonAxisZ.UseVisualStyleBackColor = true;
            this.radioButtonAxisZ.CheckedChanged += new System.EventHandler(this.radioButtonAxisZ_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.radioButtonAxisU);
            this.groupBox3.Controls.Add(this.radioButtonAxisZ);
            this.groupBox3.Controls.Add(this.radioButtonAxisY);
            this.groupBox3.Controls.Add(this.radioButtonAxisX);
            this.groupBox3.Controls.Add(this.numericUpDownPitch);
            this.groupBox3.Controls.Add(this.numericUpDownHighLimit);
            this.groupBox3.Controls.Add(this.numericUpDownLowLimit);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.numericUpDownPPR);
            this.groupBox3.Controls.Add(this.comboBoxSensorMode);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.comboBoxAxisType);
            this.groupBox3.Controls.Add(this.comboBoxPulseMode);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(12, 110);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(300, 197);
            this.groupBox3.TabIndex = 36;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "运动轴参数";
            // 
            // radioButtonAxisU
            // 
            this.radioButtonAxisU.AutoSize = true;
            this.radioButtonAxisU.Location = new System.Drawing.Point(236, 20);
            this.radioButtonAxisU.Name = "radioButtonAxisU";
            this.radioButtonAxisU.Size = new System.Drawing.Size(41, 16);
            this.radioButtonAxisU.TabIndex = 6;
            this.radioButtonAxisU.TabStop = true;
            this.radioButtonAxisU.Text = "U轴";
            this.radioButtonAxisU.UseVisualStyleBackColor = true;
            this.radioButtonAxisU.CheckedChanged += new System.EventHandler(this.radioButtonAxisU_CheckedChanged);
            // 
            // radioButtonAxisY
            // 
            this.radioButtonAxisY.AutoSize = true;
            this.radioButtonAxisY.Location = new System.Drawing.Point(127, 20);
            this.radioButtonAxisY.Name = "radioButtonAxisY";
            this.radioButtonAxisY.Size = new System.Drawing.Size(41, 16);
            this.radioButtonAxisY.TabIndex = 6;
            this.radioButtonAxisY.TabStop = true;
            this.radioButtonAxisY.Text = "Y轴";
            this.radioButtonAxisY.UseVisualStyleBackColor = true;
            this.radioButtonAxisY.CheckedChanged += new System.EventHandler(this.radioButtonAxisY_CheckedChanged);
            // 
            // radioButtonAxisX
            // 
            this.radioButtonAxisX.AutoSize = true;
            this.radioButtonAxisX.Location = new System.Drawing.Point(72, 21);
            this.radioButtonAxisX.Name = "radioButtonAxisX";
            this.radioButtonAxisX.Size = new System.Drawing.Size(41, 16);
            this.radioButtonAxisX.TabIndex = 6;
            this.radioButtonAxisX.TabStop = true;
            this.radioButtonAxisX.Text = "X轴";
            this.radioButtonAxisX.UseVisualStyleBackColor = true;
            this.radioButtonAxisX.CheckedChanged += new System.EventHandler(this.radioButtonAxisX_CheckedChanged);
            // 
            // numericUpDownPitch
            // 
            this.numericUpDownPitch.Location = new System.Drawing.Point(206, 125);
            this.numericUpDownPitch.Name = "numericUpDownPitch";
            this.numericUpDownPitch.Size = new System.Drawing.Size(65, 21);
            this.numericUpDownPitch.TabIndex = 2;
            // 
            // numericUpDownHighLimit
            // 
            this.numericUpDownHighLimit.Location = new System.Drawing.Point(206, 155);
            this.numericUpDownHighLimit.Name = "numericUpDownHighLimit";
            this.numericUpDownHighLimit.Size = new System.Drawing.Size(65, 21);
            this.numericUpDownHighLimit.TabIndex = 2;
            // 
            // numericUpDownLowLimit
            // 
            this.numericUpDownLowLimit.Location = new System.Drawing.Point(71, 155);
            this.numericUpDownLowLimit.Name = "numericUpDownLowLimit";
            this.numericUpDownLowLimit.Size = new System.Drawing.Size(52, 21);
            this.numericUpDownLowLimit.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "原点电平";
            // 
            // numericUpDownPPR
            // 
            this.numericUpDownPPR.Location = new System.Drawing.Point(71, 125);
            this.numericUpDownPPR.Name = "numericUpDownPPR";
            this.numericUpDownPPR.Size = new System.Drawing.Size(52, 21);
            this.numericUpDownPPR.TabIndex = 2;
            // 
            // comboBoxSensorMode
            // 
            this.comboBoxSensorMode.FormattingEnabled = true;
            this.comboBoxSensorMode.Location = new System.Drawing.Point(71, 98);
            this.comboBoxSensorMode.Name = "comboBoxSensorMode";
            this.comboBoxSensorMode.Size = new System.Drawing.Size(210, 20);
            this.comboBoxSensorMode.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(151, 160);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 0;
            this.label7.Text = "行程上限";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(277, 134);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(17, 12);
            this.label11.TabIndex = 0;
            this.label11.Text = "mm";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(277, 163);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(17, 12);
            this.label10.TabIndex = 0;
            this.label10.Text = "mm";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(129, 163);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(17, 12);
            this.label9.TabIndex = 0;
            this.label9.Text = "mm";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(129, 134);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(23, 12);
            this.label8.TabIndex = 0;
            this.label8.Text = "P/R";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(171, 130);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 12);
            this.label5.TabIndex = 0;
            this.label5.Text = "螺距";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 160);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 0;
            this.label6.Text = "行程下限";
            // 
            // comboBoxAxisType
            // 
            this.comboBoxAxisType.FormattingEnabled = true;
            this.comboBoxAxisType.Location = new System.Drawing.Point(71, 72);
            this.comboBoxAxisType.Name = "comboBoxAxisType";
            this.comboBoxAxisType.Size = new System.Drawing.Size(210, 20);
            this.comboBoxAxisType.TabIndex = 1;
            // 
            // comboBoxPulseMode
            // 
            this.comboBoxPulseMode.FormattingEnabled = true;
            this.comboBoxPulseMode.Location = new System.Drawing.Point(71, 46);
            this.comboBoxPulseMode.Name = "comboBoxPulseMode";
            this.comboBoxPulseMode.Size = new System.Drawing.Size(210, 20);
            this.comboBoxPulseMode.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "电机细分";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(12, 76);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(53, 12);
            this.label20.TabIndex = 0;
            this.label20.Text = "轴 类 型";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "脉冲方式";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "运动轴号";
            // 
            // buttonCardClose
            // 
            this.buttonCardClose.Location = new System.Drawing.Point(20, 46);
            this.buttonCardClose.Name = "buttonCardClose";
            this.buttonCardClose.Size = new System.Drawing.Size(100, 32);
            this.buttonCardClose.TabIndex = 3;
            this.buttonCardClose.Text = "断开";
            this.buttonCardClose.UseVisualStyleBackColor = true;
            this.buttonCardClose.Click += new System.EventHandler(this.buttonCardClose_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 24);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 12);
            this.label12.TabIndex = 0;
            this.label12.Text = "控制器IP";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.buttonMoveAdd);
            this.groupBox4.Controls.Add(this.radioButtonHigh);
            this.groupBox4.Controls.Add(this.buttonMoveSub);
            this.groupBox4.Controls.Add(this.radioButtonLow);
            this.groupBox4.Location = new System.Drawing.Point(339, 134);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(124, 128);
            this.groupBox4.TabIndex = 43;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "电机移动控制";
            // 
            // radioButtonHigh
            // 
            this.radioButtonHigh.AutoSize = true;
            this.radioButtonHigh.Location = new System.Drawing.Point(65, 20);
            this.radioButtonHigh.Name = "radioButtonHigh";
            this.radioButtonHigh.Size = new System.Drawing.Size(47, 16);
            this.radioButtonHigh.TabIndex = 5;
            this.radioButtonHigh.TabStop = true;
            this.radioButtonHigh.Text = "高速";
            this.radioButtonHigh.UseVisualStyleBackColor = true;
            // 
            // buttonMoveSub
            // 
            this.buttonMoveSub.Location = new System.Drawing.Point(12, 82);
            this.buttonMoveSub.Name = "buttonMoveSub";
            this.buttonMoveSub.Size = new System.Drawing.Size(100, 32);
            this.buttonMoveSub.TabIndex = 3;
            this.buttonMoveSub.Text = "反向运动";
            this.buttonMoveSub.UseVisualStyleBackColor = true;
            this.buttonMoveSub.MouseDown += new System.Windows.Forms.MouseEventHandler(this.buttonMoveSub_MouseDown);
            this.buttonMoveSub.MouseUp += new System.Windows.Forms.MouseEventHandler(this.buttonMoveSub_MouseUp);
            // 
            // radioButtonLow
            // 
            this.radioButtonLow.AutoSize = true;
            this.radioButtonLow.Location = new System.Drawing.Point(12, 20);
            this.radioButtonLow.Name = "radioButtonLow";
            this.radioButtonLow.Size = new System.Drawing.Size(47, 16);
            this.radioButtonLow.TabIndex = 5;
            this.radioButtonLow.TabStop = true;
            this.radioButtonLow.Text = "低速";
            this.radioButtonLow.UseVisualStyleBackColor = true;
            // 
            // label_U
            // 
            this.label_U.AutoSize = true;
            this.label_U.Location = new System.Drawing.Point(171, 55);
            this.label_U.Name = "label_U";
            this.label_U.Size = new System.Drawing.Size(77, 12);
            this.label_U.TabIndex = 0;
            this.label_U.Text = "U 轴停止 ：0";
            // 
            // buttonZero
            // 
            this.buttonZero.Location = new System.Drawing.Point(350, 96);
            this.buttonZero.Name = "buttonZero";
            this.buttonZero.Size = new System.Drawing.Size(100, 32);
            this.buttonZero.TabIndex = 39;
            this.buttonZero.Text = "位置清零";
            this.buttonZero.UseVisualStyleBackColor = true;
            this.buttonZero.Click += new System.EventHandler(this.buttonZero_Click);
            // 
            // buttonSaveInfo
            // 
            this.buttonSaveInfo.Location = new System.Drawing.Point(212, 526);
            this.buttonSaveInfo.Name = "buttonSaveInfo";
            this.buttonSaveInfo.Size = new System.Drawing.Size(100, 32);
            this.buttonSaveInfo.TabIndex = 38;
            this.buttonSaveInfo.Text = "保存参数";
            this.buttonSaveInfo.UseVisualStyleBackColor = true;
            this.buttonSaveInfo.Click += new System.EventHandler(this.buttonSaveInfo_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboBoxCardip);
            this.groupBox2.Controls.Add(this.buttonCardClose);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.buttonCardLink);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(300, 92);
            this.groupBox2.TabIndex = 42;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "控制器连接";
            // 
            // buttonCardLink
            // 
            this.buttonCardLink.Location = new System.Drawing.Point(181, 46);
            this.buttonCardLink.Name = "buttonCardLink";
            this.buttonCardLink.Size = new System.Drawing.Size(100, 32);
            this.buttonCardLink.TabIndex = 3;
            this.buttonCardLink.Text = "连接";
            this.buttonCardLink.UseVisualStyleBackColor = true;
            this.buttonCardLink.Click += new System.EventHandler(this.buttonCardLink_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label_U);
            this.groupBox1.Controls.Add(this.label_Z);
            this.groupBox1.Controls.Add(this.label_Y);
            this.groupBox1.Controls.Add(this.label_X);
            this.groupBox1.Location = new System.Drawing.Point(12, 429);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(300, 91);
            this.groupBox1.TabIndex = 37;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "轴状态显示";
            // 
            // label_Z
            // 
            this.label_Z.AutoSize = true;
            this.label_Z.Location = new System.Drawing.Point(18, 55);
            this.label_Z.Name = "label_Z";
            this.label_Z.Size = new System.Drawing.Size(77, 12);
            this.label_Z.TabIndex = 0;
            this.label_Z.Text = "Z 轴停止 ：0";
            // 
            // label_Y
            // 
            this.label_Y.AutoSize = true;
            this.label_Y.Location = new System.Drawing.Point(171, 26);
            this.label_Y.Name = "label_Y";
            this.label_Y.Size = new System.Drawing.Size(77, 12);
            this.label_Y.TabIndex = 0;
            this.label_Y.Text = "Y 轴停止 ：0";
            // 
            // label_X
            // 
            this.label_X.AutoSize = true;
            this.label_X.Location = new System.Drawing.Point(18, 26);
            this.label_X.Name = "label_X";
            this.label_X.Size = new System.Drawing.Size(77, 12);
            this.label_X.TabIndex = 0;
            this.label_X.Text = "X 轴停止 ：0";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(166, 162);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(17, 12);
            this.label31.TabIndex = 25;
            this.label31.Text = "mm";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(166, 135);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(17, 12);
            this.label22.TabIndex = 24;
            this.label22.Text = "mm";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(166, 108);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(17, 12);
            this.label30.TabIndex = 26;
            this.label30.Text = "mm";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(19, 162);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(77, 12);
            this.label26.TabIndex = 19;
            this.label26.Text = "Z轴位置/距离";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(19, 135);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(77, 12);
            this.label19.TabIndex = 20;
            this.label19.Text = "Y轴位置/距离";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(19, 108);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(77, 12);
            this.label21.TabIndex = 18;
            this.label21.Text = "X轴位置/距离";
            // 
            // textBoxDistanceZ
            // 
            this.textBoxDistanceZ.Location = new System.Drawing.Point(102, 159);
            this.textBoxDistanceZ.Name = "textBoxDistanceZ";
            this.textBoxDistanceZ.Size = new System.Drawing.Size(55, 21);
            this.textBoxDistanceZ.TabIndex = 21;
            // 
            // textBoxDistanceY
            // 
            this.textBoxDistanceY.Location = new System.Drawing.Point(102, 132);
            this.textBoxDistanceY.Name = "textBoxDistanceY";
            this.textBoxDistanceY.Size = new System.Drawing.Size(55, 21);
            this.textBoxDistanceY.TabIndex = 22;
            // 
            // textBoxDistanceX
            // 
            this.textBoxDistanceX.Location = new System.Drawing.Point(102, 105);
            this.textBoxDistanceX.Name = "textBoxDistanceX";
            this.textBoxDistanceX.Size = new System.Drawing.Size(55, 21);
            this.textBoxDistanceX.TabIndex = 23;
            // 
            // buttonMultiMoveAbs
            // 
            this.buttonMultiMoveAbs.Location = new System.Drawing.Point(38, 348);
            this.buttonMultiMoveAbs.Name = "buttonMultiMoveAbs";
            this.buttonMultiMoveAbs.Size = new System.Drawing.Size(100, 32);
            this.buttonMultiMoveAbs.TabIndex = 13;
            this.buttonMultiMoveAbs.Text = "多轴插补(Abs)";
            this.buttonMultiMoveAbs.UseVisualStyleBackColor = true;
            this.buttonMultiMoveAbs.Click += new System.EventHandler(this.buttonMultiMoveAbs_Click);
            // 
            // buttonMultiMoveInc
            // 
            this.buttonMultiMoveInc.Location = new System.Drawing.Point(168, 348);
            this.buttonMultiMoveInc.Name = "buttonMultiMoveInc";
            this.buttonMultiMoveInc.Size = new System.Drawing.Size(100, 32);
            this.buttonMultiMoveInc.TabIndex = 12;
            this.buttonMultiMoveInc.Text = "多轴插补(Inc)";
            this.buttonMultiMoveInc.UseVisualStyleBackColor = true;
            this.buttonMultiMoveInc.Click += new System.EventHandler(this.buttonMultiMoveInc_Click);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(166, 83);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(17, 12);
            this.label29.TabIndex = 11;
            this.label29.Text = "ms";
            // 
            // timerMotor
            // 
            this.timerMotor.Interval = 200;
            this.timerMotor.Tick += new System.EventHandler(this.timerMotor_Tick);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(166, 54);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(17, 12);
            this.label28.TabIndex = 11;
            this.label28.Text = "ms";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(164, 28);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(29, 12);
            this.label27.TabIndex = 11;
            this.label27.Text = "mm/s";
            // 
            // textBoxSpeed
            // 
            this.textBoxSpeed.Location = new System.Drawing.Point(102, 24);
            this.textBoxSpeed.Name = "textBoxSpeed";
            this.textBoxSpeed.Size = new System.Drawing.Size(55, 21);
            this.textBoxSpeed.TabIndex = 8;
            // 
            // textBoxATime
            // 
            this.textBoxATime.Location = new System.Drawing.Point(102, 51);
            this.textBoxATime.Name = "textBoxATime";
            this.textBoxATime.Size = new System.Drawing.Size(55, 21);
            this.textBoxATime.TabIndex = 9;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label44);
            this.groupBox6.Controls.Add(this.label39);
            this.groupBox6.Controls.Add(this.label32);
            this.groupBox6.Controls.Add(this.label33);
            this.groupBox6.Controls.Add(this.textBoxX2);
            this.groupBox6.Controls.Add(this.textBoxX1);
            this.groupBox6.Controls.Add(this.label40);
            this.groupBox6.Controls.Add(this.label34);
            this.groupBox6.Controls.Add(this.textBoxZ2);
            this.groupBox6.Controls.Add(this.textBoxZ1);
            this.groupBox6.Controls.Add(this.textBoxY2);
            this.groupBox6.Controls.Add(this.label38);
            this.groupBox6.Controls.Add(this.textBoxY1);
            this.groupBox6.Controls.Add(this.label35);
            this.groupBox6.Controls.Add(this.label37);
            this.groupBox6.Controls.Add(this.label36);
            this.groupBox6.Controls.Add(this.radioButton2);
            this.groupBox6.Controls.Add(this.radioButton4);
            this.groupBox6.Controls.Add(this.radioButton3);
            this.groupBox6.Controls.Add(this.radioButton1);
            this.groupBox6.Controls.Add(this.label31);
            this.groupBox6.Controls.Add(this.label22);
            this.groupBox6.Controls.Add(this.label30);
            this.groupBox6.Controls.Add(this.label26);
            this.groupBox6.Controls.Add(this.label19);
            this.groupBox6.Controls.Add(this.label21);
            this.groupBox6.Controls.Add(this.textBoxDistanceZ);
            this.groupBox6.Controls.Add(this.textBoxDistanceY);
            this.groupBox6.Controls.Add(this.textBoxDistanceX);
            this.groupBox6.Controls.Add(this.buttonMultiMoveAbs);
            this.groupBox6.Controls.Add(this.buttonMultiMoveInc);
            this.groupBox6.Controls.Add(this.label29);
            this.groupBox6.Controls.Add(this.label28);
            this.groupBox6.Controls.Add(this.label27);
            this.groupBox6.Controls.Add(this.textBoxSpeed);
            this.groupBox6.Controls.Add(this.textBoxATime);
            this.groupBox6.Controls.Add(this.label23);
            this.groupBox6.Controls.Add(this.textBoxDTime);
            this.groupBox6.Controls.Add(this.label24);
            this.groupBox6.Controls.Add(this.label25);
            this.groupBox6.Location = new System.Drawing.Point(500, 12);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(351, 411);
            this.groupBox6.TabIndex = 45;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "任意轨迹运动配置";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(320, 216);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(17, 12);
            this.label44.TabIndex = 43;
            this.label44.Text = "mm";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(320, 189);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(17, 12);
            this.label39.TabIndex = 42;
            this.label39.Text = "mm";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(29, 214);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(29, 12);
            this.label32.TabIndex = 40;
            this.label32.Text = "终点";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(29, 189);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(41, 12);
            this.label33.TabIndex = 41;
            this.label33.Text = "经过点";
            // 
            // textBoxX2
            // 
            this.textBoxX2.Location = new System.Drawing.Point(102, 213);
            this.textBoxX2.Name = "textBoxX2";
            this.textBoxX2.Size = new System.Drawing.Size(45, 21);
            this.textBoxX2.TabIndex = 37;
            // 
            // textBoxX1
            // 
            this.textBoxX1.Location = new System.Drawing.Point(102, 186);
            this.textBoxX1.Name = "textBoxX1";
            this.textBoxX1.Size = new System.Drawing.Size(45, 21);
            this.textBoxX1.TabIndex = 36;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(85, 216);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(17, 12);
            this.label40.TabIndex = 29;
            this.label40.Text = "X2";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(85, 189);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(17, 12);
            this.label34.TabIndex = 30;
            this.label34.Text = "X1";
            // 
            // textBoxZ2
            // 
            this.textBoxZ2.Location = new System.Drawing.Point(259, 213);
            this.textBoxZ2.Name = "textBoxZ2";
            this.textBoxZ2.Size = new System.Drawing.Size(55, 21);
            this.textBoxZ2.TabIndex = 39;
            // 
            // textBoxZ1
            // 
            this.textBoxZ1.Location = new System.Drawing.Point(259, 186);
            this.textBoxZ1.Name = "textBoxZ1";
            this.textBoxZ1.Size = new System.Drawing.Size(55, 21);
            this.textBoxZ1.TabIndex = 38;
            // 
            // textBoxY2
            // 
            this.textBoxY2.Location = new System.Drawing.Point(175, 213);
            this.textBoxY2.Name = "textBoxY2";
            this.textBoxY2.Size = new System.Drawing.Size(55, 21);
            this.textBoxY2.TabIndex = 34;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(242, 216);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(17, 12);
            this.label38.TabIndex = 28;
            this.label38.Text = "Z2";
            // 
            // textBoxY1
            // 
            this.textBoxY1.Location = new System.Drawing.Point(175, 186);
            this.textBoxY1.Name = "textBoxY1";
            this.textBoxY1.Size = new System.Drawing.Size(55, 21);
            this.textBoxY1.TabIndex = 35;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(242, 189);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(17, 12);
            this.label35.TabIndex = 33;
            this.label35.Text = "Z1";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(158, 216);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(17, 12);
            this.label37.TabIndex = 32;
            this.label37.Text = "Y2";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(158, 189);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(17, 12);
            this.label36.TabIndex = 31;
            this.label36.Text = "Y1";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(135, 253);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(95, 16);
            this.radioButton2.TabIndex = 27;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "多轴直线插补";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(46, 297);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(209, 16);
            this.radioButton4.TabIndex = 27;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "XY三点画圆弧(需要三点能构成圆))";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(46, 275);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(191, 16);
            this.radioButton3.TabIndex = 27;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "XY三点画圆(需要三点能构成圆)";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(46, 253);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(83, 16);
            this.radioButton1.TabIndex = 27;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "XY直线插补";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(36, 83);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(53, 12);
            this.label23.TabIndex = 4;
            this.label23.Text = "减速时间";
            // 
            // textBoxDTime
            // 
            this.textBoxDTime.Location = new System.Drawing.Point(102, 78);
            this.textBoxDTime.Name = "textBoxDTime";
            this.textBoxDTime.Size = new System.Drawing.Size(55, 21);
            this.textBoxDTime.TabIndex = 10;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(36, 56);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(53, 12);
            this.label24.TabIndex = 3;
            this.label24.Text = "加速时间";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(36, 28);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(53, 12);
            this.label25.TabIndex = 2;
            this.label25.Text = "运行速度";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(36, 79);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 12);
            this.label18.TabIndex = 0;
            this.label18.Text = "减速时间";
            // 
            // numericUpDownATime
            // 
            this.numericUpDownATime.Location = new System.Drawing.Point(95, 47);
            this.numericUpDownATime.Name = "numericUpDownATime";
            this.numericUpDownATime.Size = new System.Drawing.Size(65, 21);
            this.numericUpDownATime.TabIndex = 2;
            // 
            // timerStatus
            // 
            this.timerStatus.Interval = 500;
            this.timerStatus.Tick += new System.EventHandler(this.timerStatus_Tick);
            // 
            // numericUpDownDTime
            // 
            this.numericUpDownDTime.Location = new System.Drawing.Point(95, 74);
            this.numericUpDownDTime.Name = "numericUpDownDTime";
            this.numericUpDownDTime.Size = new System.Drawing.Size(65, 21);
            this.numericUpDownDTime.TabIndex = 2;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.numericUpDownDTime);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Controls.Add(this.numericUpDownATime);
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.Controls.Add(this.numericUpDownSpeed);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Location = new System.Drawing.Point(12, 313);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(300, 110);
            this.groupBox5.TabIndex = 44;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "轴速度设置";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(36, 52);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 12);
            this.label16.TabIndex = 0;
            this.label16.Text = "加速时间";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(166, 79);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(17, 12);
            this.label17.TabIndex = 0;
            this.label17.Text = "ms";
            // 
            // numericUpDownSpeed
            // 
            this.numericUpDownSpeed.Location = new System.Drawing.Point(95, 20);
            this.numericUpDownSpeed.Name = "numericUpDownSpeed";
            this.numericUpDownSpeed.Size = new System.Drawing.Size(65, 21);
            this.numericUpDownSpeed.TabIndex = 2;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(166, 52);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(17, 12);
            this.label15.TabIndex = 0;
            this.label15.Text = "ms";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(36, 25);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 12);
            this.label13.TabIndex = 0;
            this.label13.Text = "工作速度";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(166, 25);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(29, 12);
            this.label14.TabIndex = 0;
            this.label14.Text = "mm/s";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 582);
            this.Controls.Add(this.buttonHome);
            this.Controls.Add(this.buttonStop);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.buttonZero);
            this.Controls.Add(this.buttonSaveInfo);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPitch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHighLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLowLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPPR)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownATime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDTime)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpeed)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonMoveAdd;
        private System.Windows.Forms.Button buttonHome;
        private System.Windows.Forms.ComboBox comboBoxCardip;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.RadioButton radioButtonAxisZ;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton radioButtonAxisU;
        private System.Windows.Forms.RadioButton radioButtonAxisY;
        private System.Windows.Forms.RadioButton radioButtonAxisX;
        private System.Windows.Forms.NumericUpDown numericUpDownPitch;
        private System.Windows.Forms.NumericUpDown numericUpDownHighLimit;
        private System.Windows.Forms.NumericUpDown numericUpDownLowLimit;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericUpDownPPR;
        private System.Windows.Forms.ComboBox comboBoxSensorMode;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBoxAxisType;
        private System.Windows.Forms.ComboBox comboBoxPulseMode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonCardClose;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton radioButtonHigh;
        private System.Windows.Forms.Button buttonMoveSub;
        private System.Windows.Forms.RadioButton radioButtonLow;
        private System.Windows.Forms.Label label_U;
        private System.Windows.Forms.Button buttonZero;
        private System.Windows.Forms.Button buttonSaveInfo;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonCardLink;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label_Z;
        private System.Windows.Forms.Label label_Y;
        private System.Windows.Forms.Label label_X;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBoxDistanceZ;
        private System.Windows.Forms.TextBox textBoxDistanceY;
        private System.Windows.Forms.TextBox textBoxDistanceX;
        private System.Windows.Forms.Button buttonMultiMoveAbs;
        private System.Windows.Forms.Button buttonMultiMoveInc;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Timer timerMotor;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox textBoxSpeed;
        private System.Windows.Forms.TextBox textBoxATime;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBoxDTime;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.NumericUpDown numericUpDownATime;
        private System.Windows.Forms.Timer timerStatus;
        private System.Windows.Forms.NumericUpDown numericUpDownDTime;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown numericUpDownSpeed;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textBoxX2;
        private System.Windows.Forms.TextBox textBoxX1;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox textBoxZ2;
        private System.Windows.Forms.TextBox textBoxZ1;
        private System.Windows.Forms.TextBox textBoxY2;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox textBoxY1;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label39;
    }
}

